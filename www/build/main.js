<<<<<<< HEAD
webpackJsonp([48],{115:function(l,n,u){"use strict";u.d(n,"a",function(){return i});u(1),u(7),u(39);var a=u(560),t=(u.n(a),u(62)),e=u(4),i=(u.n(e),function(){function l(l,n,u,a,t){this.navCtrl=l,this.navParams=n,this.storage=u,this.alertCtrl=a,this.grsp=t,this.loaded=0,this.networth=0,this.assets=0,this.liability=0,this.pension=0,this.amtRet=0,this.ageRet=0,this.drange=!1,this.loadProgress=0,this.loadProgressAge=0,this.currAge=0,this.finAge=90,this.anotherSav=0,this.pieValues=[],this.lineIncomeValues=[],this.lineExpenseValues=[]}return l.prototype.ionViewWillEnter=function(){var l=e().format("YYYY-MM-DD").toString(),n=e().subtract(1,"month").format("YYYY-MM-DD").toString();this.end=l,this.start=n,this.load(),console.log("ionViewWillEnter DashboardPage"),this.getUserName()},l.prototype.ionViewDidLoad=function(){var l=e().format("YYYY-MM-DD").toString(),n=e().subtract(1,"month").format("YYYY-MM-DD").toString();this.end=l,this.start=n,this.load(),console.log("ionViewDidLoad DashboardPage"),this.getUserName()},l.prototype.clearInit=function(){},l.prototype.numberWithCommas=function(l){return l.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")?l.toString().replace(/\B(?=(\d{3})+(?!\d))/g,","):l},l.prototype.week=function(){var l=e().startOf("isoWeek").format("YYYY-MM-DD HH:mm:ss"),n=e().format("YYYY-MM-DD HH:mm:ss");this.getTransactionsApi(l,n)},l.prototype.month=function(){var l=e().startOf("month").format("YYYY-MM-DD HH:mm:ss"),n=e().format("YYYY-MM-DD HH:mm:ss");this.getTransactionsApi(l,n)},l.prototype.year=function(){var l=e().startOf("year").format("YYYY-MM-DD HH:mm:ss"),n=e().format("YYYY-MM-DD HH:mm:ss");this.getTransactionsApi(l,n)},l.prototype.range=function(){this.drange=!this.drange},l.prototype.getRange=function(){this.getTransactionsApi(this.from,this.to)},l.prototype.apiLoad=function(){var l=this;this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});this.grsp.dashboardLoad({uId:this.uId}).then(function(n){l.dash=JSON.parse(n.data),l.assets=l.numberWithCommas(parseInt(l.dash.rassets)),l.liability=l.numberWithCommas(parseInt(l.dash.rliabilities)),l.networth=parseInt(l.dash.rassets)-parseInt(l.dash.rliabilities),l.networth=l.numberWithCommas(l.networth),l.assetTrend=parseInt(l.dash.rassets)-parseInt(l.dash.oassets),l.liabTrend=parseInt(l.dash.rliabilities)-parseInt(l.dash.oliabilities);var u=parseInt(l.dash.oassets)-parseInt(l.dash.oliabilities);l.nwTrend=l.networth-u,l.donutLabels=l.dash.donutLabels.split(","),l.donutData=l.dash.donutData.split(","),l.lineLabels=l.dash.lineLabels.split(","),l.lineIncomeData=l.dash.lineIncomeData.split(","),l.lineExpenseData=l.dash.lineExpenseData.split(","),l.doNut(),l.liNe(),l.loaded=1}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l.prototype.retirement=function(l){for(var n=this.anotherSav,u=this.wrkingYrs,a=this.retYrs,t=n,e=0;e<u;){t+=n*Math.pow(1+.14,e),e++}var i=1/((1-Math.pow(1+.14,-1*a))/.14)*t;this.amtRet=this.numberWithCommas(i.toFixed(2)),this.annuity=this.numberWithCommas((i/a).toFixed(2))},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(t.a)})},l.prototype.doNut=function(){console.log(this.donutLabels),this.doughnutChart=new a.Chart(this.doughnutCanvas.nativeElement,{type:"doughnut",data:{labels:this.pieLabels,datasets:[{label:"User Personal Finance Status",data:this.pieValues,backgroundColor:["rgba(255, 99, 132, 0.2)","rgba(54, 162, 235, 0.2)","rgba(255, 206, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(153, 102, 255, 0.2)","rgba(255, 159, 64, 0.2)"],hoverBackgroundColor:["#FF6384","#36A2EB","#FFCE56","#FF6384","#36A2EB","#FFCE56"]}]},options:{tooltips:{callbacks:{label:function(l,n){var u=n.datasets[l.datasetIndex].data[l.index];return u.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")}}}}})},l.prototype.liNe=function(){this.lineChart=new a.Chart(this.lineCanvas.nativeElement,{type:"line",data:{labels:this.lineLabels,datasets:[{label:"Income",fill:!1,lineTension:.1,borderColor:"rgba(75,92,192,1)",borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"rgba(75,92,192,1)",pointBackgroundColor:"#fff",pointBorderWidth:1,pointHoverRadius:5,pointHoverBackgroundColor:"rgba(75,92,192,1)",pointHoverBorderColor:"rgba(220,220,220,1)",pointHoverBorderWidth:2,pointRadius:1,pointHitRadius:10,data:this.lineIncomeValues,spanGaps:!1},{label:"Expense",fill:!1,lineTension:.1,borderColor:"rgba(75,192,192,1)",borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"rgba(75,192,192,1)",pointBackgroundColor:"#fff",pointBorderWidth:1,pointHoverRadius:5,pointHoverBackgroundColor:"rgba(75,192,192,1)",pointHoverBorderColor:"rgba(220,220,220,1)",pointHoverBorderWidth:2,pointRadius:1,pointHitRadius:10,data:this.lineExpenseValues,spanGaps:!1}]},options:{scales:{xAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}],yAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}]},tooltips:{callbacks:{label:function(l){return l.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")}}}}})},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),l.userName=n.name.toUpperCase(),l.uId=n.id,l.pension=l.numberWithCommas(parseInt(n.pen)),l.apiLoad();var u=e().diff(n.dob,"years");l.savings=l.numberWithCommas(parseInt(n.savInv)),l.anotherSav=parseInt(n.savInv),l.expenses=l.numberWithCommas(parseInt(n.exp)),l.wrkingYrs=parseInt(n.ret)-u,l.retYrs=90-parseInt(n.ret),l.loadProgress=l.wrkingYrs/(90-u)*100,l.loadProgressAge=n.ret,l.currAge=u,l.getTransactionsApi(l.start,l.end),l.retirement(),l.ageRet=n.ret})},l.prototype.setApiJson=function(l,n){this.apiData={uId:this.uId,start:l,end:n}},l.prototype.getTransactionsApi=function(l,n){var u=this;this.loaded=0,this.setApiJson(l,n);this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});this.lineLabels=[],this.pieLabels=[],this.pieValues=[],this.grsp.transactions(this.apiData).then(function(l){u.trans=JSON.parse(l.data),u.transactions=JSON.parse(u.trans.transactions),u.pieLabels=u.transactions.map(function(l){return l.trans_bucket}),u.pieLabels=u.pieLabels.filter(function(l,n,u){return u.indexOf(l)===n}),console.log(u.pieLabels);Array.isArray(u.pieLabels);u.pieLabels.forEach(function(l){var n=u.transactions.filter(function(n){return n.trans_bucket==l}).map(function(l){return l.trans_amt}),a=(n=n.map(function(l){return parseInt(l)})).reduce(function(l,n){return l+n},0);u.pieValues.push(a)}),u.pieValues.length=u.pieLabels.length,console.log(u.pieValues),u.doNut(),u.transactions=JSON.parse(u.trans.transactions),u.transactions=u.transactions.filter(function(l){return"Income"==l.trans_bucket}),u.lineLabels=u.transactions.map(function(l){return l.trans_date}),u.lineLabels=u.lineLabels.filter(function(l,n,u){return u.indexOf(l)===n}),u.lineLabels=u.lineLabels.slice(-5),console.log(u.lineLabels),u.lineLabels.forEach(function(l){var n=u.transactions.filter(function(n){return n.trans_date==l}).map(function(l){return l.trans_amt}),a=(n=n.map(function(l){return parseInt(l)})).reduce(function(l,n){return l+n},0);u.lineIncomeValues.push(a)}),console.log(u.lineIncomeValues),u.etransactions=JSON.parse(u.trans.transactions),u.etransactions=u.etransactions.filter(function(l){return"Expenses"==l.trans_bucket}),u.lineExpLabels=u.etransactions.map(function(l){return l.trans_date}),u.lineExpLabels=u.lineExpLabels.filter(function(l,n,u){return u.indexOf(l)===n}),u.lineExpLabels=u.lineExpLabels.slice(-5),console.log(u.lineExpLabels),u.lineExpLabels.forEach(function(l){var n=u.etransactions.filter(function(n){return n.trans_date==l}).map(function(l){return l.trans_amt}),a=(n=n.map(function(l){return parseInt(l)})).reduce(function(l,n){return l+n},0);u.lineExpenseValues.push(a)}),console.log(u.lineExpenseValues),u.liNe(),u.loaded=1,u.alertCtrl.create({title:"No name",subTitle:"no Name"+u.transactions[0],buttons:["Dismiss"]})}).catch(function(l){u.loaded=1,console.log(l),u.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}())},150:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7),u(39);var a=u(115),t=function(){function l(l,n,u,a,t){this.navCtrl=l,this.navParams=n,this.alertCtrl=u,this.storage=a,this.grsp=t,this.initialData={id:0,name:"",dob:"",dow:"",pen:"",savInv:"",exp:"",inc:"",ret:"",email:"",pword:"",cpword:""},this.loaded=1}return l.prototype.ionViewWillEnter=function(){this.checkInit()},l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad InitialPage")},l.prototype.nextIndex=function(){var l=this.slides.getActiveIndex();this.slides.slideTo(l+1,500)},l.prototype.prevIndex=function(){var l=this.slides.getActiveIndex();this.slides.slideTo(l-1,500)},l.prototype.checkInit=function(){var l=this;this.storage.get("initialData").then(function(n){null==n||0==n.id||l.navCtrl.setRoot(a.a)})},l.prototype.setApiJson=function(){this.apiData={id:0,name:this.initialData.name,dob:this.initialData.dob,dow:this.initialData.dow,pen:this.initialData.pen,savInv:this.initialData.savInv,exp:this.initialData.exp,inc:this.initialData.inc,ret:this.initialData.ret,email:this.initialData.email,pword:this.initialData.pword},console.log(this.apiData)},l.prototype.logForm=function(){var l=this;if(this.initialData.pword!==this.initialData.cpword)return this.alertCtrl.create({title:"Error",subTitle:"Passwords Do Not Match",buttons:["Ok"]}).present(),void this.prevIndex();this.setApiJson(),this.loaded=0,console.log(this.apiData),this.grsp.addUser(this.apiData).then(function(n){(l.apiRes=n,l.apiRes=JSON.parse(n.data),l.loaded=1,1==l.apiRes.response)?(l.initialData.id=l.apiRes.uId,l.storage.set("initialData",l.initialData),l.alertCtrl.create({title:"Success",subTitle:"Successfully Saved User "+l.apiRes.uId,buttons:["Ok"]}),l.alertCtrl.create({title:"Saved",subTitle:"Thank You "+l.initialData.name.toUpperCase()+" Your Details Have Been Saved",buttons:["Ok"]}).present(),l.navCtrl.setRoot(a.a)):l.alertCtrl.create({title:"Error",subTitle:"Something went wrong, Please try again",buttons:["Dismiss"]}).present()}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}()},151:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.alertCtrl=n,this.storage=u,this.grsp=a,this.navParams=t,this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad CreditCardPage"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.uId=n.id,l.getCredApi()})},l.prototype.setApiJson=function(){this.apiData={grp:"Credit"}},l.prototype.getCredApi=function(){var l=this;this.loaded=0,this.setApiJson();this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});console.log(this.apiData),this.grsp.cards(this.apiData).then(function(n){console.log(n),l.cred=JSON.parse(n.data),l.credCrd=JSON.parse(l.cred.cards),l.loaded=1}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}()},152:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(205);var a=function(){return function(){}}()},190:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(139);var a=function(){function l(l,n,u,a,t,e,i,o){this.navCtrl=l,this.alertCtrl=n,this.navParams=u,this.grsp=a,this.loading=t,this.platform=e,this.iab=i,this.viewCtrl=o}return l.prototype.ngOnInit=function(){this.price=parseInt(this.navParams.get("price")),this.em=parseInt(this.navParams.get("email")),console.log(this.price),console.log(this.em),console.log(this.navParams.get("data")),this.chargeAmount=100*this.price},l.prototype.validateEmail=function(l){return/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(String(l).toLowerCase())},l.prototype.ChargeCard=function(){var l=this;if(this.validateEmail(this.emailValue)){var n=this.chargeAmount,u=this.emailValue;console.log(n),console.log(u);var a=this.loading.create({content:"Processing Charge…"});a.present(),this.platform.ready().then(function(){if(l.platform.is("ios")||l.platform.is("android")){var t=PaystackPop.setup({key:l.grsp.paystack,email:u,amount:n,ref:""+Math.floor(1e9*Math.random()+1),onClose:function(){a.dismiss()},callback:function(u){var t="Payment complete! Reference: "+u.reference;alert(t),console.log(t),a.dismiss(),l.navCtrl.push("PayConfPage",{res:"success",amt:n/100})}});console.log(t),t.openIframe()}else 0})}else alert("Please enter valid email to pay")},l.prototype.whatsPaystack=function(){var l=this;this.platform.ready().then(function(){l.iab.create("https://paystack.com/what-is-paystack","_blank")})},l.prototype.close=function(){this.viewCtrl.dismiss()},l}()},205:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1);var a=function(){return function(){console.log("Hello ProgressBarComponent Component"),this.text="Hello World"}}()},211:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},213:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},214:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7);var a=u(62),t=(u(39),function(){function l(l,n,u){this.navCtrl=l,this.storage=n,this.navParams=u,this.categories=[{id:1,name:"View Analytics",title:"View Analytics",description:"See How Your Numbers Are Looking",img:"assets/imgs/menu/coffee.png"},{id:2,name:"Add Transaction(s)",title:"Add Transaction(s)",description:"Anything We Might Have Missed?",img:"assets/imgs/menu/breakfast.png"},{id:3,name:"Edit Transaction(s)",title:"Edit Transaction(s)",description:"Because You Have Full Control",img:"assets/imgs/menu/munchies.png"}]}return l.prototype.ionViewDidLoad=function(){this.load(),console.log("ionViewDidLoad AnalysisPage")},l.prototype.analysisAction=function(l){if(1==l.id)this.navCtrl.push("ViewanalyticsPage",{category:1});else if(2==l.id){this.navCtrl.push("AddtransactionsPage",{category:2})}else{this.navCtrl.push("ViewtransactionsPage",{category:3})}},l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l}())},215:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},216:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7);var a=u(62),t=(u(39),function(){function l(l,n,u){this.navCtrl=l,this.storage=n,this.navParams=u,this.categories=[{id:1,name:"Savings",title:"Savings",description:"Banking Savings",img:"assets/imgs/menu/coffee.png",page:"SavingsPage"},{id:2,name:"Current",title:"Current",description:"Banking Current",img:"assets/imgs/menu/breakfast.png",page:"CurrentPage"},{id:3,name:"Fixed Deposit",title:"Fixed Deposit",description:"Banking Fixed Deposit",img:"assets/imgs/menu/munchies.png",page:"FixedDepositPage"},{id:4,name:"Foreign",title:"Foreign",description:"Banking Foreign",img:"assets/imgs/menu/coffee.png",page:"ForeignPage"}]}return l.prototype.ionViewDidLoad=function(){this.load(),console.log("ionViewDidLoad BankingPage")},l.prototype.bankAction=function(l){this.navCtrl.push(l.page)},l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l}())},217:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},218:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7);var a=u(62),t=(u(39),function(){function l(l,n,u){this.navCtrl=l,this.storage=n,this.navParams=u,this.categories=[{id:1,name:"View Budget Analytics",title:"View Budget",description:"See How Your Numbers Are Looking",img:"assets/imgs/menu/coffee.png"},{id:2,name:"Manage Budget(s)",title:"Manage Budget(s)",description:"Because You Have Full Control",img:"assets/imgs/menu/munchies.png"}]}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad BudgetPage")},l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.analysisAction=function(l){if(1==l.id)this.navCtrl.push("ViewbudgetPage",{category:1});else if(2==l.id){this.navCtrl.push("ManagebudgetPage",{category:2})}},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l}())},219:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},220:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7);var a=u(62),t=(u(39),function(){function l(l,n,u){this.navCtrl=l,this.storage=n,this.navParams=u,this.categories=[{id:1,name:"Naira",title:"Naira",description:"Naira Cards",img:"assets/imgs/menu/coffee.png",page:"NairaCardPage"},{id:2,name:"Forex",title:"Forex",description:"Forex Cards",img:"assets/imgs/menu/breakfast.png",page:"ForexCardPage"},{id:3,name:"Prepaid",title:"Prepaid",description:"Prepaid Cards",img:"assets/imgs/menu/munchies.png",page:"PrepaidCardPage"},{id:4,name:"Credit",title:"Credit",description:"Credit Cards",img:"assets/imgs/menu/coffee.png",page:"CreditCardPage"}]}return l.prototype.ionViewDidLoad=function(){this.load(),console.log("ionViewDidLoad CardsPage")},l.prototype.cardsAction=function(l){this.navCtrl.push(l.page)},l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l}())},221:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},222:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.navParams=n,this.storage=u,this.alertCtrl=a,this.grsp=t,this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad AddtransactionsPage"),this.valName=this.navParams.get("val"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.buckets=l.grsp.buckets()})},l.prototype.setApiJson=function(){this.apiData={uId:this.uId,type:this.type,desc:this.desc,bucket:this.bucket,amt:this.amt,date:this.date}},l.prototype.saveAddApi=function(){var l=this;this.loaded=0,this.setApiJson(),console.log(this.apiData),this.grsp.addTrans(this.apiData).then(function(n){(console.log(n),l.apiRes=JSON.parse(n.data),l.loaded=1,1==l.apiRes.response)?(l.alertCtrl.create({title:"Success",subTitle:"Successfully Saved Transaction "+l.apiRes.desc,buttons:["Ok"]}).present(),l.type="",l.desc="",l.bucket="",l.amt="",l.date=""):l.alertCtrl.create({title:"Failed",subTitle:"Something went wrong, Please try again",buttons:["Dismiss"]}).present()}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l.prototype.saveAdd=function(){this.desc=this.desc?this.desc:"",this.alertCtrl.create({title:"Success",subTitle:"Successfully Saved Transaction "+this.desc,buttons:["Dismiss"]}).present()},l}()},223:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},224:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.navParams=n,this.storage=u,this.alertCtrl=a,this.grsp=t,this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad EdittransactionsPage"),this.desc=this.navParams.get("trans").trans_desc,this.type=this.navParams.get("trans").trans_type,this.bucket=this.navParams.get("trans").trans_bucket,this.amt=this.navParams.get("trans").trans_amt,this.date=this.navParams.get("trans").trans_date.substring(0,10),this.tId=this.navParams.get("trans").trans_id,this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.buckets=l.grsp.buckets()})},l.prototype.saveEdit=function(){this.desc=this.desc?this.desc:"",this.alertCtrl.create({title:"Success",subTitle:"Successfully Saved Transaction "+this.desc,buttons:["Dismiss"]}).present()},l.prototype.setApiJson=function(){this.apiData={uId:this.uId,type:this.type,desc:this.desc,bucket:this.bucket,amt:this.amt,date:this.date,tid:this.tId}},l.prototype.saveEditApi=function(){var l=this;this.setApiJson(),this.loaded=0,console.log(this.apiData),this.grsp.editTrans(this.apiData).then(function(n){(console.log(n),l.apiRes=n,l.apiRes=JSON.parse(n.data),l.loaded=1,1==l.apiRes.response)?l.alertCtrl.create({title:"Success",subTitle:"Successfully Edited Transaction "+l.apiRes.desc,buttons:["Dismiss"]}).present():l.alertCtrl.create({title:"Success",subTitle:"Successfully Sav ",buttons:["Dismiss"]}).present()}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}()},225:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},226:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n){this.navCtrl=l,this.navParams=n,this.items=[{id:6,name:"limeRefreshers",title:"Lime Refreshers",description:"Decaf Colombia",img:"assets/imgs/submenu/limeRefreshers.png",category:"coffe",price:"12",likes:"1900",isliked:!0},{id:4,name:"vanillaFrappucino",title:"Vanilla Frappucino",description:"Decaf Colombia",img:"assets/imgs/submenu/vanillaFrappucino.png",category:"coffe",price:"9.85",likes:"2300",isliked:!0},{id:4,name:"espresso",title:"Espresso",description:"Decaf Colombia",img:"assets/imgs/submenu/espresso.png",category:"coffe",price:"4.50",likes:"1800",isliked:!0},{id:5,name:"icedAmericano",title:"Iced Americano",description:"Locally Roasted",img:"assets/imgs/submenu/icedAmericano.png",category:"coffe",price:"10.50",likes:"2300",isliked:!0},{id:3,name:"kickFrappe",title:"Kick Frappe",description:"Coffee Kick",img:"assets/imgs/submenu/kickFrappe.png",category:"coffe",price:"12.35",likes:"4450",isliked:!0},{id:2,name:"caramelFrappe",title:"Caramel Frappe",description:"Decaf Colombia",img:"assets/imgs/submenu/caramelFrappe.png",category:"coffe",price:"7.85",likes:"3456",isliked:!0},{id:4,name:"cappuccino",title:"Cappuccino",description:"Decaf Colombia",img:"assets/imgs/submenu/cappuccino.png",category:"coffe",price:"10.65",likes:"2300",isliked:!0},{id:1,name:"chocoFrappe",title:"Choco Frappe",description:"Chocolate Whirl",img:"assets/imgs/submenu/chocoFrappe.png",category:"coffe",price:"9",likes:"3200",isliked:!0}]}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad FavoritesPage")},l}()},227:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},228:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7);var a=u(62),t=(u(39),function(){function l(l,n,u){this.navCtrl=l,this.storage=n,this.navParams=u,this.categories=[{id:1,name:"Banking",title:"Banking",description:"Everything You Need to Have Handy on Banking",img:"assets/imgs/menu/coffee.png",page:"BankingPage"},{id:2,name:"Cards",title:"Cards",description:"Get Clarity on How Your Cards are Managed",img:"assets/imgs/menu/breakfast.png",page:"CardsPage"}]}return l.prototype.ionViewDidLoad=function(){this.load(),console.log("ionViewDidLoad FinInfoPage")},l.prototype.fiAction=function(l){this.navCtrl.push(l.page)},l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l}())},229:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},230:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.alertCtrl=n,this.storage=u,this.grsp=a,this.navParams=t,this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad ForexCardPage"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.uId=n.id,l.getForexApi()})},l.prototype.setApiJson=function(){this.apiData={grp:"Foreign"}},l.prototype.getForexApi=function(){var l=this;this.loaded=0,this.setApiJson();this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});console.log(this.apiData),this.grsp.cards(this.apiData).then(function(n){console.log(n),l.forex=n,l.forex=JSON.parse(n.data),l.forexCrd=JSON.parse(l.forex.cards),l.loaded=1}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}()},231:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},232:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},233:function(l,n,u){"use strict";u.d(n,"a",function(){return e});u(1),u(7),u(39);var a=u(62),t=u(190),e=function(){function l(l,n,u,a,t){this.navCtrl=l,this.alertCtrl=n,this.storage=u,this.grsp=a,this.navParams=t,this.showHide=!1,this.recur=!1,this.model={},this.loaded=1,this.more=0}return l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.ionViewDidLoad=function(){this.load(),console.log("ionViewDidLoad InvestmentPage"),this.valName=this.navParams.get("val"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.initialData=n,l.getInvestmentsApi()})},l.prototype.setApiJson=function(){this.apiData={uId:this.uId,type:"Gaid",prov:"Gaid",amt:this.amount,model:JSON.stringify({recurring:this.recur,frequency:this.freq,deductDate:this.deductDate})},console.log(this.apiData)},l.prototype.goPay=function(){this.setApiJson();var l={price:this.amount,email:this.initialData.email};console.log(l),this.storage.set("investData",this.apiData),this.navCtrl.push(t.a,l)},l.prototype.buyInvestApi=function(){var l=this;this.loaded=0,console.log(this.apiData),console.log("done"),this.grsp.buyInvestment(this.apiData).then(function(n){(console.log(n),l.apiRes=n,l.apiRes=JSON.parse(n.data),l.loaded=1,1==l.apiRes.response)?l.alertCtrl.create({title:"Success",subTitle:"Successfully Saved NGN"+l.amount+" of "+l.apiRes.prov+" Investment ",buttons:["Ok"]}).present():l.alertCtrl.create({title:"Failed",subTitle:"Something went wrong, Please try again",buttons:["Dismiss"]}).present()}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()}),this.storage.set("investData",null)},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l.prototype.toggleMore=function(){0==this.more?this.more=1:1==this.more&&(this.more=0)},l.prototype.setInvApiJson=function(){this.apiInvData={uId:this.uId}},l.prototype.getInvestmentsApi=function(){var l=this;this.setInvApiJson();this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});console.log(this.apiInvData),this.grsp.investments(this.apiInvData).then(function(n){console.log(n),l.inv=n,l.inv=JSON.parse(n.data),l.investments=JSON.parse(l.inv.investments),l.investments=l.investments.reverse(),l.totInvest=0,l.investments.forEach(function(n){n.actAmt=l.numberWithCommas(n.inv_amt),l.totInvest+=parseInt(n.inv_amt)}),l.totInvest=l.numberWithCommas(l.totInvest),l.loaded=1}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l.prototype.numberWithCommas=function(l){return l.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",")?l.toString().replace(/\B(?=(\d{3})+(?!\d))/g,","):l},l}()},234:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},235:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n){this.navCtrl=l,this.navParams=n,this.categories=[{id:1,name:"coffe",title:"Coffe",description:"Freshly brewed coffee",img:"assets/imgs/menu/coffee.png"},{id:2,name:"breakfast",title:"Breakfast",description:"Hot & full of flavour.",img:"assets/imgs/menu/breakfast.png"},{id:3,name:"munchies",title:"Munchies",description:"Perfectly baked goodies.",img:"assets/imgs/menu/munchies.png"},{id:4,name:"sandwiches",title:"Sandwiches",description:"Fresh, healthy and tasty.",img:"assets/imgs/menu/sandwiches.png"},{id:5,name:"tea",title:"Tea",description:"Fresh, healthy and tasty.",img:"assets/imgs/menu/tea.png"},{id:6,name:"pancake",title:"Pancake",description:"Fresh, healthy and tasty.",img:"assets/imgs/menu/pancake.png"}]}return l.prototype.goToSubMenu=function(l){this.navCtrl.push("SubMenuOnePage",{category:l})},l}()},236:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},237:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n){this.navCtrl=l,this.navParams=n,this.categories=[{id:1,name:"coffe",title:"Coffe",description:"Freshly brewed coffee",img:"assets/imgs/menu/coffee.png"},{id:2,name:"breakfast",title:"Breakfast",description:"Hot & full of flavour.",img:"assets/imgs/menu/breakfast.png"},{id:3,name:"munchies",title:"Munchies",description:"Perfectly baked goodies.",img:"assets/imgs/menu/munchies.png"},{id:4,name:"sandwiches",title:"Sandwiches",description:"Fresh, healthy and tasty.",img:"assets/imgs/menu/sandwiches.png"},{id:5,name:"tea",title:"Tea",description:"Fresh, healthy and tasty.",img:"assets/imgs/menu/tea.png"},{id:6,name:"pancake",title:"Pancake",description:"Fresh, healthy and tasty.",img:"assets/imgs/menu/pancake.png"}]}return l.prototype.goToSubMenu=function(l){this.navCtrl.push("SubMenuTwoPage",{category:l})},l}()},238:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},239:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.alertCtrl=n,this.storage=u,this.grsp=a,this.navParams=t,this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad NairaCardPage"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.uId=n.id,l.getNairaApi()})},l.prototype.setApiJson=function(){this.apiData={grp:"Naira"}},l.prototype.getNairaApi=function(){var l=this;this.loaded=0,this.setApiJson();this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});console.log(this.apiData),this.grsp.cards(this.apiData).then(function(n){console.log(n),l.naira=n,l.naira=JSON.parse(n.data),l.nairaCrd=JSON.parse(l.naira.cards),l.loaded=1}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}()},240:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},241:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.storage=n,this.alertCtrl=u,this.navParams=a,this.grsp=t,this.expenses=50,this.savInv=25,this.proj=25,this.debt=0,this.income=0,this.id=0,this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad ManagebudgetPage"),this.getUserName()},l.prototype.setApiJson=function(){this.apiData={uId:this.id,expenses:this.expenses,savInv:this.savInv,proj:this.proj,debt:this.debt,income:this.income}},l.prototype.saveBudget=function(){var l=this;1==this.sumAll()?(console.log("save budget for the month"),this.loaded=0,this.setApiJson(),console.log(this.apiData),this.grsp.addBudget(this.apiData).then(function(n){(console.log(n),l.apiRes=n,l.apiRes=JSON.parse(n.data),l.loaded=1,1==l.apiRes.response)?l.alertCtrl.create({title:"Success",subTitle:"Successfully Saved Budget",buttons:["Ok"]}).present():l.alertCtrl.create({title:"Failed",subTitle:"Something went wrong, Please try again",buttons:["Dismiss"]}).present()}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})):this.alertCtrl.create({title:"Note",subTitle:"The Total of Budget Allocation Must Be Equal to 100%",buttons:["Dismiss"]}).present()},l.prototype.sumAll=function(){return"debtor"==this.type||(this.debt=0),this.expenses+this.savInv+this.proj+this.debt==100},l.prototype.help=function(){this.alertCtrl.create({title:"Note",subTitle:"The Budget Must be Set Before The Month Starts",buttons:["Dismiss"]}).present()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),l.userName=n.name.toUpperCase(),l.id=n.id})},l}()},242:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},243:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n,u){this.navCtrl=l,this.navParams=n,this.toastCtrl=u,this.order={item:{id:10,name:"chocolateMuffin",title:"Chocolate Muffin",description:"Our muffin unites rich, dense chocolate with a gooey caramel center for bliss in every bite. As far as we're concerned, there's no such thing as too much caramel.",img:"assets/imgs/submenu/chocolateMuffin.png",category:"muffin",price:"10",likes:"1800",isliked:!0},quantity:2}}return l.prototype.placeOrder=function(){this.toastCtrl.create({message:"Placing Order of "+this.order.quantity+" "+this.order.item.title,duration:2500}).present()},l}()},244:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},245:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.alertCtrl=n,this.storage=u,this.grsp=a,this.navParams=t,this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad PrepaidCardPage"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.uId=n.id,l.getPrepApi()})},l.prototype.setApiJson=function(){this.apiData={grp:"Prepaid"}},l.prototype.getPrepApi=function(){var l=this;this.loaded=0,this.setApiJson();this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});console.log(this.apiData),this.grsp.cards(this.apiData).then(function(n){console.log(n),l.prep=n,l.prep=JSON.parse(n.data),l.prepCrd=JSON.parse(l.prep.cards),l.loaded=1}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}()},246:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},247:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n,u){this.navCtrl=l,this.navParams=n,this.toastCtrl=u}return l.prototype.changeImage=function(){this.toastCtrl.create({message:"Change Image Button Clicked",duration:2500}).present()},l.prototype.follow=function(){this.toastCtrl.create({message:"Follow Button Clicked",duration:2500}).present()},l}()},248:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},249:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},250:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(l,n){this.navCtrl=l,this.navParams=n}}()},251:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},252:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(l,n){this.navCtrl=l,this.navParams=n}}()},253:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},254:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n,u){this.navCtrl=l,this.navParams=n,this.toastCtrl=u,this.specialOffer={oldPrice:5.5,newPrice:3.55,item:"Espresso"},this.items=[{id:1,name:"chocoFrappe",title:"Choco Frappe",description:"Chocolate Whirl",img:"assets/imgs/submenu/chocoFrappe.png",category:"coffe",price:"9",likes:"3200",isliked:!1},{id:2,name:"caramelFrappe",title:"Caramel Frappe",description:"Decaf Colombia",img:"assets/imgs/submenu/caramelFrappe.png",category:"coffe",price:"7.85",likes:"3456",isliked:!1},{id:3,name:"kickFrappe",title:"Kick Frappe",description:"Coffee Kick",img:"assets/imgs/submenu/kickFrappe.png",category:"coffe",price:"12.35",likes:"4450",isliked:!1},{id:4,name:"cappuccino",title:"Cappuccino",description:"Decaf Colombia",img:"assets/imgs/submenu/cappuccino.png",category:"coffe",price:"10.65",likes:"2300",isliked:!1},{id:5,name:"icedAmericano",title:"Iced Americano",description:"Locally Roasted",img:"assets/imgs/submenu/icedAmericano.png",category:"coffe",price:"10.50",likes:"2300",isliked:!1},{id:6,name:"limeRefreshers",title:"Lime Refreshers",description:"Decaf Colombia",img:"assets/imgs/submenu/limeRefreshers.png",category:"coffe",price:"12",likes:"1900",isliked:!1},{id:4,name:"vanillaFrappucino",title:"Vanilla Frappucino",description:"Decaf Colombia",img:"assets/imgs/submenu/vanillaFrappucino.png",category:"coffe",price:"9.85",likes:"2300",isliked:!1},{id:4,name:"espresso",title:"Espresso",description:"Decaf Colombia",img:"assets/imgs/submenu/espresso.png",category:"coffe",price:"4.50",likes:"1800",isliked:!1}]}return l.prototype.ionViewDidLoad=function(){this.navParams.get("category")&&this.navParams.get("category")},l.prototype.selectItem=function(l){this.toastCtrl.create({message:"item "+l.title+" Clicked",duration:2500}).present()},l}()},255:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},256:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n,u){this.navCtrl=l,this.navParams=n,this.toastCtrl=u,this.items=[{id:1,name:"chocoFrappe",title:"Choco Frappe",description:"Chocolate Whirl",img:"assets/imgs/submenu/chocoFrappe.png",category:"coffe",price:"9",likes:"3200",isliked:!1},{id:2,name:"caramelFrappe",title:"Caramel Frappe",description:"Decaf Colombia",img:"assets/imgs/submenu/caramelFrappe.png",category:"coffe",price:"7.85",likes:"3456",isliked:!1},{id:3,name:"kickFrappe",title:"Kick Frappe",description:"Coffee Kick",img:"assets/imgs/submenu/kickFrappe.png",category:"coffe",price:"12.35",likes:"4450",isliked:!1},{id:4,name:"cappuccino",title:"Cappuccino",description:"Decaf Colombia",img:"assets/imgs/submenu/cappuccino.png",category:"coffe",price:"10.65",likes:"2300",isliked:!1},{id:5,name:"icedAmericano",title:"Iced Americano",description:"Locally Roasted",img:"assets/imgs/submenu/icedAmericano.png",category:"coffe",price:"10.50",likes:"2300",isliked:!1},{id:6,name:"limeRefreshers",title:"Lime Refreshers",description:"Decaf Colombia",img:"assets/imgs/submenu/limeRefreshers.png",category:"coffe",price:"12",likes:"1900",isliked:!1},{id:4,name:"vanillaFrappucino",title:"Vanilla Frappucino",description:"Decaf Colombia",img:"assets/imgs/submenu/vanillaFrappucino.png",category:"coffe",price:"9.85",likes:"2300",isliked:!1},{id:4,name:"espresso",title:"Espresso",description:"Decaf Colombia",img:"assets/imgs/submenu/espresso.png",category:"coffe",price:"4.50",likes:"1800",isliked:!1}]}return l.prototype.ionViewDidLoad=function(){this.navParams.get("category")&&this.navParams.get("category")},l.prototype.selectItem=function(l){this.toastCtrl.create({message:"item "+l.title+" Clicked",duration:2500}).present()},l}()},257:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},258:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t){this.navCtrl=l,this.alertCtrl=n,this.grsp=u,this.storage=a,this.navParams=t,this.setting={},this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad SettingPage"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.setting={name:n.name.toUpperCase(),dob:n.dob,dow:n.dow,pen:n.pen,savInv:n.savInv,exp:n.exp,inc:n.inc,ret:n.ret,email:n.email,pword:n.pword,cpword:""}})},l.prototype.setApiJson=function(){this.apiData={uId:this.uId,name:this.setting.name,dob:this.setting.dob,dow:this.setting.dow,pen:this.setting.pen,savInv:this.setting.savInv,exp:this.setting.exp,inc:this.setting.inc,ret:this.setting.ret,email:this.setting.email,pword:this.setting.pword},console.log(this.apiData)},l.prototype.editUser=function(){var l=this;this.setting.pword===this.setting.cpword?this.alertCtrl.create({title:"Confirm Update",message:"Do you Want to Make These Changes?",buttons:[{text:"No",role:"cancel",handler:function(){console.log("Cancel clicked")}},{text:"Yes",handler:function(){console.log("Yes clicked"),l.setApiJson(),l.loaded=0,console.log(l.apiData),l.grsp.editUser(l.apiData).then(function(n){if(console.log(n),l.apiRes=n,l.apiRes=JSON.parse(n.data),l.loaded=1,1==l.apiRes.response){l.alertCtrl.create({title:"Success",subTitle:"Successfully Edited User "+l.apiRes.name,buttons:["Dismiss"]}).present(),l.storage.set("initialData",null);l.navCtrl.setRoot("SignupPage")}else{l.alertCtrl.create({title:"Failed",subTitle:"Something Broke ",buttons:["Dismiss"]}).present()}}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})}}]}).present():this.alertCtrl.create({title:"Error",subTitle:"Passwords Do Not Match",buttons:["Ok"]}).present()},l.prototype.reset=function(){this.getUserName()},l}()},259:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},260:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7),u(39);var a=u(62),t=function(){function l(l,n,u){this.navCtrl=l,this.storage=n,this.navParams=u,this.categories=[{id:1,name:"Living Expenses",title:"Living Expenses",description:"Check Out What You Spent",img:"assets/imgs/menu/coffee.png"},{id:2,name:"Savings and Investments",title:"Savings and Investments",description:"Take A Look At Your Assets",img:"assets/imgs/menu/munchies.png"},{id:3,name:"Projects",title:"Projects",description:"How Close Are You To Your Goals ",img:"assets/imgs/menu/munchies.png"},{id:4,name:"Debt Repayment",title:"Debt Repayment",description:"See What you Owe",img:"assets/imgs/menu/breakfast.png"}]}return l.prototype.ionViewDidLoad=function(){this.load(),console.log("ionViewDidLoad ViewbudgetPage")},l.prototype.analysisAction=function(l){if(1==l.id)this.navCtrl.push("ExpensebudgetPage",{category:1});else if(2==l.id){this.navCtrl.push("SavInvbudgetPage",{category:2})}else if(3==l.id){this.navCtrl.push("ProjectbudgetPage",{category:3})}else if(4==l.id){this.navCtrl.push("DebtbudgetPage",{category:4})}},l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l}()},261:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},262:function(l,n,u){"use strict";u.d(n,"a",function(){return t});u(1),u(7),u(39);var a=u(62),t=function(){function l(l,n,u){this.navCtrl=l,this.storage=n,this.navParams=u,this.categories=[{id:1,name:"Income",title:"Income",description:"See Trends on Your Income Streams",img:"assets/imgs/menu/coffee.png"},{id:2,name:"Expenses",title:"Expenses",description:"Check Out What You Spend on",img:"assets/imgs/menu/breakfast.png"},{id:3,name:"Savings and Investments",title:"Savings and Investments",description:"Take A Look At Your Assets",img:"assets/imgs/menu/munchies.png"},{id:4,name:"Liabilities",title:"Liabilities",description:"We Have Analysed Your Liabilities",img:"assets/imgs/menu/breakfast.png"},{id:5,name:"Projects",title:"Projects",description:"How Close Are You To Your Goals ",img:"assets/imgs/menu/munchies.png"}]}return l.prototype.ionViewDidLoad=function(){this.load(),console.log("ionViewDidLoad ViewanalyticsPage")},l.prototype.analysisAction=function(l){if(1==l.id)this.navCtrl.push("IncomePage",{category:1});else if(2==l.id){this.navCtrl.push("ExpensePage",{category:2})}else if(3==l.id){this.navCtrl.push("SavInvPage",{category:3})}else if(4==l.id){this.navCtrl.push("LiabilitiesPage",{category:4})}else if(5==l.id){this.navCtrl.push("ProjectsPage",{category:5})}},l.prototype.ionViewWillEnter=function(){this.load()},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.navCtrl.setRoot(a.a)})},l}()},263:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},264:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7),u(39);var a=function(){function l(l,n,u,a,t,e){this.navCtrl=l,this.navParams=n,this.storage=u,this.alertCtrl=a,this.grsp=t,this.toastCtrl=e,this.loaded=0}return l.prototype.ionViewDidLoad=function(){this.valName=this.navParams.get("val"),this.getUserName()},l.prototype.selectItem=function(l){this.toastCtrl.create({message:"item "+l.title+" Clicked",duration:2500}).present()},l.prototype.getItems=function(l){var n=l.target.value;n&&""!=n.trim()&&(this.transactions=this.transactions.filter(function(l){return l.trans_bucket.toLowerCase().indexOf(n.toLowerCase())>-1||l.trans_type.toLowerCase().indexOf(n.toLowerCase())>-1||l.trans_bucket_child.toLowerCase().indexOf(n.toLowerCase())>-1||l.trans_bucket_grandChild.toLowerCase().indexOf(n.toLowerCase())>-1||l.trans_amt.indexOf(n)>-1||l.trans_desc.toLowerCase().indexOf(n.toLowerCase())>-1||l.trans_date.indexOf(n)>-1})),console.log(this.transactions)},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.getTransactionsApi(l.start,l.end)})},l.prototype.setApiJson=function(l,n){this.apiData={uId:this.uId,start:l,end:n}},l.prototype.getTransactionsApi=function(l,n){var u=this;this.setApiJson(l,n);this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});console.log(this.apiData),this.grsp.transactions(this.apiData).then(function(l){console.log(l),u.trans=l,u.trans=JSON.parse(l.data),u.transactions=JSON.parse(u.trans.transactions),u.loaded=1,u.alertCtrl.create({title:"No name",subTitle:"no Name"+u.transactions[0],buttons:["Dismiss"]})}).catch(function(l){u.loaded=1,u.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l.prototype.editTrans=function(l){this.navCtrl.push("EdittransactionsPage",{val:"I am View Edit-trans",trans:l})},l}()},265:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},266:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){function l(l,n,u){this.navCtrl=l,this.navParams=n,this.toastCtrl=u,this.slides=[{title:"Discover",description:"The Little Coffee Shop serves specialty coffee, fancy grilled cheese sandwiches, scratch cooking, craft ales, and cider."},{title:"Taste",description:"You like your coffee Rich and Distinctive? Sweet and ight? Maybe Intense and Creamy? no worries, We've got it!"},{title:"Love",description:"So You Are a Coffe Lover? So as we! We serve coffee with all the Love in the world. Come Love with us."}]}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad WalkthroughPage")},l.prototype.goToHomePage=function(){this.toastCtrl.create({message:"Get Started Clicked",duration:2500}).present()},l}()},267:function(l,n,u){"use strict";u.d(n,"a",function(){return a});u(1),u(7);var a=function(){return function(){}}()},280:function(l,n){function u(l){return Promise.resolve().then(function(){throw new Error("Cannot find module '"+l+"'.")})}u.keys=function(){return[]},u.resolve=u,l.exports=u,u.id=280},305:function(l,n,u){var a={"../pages/_projectsbudget/projectsbudget.module.ngfactory":[931,17],"../pages/addtransactions/addtransactions.module.ngfactory":[937,47],"../pages/analysis/analysis.module.ngfactory":[932,46],"../pages/banking/banking.module.ngfactory":[933,45],"../pages/budget/budget.module.ngfactory":[934,44],"../pages/cards/cards.module.ngfactory":[935,43],"../pages/credit-card/credit-card.module.ngfactory":[936,42],"../pages/current/current.module.ngfactory":[938,16],"../pages/dashboard/dashboard.module.ngfactory":[977,41],"../pages/debtbudget/debtbudget.module.ngfactory":[971,15],"../pages/edittransactions/edittransactions.module.ngfactory":[939,40],"../pages/expense/expense.module.ngfactory":[968,14],"../pages/expensebudget/expensebudget.module.ngfactory":[969,13],"../pages/favorites/favorites.module.ngfactory":[940,39],"../pages/fin-info/fin-info.module.ngfactory":[941,38],"../pages/fixed-deposit/fixed-deposit.module.ngfactory":[942,12],"../pages/foreign/foreign.module.ngfactory":[944,11],"../pages/forex-card/forex-card.module.ngfactory":[945,37],"../pages/income/income.module.ngfactory":[978,10],"../pages/incomebudget/incomebudget.module.ngfactory":[943,9],"../pages/initial/initial.module.ngfactory":[946,36],"../pages/investment/investment.module.ngfactory":[947,35],"../pages/liabilities/liabilities.module.ngfactory":[970,8],"../pages/liabilitiesbudget/liabilitiesbudget.module.ngfactory":[948,7],"../pages/loadsmstransactions/loadsmstransactions.module.ngfactory":[974,6],"../pages/managebudget/managebudget.module.ngfactory":[952,34],"../pages/menu-one/menu-one.module.ngfactory":[949,33],"../pages/menu-two/menu-two.module.ngfactory":[950,32],"../pages/naira-card/naira-card.module.ngfactory":[951,31],"../pages/order-details/order-details.module.ngfactory":[953,30],"../pages/pay-conf/pay-conf.module.ngfactory":[954,5],"../pages/prepaid-card/prepaid-card.module.ngfactory":[955,29],"../pages/profile/profile.module.ngfactory":[956,28],"../pages/projectbudget/projectbudget.module.ngfactory":[973,4],"../pages/projects/projects.module.ngfactory":[972,3],"../pages/sav-inv/sav-inv.module.ngfactory":[975,2],"../pages/sav-invbudget/sav-invbudget.module.ngfactory":[976,1],"../pages/savings/savings.module.ngfactory":[957,0],"../pages/setting/setting.module.ngfactory":[963,27],"../pages/signup/signup.module.ngfactory":[958,26],"../pages/splash-one/splash-one.module.ngfactory":[959,25],"../pages/splash-two/splash-two.module.ngfactory":[960,24],"../pages/sub-menu-one/sub-menu-one.module.ngfactory":[961,23],"../pages/sub-menu-two/sub-menu-two.module.ngfactory":[962,22],"../pages/viewanalytics/viewanalytics.module.ngfactory":[965,21],"../pages/viewbudget/viewbudget.module.ngfactory":[964,20],"../pages/viewtransactions/viewtransactions.module.ngfactory":[966,19],"../pages/walkthrough/walkthrough.module.ngfactory":[967,18]};function t(l){var n=a[l];return n?u.e(n[1]).then(function(){return u(n[0])}):Promise.reject(new Error("Cannot find module '"+l+"'."))}t.keys=function(){return Object.keys(a)},t.id=305,l.exports=t},32:function(l,n,u){"use strict";u.d(n,"a",function(){return i});u(1);var a=u(324),t=(u.n(a),u(325)),e=(u.n(t),u(327)),i=(u.n(e),u(7),u(189),function(){function l(l,n,u){this.http=l,this.alertCtrl=n,this.httpPlugin=u,this.baseUrl="http://gaid.io/myLeo",this.header={"Content-Type":"application/json"},this.paystack="pk_live_a52eed168e35708420a2d0aa9ed43ac22ff64f5b",console.log("Hello GlobalRestServiceProvider Provider")}return l.prototype.showAlert=function(l){this.alertCtrl.create({title:"grs alert",subTitle:l,buttons:["Dismiss"]}).present()},l.prototype.buckets=function(){return["Income","Expenses","Saving&Investments(Assets)","Liabilities","Projects (Goals)"]},l.prototype.test=function(l){return this.httpPlugin.post(this.baseUrl+"/test",l,this.header)},l.prototype.addUser=function(l){return this.httpPlugin.post(this.baseUrl+"/addUser",l,this.header)},l.prototype.editUser=function(l){return this.httpPlugin.post(this.baseUrl+"/editUser",l,this.header)},l.prototype.dashboardLoad=function(l){return this.httpPlugin.post(this.baseUrl+"/dashboard",l,this.header)},l.prototype.login=function(l){return this.httpPlugin.post(this.baseUrl+"/login",l,this.header)},l.prototype.addBudget=function(l){return this.httpPlugin.post(this.baseUrl+"/addBudget",l,this.header)},l.prototype.addTrans=function(l){return this.httpPlugin.post(this.baseUrl+"/addTrans",l,this.header)},l.prototype.editTrans=function(l){return this.httpPlugin.post(this.baseUrl+"/editTrans",l,this.header)},l.prototype.buyInvestment=function(l){return this.httpPlugin.post(this.baseUrl+"/buyInvestment",l,this.header)},l.prototype.addInitialData=function(l){return this.httpPlugin.post(this.baseUrl+"/addInitialData",l,this.header)},l.prototype.analytics=function(l){return this.httpPlugin.post(this.baseUrl+"/analytics",l,this.header)},l.prototype.fixedDeposits=function(l){return this.httpPlugin.post(this.baseUrl+"/fixedDeposits",l,this.header)},l.prototype.transactions=function(l){return this.httpPlugin.post(this.baseUrl+"/transactions",l,this.header)},l.prototype.getTransSumByMonth=function(l){return this.httpPlugin.post(this.baseUrl+"/transSum",l,this.header)},l.prototype.banking=function(l){return this.httpPlugin.post(this.baseUrl+"/banking",l,this.header)},l.prototype.cards=function(l){return this.httpPlugin.post(this.baseUrl+"/cards",l,this.header)},l.prototype.investments=function(l){return this.httpPlugin.post(this.baseUrl+"/investments",l,this.header)},l.prototype.lastDate=function(l){return this.httpPlugin.post(this.baseUrl+"/lastDate",l,this.header)},l}())},561:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(214),w=u(40),x=u(12),D=a.X({encapsulation:2,styles:[],data:{}});function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.analysisAction(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n    "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(4,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(5,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},g.b,g.a)),a.Y(8,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(18,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,["Analysis"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(23,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(24,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(27,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"tertiary");l(n,9,0,"");l(n,14,0,"menu"),l(n,27,0,u.categories)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,23,0,a._13(n,24).statusbarPadding,a._13(n,24)._hasRefresher)})}var T=a.V("page-analysis",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-analysis",[],null,null,null,M,D)),a.Y(1,49152,null,0,P.a,[d.a,w.a,x.a],null,null)],null,null)},{},{},[])},562:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(216),w=u(40),x=u(12),D=a.X({encapsulation:2,styles:[],data:{}});function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.bankAction(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n  "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n  "])),(l()(),a.Z(4,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(5,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},g.b,g.a)),a.Y(8,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(18,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,["Banking"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(23,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(24,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n  "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(27,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"tertiary");l(n,9,0,"");l(n,14,0,"menu"),l(n,27,0,u.categories)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,23,0,a._13(n,24).statusbarPadding,a._13(n,24)._hasRefresher)})}var T=a.V("page-banking",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-banking",[],null,null,null,M,D)),a.Y(1,49152,null,0,P.a,[d.a,w.a,x.a],null,null)],null,null)},{},{},[])},563:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(218),w=u(40),x=u(12),D=a.X({encapsulation:2,styles:[],data:{}});function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.analysisAction(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n    "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(4,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(5,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},g.b,g.a)),a.Y(8,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(18,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,["Budget"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(23,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(24,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(27,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"tertiary");l(n,9,0,"");l(n,14,0,"menu"),l(n,27,0,u.categories)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,23,0,a._13(n,24).statusbarPadding,a._13(n,24)._hasRefresher)})}var T=a.V("page-budget",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-budget",[],null,null,null,M,D)),a.Y(1,49152,null,0,P.a,[d.a,w.a,x.a],null,null)],null,null)},{},{},[])},564:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(220),w=u(40),x=u(12),D=a.X({encapsulation:2,styles:[],data:{}});function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.cardsAction(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n  "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n  "])),(l()(),a.Z(4,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(5,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},g.b,g.a)),a.Y(8,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(18,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,["Cards"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(23,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(24,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n  "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(27,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"tertiary");l(n,9,0,"");l(n,14,0,"menu"),l(n,27,0,u.categories)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,23,0,a._13(n,24).statusbarPadding,a._13(n,24)._hasRefresher)})}var T=a.V("page-cards",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-cards",[],null,null,null,M,D)),a.Y(1,49152,null,0,P.a,[d.a,w.a,x.a],null,null)],null,null)},{},{},[])},565:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(45),s=u(60),r=u(26),c=u(6),d=u(33),g=u(19),_=u(9),p=u(14),m=u(36),h=u(28),f=u(21),b=u(24),v=u(15),y=u(5),Y=u(10),C=u(17),Z=u(13),j=u(151),k=u(48),z=u(40),P=u(32),w=u(12),x=a.X({encapsulation:2,styles:[],data:{}});function D(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-card",[],null,null,null,null,null)),a.Y(1,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,16,"ion-card-content",[],null,null,null,null,null)),a.Y(4,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(6,0,null,null,0,"img",[["alt",""]],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(8,0,null,null,1,"h4",[],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(11,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(12,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(14,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(15,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(17,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(18,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "]))],null,function(l,n){l(n,6,0,a._2(1,"",n.context.$implicit.logo,"")),l(n,9,0,n.context.$implicit.card_banks),l(n,12,0,n.context.$implicit.ATM_withdrawal_abroad),l(n,15,0,n.context.$implicit.local_POS_withdrawals_limit),l(n,18,0,n.context.$implicit.local_ATM_withdrawals_limit)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,10,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,r.a,[i.a,a.j,a.z,[2,c.a]],null,null),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a.Z(4,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,d.b,d.a)),a.Y(5,49152,null,0,g.a,[_.a,[2,c.a],[2,p.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n    "])),(l()(),a.Z(7,0,null,3,2,"ion-title",[],null,null,null,m.b,m.a)),a.Y(8,49152,null,0,h.a,[i.a,a.j,a.z,[2,f.a],[2,g.a]],null,null),(l()(),a._20(-1,0,["CreditCard"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(13,0,null,null,8,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,b.b,b.a)),a.Y(14,4374528,null,0,v.a,[i.a,y.a,Y.a,a.j,a.z,_.a,C.a,a.u,[2,c.a],[2,p.a]],null,null),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.U(16777216,null,1,1,null,D)),a.Y(17,16384,null,0,Z.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n\n    "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(20,802816,null,0,Z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,17,0,0==u.loaded),l(n,20,0,u.credCrd)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,13,0,a._13(n,14).statusbarPadding,a._13(n,14)._hasRefresher)})}var T=a.V("page-credit-card",j.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-credit-card",[],null,null,null,M,x)),a.Y(1,49152,null,0,j.a,[p.a,k.a,z.a,P.a,w.a],null,null)],null,null)},{},{},[])},566:function(l,n,u){"use strict";u.d(n,"a",function(){return B});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(81),s=u(26),r=u(6),c=u(33),d=u(19),g=u(9),_=u(14),p=u(36),m=u(28),h=u(21),f=u(24),b=u(15),v=u(5),y=u(10),Y=u(17),C=u(13),Z=u(54),j=u(34),k=u(29),z=u(46),P=u(49),w=u(58),x=u(107),D=u(82),I=u(43),M=u(35),T=u(69),F=u(59),L=u(20),N=u(16),S=u(222),A=u(12),V=u(40),U=u(48),O=u(32),H=a.X({encapsulation:2,styles:[],data:{}});function R(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function E(l){return a._22(0,[(l()(),a.Z(0,0,null,null,2,"ion-option",[],null,null,null,null,null)),a.Y(1,16384,[[11,4]],0,o.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(2,null,["",""]))],function(l,n){l(n,1,0,n.context.$implicit)},function(l,n){l(n,2,0,n.context.$implicit)})}function $(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,16,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,s.a,[i.a,a.j,a.z,[2,r.a]],null,null),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(4,0,null,null,12,"div",[["class","logo-container"],["text-center",""]],[[4,"display",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(6,0,null,null,0,"img",[["alt","Logo"],["src","assets/imgs/logo.png"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n\n        "])),(l()(),a._20(-1,null,["\n        \n        "])),(l()(),a.Z(9,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,c.b,c.a)),a.Y(10,49152,null,0,d.a,[g.a,[2,r.a],[2,_.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n          "])),(l()(),a.Z(12,0,null,3,2,"ion-title",[],null,null,null,p.b,p.a)),a.Y(13,49152,null,0,m.a,[i.a,a.j,a.z,[2,h.a],[2,d.a]],null,null),(l()(),a._20(14,0,["Add Transactions for ",""])),(l()(),a._20(-1,3,["\n      \n        "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a._20(-1,null,["\n\n    \n  \n  "])),(l()(),a._20(-1,null,["\n  \n  \n  "])),(l()(),a.Z(19,0,null,null,126,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,f.b,f.a)),a.Y(20,4374528,null,0,b.a,[i.a,v.a,y.a,a.j,a.z,g.a,Y.a,a.u,[2,r.a],[2,_.a]],null,null),(l()(),a._20(-1,1,["\n  \n  "])),(l()(),a.U(16777216,null,1,1,null,R)),a.Y(23,16384,null,0,C.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(25,0,null,1,27,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(26,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,1,{contentLabel:0}),a._18(603979776,2,{_buttons:1}),a._18(603979776,3,{_icons:1}),a.Y(30,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(32,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(33,16384,[[1,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Transaction Type"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(36,0,null,3,15,"ion-select",[],[[2,"select-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"],[null,"keyup.space"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,37)._click(u)&&t);"keyup.space"===n&&(t=!1!==a._13(l,37)._keyup()&&t);"ngModelChange"===n&&(t=!1!==(e.type=u)&&t);return t},x.b,x.a)),a.Y(37,1228800,null,1,D.a,[g.a,k.a,i.a,a.j,a.z,[2,j.a],I.a],null,null),a._18(603979776,4,{options:1}),a._17(1024,null,M.e,function(l){return[l]},[D.a]),a.Y(40,671744,null,0,M.h,[[8,null],[8,null],[8,null],[2,M.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(42,16384,null,0,M.g,[M.f],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(44,0,null,null,2,"ion-option",[["value","cr"]],null,null,null,null,null)),a.Y(45,16384,[[4,4]],0,o.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(-1,null,["Credit"])),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(48,0,null,null,2,"ion-option",[["value","dr"]],null,null,null,null,null)),a.Y(49,16384,[[4,4]],0,o.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(-1,null,["Debit"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(54,0,null,1,16,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(55,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,5,{contentLabel:0}),a._18(603979776,6,{_buttons:1}),a._18(603979776,7,{_icons:1}),a.Y(59,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(61,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(62,16384,[[5,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Description"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(65,0,null,3,4,"ion-input",[["type",""]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.desc=u)&&a);return a},T.b,T.a)),a.Y(66,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(68,16384,null,0,M.g,[M.f],null,null),a.Y(69,5423104,null,0,F.a,[i.a,v.a,k.a,g.a,a.j,a.z,[2,b.a],[2,j.a],[2,M.f],y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(72,0,null,1,23,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(73,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,8,{contentLabel:0}),a._18(603979776,9,{_buttons:1}),a._18(603979776,10,{_icons:1}),a.Y(77,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(79,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(80,16384,[[8,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Bucket"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(83,0,null,3,11,"ion-select",[],[[2,"select-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"],[null,"keyup.space"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,84)._click(u)&&t);"keyup.space"===n&&(t=!1!==a._13(l,84)._keyup()&&t);"ngModelChange"===n&&(t=!1!==(e.bucket=u)&&t);return t},x.b,x.a)),a.Y(84,1228800,null,1,D.a,[g.a,k.a,i.a,a.j,a.z,[2,j.a],I.a],null,null),a._18(603979776,11,{options:1}),a._17(1024,null,M.e,function(l){return[l]},[D.a]),a.Y(87,671744,null,0,M.h,[[8,null],[8,null],[8,null],[2,M.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(89,16384,null,0,M.g,[M.f],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a._20(-1,null,["\n          "])),(l()(),a.U(16777216,null,null,1,null,E)),a.Y(93,802816,null,0,C.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(97,0,null,1,16,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(98,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,12,{contentLabel:0}),a._18(603979776,13,{_buttons:1}),a._18(603979776,14,{_icons:1}),a.Y(102,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(104,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(105,16384,[[12,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Amount"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(108,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.amt=u)&&a);return a},T.b,T.a)),a.Y(109,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(111,16384,null,0,M.g,[M.f],null,null),a.Y(112,5423104,null,0,F.a,[i.a,v.a,k.a,g.a,a.j,a.z,[2,b.a],[2,j.a],[2,M.f],y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(115,0,null,1,16,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(116,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,15,{contentLabel:0}),a._18(603979776,16,{_buttons:1}),a._18(603979776,17,{_icons:1}),a.Y(120,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(122,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(123,16384,[[15,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Date"])),(l()(),a._20(-1,2,["\n          "])),(l()(),a.Z(126,0,null,3,4,"ion-input",[["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.date=u)&&a);return a},T.b,T.a)),a.Y(127,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(129,16384,null,0,M.g,[M.f],null,null),a.Y(130,5423104,null,0,F.a,[i.a,v.a,k.a,g.a,a.j,a.z,[2,b.a],[2,j.a],[2,M.f],y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(133,0,null,1,11,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(134,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,18,{contentLabel:0}),a._18(603979776,19,{_buttons:1}),a._18(603979776,20,{_icons:1}),a.Y(138,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,2,["\n          "])),(l()(),a.Z(141,0,null,2,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.saveAddApi()&&a);return a},L.b,L.a)),a.Y(142,1097728,[[19,4]],0,N.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Save"])),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n  "])),(l()(),a._20(-1,null,["\n  "]))],function(l,n){var u=n.component;l(n,23,0,0==u.loaded),l(n,40,0,u.type);l(n,45,0,"cr");l(n,49,0,"dr"),l(n,66,0,u.desc);l(n,69,0,""),l(n,87,0,u.bucket),l(n,93,0,u.buckets),l(n,109,0,u.amt);l(n,112,0,"number"),l(n,127,0,u.date);l(n,130,0,"date");l(n,142,0,"")},function(l,n){var u=n.component;l(n,4,0,"block"),l(n,9,0,a._13(n,10)._hidden,a._13(n,10)._sbPadding),l(n,14,0,u.userName),l(n,19,0,a._13(n,20).statusbarPadding,a._13(n,20)._hasRefresher),l(n,36,0,a._13(n,37)._disabled,a._13(n,42).ngClassUntouched,a._13(n,42).ngClassTouched,a._13(n,42).ngClassPristine,a._13(n,42).ngClassDirty,a._13(n,42).ngClassValid,a._13(n,42).ngClassInvalid,a._13(n,42).ngClassPending),l(n,65,0,a._13(n,68).ngClassUntouched,a._13(n,68).ngClassTouched,a._13(n,68).ngClassPristine,a._13(n,68).ngClassDirty,a._13(n,68).ngClassValid,a._13(n,68).ngClassInvalid,a._13(n,68).ngClassPending),l(n,83,0,a._13(n,84)._disabled,a._13(n,89).ngClassUntouched,a._13(n,89).ngClassTouched,a._13(n,89).ngClassPristine,a._13(n,89).ngClassDirty,a._13(n,89).ngClassValid,a._13(n,89).ngClassInvalid,a._13(n,89).ngClassPending),l(n,108,0,a._13(n,111).ngClassUntouched,a._13(n,111).ngClassTouched,a._13(n,111).ngClassPristine,a._13(n,111).ngClassDirty,a._13(n,111).ngClassValid,a._13(n,111).ngClassInvalid,a._13(n,111).ngClassPending),l(n,126,0,a._13(n,129).ngClassUntouched,a._13(n,129).ngClassTouched,a._13(n,129).ngClassPristine,a._13(n,129).ngClassDirty,a._13(n,129).ngClassValid,a._13(n,129).ngClassInvalid,a._13(n,129).ngClassPending)})}var B=a.V("page-addtransactions",S.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-addtransactions",[],null,null,null,$,H)),a.Y(1,49152,null,0,S.a,[_.a,A.a,V.a,U.a,O.a],null,null)],null,null)},{},{},[])},567:function(l,n,u){"use strict";u.d(n,"a",function(){return B});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(81),s=u(26),r=u(6),c=u(33),d=u(19),g=u(9),_=u(14),p=u(36),m=u(28),h=u(21),f=u(24),b=u(15),v=u(5),y=u(10),Y=u(17),C=u(13),Z=u(54),j=u(34),k=u(29),z=u(46),P=u(49),w=u(58),x=u(107),D=u(82),I=u(43),M=u(35),T=u(69),F=u(59),L=u(20),N=u(16),S=u(224),A=u(12),V=u(40),U=u(48),O=u(32),H=a.X({encapsulation:2,styles:[],data:{}});function R(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function E(l){return a._22(0,[(l()(),a.Z(0,0,null,null,2,"ion-option",[],null,null,null,null,null)),a.Y(1,16384,[[11,4]],0,o.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(2,null,["",""]))],function(l,n){l(n,1,0,n.context.$implicit)},function(l,n){l(n,2,0,n.context.$implicit)})}function $(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(2,0,null,null,17,"ion-header",[],null,null,null,null,null)),a.Y(3,16384,null,0,s.a,[i.a,a.j,a.z,[2,r.a]],null,null),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(5,0,null,null,13,"div",[["class","logo-container"],["text-center",""]],[[4,"display",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(7,0,null,null,0,"img",[["alt","Logo"],["src","assets/imgs/logo.png"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n\n        "])),(l()(),a._20(-1,null,["\n        \n        "])),(l()(),a.Z(10,0,null,null,7,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,c.b,c.a)),a.Y(11,49152,null,0,d.a,[g.a,[2,r.a],[2,_.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n          "])),(l()(),a._20(-1,3,["\n          "])),(l()(),a.Z(14,0,null,3,2,"ion-title",[],null,null,null,p.b,p.a)),a.Y(15,49152,null,0,m.a,[i.a,a.j,a.z,[2,h.a],[2,d.a]],null,null),(l()(),a._20(16,0,["Edit ","'s Transactions"])),(l()(),a._20(-1,3,["\n\n      \n        "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a._20(-1,null,["\n\n    \n  \n  "])),(l()(),a._20(-1,null,["\n  \n  \n  "])),(l()(),a.Z(21,0,null,null,127,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,f.b,f.a)),a.Y(22,4374528,null,0,b.a,[i.a,v.a,y.a,a.j,a.z,g.a,Y.a,a.u,[2,r.a],[2,_.a]],null,null),(l()(),a._20(-1,1,["\n  \n  "])),(l()(),a.U(16777216,null,1,1,null,R)),a.Y(25,16384,null,0,C.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(27,0,null,1,27,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(28,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,1,{contentLabel:0}),a._18(603979776,2,{_buttons:1}),a._18(603979776,3,{_icons:1}),a.Y(32,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(34,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(35,16384,[[1,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Transaction Type"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(38,0,null,3,15,"ion-select",[],[[2,"select-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"],[null,"keyup.space"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,39)._click(u)&&t);"keyup.space"===n&&(t=!1!==a._13(l,39)._keyup()&&t);"ngModelChange"===n&&(t=!1!==(e.type=u)&&t);return t},x.b,x.a)),a.Y(39,1228800,null,1,D.a,[g.a,k.a,i.a,a.j,a.z,[2,j.a],I.a],null,null),a._18(603979776,4,{options:1}),a._17(1024,null,M.e,function(l){return[l]},[D.a]),a.Y(42,671744,null,0,M.h,[[8,null],[8,null],[8,null],[2,M.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(44,16384,null,0,M.g,[M.f],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(46,0,null,null,2,"ion-option",[["value","cr"]],null,null,null,null,null)),a.Y(47,16384,[[4,4]],0,o.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(-1,null,["Credit"])),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(50,0,null,null,2,"ion-option",[["value","dr"]],null,null,null,null,null)),a.Y(51,16384,[[4,4]],0,o.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(-1,null,["Debit"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(56,0,null,1,16,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(57,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,5,{contentLabel:0}),a._18(603979776,6,{_buttons:1}),a._18(603979776,7,{_icons:1}),a.Y(61,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(63,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(64,16384,[[5,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Description"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(67,0,null,3,4,"ion-input",[["type",""]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.desc=u)&&a);return a},T.b,T.a)),a.Y(68,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(70,16384,null,0,M.g,[M.f],null,null),a.Y(71,5423104,null,0,F.a,[i.a,v.a,k.a,g.a,a.j,a.z,[2,b.a],[2,j.a],[2,M.f],y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(74,0,null,1,24,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(75,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,8,{contentLabel:0}),a._18(603979776,9,{_buttons:1}),a._18(603979776,10,{_icons:1}),a.Y(79,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(81,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(82,16384,[[8,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Bucket"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(85,0,null,3,12,"ion-select",[],[[2,"select-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"],[null,"keyup.space"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,86)._click(u)&&t);"keyup.space"===n&&(t=!1!==a._13(l,86)._keyup()&&t);"ngModelChange"===n&&(t=!1!==(e.bucket=u)&&t);return t},x.b,x.a)),a.Y(86,1228800,null,1,D.a,[g.a,k.a,i.a,a.j,a.z,[2,j.a],I.a],null,null),a._18(603979776,11,{options:1}),a._17(1024,null,M.e,function(l){return[l]},[D.a]),a.Y(89,671744,null,0,M.h,[[8,null],[8,null],[8,null],[2,M.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(91,16384,null,0,M.g,[M.f],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a._20(-1,null,["\n          "])),(l()(),a._20(-1,null,["\n          "])),(l()(),a.U(16777216,null,null,1,null,E)),a.Y(96,802816,null,0,C.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n\n        "])),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(100,0,null,1,16,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(101,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,12,{contentLabel:0}),a._18(603979776,13,{_buttons:1}),a._18(603979776,14,{_icons:1}),a.Y(105,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(107,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(108,16384,[[12,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Amount"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(111,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.amt=u)&&a);return a},T.b,T.a)),a.Y(112,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(114,16384,null,0,M.g,[M.f],null,null),a.Y(115,5423104,null,0,F.a,[i.a,v.a,k.a,g.a,a.j,a.z,[2,b.a],[2,j.a],[2,M.f],y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(118,0,null,1,16,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(119,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,15,{contentLabel:0}),a._18(603979776,16,{_buttons:1}),a._18(603979776,17,{_icons:1}),a.Y(123,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(125,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(126,16384,[[15,4]],0,w.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Date"])),(l()(),a._20(-1,2,["\n          "])),(l()(),a.Z(129,0,null,3,4,"ion-input",[["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.date=u)&&a);return a},T.b,T.a)),a.Y(130,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(132,16384,null,0,M.g,[M.f],null,null),a.Y(133,5423104,null,0,F.a,[i.a,v.a,k.a,g.a,a.j,a.z,[2,b.a],[2,j.a],[2,M.f],y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.Z(136,0,null,1,11,"ion-item",[["class","item item-block"]],null,null,null,Z.b,Z.a)),a.Y(137,1097728,null,3,j.a,[k.a,i.a,a.j,a.z,[2,z.a]],null,null),a._18(335544320,18,{contentLabel:0}),a._18(603979776,19,{_buttons:1}),a._18(603979776,20,{_icons:1}),a.Y(141,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,2,["\n          "])),(l()(),a.Z(144,0,null,2,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.saveEditApi()&&a);return a},L.b,L.a)),a.Y(145,1097728,[[19,4]],0,N.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Save"])),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,1,["\n  \n  "])),(l()(),a._20(-1,null,["\n  \n"]))],function(l,n){var u=n.component;l(n,25,0,0==u.loaded),l(n,42,0,u.type);l(n,47,0,"cr");l(n,51,0,"dr"),l(n,68,0,u.desc);l(n,71,0,""),l(n,89,0,u.bucket),l(n,96,0,u.buckets),l(n,112,0,u.amt);l(n,115,0,"number"),l(n,130,0,u.date);l(n,133,0,"date");l(n,145,0,"")},function(l,n){var u=n.component;l(n,5,0,"block"),l(n,10,0,a._13(n,11)._hidden,a._13(n,11)._sbPadding),l(n,16,0,u.userName),l(n,21,0,a._13(n,22).statusbarPadding,a._13(n,22)._hasRefresher),l(n,38,0,a._13(n,39)._disabled,a._13(n,44).ngClassUntouched,a._13(n,44).ngClassTouched,a._13(n,44).ngClassPristine,a._13(n,44).ngClassDirty,a._13(n,44).ngClassValid,a._13(n,44).ngClassInvalid,a._13(n,44).ngClassPending),l(n,67,0,a._13(n,70).ngClassUntouched,a._13(n,70).ngClassTouched,a._13(n,70).ngClassPristine,a._13(n,70).ngClassDirty,a._13(n,70).ngClassValid,a._13(n,70).ngClassInvalid,a._13(n,70).ngClassPending),l(n,85,0,a._13(n,86)._disabled,a._13(n,91).ngClassUntouched,a._13(n,91).ngClassTouched,a._13(n,91).ngClassPristine,a._13(n,91).ngClassDirty,a._13(n,91).ngClassValid,a._13(n,91).ngClassInvalid,a._13(n,91).ngClassPending),l(n,111,0,a._13(n,114).ngClassUntouched,a._13(n,114).ngClassTouched,a._13(n,114).ngClassPristine,a._13(n,114).ngClassDirty,a._13(n,114).ngClassValid,a._13(n,114).ngClassInvalid,a._13(n,114).ngClassPending),l(n,129,0,a._13(n,132).ngClassUntouched,a._13(n,132).ngClassTouched,a._13(n,132).ngClassPristine,a._13(n,132).ngClassDirty,a._13(n,132).ngClassValid,a._13(n,132).ngClassInvalid,a._13(n,132).ngClassPending)})}var B=a.V("page-edittransactions",S.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-edittransactions",[],null,null,null,$,H)),a.Y(1,49152,null,0,S.a,[_.a,A.a,V.a,U.a,O.a],null,null)],null,null)},{},{},[])},568:function(l,n,u){"use strict";u.d(n,"a",function(){return O});var a=u(0),t=u(54),e=u(34),i=u(29),o=u(3),s=u(46),r=u(49),c=u(104),d=u(64),g=u(67),_=u(20),p=u(16),m=u(31),h=u(13),f=u(26),b=u(6),v=u(33),y=u(19),Y=u(9),C=u(14),Z=u(41),j=u(30),k=u(42),z=u(21),P=u(36),w=u(28),x=u(24),D=u(15),I=u(5),M=u(10),T=u(17),F=u(61),L=u(18),N=u(226),S=u(12),A=a.X({encapsulation:2,styles:[],data:{}});function V(l){return a._22(0,[(l()(),a.Z(0,0,null,null,43,"ion-item",[["class","item item-block"]],null,null,null,t.b,t.a)),a.Y(1,1097728,null,3,e.a,[i.a,o.a,a.j,a.z,[2,s.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(5,16384,null,0,r.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(7,0,null,0,4,"ion-thumbnail",[["item-start",""]],null,null,null,null,null)),a.Y(8,16384,null,0,c.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(10,0,null,null,0,"img",[],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(13,0,null,2,1,"h2",[],null,null,null,null,null)),(l()(),a._20(14,null,["",""])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(16,0,null,2,1,"p",[],null,null,null,null,null)),(l()(),a._20(17,null,["",""])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(19,0,null,2,23,"ion-row",[["class","row"],["no-padding",""]],null,null,null,null,null)),a.Y(20,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(22,0,null,null,7,"ion-col",[["class","col"],["no-padding",""]],null,null,null,null,null)),a.Y(23,16384,null,0,g.a,[],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(25,0,null,null,3,"button",[["clear",""],["color","primary"],["ion-button",""],["no-padding",""],["small",""]],null,null,null,_.b,_.a)),a.Y(26,1097728,null,0,p.a,[[8,""],o.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(27,0,["\n                  ","\n                  "])),a._16(28,4),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(31,0,null,null,10,"ion-col",[["class","col"],["no-padding",""],["text-right",""]],null,null,null,null,null)),a.Y(32,16384,null,0,g.a,[],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(34,0,null,null,6,"button",[["clear",""],["icon-end",""],["ion-button",""],["no-padding",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!=(l.context.$implicit.isliked=!l.context.$implicit.isliked)&&a);return a},_.b,_.a)),a.Y(35,1097728,null,0,p.a,[[8,""],o.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(-1,0,["\n                  "])),(l()(),a.Z(37,0,null,0,1,"ion-icon",[["name","heart"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(38,147456,null,0,m.a,[o.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n                  "])),(l()(),a._20(-1,0,["\n                "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,2,["\n        "]))],function(l,n){l(n,26,0,"primary","","");l(n,35,0,n.context.$implicit.isliked?"danger":"light","","");l(n,38,0,"heart")},function(l,n){l(n,10,0,a._2(1,"",n.context.$implicit.img,"")),l(n,14,0,n.context.$implicit.title),l(n,17,0,n.context.$implicit.description),l(n,27,0,a._21(n,27,0,l(n,28,0,a._13(n.parent,0),n.context.$implicit.price,"USD",!0,"1.2"))),l(n,37,0,a._13(n,38)._hidden)})}function U(l){return a._22(0,[a._14(0,h.c,[a.r]),(l()(),a.Z(1,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,f.a,[o.a,a.j,a.z,[2,b.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(4,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,v.b,v.a)),a.Y(5,49152,null,0,y.a,[Y.a,[2,b.a],[2,C.a],o.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},_.b,_.a)),a.Y(8,1097728,[[1,4]],0,p.a,[[8,""],o.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,Z.a,[j.a,[2,b.a],[2,p.a],[2,y.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,k.a,[o.a,a.j,a.z,[2,z.a],[2,y.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,m.a,[o.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,P.b,P.a)),a.Y(18,49152,null,0,w.a,[o.a,a.j,a.z,[2,z.a],[2,y.a]],null,null),(l()(),a._20(-1,0,["Likes"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(23,0,null,null,9,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,x.b,x.a)),a.Y(24,4374528,null,0,D.a,[o.a,I.a,M.a,a.j,a.z,Y.a,T.a,a.u,[2,b.a],[2,C.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(26,0,null,1,5,"ion-list",[["no-lines",""],["no-margin",""],["padding-horizontal",""]],null,null,null,null,null)),a.Y(27,16384,null,0,F.a,[o.a,a.j,a.z,I.a,L.l,M.a],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.U(16777216,null,null,1,null,V)),a.Y(30,802816,null,0,h.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"secondary");l(n,9,0,"");l(n,14,0,"menu"),l(n,30,0,u.items)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,23,0,a._13(n,24).statusbarPadding,a._13(n,24)._hasRefresher)})}var O=a.V("page-favorites",N.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-favorites",[],null,null,null,U,A)),a.Y(1,49152,null,0,N.a,[C.a,S.a],null,null)],null,null)},{},{},[])},569:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(228),w=u(40),x=u(12),D=a.X({encapsulation:2,styles:[],data:{}});function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.fiAction(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n  "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n  "])),(l()(),a.Z(4,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(5,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},g.b,g.a)),a.Y(8,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(18,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,["Financial Info."])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(23,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(24,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n  "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(27,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"tertiary");l(n,9,0,"");l(n,14,0,"menu"),l(n,27,0,u.categories)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,23,0,a._13(n,24).statusbarPadding,a._13(n,24)._hasRefresher)})}var T=a.V("page-fin-info",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-fin-info",[],null,null,null,M,D)),a.Y(1,49152,null,0,P.a,[d.a,w.a,x.a],null,null)],null,null)},{},{},[])},570:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(45),s=u(60),r=u(26),c=u(6),d=u(33),g=u(19),_=u(9),p=u(14),m=u(36),h=u(28),f=u(21),b=u(24),v=u(15),y=u(5),Y=u(10),C=u(17),Z=u(13),j=u(230),k=u(48),z=u(40),P=u(32),w=u(12),x=a.X({encapsulation:2,styles:[],data:{}});function D(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-card",[],null,null,null,null,null)),a.Y(1,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,16,"ion-card-content",[],null,null,null,null,null)),a.Y(4,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(6,0,null,null,0,"img",[["alt",""]],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(8,0,null,null,1,"h4",[],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(11,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(12,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(14,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(15,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(17,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(18,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "]))],null,function(l,n){l(n,6,0,a._2(1,"",n.context.$implicit.logo,"")),l(n,9,0,n.context.$implicit.card_banks),l(n,12,0,n.context.$implicit.ATM_withdrawal_abroad),l(n,15,0,n.context.$implicit.local_POS_withdrawals_limit),l(n,18,0,n.context.$implicit.local_ATM_withdrawals_limit)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,10,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,r.a,[i.a,a.j,a.z,[2,c.a]],null,null),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a.Z(4,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,d.b,d.a)),a.Y(5,49152,null,0,g.a,[_.a,[2,c.a],[2,p.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n    "])),(l()(),a.Z(7,0,null,3,2,"ion-title",[],null,null,null,m.b,m.a)),a.Y(8,49152,null,0,h.a,[i.a,a.j,a.z,[2,f.a],[2,g.a]],null,null),(l()(),a._20(-1,0,["Foreign Currency Card"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(13,0,null,null,8,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,b.b,b.a)),a.Y(14,4374528,null,0,v.a,[i.a,y.a,Y.a,a.j,a.z,_.a,C.a,a.u,[2,c.a],[2,p.a]],null,null),(l()(),a._20(-1,1,["\n  \n    "])),(l()(),a.U(16777216,null,1,1,null,D)),a.Y(17,16384,null,0,Z.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n\n    "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(20,802816,null,0,Z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,17,0,0==u.loaded),l(n,20,0,u.forexCrd)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,13,0,a._13(n,14).statusbarPadding,a._13(n,14)._hasRefresher)})}var T=a.V("page-forex-card",j.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-forex-card",[],null,null,null,M,x)),a.Y(1,49152,null,0,j.a,[p.a,k.a,z.a,P.a,w.a],null,null)],null,null)},{},{},[])},571:function(l,n,u){"use strict";u.d(n,"a",function(){return O});var a=u(0),t=u(26),e=u(3),i=u(6),o=u(33),s=u(19),r=u(9),c=u(14),d=u(36),g=u(28),_=u(21),p=u(24),m=u(15),h=u(5),f=u(10),b=u(17),v=u(460),y=u(87),Y=u(461),C=u(103),Z=u(20),j=u(16),k=u(54),z=u(34),P=u(29),w=u(46),x=u(49),D=u(58),I=u(69),M=u(35),T=u(59),F=u(150),L=u(12),N=u(48),S=u(40),A=u(32),V=a.X({encapsulation:2,styles:[],data:{}});function U(l){return a._22(0,[a._18(402653184,1,{slides:0}),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(2,0,null,null,11,"ion-header",[],null,null,null,null,null)),a.Y(3,16384,null,0,t.a,[e.a,a.j,a.z,[2,i.a]],null,null),(l()(),a._20(-1,null,["\n  "])),(l()(),a.Z(5,0,null,null,7,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,o.b,o.a)),a.Y(6,49152,null,0,s.a,[r.a,[2,i.a],[2,c.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n      "])),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(9,0,null,3,2,"ion-title",[],null,null,null,d.b,d.a)),a.Y(10,49152,null,0,g.a,[e.a,a.j,a.z,[2,_.a],[2,s.a]],null,null),(l()(),a._20(-1,0,["Welcome"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(15,0,null,null,352,"ion-content",[["class","tutorial-page"]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,p.b,p.a)),a.Y(16,4374528,null,0,m.a,[e.a,h.a,f.a,a.j,a.z,r.a,b.a,a.u,[2,i.a],[2,c.a]],null,null),(l()(),a._20(-1,1,["\n  "])),(l()(),a.Z(18,0,null,1,348,"ion-slides",[],null,null,null,v.b,v.a)),a.Y(19,1228800,[[1,4]],0,y.a,[e.a,h.a,a.u,[2,i.a],a.j,a.z],null,null),(l()(),a._20(-1,0,["\n    "])),(l()(),a.Z(21,0,null,0,9,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(22,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(24,0,null,0,1,"h1",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Welcome, We have been expecting you"])),(l()(),a._20(-1,0,["\n    \n      "])),(l()(),a.Z(27,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(28,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n\n\n    "])),(l()(),a._20(-1,0,["\n   \n    "])),(l()(),a.Z(32,0,null,0,26,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(33,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(36,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(37,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(41,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(43,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(44,16384,[[2,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Tell us your name "])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(47,0,null,3,4,"ion-input",[["name","name"],["type","text"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.name=u)&&a);return a},I.b,I.a)),a.Y(48,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(50,16384,null,0,M.g,[M.f],null,null),a.Y(51,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(55,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(56,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n\n\n    "])),(l()(),a._20(-1,0,["\n\n    "])),(l()(),a.Z(60,0,null,0,30,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(61,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(64,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(65,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,5,{contentLabel:0}),a._18(603979776,6,{_buttons:1}),a._18(603979776,7,{_icons:1}),a.Y(69,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(71,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(72,16384,[[5,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["When were you born "])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(75,0,null,3,4,"ion-input",[["name","dob"],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.dob=u)&&a);return a},I.b,I.a)),a.Y(76,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(78,16384,null,0,M.g,[M.f],null,null),a.Y(79,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(83,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(84,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(87,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(88,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n\n\n    "])),(l()(),a._20(-1,0,["\n    \n    "])),(l()(),a.Z(92,0,null,0,29,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(93,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(95,0,null,0,17,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(96,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,8,{contentLabel:0}),a._18(603979776,9,{_buttons:1}),a._18(603979776,10,{_icons:1}),a.Y(100,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(103,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(104,16384,[[8,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["When did you start work "])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(107,0,null,3,4,"ion-input",[["name","dow"],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.dow=u)&&a);return a},I.b,I.a)),a.Y(108,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(110,16384,null,0,M.g,[M.f],null,null),a.Y(111,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(114,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(115,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(118,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(119,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n    \n    "])),(l()(),a.Z(123,0,null,0,28,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(124,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(126,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(127,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,11,{contentLabel:0}),a._18(603979776,12,{_buttons:1}),a._18(603979776,13,{_icons:1}),a.Y(131,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(133,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(134,16384,[[11,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["How much (Approx) do you have in your pension account "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(137,0,null,3,4,"ion-input",[["name","pen"],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.pen=u)&&a);return a},I.b,I.a)),a.Y(138,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(140,16384,null,0,M.g,[M.f],null,null),a.Y(141,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(144,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(145,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(148,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(149,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n    \n    "])),(l()(),a.Z(153,0,null,0,28,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(154,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(156,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(157,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,14,{contentLabel:0}),a._18(603979776,15,{_buttons:1}),a._18(603979776,16,{_icons:1}),a.Y(161,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(163,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(164,16384,[[14,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["How much (Approx) was your savings and investment last month "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(167,0,null,3,4,"ion-input",[["name","savInv"],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.savInv=u)&&a);return a},I.b,I.a)),a.Y(168,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(170,16384,null,0,M.g,[M.f],null,null),a.Y(171,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(174,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(175,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(178,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(179,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n    \n    "])),(l()(),a.Z(183,0,null,0,28,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(184,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(186,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(187,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,17,{contentLabel:0}),a._18(603979776,18,{_buttons:1}),a._18(603979776,19,{_icons:1}),a.Y(191,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(193,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(194,16384,[[17,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["How much (Approx) did you spend in total last month "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(197,0,null,3,4,"ion-input",[["name","exp"],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.exp=u)&&a);return a},I.b,I.a)),a.Y(198,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(200,16384,null,0,M.g,[M.f],null,null),a.Y(201,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(204,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(205,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(208,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(209,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n    \n    "])),(l()(),a.Z(213,0,null,0,28,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(214,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(216,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(217,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,20,{contentLabel:0}),a._18(603979776,21,{_buttons:1}),a._18(603979776,22,{_icons:1}),a.Y(221,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(223,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(224,16384,[[20,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["How much (Approx) did you make in total last month "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(227,0,null,3,4,"ion-input",[["name","inc"],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.inc=u)&&a);return a},I.b,I.a)),a.Y(228,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(230,16384,null,0,M.g,[M.f],null,null),a.Y(231,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(234,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(235,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(238,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(239,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n    \n    "])),(l()(),a.Z(243,0,null,0,29,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(244,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(246,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(247,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,23,{contentLabel:0}),a._18(603979776,24,{_buttons:1}),a._18(603979776,25,{_icons:1}),a.Y(251,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(253,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(254,16384,[[23,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["At what age do you intend to retire "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(257,0,null,3,4,"ion-input",[["name","ret"],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.ret=u)&&a);return a},I.b,I.a)),a.Y(258,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(260,16384,null,0,M.g,[M.f],null,null),a.Y(261,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(264,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(265,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(268,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(269,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n\n          "])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n   \n    "])),(l()(),a.Z(274,0,null,0,29,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(275,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(277,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(278,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,26,{contentLabel:0}),a._18(603979776,27,{_buttons:1}),a._18(603979776,28,{_icons:1}),a.Y(282,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(284,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(285,16384,[[26,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Email Address "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(288,0,null,3,4,"ion-input",[["name","email"],["type","email"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.email=u)&&a);return a},I.b,I.a)),a.Y(289,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(291,16384,null,0,M.g,[M.f],null,null),a.Y(292,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(295,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(296,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(299,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(300,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n\n          "])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n   \n    "])),(l()(),a.Z(305,0,null,0,29,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(306,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(308,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(309,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,29,{contentLabel:0}),a._18(603979776,30,{_buttons:1}),a._18(603979776,31,{_icons:1}),a.Y(313,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(315,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(316,16384,[[29,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Password (Be sure) "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(319,0,null,3,4,"ion-input",[["name","pword"],["type","password"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.pword=u)&&a);return a},I.b,I.a)),a.Y(320,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(322,16384,null,0,M.g,[M.f],null,null),a.Y(323,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(326,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(327,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(330,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.nextIndex()&&a);return a},Z.b,Z.a)),a.Y(331,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Next"])),(l()(),a._20(-1,0,["\n\n          "])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n   \n    "])),(l()(),a.Z(336,0,null,0,29,"ion-slide",[],null,null,null,Y.b,Y.a)),a.Y(337,180224,null,0,C.a,[a.j,a.z,y.a],null,null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(339,0,null,0,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),a.Y(340,1097728,null,3,z.a,[P.a,e.a,a.j,a.z,[2,w.a]],null,null),a._18(335544320,32,{contentLabel:0}),a._18(603979776,33,{_buttons:1}),a._18(603979776,34,{_icons:1}),a.Y(344,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(346,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(347,16384,[[32,4]],0,D.a,[e.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Confirm Password (Be sure) "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(350,0,null,3,4,"ion-input",[["name","cpword"],["type","password"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.initialData.cpword=u)&&a);return a},I.b,I.a)),a.Y(351,671744,null,0,M.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,M.f,null,[M.h]),a.Y(353,16384,null,0,M.g,[M.f],null,null),a.Y(354,5423104,null,0,T.a,[e.a,h.a,P.a,r.a,a.j,a.z,[2,m.a],[2,z.a],[2,M.f],f.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n          "])),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(357,0,null,0,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.prevIndex()&&a);return a},Z.b,Z.a)),a.Y(358,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Previous"])),(l()(),a._20(-1,0,["\n          "])),(l()(),a._20(-1,0,["\n\n          "])),(l()(),a.Z(362,0,null,0,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.logForm()&&a);return a},Z.b,Z.a)),a.Y(363,1097728,null,0,j.a,[[8,""],e.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Submit"])),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,0,["\n\n  "])),(l()(),a._20(-1,1,["\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,6,0,"secondary");l(n,28,0,"");l(n,48,0,"name",u.initialData.name);l(n,51,0,"text");l(n,56,0,"");l(n,76,0,"dob",u.initialData.dob);l(n,79,0,"date");l(n,84,0,"primary","");l(n,88,0,"");l(n,108,0,"dow",u.initialData.dow);l(n,111,0,"date");l(n,115,0,"primary","");l(n,119,0,"");l(n,138,0,"pen",u.initialData.pen);l(n,141,0,"number");l(n,145,0,"primary","");l(n,149,0,"");l(n,168,0,"savInv",u.initialData.savInv);l(n,171,0,"number");l(n,175,0,"primary","");l(n,179,0,"");l(n,198,0,"exp",u.initialData.exp);l(n,201,0,"number");l(n,205,0,"primary","");l(n,209,0,"");l(n,228,0,"inc",u.initialData.inc);l(n,231,0,"number");l(n,235,0,"primary","");l(n,239,0,"");l(n,258,0,"ret",u.initialData.ret);l(n,261,0,"number");l(n,265,0,"primary","");l(n,269,0,"");l(n,289,0,"email",u.initialData.email);l(n,292,0,"email");l(n,296,0,"primary","");l(n,300,0,"");l(n,320,0,"pword",u.initialData.pword);l(n,323,0,"password");l(n,327,0,"primary","");l(n,331,0,"");l(n,351,0,"cpword",u.initialData.cpword);l(n,354,0,"password");l(n,358,0,"primary","");l(n,363,0,"")},function(l,n){l(n,5,0,a._13(n,6)._hidden,a._13(n,6)._sbPadding),l(n,15,0,a._13(n,16).statusbarPadding,a._13(n,16)._hasRefresher),l(n,47,0,a._13(n,50).ngClassUntouched,a._13(n,50).ngClassTouched,a._13(n,50).ngClassPristine,a._13(n,50).ngClassDirty,a._13(n,50).ngClassValid,a._13(n,50).ngClassInvalid,a._13(n,50).ngClassPending),l(n,75,0,a._13(n,78).ngClassUntouched,a._13(n,78).ngClassTouched,a._13(n,78).ngClassPristine,a._13(n,78).ngClassDirty,a._13(n,78).ngClassValid,a._13(n,78).ngClassInvalid,a._13(n,78).ngClassPending),l(n,107,0,a._13(n,110).ngClassUntouched,a._13(n,110).ngClassTouched,a._13(n,110).ngClassPristine,a._13(n,110).ngClassDirty,a._13(n,110).ngClassValid,a._13(n,110).ngClassInvalid,a._13(n,110).ngClassPending),l(n,137,0,a._13(n,140).ngClassUntouched,a._13(n,140).ngClassTouched,a._13(n,140).ngClassPristine,a._13(n,140).ngClassDirty,a._13(n,140).ngClassValid,a._13(n,140).ngClassInvalid,a._13(n,140).ngClassPending),l(n,167,0,a._13(n,170).ngClassUntouched,a._13(n,170).ngClassTouched,a._13(n,170).ngClassPristine,a._13(n,170).ngClassDirty,a._13(n,170).ngClassValid,a._13(n,170).ngClassInvalid,a._13(n,170).ngClassPending),l(n,197,0,a._13(n,200).ngClassUntouched,a._13(n,200).ngClassTouched,a._13(n,200).ngClassPristine,a._13(n,200).ngClassDirty,a._13(n,200).ngClassValid,a._13(n,200).ngClassInvalid,a._13(n,200).ngClassPending),l(n,227,0,a._13(n,230).ngClassUntouched,a._13(n,230).ngClassTouched,a._13(n,230).ngClassPristine,a._13(n,230).ngClassDirty,a._13(n,230).ngClassValid,a._13(n,230).ngClassInvalid,a._13(n,230).ngClassPending),l(n,257,0,a._13(n,260).ngClassUntouched,a._13(n,260).ngClassTouched,a._13(n,260).ngClassPristine,a._13(n,260).ngClassDirty,a._13(n,260).ngClassValid,a._13(n,260).ngClassInvalid,a._13(n,260).ngClassPending),l(n,288,0,a._13(n,291).ngClassUntouched,a._13(n,291).ngClassTouched,a._13(n,291).ngClassPristine,a._13(n,291).ngClassDirty,a._13(n,291).ngClassValid,a._13(n,291).ngClassInvalid,a._13(n,291).ngClassPending),l(n,319,0,a._13(n,322).ngClassUntouched,a._13(n,322).ngClassTouched,a._13(n,322).ngClassPristine,a._13(n,322).ngClassDirty,a._13(n,322).ngClassValid,a._13(n,322).ngClassInvalid,a._13(n,322).ngClassPending),l(n,350,0,a._13(n,353).ngClassUntouched,a._13(n,353).ngClassTouched,a._13(n,353).ngClassPristine,a._13(n,353).ngClassDirty,a._13(n,353).ngClassValid,a._13(n,353).ngClassInvalid,a._13(n,353).ngClassPending)})}var O=a.V("page-initial",F.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-initial",[],null,null,null,U,V)),a.Y(1,49152,null,0,F.a,[c.a,L.a,N.a,S.a,A.a],null,null)],null,null)},{},{},[])},572:function(l,n,u){"use strict";u.d(n,"a",function(){return sl});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(54),s=u(34),r=u(29),c=u(46),d=u(49),g=u(58),_=u(107),p=u(82),m=u(9),h=u(43),f=u(35),b=u(81),v=u(69),y=u(59),Y=u(5),C=u(15),Z=u(10),j=u(20),k=u(16),z=u(45),P=u(60),w=u(13),x=u(26),D=u(6),I=u(33),M=u(19),T=u(14),F=u(41),L=u(30),N=u(42),S=u(21),A=u(31),V=u(36),U=u(28),O=u(24),H=u(17),R=u(925),E=u(126),$=u(233),B=u(48),J=u(40),W=u(32),X=u(12),G=a.X({encapsulation:2,styles:[],data:{}});function q(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function K(l){return a._22(0,[(l()(),a.Z(0,0,null,null,26,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(1,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,8,{contentLabel:0}),a._18(603979776,9,{_buttons:1}),a._18(603979776,10,{_icons:1}),a.Y(5,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(7,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(8,16384,[[8,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["How frequently?"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(11,0,null,3,14,"ion-select",[],[[2,"select-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"],[null,"keyup.space"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,12)._click(u)&&t);"keyup.space"===n&&(t=!1!==a._13(l,12)._keyup()&&t);"ngModelChange"===n&&(t=!1!==(e.freq=u)&&t);return t},_.b,_.a)),a.Y(12,1228800,null,1,p.a,[m.a,r.a,i.a,a.j,a.z,[2,s.a],h.a],null,null),a._18(603979776,11,{options:1}),a._17(1024,null,f.e,function(l){return[l]},[p.a]),a.Y(15,671744,null,0,f.h,[[8,null],[8,null],[8,null],[2,f.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,f.f,null,[f.h]),a.Y(17,16384,null,0,f.g,[f.f],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(21,0,null,null,2,"ion-option",[["value","monthly"]],null,null,null,null,null)),a.Y(22,16384,[[11,4]],0,b.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(-1,null,["Monthly"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,2,["\n    "]))],function(l,n){l(n,15,0,n.component.freq);l(n,22,0,"monthly")},function(l,n){l(n,11,0,a._13(n,12)._disabled,a._13(n,17).ngClassUntouched,a._13(n,17).ngClassTouched,a._13(n,17).ngClassPristine,a._13(n,17).ngClassDirty,a._13(n,17).ngClassValid,a._13(n,17).ngClassInvalid,a._13(n,17).ngClassPending)})}function Q(l){return a._22(0,[(l()(),a.Z(0,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(1,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,12,{contentLabel:0}),a._18(603979776,13,{_buttons:1}),a._18(603979776,14,{_icons:1}),a.Y(5,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(7,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(8,16384,[[12,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["What Day of the Month?"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(11,0,null,3,4,"ion-input",[["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.deductDate=u)&&a);return a},v.b,v.a)),a.Y(12,671744,null,0,f.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,f.f,null,[f.h]),a.Y(14,16384,null,0,f.g,[f.f],null,null),a.Y(15,5423104,null,0,y.a,[i.a,Y.a,r.a,m.a,a.j,a.z,[2,C.a],[2,s.a],[2,f.f],Z.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n    "]))],function(l,n){l(n,12,0,n.component.deductDate);l(n,15,0,"date")},function(l,n){l(n,11,0,a._13(n,14).ngClassUntouched,a._13(n,14).ngClassTouched,a._13(n,14).ngClassPristine,a._13(n,14).ngClassDirty,a._13(n,14).ngClassValid,a._13(n,14).ngClassInvalid,a._13(n,14).ngClassPending)})}function ll(l){return a._22(0,[(l()(),a.Z(0,0,null,null,11,"ion-item",[["class","item item-block"],["no-lines",""]],null,null,null,o.b,o.a)),a.Y(1,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,15,{contentLabel:0}),a._18(603979776,16,{_buttons:1}),a._18(603979776,17,{_icons:1}),a.Y(5,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(8,0,null,2,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.goPay()&&a);return a},j.b,j.a)),a.Y(9,1097728,[[16,4]],0,k.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Buy"])),(l()(),a._20(-1,2,["\n    "]))],function(l,n){l(n,9,0,"")},null)}function nl(l){return a._22(0,[(l()(),a.Z(0,0,null,null,0,"hr",[],null,null,null,null,null))],null,null)}function ul(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"h5",[["text-center",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        My Investments\n    "]))],null,null)}function al(l){return a._22(0,[(l()(),a.Z(0,0,null,null,3,"i",[["padding",""],["text-left",""]],null,null,null,null,null)),(l()(),a.Z(1,0,null,null,1,"b",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Total:"])),(l()(),a._20(3,null,[" NGN",""]))],null,function(l,n){l(n,3,0,n.component.totInvest)})}function tl(l){return a._22(0,[(l()(),a.Z(0,0,null,null,2,"button",[["float-right",""],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.toggleMore()&&a);return a},j.b,j.a)),a.Y(1,1097728,null,0,k.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Show More"]))],function(l,n){l(n,1,0,"")},null)}function el(l){return a._22(0,[(l()(),a.Z(0,0,null,null,15,"ion-card",[],null,null,null,null,null)),a.Y(1,16384,null,0,z.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n           "])),(l()(),a.Z(3,0,null,null,11,"ion-card-content",[],null,null,null,null,null)),a.Y(4,16384,null,0,P.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n               "])),(l()(),a.Z(6,0,null,null,1,"h4",[],null,null,null,null,null)),(l()(),a._20(7,null,["",""])),(l()(),a._20(-1,null,["\n               "])),(l()(),a.Z(9,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(10,null,["NGN",""])),(l()(),a._20(-1,null,["\n               "])),(l()(),a.Z(12,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(13,null,["",""])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n        "]))],null,function(l,n){l(n,7,0,n.context.$implicit.inv_type),l(n,10,0,n.context.$implicit.actAmt),l(n,13,0,n.context.$implicit.inv_date)})}function il(l){return a._22(0,[(l()(),a.Z(0,0,null,null,4,"div",[],null,null,null,null,null)),(l()(),a._20(-1,null,["\n\n       "])),(l()(),a.U(16777216,null,null,1,null,el)),a.Y(3,802816,null,0,w.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n    "]))],function(l,n){l(n,3,0,n.component.investments)},null)}function ol(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,21,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,x.a,[i.a,a.j,a.z,[2,D.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(4,0,null,null,17,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,I.b,I.a)),a.Y(5,49152,null,0,M.a,[m.a,[2,D.a],[2,T.a],i.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},j.b,j.a)),a.Y(8,1097728,[[1,4]],0,k.a,[[8,""],i.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,F.a,[L.a,[2,D.a],[2,k.a],[2,M.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,N.a,[i.a,a.j,a.z,[2,S.a],[2,M.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n         "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,A.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n       "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(18,0,null,3,2,"ion-title",[],null,null,null,V.b,V.a)),a.Y(19,49152,null,0,U.a,[i.a,a.j,a.z,[2,S.a],[2,M.a]],null,null),(l()(),a._20(20,0,["","'s Gaid Investment"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n \n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(24,0,null,null,68,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,O.b,O.a)),a.Y(25,4374528,null,0,C.a,[i.a,Y.a,Z.a,a.j,a.z,m.a,H.a,a.u,[2,D.a],[2,T.a]],null,null),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.U(16777216,null,1,1,null,q)),a.Y(28,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n\n    "])),(l()(),a.Z(30,0,null,1,16,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(31,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(35,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(37,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(38,16384,[[2,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["How much would you like to invest?"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(41,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.amount=u)&&a);return a},v.b,v.a)),a.Y(42,671744,null,0,f.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,f.f,null,[f.h]),a.Y(44,16384,null,0,f.g,[f.f],null,null),a.Y(45,5423104,null,0,y.a,[i.a,Y.a,r.a,m.a,a.j,a.z,[2,C.a],[2,s.a],[2,f.f],Z.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n    "])),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.Z(48,0,null,1,17,"ion-item",[["class","item item-block"],["no-lines",""]],null,null,null,o.b,o.a)),a.Y(49,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,5,{contentLabel:0}),a._18(603979776,6,{_buttons:1}),a._18(603979776,7,{_icons:1}),a.Y(53,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(55,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(56,16384,[[5,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Recurring?"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(59,0,null,0,5,"ion-checkbox",[],[[2,"checkbox-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,60)._click(u)&&t);"ngModelChange"===n&&(t=!1!==(e.recur=u)&&t);return t},R.b,R.a)),a.Y(60,1228800,null,0,E.a,[i.a,r.a,[2,s.a],a.j,a.z],null,null),a._17(1024,null,f.e,function(l){return[l]},[E.a]),a.Y(62,671744,null,0,f.h,[[8,null],[8,null],[8,null],[2,f.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,f.f,null,[f.h]),a.Y(64,16384,null,0,f.g,[f.f],null,null),(l()(),a._20(-1,2,["\n    "])),(l()(),a._20(-1,1,["\n   "])),(l()(),a.Z(67,0,null,1,0,"hr",[],null,null,null,null,null)),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,K)),a.Y(70,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.U(16777216,null,1,1,null,Q)),a.Y(73,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n  \n    "])),(l()(),a.U(16777216,null,1,1,null,ll)),a.Y(76,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n  \n\n\n\n    "])),(l()(),a.U(16777216,null,1,1,null,nl)),a.Y(79,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,ul)),a.Y(82,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,al)),a.Y(85,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,tl)),a.Y(88,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n   "])),(l()(),a.U(16777216,null,1,1,null,il)),a.Y(91,16384,null,0,w.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n  "])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"tertiary");l(n,9,0,"");l(n,14,0,"menu"),l(n,28,0,0==u.loaded),l(n,42,0,u.amount);l(n,45,0,"number"),l(n,62,0,u.recur),l(n,70,0,1==u.recur),l(n,73,0,"monthly"==u.freq&&1==u.recur),l(n,76,0,u.amount),l(n,79,0,u.investments),l(n,82,0,u.investments),l(n,85,0,u.investments),l(n,88,0,u.investments),l(n,91,0,1==u.more)},function(l,n){var u=n.component;l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,20,0,u.userName),l(n,24,0,a._13(n,25).statusbarPadding,a._13(n,25)._hasRefresher),l(n,41,0,a._13(n,44).ngClassUntouched,a._13(n,44).ngClassTouched,a._13(n,44).ngClassPristine,a._13(n,44).ngClassDirty,a._13(n,44).ngClassValid,a._13(n,44).ngClassInvalid,a._13(n,44).ngClassPending),l(n,59,0,a._13(n,60)._disabled,a._13(n,64).ngClassUntouched,a._13(n,64).ngClassTouched,a._13(n,64).ngClassPristine,a._13(n,64).ngClassDirty,a._13(n,64).ngClassValid,a._13(n,64).ngClassInvalid,a._13(n,64).ngClassPending)})}var sl=a.V("page-investment",$.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-investment",[],null,null,null,ol,G)),a.Y(1,49152,null,0,$.a,[T.a,B.a,J.a,W.a,X.a],null,null)],null,null)},{},{},[])},573:function(l,n,u){"use strict";u.d(n,"a",function(){return S});var a=u(0),t=u(67),e=u(45),i=u(3),o=u(60),s=u(125),r=u(26),c=u(6),d=u(33),g=u(19),_=u(9),p=u(14),m=u(20),h=u(16),f=u(41),b=u(30),v=u(42),y=u(21),Y=u(31),C=u(36),Z=u(28),j=u(24),k=u(15),z=u(5),P=u(10),w=u(17),x=u(84),D=u(64),I=u(13),M=u(235),T=u(12),F=a.X({encapsulation:2,styles:[],data:{}});function L(l){return a._22(0,[(l()(),a.Z(0,0,null,null,19,"ion-col",[["align-self-center",""],["class","col"],["col-6",""],["no-padding",""]],null,null,null,null,null)),a.Y(1,16384,null,0,t.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(3,0,null,null,15,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.goToSubMenu(l.context.$implicit)&&a);return a},null,null)),a.Y(4,16384,null,0,e.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(6,0,null,null,0,"img",[],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(8,0,null,null,9,"ion-card-content",[],null,null,null,null,null)),a.Y(9,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(11,0,null,null,2,"ion-card-title",[],null,null,null,null,null)),a.Y(12,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(13,null,["\n                            ","\n                        "])),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(15,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(16,null,[""," "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,null,["\n            "]))],null,function(l,n){l(n,6,0,a._2(1,"",n.context.$implicit.img,"")),l(n,13,0,n.context.$implicit.title),l(n,16,0,n.context.$implicit.description)})}function N(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(1,16384,null,0,r.a,[i.a,a.j,a.z,[2,c.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(3,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,d.b,d.a)),a.Y(4,49152,null,0,g.a,[_.a,[2,c.a],[2,p.a],i.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,8).toggle()&&t);return t},m.b,m.a)),a.Y(7,1097728,[[1,4]],0,h.a,[[8,""],i.a,a.j,a.z],null,null),a.Y(8,1064960,null,0,f.a,[b.a,[2,c.a],[2,h.a],[2,g.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(9,16384,null,1,v.a,[i.a,a.j,a.z,[2,y.a],[2,g.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n  "])),(l()(),a.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(13,147456,null,0,Y.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n"])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(16,0,null,3,2,"ion-title",[],null,null,null,C.b,C.a)),a.Y(17,49152,null,0,Z.a,[i.a,a.j,a.z,[2,y.a],[2,g.a]],null,null),(l()(),a._20(-1,0,["Menu"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(22,0,null,null,13,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,j.b,j.a)),a.Y(23,4374528,null,0,k.a,[i.a,z.a,P.a,a.j,a.z,_.a,w.a,a.u,[2,c.a],[2,p.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(25,0,null,1,9,"ion-grid",[["class","grid"],["no-padding",""]],null,null,null,null,null)),a.Y(26,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(28,0,null,null,5,"ion-row",[["class","row"],["justify-content-center",""],["no-padding",""]],null,null,null,null,null)),a.Y(29,16384,null,0,D.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.U(16777216,null,null,1,null,L)),a.Y(32,802816,null,0,I.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,4,0,"tertiary");l(n,8,0,"");l(n,13,0,"menu"),l(n,32,0,u.categories)},function(l,n){l(n,3,0,a._13(n,4)._hidden,a._13(n,4)._sbPadding),l(n,6,0,a._13(n,8).isHidden),l(n,12,0,a._13(n,13)._hidden),l(n,22,0,a._13(n,23).statusbarPadding,a._13(n,23)._hasRefresher)})}var S=a.V("page-menu-one",M.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-menu-one",[],null,null,null,N,F)),a.Y(1,49152,null,0,M.a,[p.a,T.a],null,null)],null,null)},{},{},[])},574:function(l,n,u){"use strict";u.d(n,"a",function(){return M});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(237),w=u(12),x=a.X({encapsulation:2,styles:[],data:{}});function D(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.goToSubMenu(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n    "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(1,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(3,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(4,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,8).toggle()&&t);return t},g.b,g.a)),a.Y(7,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(8,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(9,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(13,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(16,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(17,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,["Menu"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(22,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(23,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,D)),a.Y(26,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,4,0,"tertiary");l(n,8,0,"");l(n,13,0,"menu"),l(n,26,0,u.categories)},function(l,n){l(n,3,0,a._13(n,4)._hidden,a._13(n,4)._sbPadding),l(n,6,0,a._13(n,8).isHidden),l(n,12,0,a._13(n,13)._hidden),l(n,22,0,a._13(n,23).statusbarPadding,a._13(n,23)._hasRefresher)})}var M=a.V("page-menu-two",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-menu-two",[],null,null,null,I,x)),a.Y(1,49152,null,0,P.a,[d.a,w.a],null,null)],null,null)},{},{},[])},575:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(45),s=u(60),r=u(26),c=u(6),d=u(33),g=u(19),_=u(9),p=u(14),m=u(36),h=u(28),f=u(21),b=u(24),v=u(15),y=u(5),Y=u(10),C=u(17),Z=u(13),j=u(239),k=u(48),z=u(40),P=u(32),w=u(12),x=a.X({encapsulation:2,styles:[],data:{}});function D(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-card",[],null,null,null,null,null)),a.Y(1,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,16,"ion-card-content",[],null,null,null,null,null)),a.Y(4,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(6,0,null,null,0,"img",[["alt",""]],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(8,0,null,null,1,"h4",[],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(11,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(12,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(14,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(15,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(17,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(18,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "]))],null,function(l,n){l(n,6,0,a._2(1,"",n.context.$implicit.logo,"")),l(n,9,0,n.context.$implicit.card_banks),l(n,12,0,n.context.$implicit.ATM_withdrawal_abroad),l(n,15,0,n.context.$implicit.local_POS_withdrawals_limit),l(n,18,0,n.context.$implicit.local_ATM_withdrawals_limit)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,10,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,r.a,[i.a,a.j,a.z,[2,c.a]],null,null),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a.Z(4,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,d.b,d.a)),a.Y(5,49152,null,0,g.a,[_.a,[2,c.a],[2,p.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n    "])),(l()(),a.Z(7,0,null,3,2,"ion-title",[],null,null,null,m.b,m.a)),a.Y(8,49152,null,0,h.a,[i.a,a.j,a.z,[2,f.a],[2,g.a]],null,null),(l()(),a._20(-1,0,["Naira Debit Card"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(13,0,null,null,8,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,b.b,b.a)),a.Y(14,4374528,null,0,v.a,[i.a,y.a,Y.a,a.j,a.z,_.a,C.a,a.u,[2,c.a],[2,p.a]],null,null),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.U(16777216,null,1,1,null,D)),a.Y(17,16384,null,0,Z.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n\n    "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(20,802816,null,0,Z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,17,0,0==u.loaded),l(n,20,0,u.nairaCrd)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,13,0,a._13(n,14).statusbarPadding,a._13(n,14)._hasRefresher)})}var T=a.V("page-naira-card",j.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-naira-card",[],null,null,null,M,x)),a.Y(1,49152,null,0,j.a,[p.a,k.a,z.a,P.a,w.a],null,null)],null,null)},{},{},[])},576:function(l,n,u){"use strict";u.d(n,"a",function(){return G});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(54),s=u(34),r=u(29),c=u(46),d=u(49),g=u(58),_=u(69),p=u(35),m=u(59),h=u(5),f=u(9),b=u(15),v=u(10),y=u(20),Y=u(16),C=u(26),Z=u(6),j=u(33),k=u(19),z=u(14),P=u(36),w=u(28),x=u(21),D=u(24),I=u(17),M=u(13),T=u(45),F=u(60),L=u(31),N=u(107),S=u(82),A=u(43),V=u(81),U=u(241),O=u(40),H=u(48),R=u(12),E=u(32),$=a.X({encapsulation:2,styles:[],data:{}});function B(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function J(l){return a._22(0,[(l()(),a.Z(0,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(1,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,17,{contentLabel:0}),a._18(603979776,18,{_buttons:1}),a._18(603979776,19,{_icons:1}),a.Y(5,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(7,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(8,16384,[[17,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Debt Repayment(%)"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(11,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.debt=u)&&a);return a},_.b,_.a)),a.Y(12,671744,null,0,p.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,p.f,null,[p.h]),a.Y(14,16384,null,0,p.g,[p.f],null,null),a.Y(15,5423104,null,0,m.a,[i.a,h.a,r.a,f.a,a.j,a.z,[2,b.a],[2,s.a],[2,p.f],v.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "]))],function(l,n){l(n,12,0,n.component.debt);l(n,15,0,"number")},function(l,n){l(n,11,0,a._13(n,14).ngClassUntouched,a._13(n,14).ngClassTouched,a._13(n,14).ngClassPristine,a._13(n,14).ngClassDirty,a._13(n,14).ngClassValid,a._13(n,14).ngClassInvalid,a._13(n,14).ngClassPending)})}function W(l){return a._22(0,[(l()(),a.Z(0,0,null,null,11,"ion-item",[["class","item item-block"],["no-lines",""]],null,null,null,o.b,o.a)),a.Y(1,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,20,{contentLabel:0}),a._18(603979776,21,{_buttons:1}),a._18(603979776,22,{_icons:1}),a.Y(5,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(8,0,null,2,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.saveBudget()&&a);return a},y.b,y.a)),a.Y(9,1097728,[[21,4]],0,Y.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Save"])),(l()(),a._20(-1,2,["\n    "]))],function(l,n){l(n,9,0,"")},null)}function X(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,16,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,C.a,[i.a,a.j,a.z,[2,Z.a]],null,null),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a.Z(4,0,null,null,12,"div",[["class","logo-container"],["text-center",""]],[[4,"display",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(6,0,null,null,0,"img",[["alt","Logo"],["src","assets/imgs/logo.png"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n\n      "])),(l()(),a._20(-1,null,["\n      \n      "])),(l()(),a.Z(9,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,j.b,j.a)),a.Y(10,49152,null,0,k.a,[f.a,[2,Z.a],[2,z.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(12,0,null,3,2,"ion-title",[],null,null,null,P.b,P.a)),a.Y(13,49152,null,0,w.a,[i.a,a.j,a.z,[2,x.a],[2,k.a]],null,null),(l()(),a._20(14,0,["","'s Budget"])),(l()(),a._20(-1,3,["\n        \n      "])),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a._20(-1,null,["\n\n  \n\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(19,0,null,null,143,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,D.b,D.a)),a.Y(20,4374528,null,0,b.a,[i.a,h.a,v.a,a.j,a.z,f.a,I.a,a.u,[2,Z.a],[2,z.a]],null,null),(l()(),a._20(-1,1,["\n\n"])),(l()(),a.U(16777216,null,1,1,null,B)),a.Y(23,16384,null,0,M.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n  "])),(l()(),a._20(-1,1,["\n  "])),(l()(),a.Z(26,0,null,1,59,"ion-card",[],null,null,null,null,null)),a.Y(27,16384,null,0,T.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(29,0,null,null,55,"ion-card-content",[],null,null,null,null,null)),a.Y(30,16384,null,0,F.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a.Z(32,0,null,null,4,"button",[["color","secondary"],["float-right",""],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.help()&&a);return a},y.b,y.a)),a.Y(33,1097728,null,0,Y.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a.Z(34,0,null,0,2,"strong",[],null,null,null,null,null)),(l()(),a.Z(35,0,null,null,1,"ion-icon",[["color","danger"],["name","help"],["role","img"],["style","font-size: 45px"]],[[2,"hide",null]],null,null,null,null)),a.Y(36,147456,null,0,L.a,[i.a,a.j,a.z],{color:[0,"color"],name:[1,"name"]},null),(l()(),a._20(-1,null,["\n\n  \n  "])),(l()(),a.Z(38,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(39,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,1,{contentLabel:0}),a._18(603979776,2,{_buttons:1}),a._18(603979776,3,{_icons:1}),a.Y(43,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(45,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(46,16384,[[1,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Projected Monthly Income(NGN)"])),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(49,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.income=u)&&a);return a},_.b,_.a)),a.Y(50,671744,null,0,p.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,p.f,null,[p.h]),a.Y(52,16384,null,0,p.g,[p.f],null,null),a.Y(53,5423104,null,0,m.a,[i.a,h.a,r.a,f.a,a.j,a.z,[2,b.a],[2,s.a],[2,p.f],v.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n    "])),(l()(),a._20(-1,null,["\n  \n  "])),(l()(),a.Z(56,0,null,null,27,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(57,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,4,{contentLabel:0}),a._18(603979776,5,{_buttons:1}),a._18(603979776,6,{_icons:1}),a.Y(61,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(63,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(64,16384,[[4,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Are You Servicing Debts ?"])),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(67,0,null,3,15,"ion-select",[],[[2,"select-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"],[null,"keyup.space"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,68)._click(u)&&t);"keyup.space"===n&&(t=!1!==a._13(l,68)._keyup()&&t);"ngModelChange"===n&&(t=!1!==(e.type=u)&&t);return t},N.b,N.a)),a.Y(68,1228800,null,1,S.a,[f.a,r.a,i.a,a.j,a.z,[2,s.a],A.a],null,null),a._18(603979776,7,{options:1}),a._17(1024,null,p.e,function(l){return[l]},[S.a]),a.Y(71,671744,null,0,p.h,[[8,null],[8,null],[8,null],[2,p.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,p.f,null,[p.h]),a.Y(73,16384,null,0,p.g,[p.f],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(75,0,null,null,2,"ion-option",[["value","debtor"]],null,null,null,null,null)),a.Y(76,16384,[[7,4]],0,V.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(-1,null,["Yes"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(79,0,null,null,2,"ion-option",[["value","ndebtor"]],null,null,null,null,null)),a.Y(80,16384,[[7,4]],0,V.a,[a.j],{value:[0,"value"]},null),(l()(),a._20(-1,null,["No"])),(l()(),a._20(-1,null,["\n      "])),(l()(),a._20(-1,2,["\n    "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(87,0,null,1,0,"br",[],null,null,null,null,null)),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(89,0,null,1,0,"br",[],null,null,null,null,null)),(l()(),a._20(-1,1,["\n  \n    "])),(l()(),a.Z(91,0,null,1,70,"ion-card",[],null,null,null,null,null)),a.Y(92,16384,null,0,T.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(94,0,null,null,66,"ion-card-content",[],null,null,null,null,null)),a.Y(95,16384,null,0,F.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n    \n  "])),(l()(),a.Z(97,0,null,null,1,"i",[],null,null,null,null,null)),(l()(),a._20(-1,null,[" Assign Pecentages of Your Income to Each Allocation "])),(l()(),a._20(-1,null,["\n \n   \n    "])),(l()(),a.Z(100,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(101,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,8,{contentLabel:0}),a._18(603979776,9,{_buttons:1}),a._18(603979776,10,{_icons:1}),a.Y(105,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(107,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(108,16384,[[8,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Living Expenses(%)"])),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(111,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.expenses=u)&&a);return a},_.b,_.a)),a.Y(112,671744,null,0,p.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,p.f,null,[p.h]),a.Y(114,16384,null,0,p.g,[p.f],null,null),a.Y(115,5423104,null,0,m.a,[i.a,h.a,r.a,f.a,a.j,a.z,[2,b.a],[2,s.a],[2,p.f],v.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(119,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(120,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,11,{contentLabel:0}),a._18(603979776,12,{_buttons:1}),a._18(603979776,13,{_icons:1}),a.Y(124,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(126,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(127,16384,[[11,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Savings & Investments(%)"])),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(130,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.savInv=u)&&a);return a},_.b,_.a)),a.Y(131,671744,null,0,p.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,p.f,null,[p.h]),a.Y(133,16384,null,0,p.g,[p.f],null,null),a.Y(134,5423104,null,0,m.a,[i.a,h.a,r.a,f.a,a.j,a.z,[2,b.a],[2,s.a],[2,p.f],v.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(137,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(138,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,14,{contentLabel:0}),a._18(603979776,15,{_buttons:1}),a._18(603979776,16,{_icons:1}),a.Y(142,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n      "])),(l()(),a.Z(144,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(145,16384,[[14,4]],0,g.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["Projects(%)"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(148,0,null,3,4,"ion-input",[["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.proj=u)&&a);return a},_.b,_.a)),a.Y(149,671744,null,0,p.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,p.f,null,[p.h]),a.Y(151,16384,null,0,p.g,[p.f],null,null),a.Y(152,5423104,null,0,m.a,[i.a,h.a,r.a,f.a,a.j,a.z,[2,b.a],[2,s.a],[2,p.f],v.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n      "])),(l()(),a._20(-1,null,["\n    \n      "])),(l()(),a.U(16777216,null,null,1,null,J)),a.Y(156,16384,null,0,M.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.U(16777216,null,null,1,null,W)),a.Y(159,16384,null,0,M.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,null,["\n  "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,1,["\n\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,23,0,0==u.loaded);l(n,33,0,"secondary","");l(n,36,0,"danger","help"),l(n,50,0,u.income);l(n,53,0,"number"),l(n,71,0,u.type);l(n,76,0,"debtor");l(n,80,0,"ndebtor"),l(n,112,0,u.expenses);l(n,115,0,"number"),l(n,131,0,u.savInv);l(n,134,0,"number"),l(n,149,0,u.proj);l(n,152,0,"number"),l(n,156,0,"debtor"==u.type),l(n,159,0,u.id>0)},function(l,n){var u=n.component;l(n,4,0,"block"),l(n,9,0,a._13(n,10)._hidden,a._13(n,10)._sbPadding),l(n,14,0,u.userName),l(n,19,0,a._13(n,20).statusbarPadding,a._13(n,20)._hasRefresher),l(n,35,0,a._13(n,36)._hidden),l(n,49,0,a._13(n,52).ngClassUntouched,a._13(n,52).ngClassTouched,a._13(n,52).ngClassPristine,a._13(n,52).ngClassDirty,a._13(n,52).ngClassValid,a._13(n,52).ngClassInvalid,a._13(n,52).ngClassPending),l(n,67,0,a._13(n,68)._disabled,a._13(n,73).ngClassUntouched,a._13(n,73).ngClassTouched,a._13(n,73).ngClassPristine,a._13(n,73).ngClassDirty,a._13(n,73).ngClassValid,a._13(n,73).ngClassInvalid,a._13(n,73).ngClassPending),l(n,111,0,a._13(n,114).ngClassUntouched,a._13(n,114).ngClassTouched,a._13(n,114).ngClassPristine,a._13(n,114).ngClassDirty,a._13(n,114).ngClassValid,a._13(n,114).ngClassInvalid,a._13(n,114).ngClassPending),l(n,130,0,a._13(n,133).ngClassUntouched,a._13(n,133).ngClassTouched,a._13(n,133).ngClassPristine,a._13(n,133).ngClassDirty,a._13(n,133).ngClassValid,a._13(n,133).ngClassInvalid,a._13(n,133).ngClassPending),l(n,148,0,a._13(n,151).ngClassUntouched,a._13(n,151).ngClassTouched,a._13(n,151).ngClassPristine,a._13(n,151).ngClassDirty,a._13(n,151).ngClassValid,a._13(n,151).ngClassInvalid,a._13(n,151).ngClassPending)})}var G=a.V("page-managebudget",U.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-managebudget",[],null,null,null,X,$)),a.Y(1,49152,null,0,U.a,[z.a,O.a,H.a,R.a,E.a],null,null)],null,null)},{},{},[])},577:function(l,n,u){"use strict";u.d(n,"a",function(){return E});var a=u(0),t=u(13),e=u(26),i=u(3),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(64),P=u(67),w=u(58),x=u(54),D=u(34),I=u(29),M=u(46),T=u(49),F=u(107),L=u(82),N=u(43),S=u(35),A=u(81),V=u(243),U=u(12),O=u(70),H=a.X({encapsulation:2,styles:[],data:{}});function R(l){return a._22(0,[a._14(0,t.c,[a.r]),(l()(),a.Z(1,0,null,null,27,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,e.a,[i.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(4,0,null,null,23,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(5,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],i.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},g.b,g.a)),a.Y(8,1097728,[[1,4]],0,_.a,[[8,""],i.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,h.a,[i.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,b.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(18,49152,null,0,y.a,[i.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,["Chocolate Muffin"])),(l()(),a._20(-1,3,["\n\n        "])),(l()(),a.Z(21,0,null,3,5,"button",[["class","right-header-icon"],["clear",""],["icon-only",""],["ion-button",""]],null,[[null,"click"]],function(l,n,u){var a=!0,t=l.component;"click"===n&&(a=!1!=(t.order.item.isliked=!t.order.item.isliked)&&a);return a},g.b,g.a)),a.Y(22,1097728,null,0,_.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],clear:[1,"clear"]},null),(l()(),a._20(-1,0,["\n            "])),(l()(),a.Z(24,0,null,0,1,"ion-icon",[["name","heart"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(25,147456,null,0,b.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(30,0,null,null,94,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(31,4374528,null,0,C.a,[i.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(33,0,null,1,8,"ion-row",[["class","row"]],null,null,null,null,null)),a.Y(34,16384,null,0,z.a,[],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(36,0,null,null,4,"ion-col",[["class","col"],["padding",""],["text-center",""]],null,null,null,null,null)),a.Y(37,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(39,0,null,null,0,"img",[["alt",""]],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(43,0,null,1,9,"ion-row",[["class","row"]],null,null,null,null,null)),a.Y(44,16384,null,0,z.a,[],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(46,0,null,null,5,"ion-col",[["class","col"],["padding-horizontal",""]],null,null,null,null,null)),a.Y(47,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(49,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(50,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(54,0,null,1,28,"ion-row",[["class","row"],["text-center",""]],null,null,null,null,null)),a.Y(55,16384,null,0,z.a,[],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(57,0,null,null,12,"ion-col",[["class","col"]],null,null,null,null,null)),a.Y(58,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(60,0,null,null,2,"ion-label",[],null,null,null,null,null)),a.Y(61,16384,null,0,w.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["\n                Quantity\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(64,0,null,null,4,"h2",[],null,[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,96).open()&&t);return t},null,null)),(l()(),a._20(65,null,["\n                ","\n                "])),(l()(),a.Z(66,0,null,null,1,"ion-icon",[["name","ios-arrow-down"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(67,147456,null,0,b.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(71,0,null,null,10,"ion-col",[["class","total col"]],null,null,null,null,null)),a.Y(72,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(74,0,null,null,2,"ion-label",[],null,null,null,null,null)),a.Y(75,16384,null,0,w.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,["\n                Total\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(78,0,null,null,2,"h2",[],null,null,null,null,null)),(l()(),a._20(79,null,["\n                ","\n            "])),a._16(80,4),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(84,0,null,1,2,"button",[["color","primary"],["full",""],["ion-button",""],["no-margin",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.placeOrder()&&a);return a},g.b,g.a)),a.Y(85,1097728,null,0,_.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],full:[1,"full"]},null),(l()(),a._20(-1,0,[" Place The Order"])),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.Z(88,0,null,1,35,"ion-item",[["class","item item-block"],["style","display:none;"]],null,null,null,x.b,x.a)),a.Y(89,1097728,null,3,D.a,[I.a,i.a,a.j,a.z,[2,M.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(93,16384,null,0,T.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(95,0,null,3,27,"ion-select",[],[[2,"select-disabled",null],[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"],[null,"click"],[null,"keyup.space"]],function(l,n,u){var t=!0,e=l.component;"click"===n&&(t=!1!==a._13(l,96)._click(u)&&t);"keyup.space"===n&&(t=!1!==a._13(l,96)._keyup()&&t);"ngModelChange"===n&&(t=!1!==(e.order.quantity=u)&&t);return t},F.b,F.a)),a.Y(96,1228800,[["ionSelect",4]],1,L.a,[c.a,I.a,i.a,a.j,a.z,[2,D.a],N.a],null,null),a._18(603979776,5,{options:1}),a._17(1024,null,S.e,function(l){return[l]},[L.a]),a.Y(99,671744,null,0,S.h,[[8,null],[8,null],[8,null],[2,S.e]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(101,16384,null,0,S.g,[S.f],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(103,0,null,null,2,"ion-option",[],null,null,null,null,null)),a.Y(104,16384,[[5,4]],0,A.a,[a.j],null,null),(l()(),a._20(-1,null,["1"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(107,0,null,null,2,"ion-option",[],null,null,null,null,null)),a.Y(108,16384,[[5,4]],0,A.a,[a.j],null,null),(l()(),a._20(-1,null,["2"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(111,0,null,null,2,"ion-option",[],null,null,null,null,null)),a.Y(112,16384,[[5,4]],0,A.a,[a.j],null,null),(l()(),a._20(-1,null,["3"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(115,0,null,null,2,"ion-option",[],null,null,null,null,null)),a.Y(116,16384,[[5,4]],0,A.a,[a.j],null,null),(l()(),a._20(-1,null,["4"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(119,0,null,null,2,"ion-option",[],null,null,null,null,null)),a.Y(120,16384,[[5,4]],0,A.a,[a.j],null,null),(l()(),a._20(-1,null,["5"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,2,["\n    "])),(l()(),a._20(-1,1,["\n\n"]))],function(l,n){var u=n.component;l(n,5,0,"secondary");l(n,9,0,"");l(n,14,0,"menu");l(n,22,0,u.order.item.isliked?"danger":"light","");l(n,25,0,"heart");l(n,67,0,"ios-arrow-down");l(n,85,0,"primary",""),l(n,99,0,u.order.quantity)},function(l,n){var u=n.component;l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,24,0,a._13(n,25)._hidden),l(n,30,0,a._13(n,31).statusbarPadding,a._13(n,31)._hasRefresher),l(n,39,0,a._2(1,"",u.order.item.img,"")),l(n,50,0,u.order.item.description),l(n,65,0,u.order.quantity),l(n,66,0,a._13(n,67)._hidden),l(n,79,0,a._21(n,79,0,l(n,80,0,a._13(n,0),u.order.quantity*u.order.item.price,"USD",!0,"1.0-2"))),l(n,95,0,a._13(n,96)._disabled,a._13(n,101).ngClassUntouched,a._13(n,101).ngClassTouched,a._13(n,101).ngClassPristine,a._13(n,101).ngClassDirty,a._13(n,101).ngClassValid,a._13(n,101).ngClassInvalid,a._13(n,101).ngClassPending)})}var E=a.V("page-order-details",V.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-order-details",[],null,null,null,R,H)),a.Y(1,49152,null,0,V.a,[d.a,U.a,O.a],null,null)],null,null)},{},{},[])},578:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(45),s=u(60),r=u(26),c=u(6),d=u(33),g=u(19),_=u(9),p=u(14),m=u(36),h=u(28),f=u(21),b=u(24),v=u(15),y=u(5),Y=u(10),C=u(17),Z=u(13),j=u(245),k=u(48),z=u(40),P=u(32),w=u(12),x=a.X({encapsulation:2,styles:[],data:{}});function D(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-card",[],null,null,null,null,null)),a.Y(1,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,16,"ion-card-content",[],null,null,null,null,null)),a.Y(4,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(6,0,null,null,0,"img",[["alt",""]],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(8,0,null,null,1,"h4",[],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(11,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(12,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(14,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(15,null,["",""])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(17,0,null,null,1,"p",[["text-right",""]],null,null,null,null,null)),(l()(),a._20(18,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "]))],null,function(l,n){l(n,6,0,a._2(1,"",n.context.$implicit.logo,"")),l(n,9,0,n.context.$implicit.card_banks),l(n,12,0,n.context.$implicit.ATM_withdrawal_abroad),l(n,15,0,n.context.$implicit.local_POS_withdrawals_limit),l(n,18,0,n.context.$implicit.local_ATM_withdrawals_limit)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,10,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,r.a,[i.a,a.j,a.z,[2,c.a]],null,null),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a.Z(4,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,d.b,d.a)),a.Y(5,49152,null,0,g.a,[_.a,[2,c.a],[2,p.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n    "])),(l()(),a.Z(7,0,null,3,2,"ion-title",[],null,null,null,m.b,m.a)),a.Y(8,49152,null,0,h.a,[i.a,a.j,a.z,[2,f.a],[2,g.a]],null,null),(l()(),a._20(-1,0,["Prepaid Card"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(13,0,null,null,8,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,b.b,b.a)),a.Y(14,4374528,null,0,v.a,[i.a,y.a,Y.a,a.j,a.z,_.a,C.a,a.u,[2,c.a],[2,p.a]],null,null),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.U(16777216,null,1,1,null,D)),a.Y(17,16384,null,0,Z.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n\n    "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(20,802816,null,0,Z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,17,0,0==u.loaded),l(n,20,0,u.prepCrd)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,13,0,a._13(n,14).statusbarPadding,a._13(n,14)._hasRefresher)})}var T=a.V("page-prepaid-card",j.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-prepaid-card",[],null,null,null,M,x)),a.Y(1,49152,null,0,j.a,[p.a,k.a,z.a,P.a,w.a],null,null)],null,null)},{},{},[])},579:function(l,n,u){"use strict";u.d(n,"a",function(){return L});var a=u(0),t=u(26),e=u(3),i=u(6),o=u(33),s=u(19),r=u(9),c=u(14),d=u(20),g=u(16),_=u(41),p=u(30),m=u(42),h=u(21),f=u(31),b=u(36),v=u(28),y=u(926),Y=u(91),C=u(24),Z=u(15),j=u(5),k=u(10),z=u(17),P=u(84),w=u(64),x=u(67),D=u(247),I=u(12),M=u(70),T=a.X({encapsulation:2,styles:[],data:{}});function F(l){return a._22(0,[(l()(),a.Z(0,0,null,null,36,"ion-header",[],null,null,null,null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z,[2,i.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(3,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,o.b,o.a)),a.Y(4,49152,null,0,s.a,[r.a,[2,i.a],[2,c.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,8).toggle()&&t);return t},d.b,d.a)),a.Y(7,1097728,[[1,4]],0,g.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(8,1064960,null,0,_.a,[p.a,[2,i.a],[2,g.a],[2,s.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(9,16384,null,1,m.a,[e.a,a.j,a.z,[2,h.a],[2,s.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(13,147456,null,0,f.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(16,0,null,3,2,"ion-title",[],null,null,null,b.b,b.a)),a.Y(17,49152,null,0,v.a,[e.a,a.j,a.z,[2,h.a],[2,s.a]],null,null),(l()(),a._20(-1,0,["Profile"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(21,0,null,null,14,"div",[["class","profile-name"],["icon-end",""],["text-start",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(23,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Alexander Karev"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(26,0,null,null,8,"p",[["icon-end",""]],null,null,null,null,null)),(l()(),a._20(-1,null,[" UI Designer & Coffee Lover\n\n            "])),(l()(),a.Z(28,0,null,null,5,"button",[["color","secondary"],["float-right",""],["icon-only",""],["ion-fab",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.changeImage()&&a);return a},y.b,y.a)),a.Y(29,49152,null,0,Y.a,[e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,0,["\n              "])),(l()(),a.Z(31,0,null,0,1,"ion-icon",[["name","camera"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(32,147456,null,0,f.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n           "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(38,0,null,null,57,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,C.b,C.a)),a.Y(39,4374528,null,0,Z.a,[e.a,j.a,k.a,a.j,a.z,r.a,z.a,a.u,[2,i.a],[2,c.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(41,0,null,1,1,"article",[["padding-vertical",""],["text-center",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        Born in Singapore, received my design training in London and New York, and am always striving to create work that combines strategic design with compelling visuals and an attention to detail.\n    "])),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.Z(44,0,null,1,46,"ion-grid",[["class","grid"]],null,null,null,null,null)),a.Y(45,16384,null,0,P.a,[],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(47,0,null,null,42,"ion-row",[["class","row"],["text-center",""]],null,null,null,null,null)),a.Y(48,16384,null,0,w.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(50,0,null,null,8,"ion-col",[["class","col"],["col3",""]],null,null,null,null,null)),a.Y(51,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(53,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(-1,null,["0"])),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(56,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Orders"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(60,0,null,null,8,"ion-col",[["class","col"],["col3",""]],null,null,null,null,null)),a.Y(61,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(63,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(-1,null,["30"])),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(66,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Likes"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(70,0,null,null,8,"ion-col",[["class","col"],["col3",""]],null,null,null,null,null)),a.Y(71,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(73,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(-1,null,["25"])),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(76,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Follower"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(80,0,null,null,8,"ion-col",[["class","col"],["col3",""]],null,null,null,null,null)),a.Y(81,16384,null,0,x.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(83,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(-1,null,["60"])),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(86,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Following"])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.Z(92,0,null,1,2,"button",[["color","primary"],["full",""],["ion-button",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.follow()&&a);return a},d.b,d.a)),a.Y(93,1097728,null,0,g.a,[[8,""],e.a,a.j,a.z],{color:[0,"color"],full:[1,"full"]},null),(l()(),a._20(-1,0,["\n    Follow\n  "])),(l()(),a._20(-1,1,["\n"]))],function(l,n){l(n,4,0,"tertiary");l(n,8,0,"");l(n,13,0,"menu");l(n,29,0,"secondary");l(n,32,0,"camera");l(n,93,0,"primary","")},function(l,n){l(n,3,0,a._13(n,4)._hidden,a._13(n,4)._sbPadding),l(n,6,0,a._13(n,8).isHidden),l(n,12,0,a._13(n,13)._hidden),l(n,31,0,a._13(n,32)._hidden),l(n,38,0,a._13(n,39).statusbarPadding,a._13(n,39)._hasRefresher)})}var L=a.V("page-profile",D.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-profile",[],null,null,null,F,T)),a.Y(1,49152,null,0,D.a,[c.a,I.a,M.a],null,null)],null,null)},{},{},[])},580:function(l,n,u){"use strict";u.d(n,"a",function(){return O});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(54),s=u(34),r=u(29),c=u(46),d=u(49),g=u(13),_=u(61),p=u(5),m=u(18),h=u(10),f=u(69),b=u(35),v=u(59),y=u(9),Y=u(15),C=u(20),Z=u(16),j=u(24),k=u(17),z=u(6),P=u(14),w=u(181),x=u(927),D=u(102),I=u(62),M=u(32),T=u(48),F=u(40),L=u(12),N=a.X({encapsulation:2,styles:[],data:{}});function S(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function A(l){return a._22(0,[(l()(),a.Z(0,0,null,null,9,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(1,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(5,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n                  "])),(l()(),a.U(16777216,null,2,1,null,S)),a.Y(8,16384,null,0,g.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,2,["\n               \n                "]))],function(l,n){l(n,8,0,0==n.component.loaded)},null)}function V(l){return a._22(0,[(l()(),a.Z(0,0,null,null,54,"div",[["class","form-container"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(2,0,null,null,45,"ion-list",[],null,null,null,null,null)),a.Y(3,16384,null,0,_.a,[i.a,a.j,a.z,p.a,m.l,h.a],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.U(16777216,null,null,1,null,A)),a.Y(6,16384,null,0,g.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(8,0,null,null,12,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(9,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,5,{contentLabel:0}),a._18(603979776,6,{_buttons:1}),a._18(603979776,7,{_icons:1}),a.Y(13,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n                    "])),(l()(),a.Z(15,0,null,3,4,"ion-input",[["name","email"],["placeholder","Email"],["type","email"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.login.email=u)&&a);return a},f.b,f.a)),a.Y(16,671744,null,0,b.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,b.f,null,[b.h]),a.Y(18,16384,null,0,b.g,[b.f],null,null),a.Y(19,5423104,null,0,v.a,[i.a,p.a,r.a,y.a,a.j,a.z,[2,Y.a],[2,s.a],[2,b.f],h.a],{type:[0,"type"],placeholder:[1,"placeholder"]},null),(l()(),a._20(-1,2,["\n                "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(23,0,null,null,12,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(24,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,8,{contentLabel:0}),a._18(603979776,9,{_buttons:1}),a._18(603979776,10,{_icons:1}),a.Y(28,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n                "])),(l()(),a.Z(30,0,null,3,4,"ion-input",[["name","pword"],["placeholder","Password"],["type","password"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.login.pword=u)&&a);return a},f.b,f.a)),a.Y(31,671744,null,0,b.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,b.f,null,[b.h]),a.Y(33,16384,null,0,b.g,[b.f],null,null),a.Y(34,5423104,null,0,v.a,[i.a,p.a,r.a,y.a,a.j,a.z,[2,Y.a],[2,s.a],[2,b.f],h.a],{type:[0,"type"],placeholder:[1,"placeholder"]},null),(l()(),a._20(-1,2,["\n            "])),(l()(),a._20(-1,null,["\n\n            "])),(l()(),a.Z(37,0,null,null,9,"ion-item",[["class","item item-block"],["no-lines",""],["text-center",""]],null,null,null,o.b,o.a)),a.Y(38,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,11,{contentLabel:0}),a._18(603979776,12,{_buttons:1}),a._18(603979776,13,{_icons:1}),a.Y(42,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["                \n                No Account? "])),(l()(),a.Z(44,0,null,2,1,"a",[["color","primary"]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.signUp()&&a);return a},null,null)),(l()(),a._20(-1,null,["Sign Up"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a._20(-1,null,["\n                \n            "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(49,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(51,0,null,null,2,"button",[["color","primary"],["full",""],["ion-button",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.loginCheck()&&a);return a},C.b,C.a)),a.Y(52,1097728,null,0,Z.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],full:[1,"full"]},null),(l()(),a._20(-1,0,["Sign In"])),(l()(),a._20(-1,null,["\n\n\n\n\n        \n\n\n\n\n    "]))],function(l,n){var u=n.component;l(n,6,0,0==u.loaded);l(n,16,0,"email",u.login.email);l(n,19,0,"email","Email");l(n,31,0,"pword",u.login.pword);l(n,34,0,"password","Password");l(n,52,0,"primary","")},function(l,n){l(n,15,0,a._13(n,18).ngClassUntouched,a._13(n,18).ngClassTouched,a._13(n,18).ngClassPristine,a._13(n,18).ngClassDirty,a._13(n,18).ngClassValid,a._13(n,18).ngClassInvalid,a._13(n,18).ngClassPending),l(n,30,0,a._13(n,33).ngClassUntouched,a._13(n,33).ngClassTouched,a._13(n,33).ngClassPristine,a._13(n,33).ngClassDirty,a._13(n,33).ngClassValid,a._13(n,33).ngClassInvalid,a._13(n,33).ngClassPending)})}function U(l){return a._22(0,[(l()(),a.Z(0,0,null,null,25,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,j.b,j.a)),a.Y(1,16384,null,0,g.o,[],{ngSwitch:[0,"ngSwitch"]},null),a.Y(2,4374528,null,0,Y.a,[i.a,p.a,h.a,a.j,a.z,y.a,k.a,a.u,[2,z.a],[2,P.a]],null,null),(l()(),a._20(-1,1,["\n\n    "])),(l()(),a.Z(4,0,null,1,16,"div",[["class","logo-container"],["text-center",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(6,0,null,null,0,"img",[["alt","Logo"],["src","assets/imgs/logo.png"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n\n        "])),(l()(),a.Z(8,0,null,null,11,"ion-segment",[["align-self-end",""],["color","secondary"],["mode","md"],["text-center",""],["text-uppercase",""]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null],[2,"segment-disabled",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.screen=u)&&a);return a},null,null)),a.Y(9,671744,null,0,b.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,b.f,null,[b.h]),a.Y(11,16384,null,0,b.g,[b.f],null,null),a.Y(12,1196032,null,1,w.a,[i.a,a.j,a.z,[2,b.f]],{color:[0,"color"],mode:[1,"mode"]},null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(15,0,null,null,2,"ion-segment-button",[["class","segment-button"],["role","button"],["tappable",""],["value","signin"]],[[2,"segment-button-disabled",null],[2,"segment-activated",null],[1,"aria-pressed",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,16).onClick()&&t);return t},x.b,x.a)),a.Y(16,114688,[[1,4]],0,D.a,[],{value:[0,"value"]},null),(l()(),a._20(-1,0,["\n                Sign In\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n\n\n    "])),(l()(),a.U(16777216,null,1,1,null,V)),a.Y(23,278528,null,0,g.p,[a.I,a.F,g.o],{ngSwitchCase:[0,"ngSwitchCase"]},null),(l()(),a._20(-1,1,["\n    "])),(l()(),a._20(-1,1,["\n"])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,1,0,u.screen),l(n,9,0,u.screen);l(n,12,0,"secondary","md");l(n,16,0,"signin");l(n,23,0,"signin")},function(l,n){l(n,0,0,a._13(n,2).statusbarPadding,a._13(n,2)._hasRefresher),l(n,8,0,a._13(n,11).ngClassUntouched,a._13(n,11).ngClassTouched,a._13(n,11).ngClassPristine,a._13(n,11).ngClassDirty,a._13(n,11).ngClassValid,a._13(n,11).ngClassInvalid,a._13(n,11).ngClassPending,a._13(n,12)._disabled),l(n,15,0,a._13(n,16)._disabled,a._13(n,16).isActive,a._13(n,16).isActive)})}var O=a.V("page-signup",I.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-signup",[],null,null,null,U,N)),a.Y(1,49152,null,0,I.a,[P.a,M.a,T.a,F.a,L.a],null,null)],null,null)},{},{},[])},581:function(l,n,u){"use strict";u.d(n,"a",function(){return P});var a=u(0),t=u(26),e=u(3),i=u(6),o=u(33),s=u(19),r=u(9),c=u(14),d=u(20),g=u(16),_=u(41),p=u(30),m=u(42),h=u(21),f=u(31),b=u(24),v=u(15),y=u(5),Y=u(10),C=u(17),Z=u(250),j=u(12),k=a.X({encapsulation:2,styles:[],data:{}});function z(l){return a._22(0,[(l()(),a.Z(0,0,null,null,16,"ion-header",[],null,null,null,null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z,[2,i.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(3,0,null,null,12,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,o.b,o.a)),a.Y(4,49152,null,0,s.a,[r.a,[2,i.a],[2,c.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,8).toggle()&&t);return t},d.b,d.a)),a.Y(7,1097728,[[1,4]],0,g.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(8,1064960,null,0,_.a,[p.a,[2,i.a],[2,g.a],[2,s.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(9,16384,null,1,m.a,[e.a,a.j,a.z,[2,h.a],[2,s.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(13,147456,null,0,f.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(18,0,null,null,4,"ion-content",[["text-center",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,b.b,b.a)),a.Y(19,4374528,null,0,v.a,[e.a,y.a,Y.a,a.j,a.z,r.a,C.a,a.u,[2,i.a],[2,c.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(21,0,null,1,0,"img",[["alt","Logo"],["src","assets/imgs/logo-gray.png"]],null,null,null,null,null)),(l()(),a._20(-1,1,["\n"]))],function(l,n){l(n,4,0,"secondary");l(n,8,0,"");l(n,13,0,"menu")},function(l,n){l(n,3,0,a._13(n,4)._hidden,a._13(n,4)._sbPadding),l(n,6,0,a._13(n,8).isHidden),l(n,12,0,a._13(n,13)._hidden),l(n,18,0,a._13(n,19).statusbarPadding,a._13(n,19)._hasRefresher)})}var P=a.V("page-splash-one",Z.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-splash-one",[],null,null,null,z,k)),a.Y(1,49152,null,0,Z.a,[c.a,j.a],null,null)],null,null)},{},{},[])},582:function(l,n,u){"use strict";u.d(n,"a",function(){return f});var a=u(0),t=u(24),e=u(15),i=u(3),o=u(5),s=u(10),r=u(9),c=u(17),d=u(6),g=u(14),_=u(252),p=u(12),m=a.X({encapsulation:2,styles:[],data:{}});function h(l){return a._22(0,[(l()(),a.Z(0,0,null,null,4,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,t.b,t.a)),a.Y(1,4374528,null,0,e.a,[i.a,o.a,s.a,a.j,a.z,r.a,c.a,a.u,[2,d.a],[2,g.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(3,0,null,1,0,"img",[["alt",""],["src","assets/imgs/logo.png"]],null,null,null,null,null)),(l()(),a._20(-1,1,["\n"]))],null,function(l,n){l(n,0,0,a._13(n,1).statusbarPadding,a._13(n,1)._hasRefresher)})}var f=a.V("page-splash-two",_.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-splash-two",[],null,null,null,h,m)),a.Y(1,49152,null,0,_.a,[g.a,p.a],null,null)],null,null)},{},{},[])},583:function(l,n,u){"use strict";u.d(n,"a",function(){return H});var a=u(0),t=u(54),e=u(34),i=u(29),o=u(3),s=u(46),r=u(49),c=u(104),d=u(64),g=u(67),_=u(20),p=u(16),m=u(31),h=u(13),f=u(26),b=u(6),v=u(33),y=u(19),Y=u(9),C=u(14),Z=u(41),j=u(30),k=u(42),z=u(21),P=u(36),w=u(28),x=u(24),D=u(15),I=u(5),M=u(10),T=u(17),F=u(61),L=u(18),N=u(254),S=u(12),A=u(70),V=a.X({encapsulation:2,styles:[],data:{}});function U(l){return a._22(0,[(l()(),a.Z(0,0,null,null,47,"ion-item",[["class","item item-block"]],null,null,null,t.b,t.a)),a.Y(1,1097728,null,3,e.a,[i.a,o.a,a.j,a.z,[2,s.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(5,16384,null,0,r.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(7,0,null,0,4,"ion-thumbnail",[["item-start",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),a.Y(8,16384,null,0,c.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(10,0,null,null,0,"img",[],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(13,0,null,2,1,"h2",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),(l()(),a._20(14,null,["",""])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(16,0,null,2,1,"p",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),(l()(),a._20(17,null,["",""])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(19,0,null,2,27,"ion-row",[["class","row"],["no-padding",""]],null,null,null,null,null)),a.Y(20,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(22,0,null,null,7,"ion-col",[["class","col"],["no-padding",""]],null,null,null,null,null)),a.Y(23,16384,null,0,g.a,[],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(25,0,null,null,3,"button",[["clear",""],["color","primary"],["ion-button",""],["no-padding",""],["small",""]],null,null,null,_.b,_.a)),a.Y(26,1097728,null,0,p.a,[[8,""],o.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(27,0,["\n                  ","\n                  "])),a._16(28,4),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(31,0,null,null,2,"ion-col",[["class","col"]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),a.Y(32,16384,null,0,g.a,[],null,null),(l()(),a._20(-1,null,["\n\n                "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(35,0,null,null,10,"ion-col",[["class","col"],["no-padding",""],["text-right",""]],null,null,null,null,null)),a.Y(36,16384,null,0,g.a,[],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(38,0,null,null,6,"button",[["clear",""],["icon-start",""],["ion-button",""],["no-padding",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!=(l.context.$implicit.isliked=!l.context.$implicit.isliked)&&a);return a},_.b,_.a)),a.Y(39,1097728,null,0,p.a,[[8,""],o.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(-1,0,["\n                  "])),(l()(),a.Z(41,0,null,0,1,"ion-icon",[["name","heart"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(42,147456,null,0,m.a,[o.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(43,0,["\n                  ","\n                "])),a._16(44,1),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,2,["\n        "]))],function(l,n){l(n,26,0,"primary","","");l(n,39,0,n.context.$implicit.isliked?"danger":"light","","");l(n,42,0,"heart")},function(l,n){l(n,10,0,a._2(1,"",n.context.$implicit.img,"")),l(n,14,0,n.context.$implicit.title),l(n,17,0,n.context.$implicit.description),l(n,27,0,a._21(n,27,0,l(n,28,0,a._13(n.parent,0),n.context.$implicit.price,"USD",!0,"1.2"))),l(n,41,0,a._13(n,42)._hidden),l(n,43,0,a._21(n,43,0,l(n,44,0,a._13(n.parent,1),n.context.$implicit.likes)))})}function O(l){return a._22(0,[a._14(0,h.c,[a.r]),a._14(0,h.e,[a.r]),(l()(),a.Z(2,0,null,null,42,"ion-header",[],null,null,null,null,null)),a.Y(3,16384,null,0,f.a,[o.a,a.j,a.z,[2,b.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(5,0,null,null,16,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,v.b,v.a)),a.Y(6,49152,null,0,y.a,[Y.a,[2,b.a],[2,C.a],o.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(8,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,10).toggle()&&t);return t},_.b,_.a)),a.Y(9,1097728,[[1,4]],0,p.a,[[8,""],o.a,a.j,a.z],null,null),a.Y(10,1064960,null,0,Z.a,[j.a,[2,b.a],[2,p.a],[2,y.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(11,16384,null,1,k.a,[o.a,a.j,a.z,[2,z.a],[2,y.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(14,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(15,147456,null,0,m.a,[o.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(18,0,null,3,2,"ion-title",[],null,null,null,P.b,P.a)),a.Y(19,49152,null,0,w.a,[o.a,a.j,a.z,[2,z.a],[2,y.a]],null,null),(l()(),a._20(-1,0,["Coffee"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(23,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(25,0,null,null,18,"div",[["class","offer"],["text-center",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(27,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Special Offer"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(30,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(31,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(33,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Blue Ridge Blend"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(36,0,null,null,6,"p",[["class","old-price"],["style","margin-top: 20px;"],["text-left",""]],null,null,null,null,null)),(l()(),a._20(37,null,["\n            ","\n            "])),a._16(38,4),(l()(),a.Z(39,0,null,null,2,"span",[["class","new-price"],["float-right",""]],null,null,null,null,null)),(l()(),a._20(40,null,["\n              ","\n          "])),a._16(41,4),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(46,0,null,null,9,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,x.b,x.a)),a.Y(47,4374528,null,0,D.a,[o.a,I.a,M.a,a.j,a.z,Y.a,T.a,a.u,[2,b.a],[2,C.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(49,0,null,1,5,"ion-list",[["no-lines",""],["no-margin",""]],null,null,null,null,null)),a.Y(50,16384,null,0,F.a,[o.a,a.j,a.z,I.a,L.l,M.a],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.U(16777216,null,null,1,null,U)),a.Y(53,802816,null,0,h.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,10,0,"");l(n,15,0,"menu"),l(n,53,0,u.items)},function(l,n){var u=n.component;l(n,5,0,a._13(n,6)._hidden,a._13(n,6)._sbPadding),l(n,8,0,a._13(n,10).isHidden),l(n,14,0,a._13(n,15)._hidden),l(n,31,0,u.specialOffer.item),l(n,37,0,a._21(n,37,0,l(n,38,0,a._13(n,0),u.specialOffer.oldPrice,"USD",!0,"1.0-2"))),l(n,40,0,a._21(n,40,0,l(n,41,0,a._13(n,0),u.specialOffer.newPrice,"USD",!0,"1.0-2"))),l(n,46,0,a._13(n,47).statusbarPadding,a._13(n,47)._hasRefresher)})}var H=a.V("page-sub-menu-one",N.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-sub-menu-one",[],null,null,null,O,V)),a.Y(1,49152,null,0,N.a,[C.a,S.a,A.a],null,null)],null,null)},{},{},[])},584:function(l,n,u){"use strict";u.d(n,"a",function(){return A});var a=u(0),t=u(67),e=u(45),i=u(3),o=u(60),s=u(125),r=u(64),c=u(20),d=u(16),g=u(31),_=u(13),p=u(26),m=u(6),h=u(33),f=u(19),b=u(9),v=u(14),y=u(41),Y=u(30),C=u(42),Z=u(21),j=u(36),k=u(28),z=u(24),P=u(15),w=u(5),x=u(10),D=u(17),I=u(84),M=u(256),T=u(12),F=u(70),L=a.X({encapsulation:2,styles:[],data:{}});function N(l){return a._22(0,[(l()(),a.Z(0,0,null,null,48,"ion-col",[["align-self-center",""],["class","col"],["col-6",""],["no-padding",""]],null,null,null,null,null)),a.Y(1,16384,null,0,t.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(3,0,null,null,44,"ion-card",[],null,null,null,null,null)),a.Y(4,16384,null,0,e.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(6,0,null,null,40,"ion-card-content",[],null,null,null,null,null)),a.Y(7,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(9,0,null,null,2,"ion-card-title",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),a.Y(10,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(11,null,["\n                            ","\n                        "])),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(13,0,null,null,1,"p",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),(l()(),a._20(14,null,[""," "])),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(16,0,null,null,0,"img",[],[[8,"src",4]],[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(18,0,null,null,27,"ion-row",[["class","row"],["no-padding",""]],null,null,null,null,null)),a.Y(19,16384,null,0,r.a,[],null,null),(l()(),a._20(-1,null,["\n                            "])),(l()(),a.Z(21,0,null,null,7,"ion-col",[["class","col"],["no-padding",""]],null,null,null,null,null)),a.Y(22,16384,null,0,t.a,[],null,null),(l()(),a._20(-1,null,["\n                                "])),(l()(),a.Z(24,0,null,null,3,"button",[["clear",""],["color","primary"],["ion-button",""],["no-padding",""],["small",""]],null,null,null,c.b,c.a)),a.Y(25,1097728,null,0,d.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(26,0,["\n                              ","\n                              "])),a._16(27,4),(l()(),a._20(-1,null,["\n                            "])),(l()(),a._20(-1,null,["\n                            "])),(l()(),a.Z(30,0,null,null,2,"ion-col",[["class","col"]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.selectItem(l.context.$implicit)&&a);return a},null,null)),a.Y(31,16384,null,0,t.a,[],null,null),(l()(),a._20(-1,null,["\n\n                            "])),(l()(),a._20(-1,null,["\n                            "])),(l()(),a.Z(34,0,null,null,10,"ion-col",[["class","col"],["no-padding",""],["text-right",""]],null,null,null,null,null)),a.Y(35,16384,null,0,t.a,[],null,null),(l()(),a._20(-1,null,["\n                                "])),(l()(),a.Z(37,0,null,null,6,"button",[["clear",""],["icon-start",""],["ion-button",""],["no-padding",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!=(l.context.$implicit.isliked=!l.context.$implicit.isliked)&&a);return a},c.b,c.a)),a.Y(38,1097728,null,0,d.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(-1,0,["\n                              "])),(l()(),a.Z(40,0,null,0,1,"ion-icon",[["name","heart"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(41,147456,null,0,g.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(42,0,["\n                              ","\n                            "])),a._16(43,1),(l()(),a._20(-1,null,["\n                            "])),(l()(),a._20(-1,null,["\n                        "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,null,["\n            "]))],function(l,n){l(n,25,0,"primary","","");l(n,38,0,n.context.$implicit.isliked?"danger":"light","","");l(n,41,0,"heart")},function(l,n){l(n,11,0,n.context.$implicit.title),l(n,14,0,n.context.$implicit.description),l(n,16,0,a._2(1,"",n.context.$implicit.img,"")),l(n,26,0,a._21(n,26,0,l(n,27,0,a._13(n.parent,0),n.context.$implicit.price,"USD",!0,"1.2"))),l(n,40,0,a._13(n,41)._hidden),l(n,42,0,a._21(n,42,0,l(n,43,0,a._13(n.parent,1),n.context.$implicit.likes)))})}function S(l){return a._22(0,[a._14(0,_.c,[a.r]),a._14(0,_.e,[a.r]),(l()(),a.Z(2,0,null,null,31,"ion-header",[],null,null,null,null,null)),a.Y(3,16384,null,0,p.a,[i.a,a.j,a.z,[2,m.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(5,0,null,null,16,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,h.b,h.a)),a.Y(6,49152,null,0,f.a,[b.a,[2,m.a],[2,v.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(8,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,10).toggle()&&t);return t},c.b,c.a)),a.Y(9,1097728,[[1,4]],0,d.a,[[8,""],i.a,a.j,a.z],null,null),a.Y(10,1064960,null,0,y.a,[Y.a,[2,m.a],[2,d.a],[2,f.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(11,16384,null,1,C.a,[i.a,a.j,a.z,[2,Z.a],[2,f.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(14,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(15,147456,null,0,g.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(18,0,null,3,2,"ion-title",[],null,null,null,j.b,j.a)),a.Y(19,49152,null,0,k.a,[i.a,a.j,a.z,[2,Z.a],[2,f.a]],null,null),(l()(),a._20(-1,0,["Coffee"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(23,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(25,0,null,null,7,"div",[["class","offer"],["text-center",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(27,0,null,null,1,"h2",[],null,null,null,null,null)),(l()(),a._20(-1,null,["The Science of Delicious."])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(30,0,null,null,1,"p",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Amazing coffees from around the world! "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(35,0,null,null,13,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,z.b,z.a)),a.Y(36,4374528,null,0,P.a,[i.a,w.a,x.a,a.j,a.z,b.a,D.a,a.u,[2,m.a],[2,v.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(38,0,null,1,9,"ion-grid",[["class","grid"],["no-padding",""]],null,null,null,null,null)),a.Y(39,16384,null,0,I.a,[],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(41,0,null,null,5,"ion-row",[["class","row"],["justify-content-center",""],["no-padding",""]],null,null,null,null,null)),a.Y(42,16384,null,0,r.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.U(16777216,null,null,1,null,N)),a.Y(45,802816,null,0,_.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,10,0,"");l(n,15,0,"menu"),l(n,45,0,u.items)},function(l,n){l(n,5,0,a._13(n,6)._hidden,a._13(n,6)._sbPadding),l(n,8,0,a._13(n,10).isHidden),l(n,14,0,a._13(n,15)._hidden),l(n,35,0,a._13(n,36).statusbarPadding,a._13(n,36)._hasRefresher)})}var A=a.V("page-sub-menu-two",M.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-sub-menu-two",[],null,null,null,S,L)),a.Y(1,49152,null,0,M.a,[v.a,T.a,F.a],null,null)],null,null)},{},{},[])},585:function(l,n,u){"use strict";u.d(n,"a",function(){return J});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(26),s=u(6),r=u(33),c=u(19),d=u(9),g=u(14),_=u(20),p=u(16),m=u(41),h=u(30),f=u(42),b=u(21),v=u(31),y=u(36),Y=u(28),C=u(24),Z=u(15),j=u(5),k=u(10),z=u(17),P=u(13),w=u(61),x=u(18),D=u(54),I=u(34),M=u(29),T=u(46),F=u(49),L=u(58),N=u(69),S=u(35),A=u(59),V=u(258),U=u(48),O=u(32),H=u(40),R=u(12),E=a.X({encapsulation:2,styles:[],data:{}});function $(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function B(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(1,16384,null,0,o.a,[i.a,a.j,a.z,[2,s.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(3,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,r.b,r.a)),a.Y(4,49152,null,0,c.a,[d.a,[2,s.a],[2,g.a],i.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,8).toggle()&&t);return t},_.b,_.a)),a.Y(7,1097728,[[1,4]],0,p.a,[[8,""],i.a,a.j,a.z],null,null),a.Y(8,1064960,null,0,m.a,[h.a,[2,s.a],[2,p.a],[2,c.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(9,16384,null,1,f.a,[i.a,a.j,a.z,[2,b.a],[2,c.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n    "])),(l()(),a.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(13,147456,null,0,v.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n  "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(16,0,null,3,2,"ion-title",[],null,null,null,y.b,y.a)),a.Y(17,49152,null,0,Y.a,[i.a,a.j,a.z,[2,b.a],[2,c.a]],null,null),(l()(),a._20(-1,0,["Setting"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(22,0,null,null,227,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,C.b,C.a)),a.Y(23,4374528,null,0,Z.a,[i.a,j.a,k.a,a.j,a.z,d.a,z.a,a.u,[2,s.a],[2,g.a]],null,null),(l()(),a._20(-1,1,["\n        "])),(l()(),a.U(16777216,null,1,1,null,$)),a.Y(26,16384,null,0,P.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n    \n    "])),(l()(),a.Z(28,0,null,1,220,"ion-list",[["no-lines",""],["no-margin",""]],null,null,null,null,null)),a.Y(29,16384,null,0,w.a,[i.a,a.j,a.z,j.a,x.l,k.a],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(31,0,null,null,9,"ion-item",[["class","item item-block"],["text-center",""]],null,null,null,D.b,D.a)),a.Y(32,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(36,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n        "])),(l()(),a.Z(38,0,null,2,1,"h2",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Edit Settings Here"])),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(42,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(43,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,5,{contentLabel:0}),a._18(603979776,6,{_buttons:1}),a._18(603979776,7,{_icons:1}),a.Y(47,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(49,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(50,16384,[[5,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Name"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(53,0,null,3,4,"ion-input",[["text-right",""],["type","text"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.name=u)&&a);return a},N.b,N.a)),a.Y(54,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(56,16384,null,0,S.g,[S.f],null,null),a.Y(57,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(60,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(61,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,8,{contentLabel:0}),a._18(603979776,9,{_buttons:1}),a._18(603979776,10,{_icons:1}),a.Y(65,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(67,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(68,16384,[[8,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" DOB"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(71,0,null,3,4,"ion-input",[["text-right",""],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.dob=u)&&a);return a},N.b,N.a)),a.Y(72,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(74,16384,null,0,S.g,[S.f],null,null),a.Y(75,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(78,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(79,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,11,{contentLabel:0}),a._18(603979776,12,{_buttons:1}),a._18(603979776,13,{_icons:1}),a.Y(83,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(85,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(86,16384,[[11,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" DOW"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(89,0,null,3,4,"ion-input",[["text-right",""],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.dow=u)&&a);return a},N.b,N.a)),a.Y(90,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(92,16384,null,0,S.g,[S.f],null,null),a.Y(93,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(96,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(97,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,14,{contentLabel:0}),a._18(603979776,15,{_buttons:1}),a._18(603979776,16,{_icons:1}),a.Y(101,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(103,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(104,16384,[[14,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Pension"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(107,0,null,3,4,"ion-input",[["text-right",""],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.pen=u)&&a);return a},N.b,N.a)),a.Y(108,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(110,16384,null,0,S.g,[S.f],null,null),a.Y(111,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(114,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(115,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,17,{contentLabel:0}),a._18(603979776,18,{_buttons:1}),a._18(603979776,19,{_icons:1}),a.Y(119,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(121,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(122,16384,[[17,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Assets (at signup)"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(125,0,null,3,4,"ion-input",[["text-right",""],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.savInv=u)&&a);return a},N.b,N.a)),a.Y(126,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(128,16384,null,0,S.g,[S.f],null,null),a.Y(129,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(132,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(133,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,20,{contentLabel:0}),a._18(603979776,21,{_buttons:1}),a._18(603979776,22,{_icons:1}),a.Y(137,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(139,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(140,16384,[[20,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Expenses"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(143,0,null,3,4,"ion-input",[["text-right",""],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.exp=u)&&a);return a},N.b,N.a)),a.Y(144,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(146,16384,null,0,S.g,[S.f],null,null),a.Y(147,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(150,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(151,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,23,{contentLabel:0}),a._18(603979776,24,{_buttons:1}),a._18(603979776,25,{_icons:1}),a.Y(155,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(157,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(158,16384,[[23,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Income"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(161,0,null,3,4,"ion-input",[["text-right",""],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.inc=u)&&a);return a},N.b,N.a)),a.Y(162,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(164,16384,null,0,S.g,[S.f],null,null),a.Y(165,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(168,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(169,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,26,{contentLabel:0}),a._18(603979776,27,{_buttons:1}),a._18(603979776,28,{_icons:1}),a.Y(173,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(175,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(176,16384,[[26,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Retirement Age"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(179,0,null,3,4,"ion-input",[["text-right",""],["type","number"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.ret=u)&&a);return a},N.b,N.a)),a.Y(180,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(182,16384,null,0,S.g,[S.f],null,null),a.Y(183,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(186,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(187,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,29,{contentLabel:0}),a._18(603979776,30,{_buttons:1}),a._18(603979776,31,{_icons:1}),a.Y(191,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(193,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(194,16384,[[29,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Email"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(197,0,null,3,4,"ion-input",[["text-right",""],["type","email"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.email=u)&&a);return a},N.b,N.a)),a.Y(198,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(200,16384,null,0,S.g,[S.f],null,null),a.Y(201,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(204,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(205,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,32,{contentLabel:0}),a._18(603979776,33,{_buttons:1}),a._18(603979776,34,{_icons:1}),a.Y(209,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(211,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(212,16384,[[32,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Password"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(215,0,null,3,4,"ion-input",[["text-right",""],["type","password"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.pword=u)&&a);return a},N.b,N.a)),a.Y(216,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(218,16384,null,0,S.g,[S.f],null,null),a.Y(219,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(222,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,D.b,D.a)),a.Y(223,1097728,null,3,I.a,[M.a,i.a,a.j,a.z,[2,T.a]],null,null),a._18(335544320,35,{contentLabel:0}),a._18(603979776,36,{_buttons:1}),a._18(603979776,37,{_icons:1}),a.Y(227,16384,null,0,F.a,[],null,null),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(229,0,null,1,2,"ion-label",[],null,null,null,null,null)),a.Y(230,16384,[[35,4]],0,L.a,[i.a,a.j,a.z,[8,null],[8,null],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" Confirm Password"])),(l()(),a._20(-1,2,["\n            "])),(l()(),a.Z(233,0,null,3,4,"ion-input",[["text-right",""],["type","password"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.setting.cpword=u)&&a);return a},N.b,N.a)),a.Y(234,671744,null,0,S.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),a._17(2048,null,S.f,null,[S.h]),a.Y(236,16384,null,0,S.g,[S.f],null,null),a.Y(237,5423104,null,0,A.a,[i.a,j.a,M.a,d.a,a.j,a.z,[2,Z.a],[2,I.a],[2,S.f],k.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n        "])),(l()(),a._20(-1,null,["\n\n        "])),(l()(),a.Z(240,0,null,null,2,"button",[["color","primary"],["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.editUser()&&a);return a},_.b,_.a)),a.Y(241,1097728,null,0,p.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],small:[1,"small"]},null),(l()(),a._20(-1,0,["Update"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(244,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.reset()&&a);return a},_.b,_.a)),a.Y(245,1097728,null,0,p.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Reset"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,4,0,"secondary");l(n,8,0,"");l(n,13,0,"menu"),l(n,26,0,0==u.loaded),l(n,54,0,u.setting.name);l(n,57,0,"text"),l(n,72,0,u.setting.dob);l(n,75,0,"date"),l(n,90,0,u.setting.dow);l(n,93,0,"date"),l(n,108,0,u.setting.pen);l(n,111,0,"number"),l(n,126,0,u.setting.savInv);l(n,129,0,"number"),l(n,144,0,u.setting.exp);l(n,147,0,"number"),l(n,162,0,u.setting.inc);l(n,165,0,"number"),l(n,180,0,u.setting.ret);l(n,183,0,"number"),l(n,198,0,u.setting.email);l(n,201,0,"email"),l(n,216,0,u.setting.pword);l(n,219,0,"password"),l(n,234,0,u.setting.cpword);l(n,237,0,"password");l(n,241,0,"primary","");l(n,245,0,"")},function(l,n){l(n,3,0,a._13(n,4)._hidden,a._13(n,4)._sbPadding),l(n,6,0,a._13(n,8).isHidden),l(n,12,0,a._13(n,13)._hidden),l(n,22,0,a._13(n,23).statusbarPadding,a._13(n,23)._hasRefresher),l(n,53,0,a._13(n,56).ngClassUntouched,a._13(n,56).ngClassTouched,a._13(n,56).ngClassPristine,a._13(n,56).ngClassDirty,a._13(n,56).ngClassValid,a._13(n,56).ngClassInvalid,a._13(n,56).ngClassPending),l(n,71,0,a._13(n,74).ngClassUntouched,a._13(n,74).ngClassTouched,a._13(n,74).ngClassPristine,a._13(n,74).ngClassDirty,a._13(n,74).ngClassValid,a._13(n,74).ngClassInvalid,a._13(n,74).ngClassPending),l(n,89,0,a._13(n,92).ngClassUntouched,a._13(n,92).ngClassTouched,a._13(n,92).ngClassPristine,a._13(n,92).ngClassDirty,a._13(n,92).ngClassValid,a._13(n,92).ngClassInvalid,a._13(n,92).ngClassPending),l(n,107,0,a._13(n,110).ngClassUntouched,a._13(n,110).ngClassTouched,a._13(n,110).ngClassPristine,a._13(n,110).ngClassDirty,a._13(n,110).ngClassValid,a._13(n,110).ngClassInvalid,a._13(n,110).ngClassPending),l(n,125,0,a._13(n,128).ngClassUntouched,a._13(n,128).ngClassTouched,a._13(n,128).ngClassPristine,a._13(n,128).ngClassDirty,a._13(n,128).ngClassValid,a._13(n,128).ngClassInvalid,a._13(n,128).ngClassPending),l(n,143,0,a._13(n,146).ngClassUntouched,a._13(n,146).ngClassTouched,a._13(n,146).ngClassPristine,a._13(n,146).ngClassDirty,a._13(n,146).ngClassValid,a._13(n,146).ngClassInvalid,a._13(n,146).ngClassPending),l(n,161,0,a._13(n,164).ngClassUntouched,a._13(n,164).ngClassTouched,a._13(n,164).ngClassPristine,a._13(n,164).ngClassDirty,a._13(n,164).ngClassValid,a._13(n,164).ngClassInvalid,a._13(n,164).ngClassPending),l(n,179,0,a._13(n,182).ngClassUntouched,a._13(n,182).ngClassTouched,a._13(n,182).ngClassPristine,a._13(n,182).ngClassDirty,a._13(n,182).ngClassValid,a._13(n,182).ngClassInvalid,a._13(n,182).ngClassPending),l(n,197,0,a._13(n,200).ngClassUntouched,a._13(n,200).ngClassTouched,a._13(n,200).ngClassPristine,a._13(n,200).ngClassDirty,a._13(n,200).ngClassValid,a._13(n,200).ngClassInvalid,a._13(n,200).ngClassPending),l(n,215,0,a._13(n,218).ngClassUntouched,a._13(n,218).ngClassTouched,a._13(n,218).ngClassPristine,a._13(n,218).ngClassDirty,a._13(n,218).ngClassValid,a._13(n,218).ngClassInvalid,a._13(n,218).ngClassPending),l(n,233,0,a._13(n,236).ngClassUntouched,a._13(n,236).ngClassTouched,a._13(n,236).ngClassPristine,a._13(n,236).ngClassDirty,a._13(n,236).ngClassValid,a._13(n,236).ngClassInvalid,a._13(n,236).ngClassPending)})}var J=a.V("page-setting",V.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-setting",[],null,null,null,B,E)),a.Y(1,49152,null,0,V.a,[g.a,U.a,O.a,H.a,R.a],null,null)],null,null)},{},{},[])},586:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(260),w=u(40),x=u(12),D=a.X({encapsulation:2,styles:[],data:{}});function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.analysisAction(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,[" \n    "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function M(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(2,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(4,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(5,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(7,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,9).toggle()&&t);return t},g.b,g.a)),a.Y(8,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(9,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(10,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n        "])),(l()(),a.Z(13,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(14,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n      "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(17,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(18,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,[" View Budget Analytics"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n  "])),(l()(),a._20(-1,null,["\n  \n  \n  "])),(l()(),a.Z(23,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(24,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(27,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n  "])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,5,0,"tertiary");l(n,9,0,"");l(n,14,0,"menu"),l(n,27,0,u.categories)},function(l,n){l(n,4,0,a._13(n,5)._hidden,a._13(n,5)._sbPadding),l(n,7,0,a._13(n,9).isHidden),l(n,13,0,a._13(n,14)._hidden),l(n,23,0,a._13(n,24).statusbarPadding,a._13(n,24)._hasRefresher)})}var T=a.V("page-viewbudget",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-viewbudget",[],null,null,null,M,D)),a.Y(1,49152,null,0,P.a,[d.a,w.a,x.a],null,null)],null,null)},{},{},[])},587:function(l,n,u){"use strict";u.d(n,"a",function(){return T});var a=u(0),t=u(45),e=u(3),i=u(26),o=u(6),s=u(33),r=u(19),c=u(9),d=u(14),g=u(20),_=u(16),p=u(41),m=u(30),h=u(42),f=u(21),b=u(31),v=u(36),y=u(28),Y=u(24),C=u(15),Z=u(5),j=u(10),k=u(17),z=u(13),P=u(262),w=u(40),x=u(12),D=a.X({encapsulation:2,styles:[],data:{}});function I(l){return a._22(0,[(l()(),a.Z(0,0,null,null,10,"ion-card",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.analysisAction(l.context.$implicit)&&a);return a},null,null)),a.Y(1,16384,null,0,t.a,[e.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(3,0,null,null,0,"div",[["class","img"]],[[4,"background",null]],null,null,null,null)),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(5,0,null,null,1,"div",[["class","card-title"]],null,null,null,null,null)),(l()(),a._20(6,null,["",""])),(l()(),a._20(-1,null,["\n      "])),(l()(),a.Z(8,0,null,null,1,"div",[["class","card-subtitle"]],null,null,null,null,null)),(l()(),a._20(9,null,["",""])),(l()(),a._20(-1,null,[" \n  "]))],null,function(l,n){l(n,3,0,"url("+n.context.$implicit.img+")"),l(n,6,0,n.context.$implicit.title),l(n,9,0,n.context.$implicit.description)})}function M(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(1,16384,null,0,i.a,[e.a,a.j,a.z,[2,o.a]],null,null),(l()(),a._20(-1,null,["\n  "])),(l()(),a.Z(3,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","tertiary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,s.b,s.a)),a.Y(4,49152,null,0,r.a,[c.a,[2,o.a],[2,d.a],e.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,8).toggle()&&t);return t},g.b,g.a)),a.Y(7,1097728,[[1,4]],0,_.a,[[8,""],e.a,a.j,a.z],null,null),a.Y(8,1064960,null,0,p.a,[m.a,[2,o.a],[2,_.a],[2,r.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(9,16384,null,1,h.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(13,147456,null,0,b.a,[e.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,3,["\n      "])),(l()(),a.Z(16,0,null,3,2,"ion-title",[],null,null,null,v.b,v.a)),a.Y(17,49152,null,0,y.a,[e.a,a.j,a.z,[2,f.a],[2,r.a]],null,null),(l()(),a._20(-1,0,[" View Analytics"])),(l()(),a._20(-1,3,["\n  "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(22,0,null,null,5,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Y.b,Y.a)),a.Y(23,4374528,null,0,C.a,[e.a,Z.a,j.a,a.j,a.z,c.a,k.a,a.u,[2,o.a],[2,d.a]],null,null),(l()(),a._20(-1,1,["\n  "])),(l()(),a.U(16777216,null,1,1,null,I)),a.Y(26,802816,null,0,z.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,4,0,"tertiary");l(n,8,0,"");l(n,13,0,"menu"),l(n,26,0,u.categories)},function(l,n){l(n,3,0,a._13(n,4)._hidden,a._13(n,4)._sbPadding),l(n,6,0,a._13(n,8).isHidden),l(n,12,0,a._13(n,13)._hidden),l(n,22,0,a._13(n,23).statusbarPadding,a._13(n,23)._hasRefresher)})}var T=a.V("page-viewanalytics",P.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-viewanalytics",[],null,null,null,M,D)),a.Y(1,49152,null,0,P.a,[d.a,w.a,x.a],null,null)],null,null)},{},{},[])},588:function(l,n,u){"use strict";u.d(n,"a",function(){return q});var a=u(0),t=u(57),e=u(51),i=u(3),o=u(54),s=u(34),r=u(29),c=u(46),d=u(49),g=u(104),_=u(64),p=u(67),m=u(20),h=u(16),f=u(31),b=u(13),v=u(26),y=u(6),Y=u(33),C=u(19),Z=u(9),j=u(14),k=u(41),z=u(30),P=u(42),w=u(21),x=u(36),D=u(28),I=u(24),M=u(15),T=u(5),F=u(10),L=u(17),N=u(928),S=u(137),A=u(35),V=u(61),U=u(18),O=u(264),H=u(12),R=u(40),E=u(48),$=u(32),B=u(70),J=a.X({encapsulation:2,styles:[],data:{}});function W(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function X(l){return a._22(0,[(l()(),a.Z(0,0,null,null,46,"ion-item",[["class","item item-block"]],null,null,null,o.b,o.a)),a.Y(1,1097728,null,3,s.a,[r.a,i.a,a.j,a.z,[2,c.a]],null,null),a._18(335544320,2,{contentLabel:0}),a._18(603979776,3,{_buttons:1}),a._18(603979776,4,{_icons:1}),a.Y(5,16384,null,0,d.a,[],null,null),(l()(),a._20(-1,2,["\n                "])),(l()(),a.Z(7,0,null,0,4,"ion-thumbnail",[["item-start",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.editTrans(l.context.$implicit)&&a);return a},null,null)),a.Y(8,16384,null,0,g.a,[],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(10,0,null,null,0,"img",[["src","assets/imgs/submenu/limeRefreshers.png"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,2,["\n                "])),(l()(),a.Z(13,0,null,2,1,"h2",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.editTrans(l.context.$implicit)&&a);return a},null,null)),(l()(),a._20(14,null,[" ",""])),(l()(),a._20(-1,2,["\n                "])),(l()(),a.Z(16,0,null,2,1,"p",[],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.editTrans(l.context.$implicit)&&a);return a},null,null)),(l()(),a._20(17,null,["",""])),(l()(),a._20(-1,2,["\n                "])),(l()(),a.Z(19,0,null,2,26,"ion-row",[["class","row"],["no-padding",""]],null,null,null,null,null)),a.Y(20,16384,null,0,_.a,[],null,null),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(22,0,null,null,7,"ion-col",[["class","col"],["no-padding",""]],null,null,null,null,null)),a.Y(23,16384,null,0,p.a,[],null,null),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(25,0,null,null,3,"button",[["clear",""],["color","primary"],["ion-button",""],["no-padding",""],["small",""]],null,null,null,m.b,m.a)),a.Y(26,1097728,null,0,h.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(27,0,["\n                     NGN ","\n                      "])),a._16(28,1),(l()(),a._20(-1,null,["\n                    "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(31,0,null,null,2,"ion-col",[["class","col"]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.editTrans(l.context.$implicit)&&a);return a},null,null)),a.Y(32,16384,null,0,p.a,[],null,null),(l()(),a._20(-1,null,["\n    \n                    "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(35,0,null,null,9,"ion-col",[["class","col"],["no-padding",""],["text-right",""]],null,null,null,null,null)),a.Y(36,16384,null,0,p.a,[],null,null),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(38,0,null,null,5,"button",[["clear",""],["icon-start",""],["ion-button",""],["no-padding",""],["small",""]],null,null,null,m.b,m.a)),a.Y(39,1097728,null,0,h.a,[[8,""],i.a,a.j,a.z],{color:[0,"color"],small:[1,"small"],clear:[2,"clear"]},null),(l()(),a._20(40,0,["\n                            ","\n                            "])),(l()(),a.Z(41,0,null,0,1,"ion-icon",[["name","trash"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(42,147456,null,0,f.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n                    "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,2,["\n            "]))],function(l,n){l(n,26,0,"primary","","");l(n,39,0,"light","","");l(n,42,0,"trash")},function(l,n){l(n,14,0,"dr"==n.context.$implicit.trans_type?"Debit":"Credit"),l(n,17,0,n.context.$implicit.trans_desc),l(n,27,0,a._21(n,27,0,l(n,28,0,a._13(n.parent,0),n.context.$implicit.trans_amt))),l(n,40,0,n.context.$implicit.trans_bucket),l(n,41,0,a._13(n,42)._hidden)})}function G(l){return a._22(0,[a._14(0,b.e,[a.r]),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(3,0,null,null,21,"ion-header",[],null,null,null,null,null)),a.Y(4,16384,null,0,v.a,[i.a,a.j,a.z,[2,y.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(6,0,null,null,16,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,Y.b,Y.a)),a.Y(7,49152,null,0,C.a,[Z.a,[2,y.a],[2,j.a],i.a,a.j,a.z],null,null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(9,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,11).toggle()&&t);return t},m.b,m.a)),a.Y(10,1097728,[[1,4]],0,h.a,[[8,""],i.a,a.j,a.z],null,null),a.Y(11,1064960,null,0,k.a,[z.a,[2,y.a],[2,h.a],[2,C.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(12,16384,null,1,P.a,[i.a,a.j,a.z,[2,w.a],[2,C.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n      "])),(l()(),a.Z(15,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(16,147456,null,0,f.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(19,0,null,3,2,"ion-title",[],null,null,null,x.b,x.a)),a.Y(20,49152,null,0,D.a,[i.a,a.j,a.z,[2,w.a],[2,C.a]],null,null),(l()(),a._20(21,0,["","'s Transactions"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(26,0,null,null,20,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,I.b,I.a)),a.Y(27,4374528,null,0,M.a,[i.a,T.a,F.a,a.j,a.z,Z.a,L.a,a.u,[2,y.a],[2,j.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(29,0,null,1,1,"ion-searchbar",[],[[2,"searchbar-animated",null],[2,"searchbar-has-value",null],[2,"searchbar-active",null],[2,"searchbar-show-cancel",null],[2,"searchbar-left-aligned",null],[2,"searchbar-has-focus",null]],[[null,"ionInput"]],function(l,n,u){var a=!0;"ionInput"===n&&(a=!1!==l.component.getItems(u)&&a);return a},N.b,N.a)),a.Y(30,1294336,null,0,S.a,[i.a,T.a,a.j,a.z,[2,A.f]],null,{ionInput:"ionInput"}),(l()(),a._20(-1,1,[" "])),(l()(),a.Z(32,0,null,1,3,"button",[["ion-button",""],["small",""]],null,null,null,m.b,m.a)),a.Y(33,1097728,null,0,h.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a.Z(34,0,null,0,1,"ion-icon",[["color","secondary"],["name","refresh"],["role","img"]],[[2,"hide",null]],[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.getTransactionsApi()&&a);return a},null,null)),a.Y(35,147456,null,0,f.a,[i.a,a.j,a.z],{color:[0,"color"],name:[1,"name"]},null),(l()(),a._20(-1,1,["\n        \n    "])),(l()(),a.U(16777216,null,1,1,null,W)),a.Y(38,16384,null,0,b.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n    \n\n\n    "])),(l()(),a.Z(40,0,null,1,5,"ion-list",[["no-lines",""],["no-margin",""]],null,null,null,null,null)),a.Y(41,16384,null,0,V.a,[i.a,a.j,a.z,T.a,U.l,F.a],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.U(16777216,null,null,1,null,X)),a.Y(44,802816,null,0,b.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,1,["\n\n\n\n"]))],function(l,n){var u=n.component;l(n,11,0,"");l(n,16,0,"menu"),l(n,30,0);l(n,33,0,"");l(n,35,0,"secondary","refresh"),l(n,38,0,0==u.loaded),l(n,44,0,u.transactions)},function(l,n){var u=n.component;l(n,6,0,a._13(n,7)._hidden,a._13(n,7)._sbPadding),l(n,9,0,a._13(n,11).isHidden),l(n,15,0,a._13(n,16)._hidden),l(n,21,0,u.userName),l(n,26,0,a._13(n,27).statusbarPadding,a._13(n,27)._hasRefresher),l(n,29,0,a._13(n,30)._animated,a._13(n,30)._value,a._13(n,30)._isActive,a._13(n,30)._showCancelButton,a._13(n,30)._shouldAlignLeft,a._13(n,30)._isFocus),l(n,34,0,a._13(n,35)._hidden)})}var q=a.V("page-viewtransactions",O.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-viewtransactions",[],null,null,null,G,J)),a.Y(1,49152,null,0,O.a,[j.a,H.a,R.a,E.a,$.a,B.a],null,null)],null,null)},{},{},[])},589:function(l,n,u){"use strict";u.d(n,"a",function(){return N});var a=u(0),t=u(461),e=u(103),i=u(87),o=u(20),s=u(16),r=u(3),c=u(26),d=u(6),g=u(33),_=u(19),p=u(9),m=u(14),h=u(41),f=u(30),b=u(42),v=u(21),y=u(31),Y=u(36),C=u(28),Z=u(24),j=u(15),k=u(5),z=u(10),P=u(17),w=u(460),x=u(13),D=u(266),I=u(12),M=u(70),T=a.X({encapsulation:2,styles:[],data:{}});function F(l){return a._22(0,[(l()(),a.Z(0,0,null,null,15,"ion-slide",[],null,null,null,t.b,t.a)),a.Y(1,180224,null,0,e.a,[a.j,a.z,i.a],null,null),(l()(),a._20(-1,0,["\n            "])),(l()(),a.Z(3,0,null,0,3,"div",[["class","image-container"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(5,0,null,null,0,"img",[],[[8,"src",4]],null,null,null,null)),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,0,["\n            "])),(l()(),a.Z(8,0,null,0,0,"h2",[["class","slide-title"],["text-uppercase",""]],[[8,"innerHTML",1]],null,null,null,null)),(l()(),a._20(-1,0,["\n            "])),(l()(),a.Z(10,0,null,0,0,"p",[],[[8,"innerHTML",1]],null,null,null,null)),(l()(),a._20(-1,0,["\n            "])),(l()(),a.Z(12,0,null,0,2,"button",[["color","primary"],["ion-button",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.goToHomePage()&&a);return a},o.b,o.a)),a.Y(13,1097728,null,0,s.a,[[8,""],r.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,0,["\n              Get Started!\n            "])),(l()(),a._20(-1,0,["\n        "]))],function(l,n){l(n,13,0,"primary")},function(l,n){l(n,5,0,"assets/imgs/logo-gray.png"),l(n,8,0,n.context.$implicit.title),l(n,10,0,n.context.$implicit.description)})}function L(l){return a._22(0,[(l()(),a.Z(0,0,null,null,20,"ion-header",[],null,null,null,null,null)),a.Y(1,16384,null,0,c.a,[r.a,a.j,a.z,[2,d.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(3,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,g.b,g.a)),a.Y(4,49152,null,0,_.a,[p.a,[2,d.a],[2,m.a],r.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,8).toggle()&&t);return t},o.b,o.a)),a.Y(7,1097728,[[1,4]],0,s.a,[[8,""],r.a,a.j,a.z],null,null),a.Y(8,1064960,null,0,h.a,[f.a,[2,d.a],[2,s.a],[2,_.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(9,16384,null,1,b.a,[r.a,a.j,a.z,[2,v.a],[2,_.a]],null,null),a._18(603979776,1,{_buttons:1}),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(13,147456,null,0,y.a,[r.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n        "])),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(16,0,null,3,2,"ion-title",[],null,null,null,Y.b,Y.a)),a.Y(17,49152,null,0,C.a,[r.a,a.j,a.z,[2,v.a],[2,_.a]],null,null),(l()(),a._20(-1,0,["Walkthrough"])),(l()(),a._20(-1,3,["\n    "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(22,0,null,null,9,"ion-content",[["class","tutorial-page"]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Z.b,Z.a)),a.Y(23,4374528,null,0,j.a,[r.a,k.a,z.a,a.j,a.z,p.a,P.a,a.u,[2,d.a],[2,m.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.Z(25,0,null,1,5,"ion-slides",[["pager",""]],null,null,null,w.b,w.a)),a.Y(26,1228800,null,0,i.a,[r.a,k.a,a.u,[2,d.a],a.j,a.z],{pager:[0,"pager"]},null),(l()(),a._20(-1,0,["\n        "])),(l()(),a.U(16777216,null,0,1,null,F)),a.Y(29,802816,null,0,x.j,[a.I,a.F,a.p],{ngForOf:[0,"ngForOf"]},null),(l()(),a._20(-1,0,["\n    "])),(l()(),a._20(-1,1,["\n"]))],function(l,n){var u=n.component;l(n,4,0,"secondary");l(n,8,0,"");l(n,13,0,"menu");l(n,26,0,""),l(n,29,0,u.slides)},function(l,n){l(n,3,0,a._13(n,4)._hidden,a._13(n,4)._sbPadding),l(n,6,0,a._13(n,8).isHidden),l(n,12,0,a._13(n,13)._hidden),l(n,22,0,a._13(n,23).statusbarPadding,a._13(n,23)._hasRefresher)})}var N=a.V("page-walkthrough",D.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-walkthrough",[],null,null,null,L,T)),a.Y(1,49152,null,0,D.a,[m.a,I.a,M.a],null,null)],null,null)},{},{},[])},590:function(l,n,u){"use strict";var a=u(0),t=u(57),e=u(51),i=u(3),o=u(45),s=u(60),r=u(54),c=u(34),d=u(29),g=u(46),_=u(49),p=u(58),m=u(69),h=u(35),f=u(59),b=u(5),v=u(9),y=u(15),Y=u(10),C=u(20),Z=u(16),j=u(26),k=u(6),z=u(33),P=u(19),w=u(14),x=u(41),D=u(30),I=u(42),M=u(21),T=u(31),F=u(36),L=u(28),N=u(24),S=u(17),A=u(13),V=u(84),U=u(64),O=u(67),H=u(205),R=a.X({encapsulation:2,styles:[],data:{}});function E(l){return a._22(0,[(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(1,0,null,null,4,"div",[["class","progress-outer"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n  "])),(l()(),a.Z(3,0,null,null,1,"div",[["class","progress-inner"]],[[4,"width",null]],null,null,null,null)),(l()(),a._20(4,null,["\n      "," Years\n  "])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n"])),(l()(),a.Z(8,0,null,null,7,"div",[["class","progress-label"]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n\n  "])),(l()(),a.Z(10,0,null,null,1,"i",[["float-left",""]],null,null,null,null,null)),(l()(),a._20(11,null,[""," Year(s)"])),(l()(),a._20(-1,null,["\n  "])),(l()(),a.Z(13,0,null,null,1,"i",[["float-right",""]],null,null,null,null,null)),(l()(),a._20(14,null,[""," Year(s)"])),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n"]))],null,function(l,n){var u=n.component;l(n,3,0,u.progress+"%"),l(n,4,0,u.progressAge),l(n,11,0,u.tleft),l(n,14,0,u.tright)})}var $=u(115),B=u(12),J=u(40),W=u(48),X=u(32);u.d(n,"a",function(){return ll});var G=a.X({encapsulation:2,styles:[],data:{}});function q(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,t.b,t.a)),a.Y(1,114688,null,0,e.a,[i.a,a.j,a.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,a._13(n,1)._paused)})}function K(l){return a._22(0,[(l()(),a.Z(0,0,null,null,46,"ion-card",[],null,null,null,null,null)),a.Y(1,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          \n        "])),(l()(),a.Z(3,0,null,null,42,"ion-card-content",[],null,null,null,null,null)),a.Y(4,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(6,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,r.b,r.a)),a.Y(7,1097728,null,3,c.a,[d.a,i.a,a.j,a.z,[2,g.a]],null,null),a._18(335544320,4,{contentLabel:0}),a._18(603979776,5,{_buttons:1}),a._18(603979776,6,{_icons:1}),a.Y(11,16384,null,0,_.a,[],null,null),(l()(),a._20(-1,2,["\n              "])),(l()(),a.Z(13,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(14,16384,[[4,4]],0,p.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" From "])),(l()(),a._20(-1,2,["\n              "])),(l()(),a.Z(17,0,null,3,4,"ion-input",[["name","from"],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.from=u)&&a);return a},m.b,m.a)),a.Y(18,671744,null,0,h.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,h.f,null,[h.h]),a.Y(20,16384,null,0,h.g,[h.f],null,null),a.Y(21,5423104,null,0,f.a,[i.a,b.a,d.a,v.a,a.j,a.z,[2,y.a],[2,c.a],[2,h.f],Y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(24,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,r.b,r.a)),a.Y(25,1097728,null,3,c.a,[d.a,i.a,a.j,a.z,[2,g.a]],null,null),a._18(335544320,7,{contentLabel:0}),a._18(603979776,8,{_buttons:1}),a._18(603979776,9,{_icons:1}),a.Y(29,16384,null,0,_.a,[],null,null),(l()(),a._20(-1,2,["\n              "])),(l()(),a.Z(31,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),a.Y(32,16384,[[7,4]],0,p.a,[i.a,a.j,a.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),a._20(-1,null,[" To "])),(l()(),a._20(-1,2,["\n              "])),(l()(),a.Z(35,0,null,3,4,"ion-input",[["name","to"],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.to=u)&&a);return a},m.b,m.a)),a.Y(36,671744,null,0,h.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),a._17(2048,null,h.f,null,[h.h]),a.Y(38,16384,null,0,h.g,[h.f],null,null),a.Y(39,5423104,null,0,f.a,[i.a,b.a,d.a,v.a,a.j,a.z,[2,y.a],[2,c.a],[2,h.f],Y.a],{type:[0,"type"]},null),(l()(),a._20(-1,2,["\n            "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(42,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.getRange()&&a);return a},C.b,C.a)),a.Y(43,1097728,null,0,Z.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Go"])),(l()(),a._20(-1,null,["\n           \n         "])),(l()(),a._20(-1,null,["\n      "]))],function(l,n){var u=n.component;l(n,18,0,"from",u.from);l(n,21,0,"date");l(n,36,0,"to",u.to);l(n,39,0,"date");l(n,43,0,"")},function(l,n){l(n,17,0,a._13(n,20).ngClassUntouched,a._13(n,20).ngClassTouched,a._13(n,20).ngClassPristine,a._13(n,20).ngClassDirty,a._13(n,20).ngClassValid,a._13(n,20).ngClassInvalid,a._13(n,20).ngClassPending),l(n,35,0,a._13(n,38).ngClassUntouched,a._13(n,38).ngClassTouched,a._13(n,38).ngClassPristine,a._13(n,38).ngClassDirty,a._13(n,38).ngClassValid,a._13(n,38).ngClassInvalid,a._13(n,38).ngClassPending)})}function Q(l){return a._22(0,[a._18(402653184,1,{doughnutCanvas:0}),a._18(402653184,2,{lineCanvas:0}),(l()(),a._20(-1,null,["\n"])),(l()(),a._20(-1,null,["\n\n"])),(l()(),a.Z(4,0,null,null,27,"ion-header",[],null,null,null,null,null)),a.Y(5,16384,null,0,j.a,[i.a,a.j,a.z,[2,k.a]],null,null),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(7,0,null,null,16,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,z.b,z.a)),a.Y(8,49152,null,0,P.a,[v.a,[2,k.a],[2,w.a],i.a,a.j,a.z],{color:[0,"color"]},null),(l()(),a._20(-1,3,["\n        "])),(l()(),a.Z(10,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var t=!0;"click"===n&&(t=!1!==a._13(l,12).toggle()&&t);return t},C.b,C.a)),a.Y(11,1097728,[[3,4]],0,Z.a,[[8,""],i.a,a.j,a.z],null,null),a.Y(12,1064960,null,0,x.a,[D.a,[2,k.a],[2,Z.a],[2,P.a]],{menuToggle:[0,"menuToggle"]},null),a.Y(13,16384,null,1,I.a,[i.a,a.j,a.z,[2,M.a],[2,P.a]],null,null),a._18(603979776,3,{_buttons:1}),(l()(),a._20(-1,0,["\n          "])),(l()(),a.Z(16,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(17,147456,null,0,T.a,[i.a,a.j,a.z],{name:[0,"name"]},null),(l()(),a._20(-1,0,["\n        "])),(l()(),a._20(-1,3,["\n    "])),(l()(),a.Z(20,0,null,3,2,"ion-title",[],null,null,null,F.b,F.a)),a.Y(21,49152,null,0,L.a,[i.a,a.j,a.z,[2,M.a],[2,P.a]],null,null),(l()(),a._20(22,0,["","'s Dashboard"])),(l()(),a._20(-1,3,["\n\n    "])),(l()(),a._20(-1,null,["\n\n    "])),(l()(),a.Z(25,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(-1,null,["\n    "])),(l()(),a.Z(27,0,null,null,3,"div",[["class","offer"],["text-center",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,null,["\n    "])),(l()(),a._20(-1,null,["\n\n\n  "])),(l()(),a._20(-1,null,["\n\n\n"])),(l()(),a.Z(33,0,null,null,203,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,N.b,N.a)),a.Y(34,4374528,null,0,y.a,[i.a,b.a,Y.a,a.j,a.z,v.a,S.a,a.u,[2,k.a],[2,w.a]],null,null),(l()(),a._20(-1,1,["\n    "])),(l()(),a.U(16777216,null,1,1,null,q)),a.Y(37,16384,null,0,A.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n\n    "])),(l()(),a.Z(39,0,null,1,22,"ion-grid",[["class","grid"]],null,null,null,null,null)),a.Y(40,16384,null,0,V.a,[],null,null),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(42,0,null,null,18,"ion-row",[["align-items-center",""],["class","row"],["justify-content-center",""]],null,null,null,null,null)),a.Y(43,16384,null,0,U.a,[],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(45,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.week()&&a);return a},C.b,C.a)),a.Y(46,1097728,null,0,Z.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Week"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(49,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.month()&&a);return a},C.b,C.a)),a.Y(50,1097728,null,0,Z.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Month"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(53,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.year()&&a);return a},C.b,C.a)),a.Y(54,1097728,null,0,Z.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Year"])),(l()(),a._20(-1,null,["\n        "])),(l()(),a.Z(57,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.range()&&a);return a},C.b,C.a)),a.Y(58,1097728,null,0,Z.a,[[8,""],i.a,a.j,a.z],{small:[0,"small"]},null),(l()(),a._20(-1,0,["Range"])),(l()(),a._20(-1,null,["\n  \n    "])),(l()(),a._20(-1,null,["\n  "])),(l()(),a._20(-1,1,["\n  \n      "])),(l()(),a.U(16777216,null,1,1,null,K)),a.Y(64,16384,null,0,A.k,[a.I,a.F],{ngIf:[0,"ngIf"]},null),(l()(),a._20(-1,1,["\n\n\n\n      "])),(l()(),a.Z(66,0,null,1,9,"ion-card",[],null,null,null,null,null)),a.Y(67,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(70,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),a.Y(71,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(73,0,[[1,0],["doughnutCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),a._20(-1,null,["\n          "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,1,["\n        "])),(l()(),a._20(-1,1,["\n        "])),(l()(),a.Z(78,0,null,1,8,"ion-card",[],null,null,null,null,null)),a.Y(79,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(81,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),a.Y(82,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(84,0,[[2,0],["lineCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),a._20(-1,null,["\n          "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,1,["\n        \n        \n        \n        "])),(l()(),a._20(-1,1,["\n        "])),(l()(),a.Z(89,0,null,1,56,"ion-card",[],null,null,null,null,null)),a.Y(90,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(92,0,null,null,52,"ion-card-content",[],null,null,null,null,null)),a.Y(93,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n        \n        "])),(l()(),a.Z(95,0,null,null,48,"ion-grid",[["class","grid"]],null,null,null,null,null)),a.Y(96,16384,null,0,V.a,[],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(98,0,null,null,44,"ion-row",[["class","row"]],null,null,null,null,null)),a.Y(99,16384,null,0,U.a,[],null,null),(l()(),a._20(-1,null,["\n        \n            "])),(l()(),a.Z(101,0,null,null,7,"ion-col",[["class","col"],["col-12",""]],null,null,null,null,null)),a.Y(102,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n              \n              "])),(l()(),a.Z(104,0,null,null,3,"h1",[],null,null,null,null,null)),(l()(),a._20(105,null,["Networth(NGN): "," "])),(l()(),a.Z(106,0,null,null,1,"ion-icon",[["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(107,147456,null,0,T.a,[i.a,a.j,a.z],{color:[0,"color"],name:[1,"name"]},null),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(110,0,null,null,9,"ion-col",[["class","col"],["col-4",""]],null,null,null,null,null)),a.Y(111,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n              "])),(l()(),a.Z(113,0,null,null,5,"h5",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Assets(NGN): "])),(l()(),a.Z(115,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(116,null,[" "," "])),(l()(),a.Z(117,0,null,null,1,"ion-icon",[["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(118,147456,null,0,T.a,[i.a,a.j,a.z],{color:[0,"color"],name:[1,"name"]},null),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(121,0,null,null,9,"ion-col",[["class","col"],["col-4",""]],null,null,null,null,null)),a.Y(122,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n              "])),(l()(),a.Z(124,0,null,null,5,"h5",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Liability(NGN): "])),(l()(),a.Z(126,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(127,null,[" "," "])),(l()(),a.Z(128,0,null,null,1,"ion-icon",[["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(129,147456,null,0,T.a,[i.a,a.j,a.z],{color:[0,"color"],name:[1,"name"]},null),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(132,0,null,null,9,"ion-col",[["class","col"],["col-4",""]],null,null,null,null,null)),a.Y(133,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n              "])),(l()(),a.Z(135,0,null,null,5,"h5",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Pension(NGN): "])),(l()(),a.Z(137,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(138,null,[" "," "])),(l()(),a.Z(139,0,null,null,1,"ion-icon",[["color","primary"],["name","arrow-round-up"],["role","img"]],[[2,"hide",null]],null,null,null,null)),a.Y(140,147456,null,0,T.a,[i.a,a.j,a.z],{color:[0,"color"],name:[1,"name"]},null),(l()(),a._20(-1,null,["\n            "])),(l()(),a._20(-1,null,["\n          "])),(l()(),a._20(-1,null,["\n        \n        "])),(l()(),a._20(-1,null,["\n        \n          "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,1,["\n       \n        "])),(l()(),a.Z(147,0,null,1,52,"ion-card",[],null,null,null,null,null)),a.Y(148,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(150,0,null,null,48,"ion-card-content",[],null,null,null,null,null)),a.Y(151,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n            \n            "])),(l()(),a.Z(153,0,null,null,40,"ion-grid",[["class","grid"]],null,null,null,null,null)),a.Y(154,16384,null,0,V.a,[],null,null),(l()(),a._20(-1,null,["\n              "])),(l()(),a.Z(156,0,null,null,36,"ion-row",[["class","row"]],null,null,null,null,null)),a.Y(157,16384,null,0,U.a,[],null,null),(l()(),a._20(-1,null,["\n                \n                    "])),(l()(),a.Z(159,0,null,null,5,"ion-col",[["class","col"],["col-12",""]],null,null,null,null,null)),a.Y(160,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n                        "])),(l()(),a.Z(162,0,null,null,1,"h1",[["text-center",""]],null,null,null,null,null)),(l()(),a._20(-1,null,["Retirement Plan (NGN) "])),(l()(),a._20(-1,null,["\n                      \n                    "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(166,0,null,null,7,"ion-col",[["class","col"],["col-3",""]],null,null,null,null,null)),a.Y(167,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n                      "])),(l()(),a.Z(169,0,null,null,3,"h5",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Savings: "])),(l()(),a.Z(171,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(172,null,[" "," "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(175,0,null,null,7,"ion-col",[["class","col"],["col-3",""]],null,null,null,null,null)),a.Y(176,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n                      "])),(l()(),a.Z(178,0,null,null,3,"h5",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Expenses: "])),(l()(),a.Z(180,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(181,null,[" "," "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a.Z(184,0,null,null,7,"ion-col",[["class","col"],["col-6",""]],null,null,null,null,null)),a.Y(185,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n                      "])),(l()(),a.Z(187,0,null,null,3,"h5",[],null,null,null,null,null)),(l()(),a._20(188,null,["Annuity For "," Years: "])),(l()(),a.Z(189,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(190,null,[" "," "])),(l()(),a._20(-1,null,["\n                    "])),(l()(),a._20(-1,null,["\n                  "])),(l()(),a._20(-1,null,["\n                \n                "])),(l()(),a._20(-1,null,["\n          \n            "])),(l()(),a._20(-1,null,["\n            "])),(l()(),a.Z(196,0,null,null,1,"progress-bar",[],null,null,null,E,R)),a.Y(197,49152,null,0,H.a,[],{progress:[0,"progress"],progressAge:[1,"progressAge"],tleft:[2,"tleft"],tright:[3,"tright"]},null),(l()(),a._20(-1,null,["\n         \n          "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,1,["\n        \n        \n        "])),(l()(),a.Z(201,0,null,1,0,"hr",[],null,null,null,null,null)),(l()(),a._20(-1,1,["\n        "])),(l()(),a.Z(203,0,null,1,32,"ion-card",[],null,null,null,null,null)),a.Y(204,16384,null,0,o.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n          "])),(l()(),a.Z(206,0,null,null,28,"ion-card-content",[],null,null,null,null,null)),a.Y(207,16384,null,0,s.a,[i.a,a.j,a.z],null,null),(l()(),a._20(-1,null,["\n            \n            "])),(l()(),a.Z(209,0,null,null,24,"ion-grid",[["class","grid"]],null,null,null,null,null)),a.Y(210,16384,null,0,V.a,[],null,null),(l()(),a._20(-1,null,["\n                "])),(l()(),a.Z(212,0,null,null,20,"ion-row",[["class","row"]],null,null,null,null,null)),a.Y(213,16384,null,0,U.a,[],null,null),(l()(),a._20(-1,null,["\n                  \n                \n                  "])),(l()(),a.Z(215,0,null,null,7,"ion-col",[["class","col"],["col-6",""]],null,null,null,null,null)),a.Y(216,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n                      "])),(l()(),a.Z(218,0,null,null,3,"h5",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Amount to Retire(NGN):\n                      "])),(l()(),a.Z(220,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(221,null,["\n                      ",""])),(l()(),a._20(-1,null,["\n                  "])),(l()(),a._20(-1,null,["\n                  "])),(l()(),a.Z(224,0,null,null,7,"ion-col",[["class","col"],["col-6",""]],null,null,null,null,null)),a.Y(225,16384,null,0,O.a,[],null,null),(l()(),a._20(-1,null,["\n                      "])),(l()(),a.Z(227,0,null,null,3,"h5",[],null,null,null,null,null)),(l()(),a._20(-1,null,["Age to Retire(Years):"])),(l()(),a.Z(229,0,null,null,0,"br",[],null,null,null,null,null)),(l()(),a._20(230,null,["",""])),(l()(),a._20(-1,null,["\n                  "])),(l()(),a._20(-1,null,["\n                "])),(l()(),a._20(-1,null,["\n              \n              "])),(l()(),a._20(-1,null,["\n        \n          "])),(l()(),a._20(-1,null,["\n        "])),(l()(),a._20(-1,1,["\n\n\n\n  "])),(l()(),a._20(-1,null,["\n"]))],function(l,n){var u=n.component;l(n,8,0,"secondary");l(n,12,0,"");l(n,17,0,"menu"),l(n,37,0,0==u.loaded);l(n,46,0,"");l(n,50,0,"");l(n,54,0,"");l(n,58,0,""),l(n,64,0,u.drange),l(n,107,0,a._2(1,"",u.nwTrend>0?"primary":"danger",""),a._2(1,"",u.nwTrend>0?"arrow-round-up":"arrow-round-down","")),l(n,118,0,a._2(1,"",u.assetTrend>0?"primary":"danger",""),a._2(1,"",u.assetTrend>0?"arrow-round-up":"arrow-round-down","")),l(n,129,0,a._2(1,"",u.liabTrend>0?"primary":"danger",""),a._2(1,"",u.liabTrend>0?"arrow-round-up":"arrow-round-down",""));l(n,140,0,"primary","arrow-round-up"),l(n,197,0,u.loadProgress,u.loadProgressAge,u.currAge,u.finAge)},function(l,n){var u=n.component;l(n,7,0,a._13(n,8)._hidden,a._13(n,8)._sbPadding),l(n,10,0,a._13(n,12).isHidden),l(n,16,0,a._13(n,17)._hidden),l(n,22,0,u.userName),l(n,33,0,a._13(n,34).statusbarPadding,a._13(n,34)._hasRefresher),l(n,105,0,u.networth),l(n,106,0,a._13(n,107)._hidden),l(n,116,0,u.assets),l(n,117,0,a._13(n,118)._hidden),l(n,127,0,u.liability),l(n,128,0,a._13(n,129)._hidden),l(n,138,0,u.pension),l(n,139,0,a._13(n,140)._hidden),l(n,172,0,u.savings),l(n,181,0,u.expenses),l(n,188,0,u.retYrs),l(n,190,0,u.annuity),l(n,221,0,u.amtRet),l(n,230,0,u.ageRet)})}var ll=a.V("page-dashboard",$.a,function(l){return a._22(0,[(l()(),a.Z(0,0,null,null,1,"page-dashboard",[],null,null,null,Q,G)),a.Y(1,49152,null,0,$.a,[w.a,B.a,J.a,W.a,X.a],null,null)],null,null)},{},{},[])},591:function(l,n,u){"use strict";Object.defineProperty(n,"__esModule",{value:!0});var a=u(72),t=u(0),e=(u(1),u(211)),i=u(244),o=u(229),s=u(238),r=u(219),c=u(139),d=(u(7),u(190)),g=function(){return function(){}}(),_=u(148),p=u(149),m=function(){return function(l){this.navCtrl=l}}(),h=u(39),f=function(){function l(l,n,u,a){var t=this;this.platform=l,this.statusBar=n,this.storage=u,this.splashScreen=a,this.rootPage=m,this.initializeApp(),this.pages=[{title:"Home",component:m},{title:"Dashboard",component:"DashboardPage"},{title:"Gaid Investment",component:"InvestmentPage"},{title:"Analysis",component:"AnalysisPage"},{title:"Budget",component:"BudgetPage"},{title:"Setting",component:"SettingPage"},{title:"Sync",component:"LoadsmstransactionsPage"},{title:"Logout",component:"InitialPage"}],this.storage.get("initialData").then(function(l){var n;null==l||0==l.id?(t.nav.setRoot((n={title:"Sign In",component:"SignupPage"}).component),t.activePage=n):(t.nav.setRoot((n=t.pages[1]).component),t.activePage=n)})}return l.prototype.initializeApp=function(){var l=this;this.platform.ready().then(function(){l.statusBar.styleDefault(),l.splashScreen.hide()})},l.prototype.openPage=function(l){if(this.load(),"Logout"==l.title){this.storage.set("initialData",null);var n={title:"Sign In",component:"SignupPage"};this.nav.setRoot(n.component),this.activePage=n}else this.nav.setRoot(l.component),this.activePage=l},l.prototype.load=function(){var l=this;this.storage.get("initialData").then(function(n){console.log(n),null==n&&l.nav.setRoot("SignupPage")})},l}(),b=u(249),v=u(251),y=u(248),Y=u(265),C=u(234),Z=u(236),j=u(253),k=u(255),z=u(242),P=u(225),w=u(246),x=u(257),D=u(32),I=u(208),M=u(209),T=u(210),F=u(188),L=u(189),N=u(231),S=u(267),A=u(213),V=u(261),U=u(221),O=u(263),H=u(223),R=u(232),E=u(152),$=u(217),B=u(240),J=u(259),W=u(227),X=u(215),G=function(){return function(){}}(),q=u(95),K=u(551),Q=u(552),ll=u(553),nl=u(554),ul=u(555),al=u(556),tl=u(557),el=u(558),il=u(559),ol=u(581),sl=u(582),rl=u(580),cl=u(589),dl=u(573),gl=u(574),_l=u(583),pl=u(584),ml=u(577),hl=u(568),fl=u(579),bl=u(585),vl=u(571),yl=u(590),Yl=u(561),Cl=u(587),Zl=u(566),jl=u(588),kl=u(567),zl=u(572),Pl=u(563),wl=u(576),xl=u(586),Dl=u(26),Il=u(3),Ml=u(6),Tl=u(33),Fl=u(19),Ll=u(9),Nl=u(14),Sl=u(36),Al=u(28),Vl=u(21),Ul=u(24),Ol=u(15),Hl=u(5),Rl=u(10),El=u(17),$l=u(61),Bl=u(18),Jl=u(54),Wl=u(34),Xl=u(29),Gl=u(46),ql=u(49),Kl=u(69),Ql=u(35),ln=u(59),nn=u(84),un=u(20),an=u(16),tn=u(31),en=u(48),on=u(12),sn=u(114),rn=t.X({encapsulation:2,styles:[],data:{}});function cn(l){return t._22(0,[t._18(402653184,1,{email_add:0}),t._18(402653184,2,{card_number:0}),t._18(402653184,3,{expiryMonth:0}),t._18(402653184,4,{expiryYear:0}),t._18(402653184,5,{cvc:0}),(l()(),t._20(-1,null,["\n"])),(l()(),t.Z(6,0,null,null,10,"ion-header",[],null,null,null,null,null)),t.Y(7,16384,null,0,Dl.a,[Il.a,t.j,t.z,[2,Ml.a]],null,null),(l()(),t._20(-1,null,["\n\n    "])),(l()(),t.Z(9,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,Tl.b,Tl.a)),t.Y(10,49152,null,0,Fl.a,[Ll.a,[2,Ml.a],[2,Nl.a],Il.a,t.j,t.z],null,null),(l()(),t._20(-1,3,["\n\n        "])),(l()(),t.Z(12,0,null,3,2,"ion-title",[],null,null,null,Sl.b,Sl.a)),t.Y(13,49152,null,0,Al.a,[Il.a,t.j,t.z,[2,Vl.a],[2,Fl.a]],null,null),(l()(),t._20(-1,0,["Make Payment"])),(l()(),t._20(-1,3,["\n\n    "])),(l()(),t._20(-1,null,["\n\n"])),(l()(),t._20(-1,null,["\n\n"])),(l()(),t.Z(18,0,null,null,59,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Ul.b,Ul.a)),t.Y(19,4374528,null,0,Ol.a,[Il.a,Hl.a,Rl.a,t.j,t.z,Ll.a,El.a,t.u,[2,Ml.a],[2,Nl.a]],null,null),(l()(),t._20(-1,1,["\n\n    "])),(l()(),t.Z(21,0,null,1,1,"div",[["class","image"],["text-center",""]],null,null,null,null,null)),(l()(),t.Z(22,0,null,null,0,"img",[["src","assets/imgs/paystack_logo.png"]],null,null,null,null,null)),(l()(),t._20(-1,1,["\n\n    "])),(l()(),t._20(-1,1,["\n    "])),(l()(),t._20(-1,1,["\n\n    "])),(l()(),t.Z(26,0,null,1,34,"div",[["class","input-area"]],null,null,null,null,null)),(l()(),t._20(-1,null,["\n\n        "])),(l()(),t.Z(28,0,null,null,31,"ion-list",[],null,null,null,null,null)),t.Y(29,16384,null,0,$l.a,[Il.a,t.j,t.z,Hl.a,Bl.l,Rl.a],null,null),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t.Z(31,0,null,null,12,"ion-item",[["class","item item-block"]],null,null,null,Jl.b,Jl.a)),t.Y(32,1097728,null,3,Wl.a,[Xl.a,Il.a,t.j,t.z,[2,Gl.a]],null,null),t._18(335544320,6,{contentLabel:0}),t._18(603979776,7,{_buttons:1}),t._18(603979776,8,{_icons:1}),t.Y(36,16384,null,0,ql.a,[],null,null),(l()(),t._20(-1,2,["\n\n                "])),(l()(),t.Z(38,0,null,3,4,"ion-input",[["placeholder","Email Address"],["type","text"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,u){var a=!0;"ngModelChange"===n&&(a=!1!==(l.component.emailValue=u)&&a);return a},Kl.b,Kl.a)),t.Y(39,671744,null,0,Ql.h,[[8,null],[8,null],[8,null],[8,null]],{model:[0,"model"]},{update:"ngModelChange"}),t._17(2048,null,Ql.f,null,[Ql.h]),t.Y(41,16384,null,0,Ql.g,[Ql.f],null,null),t.Y(42,5423104,[[1,4],["email_add",4]],0,ln.a,[Il.a,Hl.a,Xl.a,Ll.a,t.j,t.z,[2,Ol.a],[2,Wl.a],[2,Ql.f],Rl.a],{type:[0,"type"],placeholder:[1,"placeholder"]},null),(l()(),t._20(-1,2,["\n\n            "])),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t.Z(46,0,null,null,4,"ion-grid",[["class","grid"],["padding",""]],null,null,null,null,null)),t.Y(47,16384,null,0,nn.a,[],null,null),(l()(),t._20(-1,null,["\n\n                "])),(l()(),t._20(-1,null,["\n\n                "])),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t.Z(52,0,null,null,6,"div",[["class","pay-button"]],null,null,null,null,null)),(l()(),t._20(-1,null,["\n\n                "])),(l()(),t._20(-1,null,["\n                "])),(l()(),t.Z(55,0,null,null,2,"button",[["full",""],["ion-button",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.ChargeCard()&&a);return a},un.b,un.a)),t.Y(56,1097728,null,0,an.a,[[8,""],Il.a,t.j,t.z],{full:[0,"full"]},null),(l()(),t._20(57,0,["PAY NGN "," "])),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t._20(-1,null,["\n\n        "])),(l()(),t._20(-1,null,["\n\n    "])),(l()(),t._20(-1,1,["\n\n    "])),(l()(),t.Z(62,0,null,1,14,"div",[["class","footer"]],null,null,null,null,null)),(l()(),t._20(-1,null,["\n\n        "])),(l()(),t.Z(64,0,null,null,4,"div",[["class","secured"]],null,null,null,null,null)),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t.Z(66,0,null,null,1,"ion-icon",[["item-start",""],["name","lock"],["role","img"]],[[2,"hide",null]],null,null,null,null)),t.Y(67,147456,null,0,tn.a,[Il.a,t.j,t.z],{name:[0,"name"]},null),(l()(),t._20(-1,null,["\n\n            SECURED BY PAYSTACK\n\n        "])),(l()(),t._20(-1,null,["\n\n        "])),(l()(),t.Z(70,0,null,null,5,"div",[["class","whatspaystack"]],null,null,null,null,null)),(l()(),t._20(-1,null,["\n\n            "])),(l()(),t.Z(72,0,null,null,2,"button",[["class","app-font-25"],["ion-button",""],["outline",""],["round",""],["small",""]],null,[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==l.component.whatsPaystack()&&a);return a},un.b,un.a)),t.Y(73,1097728,null,0,an.a,[[8,""],Il.a,t.j,t.z],{small:[0,"small"],outline:[1,"outline"],round:[2,"round"]},null),(l()(),t._20(-1,0,[" WHAT IS PAYSTACK?"])),(l()(),t._20(-1,null,["\n\n        "])),(l()(),t._20(-1,null,["\n\n    "])),(l()(),t._20(-1,1,["\n\n"]))],function(l,n){l(n,39,0,n.component.emailValue);l(n,42,0,"text","Email Address");l(n,56,0,"");l(n,67,0,"lock");l(n,73,0,"","","")},function(l,n){var u=n.component;l(n,9,0,t._13(n,10)._hidden,t._13(n,10)._sbPadding),l(n,18,0,t._13(n,19).statusbarPadding,t._13(n,19)._hasRefresher),l(n,38,0,t._13(n,41).ngClassUntouched,t._13(n,41).ngClassTouched,t._13(n,41).ngClassPristine,t._13(n,41).ngClassDirty,t._13(n,41).ngClassValid,t._13(n,41).ngClassInvalid,t._13(n,41).ngClassPending),l(n,57,0,u.price),l(n,66,0,t._13(n,67)._hidden)})}var dn=t.V("page-paystack",d.a,function(l){return t._22(0,[(l()(),t.Z(0,0,null,null,1,"page-paystack",[],null,null,null,cn,rn)),t.Y(1,114688,null,0,d.a,[Nl.a,en.a,on.a,D.a,sn.a,Hl.a,c.a,Ml.a],null,null)],function(l,n){l(n,1,0)},null)},{},{},[]),gn=u(569),_n=u(562),pn=u(564),mn=u(575),hn=u(570),fn=u(578),bn=u(565),vn=u(168),yn=u(30),Yn=u(929),Cn=u(80),Zn=u(131),jn=u(13),kn=u(930),zn=u(100),Pn=u(77),wn=u(43),xn=u(40),Dn=t.X({encapsulation:2,styles:[],data:{}});function In(l){return t._22(0,[(l()(),t.Z(0,0,null,null,7,"ion-item",[["class","menu-item item item-block"],["menuClose",""]],[[2,"highlight",null]],[[null,"click"]],function(l,n,u){var a=!0,e=l.component;"click"===n&&(a=!1!==t._13(l,6).close()&&a);"click"===n&&(a=!1!==e.openPage(l.context.$implicit)&&a);return a},Jl.b,Jl.a)),t.Y(1,1097728,null,3,Wl.a,[Xl.a,Il.a,t.j,t.z,[2,Gl.a]],null,null),t._18(335544320,4,{contentLabel:0}),t._18(603979776,5,{_buttons:1}),t._18(603979776,6,{_icons:1}),t.Y(5,16384,null,0,ql.a,[],null,null),t.Y(6,16384,null,0,vn.a,[yn.a],{menuClose:[0,"menuClose"]},null),(l()(),t._20(7,2,["\n                ","\n            "]))],function(l,n){l(n,6,0,"")},function(l,n){l(n,0,0,n.component.activePage==n.context.$implicit),l(n,7,0,n.context.$implicit.title)})}function Mn(l){return t._22(0,[t._18(402653184,1,{nav:0}),(l()(),t.Z(1,0,null,null,21,"ion-menu",[["role","navigation"]],null,null,null,Yn.b,Yn.a)),t._17(6144,null,Cn.a,null,[Zn.a]),t.Y(3,245760,null,2,Zn.a,[yn.a,t.j,Il.a,Hl.a,t.z,El.a,Bl.l,Rl.a,Ll.a],{content:[0,"content"]},null),t._18(335544320,2,{menuContent:0}),t._18(335544320,3,{menuNav:0}),(l()(),t._20(-1,0,["\n    "])),(l()(),t.Z(7,0,null,0,14,"ion-content",[],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Ul.b,Ul.a)),t.Y(8,4374528,[[2,4]],0,Ol.a,[Il.a,Hl.a,Rl.a,t.j,t.z,Ll.a,El.a,t.u,[2,Ml.a],[2,Nl.a]],null,null),(l()(),t._20(-1,1,["\n        "])),(l()(),t.Z(10,0,null,1,3,"div",[["class","menu-header"],["text-center",""]],null,null,null,null,null)),(l()(),t._20(-1,null,["\n            "])),(l()(),t.Z(12,0,null,null,0,"img",[["alt",""],["src","assets/imgs/logo-gray.png"]],null,null,null,null,null)),(l()(),t._20(-1,null,["\n        "])),(l()(),t._20(-1,1,["\n\n        "])),(l()(),t.Z(15,0,null,1,5,"ion-list",[["no-lines",""]],null,null,null,null,null)),t.Y(16,16384,null,0,$l.a,[Il.a,t.j,t.z,Hl.a,Bl.l,Rl.a],null,null),(l()(),t._20(-1,null,["\n            "])),(l()(),t.U(16777216,null,null,1,null,In)),t.Y(19,802816,null,0,jn.j,[t.I,t.F,t.p],{ngForOf:[0,"ngForOf"]},null),(l()(),t._20(-1,null,["\n        "])),(l()(),t._20(-1,1,["\n    "])),(l()(),t._20(-1,0,["\n"])),(l()(),t._20(-1,null,["\n\n"])),(l()(),t._20(-1,null,["\n"])),(l()(),t.Z(25,0,null,null,2,"ion-nav",[["swipeBackEnabled","false"]],null,null,null,kn.b,kn.a)),t._17(6144,null,Cn.a,null,[zn.a]),t.Y(27,4374528,[[1,4],["content",4]],0,zn.a,[[2,Ml.a],[2,Nl.a],Ll.a,Il.a,Hl.a,t.j,t.u,t.z,t.i,Bl.l,Pn.a,[2,wn.a],Rl.a,t.k],{swipeBackEnabled:[0,"swipeBackEnabled"],root:[1,"root"]},null)],function(l,n){var u=n.component;l(n,3,0,t._13(n,27)),l(n,19,0,u.pages);l(n,27,0,"false",u.rootPage)},function(l,n){l(n,7,0,t._13(n,8).statusbarPadding,t._13(n,8)._hasRefresher)})}var Tn=t.V("ng-component",f,function(l){return t._22(0,[(l()(),t.Z(0,0,null,null,1,"ng-component",[],null,null,null,Mn,Dn)),t.Y(1,49152,null,0,f,[Hl.a,p.a,xn.a,_.a],null,null)],null,null)},{},{},[]),Fn=u(41),Ln=u(42),Nn=t.X({encapsulation:2,styles:[],data:{}});function Sn(l){return t._22(0,[(l()(),t.Z(0,0,null,null,16,"ion-header",[],null,null,null,null,null)),t.Y(1,16384,null,0,Dl.a,[Il.a,t.j,t.z,[2,Ml.a]],null,null),(l()(),t._20(-1,null,["\n    "])),(l()(),t.Z(3,0,null,null,12,"ion-navbar",[["class","toolbar"],["color","secondary"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,Tl.b,Tl.a)),t.Y(4,49152,null,0,Fl.a,[Ll.a,[2,Ml.a],[2,Nl.a],Il.a,t.j,t.z],{color:[0,"color"]},null),(l()(),t._20(-1,3,["\n        "])),(l()(),t.Z(6,0,null,0,8,"button",[["ion-button",""],["menuToggle",""]],[[8,"hidden",0]],[[null,"click"]],function(l,n,u){var a=!0;"click"===n&&(a=!1!==t._13(l,8).toggle()&&a);return a},un.b,un.a)),t.Y(7,1097728,[[1,4]],0,an.a,[[8,""],Il.a,t.j,t.z],null,null),t.Y(8,1064960,null,0,Fn.a,[yn.a,[2,Ml.a],[2,an.a],[2,Fl.a]],{menuToggle:[0,"menuToggle"]},null),t.Y(9,16384,null,1,Ln.a,[Il.a,t.j,t.z,[2,Vl.a],[2,Fl.a]],null,null),t._18(603979776,1,{_buttons:1}),(l()(),t._20(-1,0,["\n      "])),(l()(),t.Z(12,0,null,0,1,"ion-icon",[["name","menu"],["role","img"]],[[2,"hide",null]],null,null,null,null)),t.Y(13,147456,null,0,tn.a,[Il.a,t.j,t.z],{name:[0,"name"]},null),(l()(),t._20(-1,0,["\n    "])),(l()(),t._20(-1,3,["\n    "])),(l()(),t._20(-1,null,["\n"])),(l()(),t._20(-1,null,["\n\n"])),(l()(),t.Z(18,0,null,null,4,"ion-content",[["text-center",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,Ul.b,Ul.a)),t.Y(19,4374528,null,0,Ol.a,[Il.a,Hl.a,Rl.a,t.j,t.z,Ll.a,El.a,t.u,[2,Ml.a],[2,Nl.a]],null,null),(l()(),t._20(-1,1,["\n    "])),(l()(),t.Z(21,0,null,1,0,"img",[["alt","Logo"],["src","assets/imgs/logo-gray.png"]],null,null,null,null,null)),(l()(),t._20(-1,1,["\n"]))],function(l,n){l(n,4,0,"secondary");l(n,8,0,"");l(n,13,0,"menu")},function(l,n){l(n,3,0,t._13(n,4)._hidden,t._13(n,4)._sbPadding),l(n,6,0,t._13(n,8).isHidden),l(n,12,0,t._13(n,13)._hidden),l(n,18,0,t._13(n,19).statusbarPadding,t._13(n,19)._hasRefresher)})}var An=t.V("page-home",m,function(l){return t._22(0,[(l()(),t.Z(0,0,null,null,1,"page-home",[],null,null,null,Sn,Nn)),t.Y(1,49152,null,0,m,[Nl.a],null,null)],null,null)},{},{},[]),Vn=u(171),Un=u(161),On=u(170),Hn=u(79),Rn=u(206),En=u(97),$n=u(89),Bn=u(175),Jn=u(127),Wn=u(178),Xn=u(173),Gn=u(70),qn=u(550),Kn=u(172),Qn=u(169),lu=u(174),nu=u(151),uu=t.W(G,[q.b],function(l){return t._10([t._11(512,t.i,t.S,[[8,[K.a,Q.a,ll.a,nl.a,ul.a,al.a,tl.a,el.a,il.a,ol.a,sl.a,rl.a,cl.a,dl.a,gl.a,_l.a,pl.a,ml.a,hl.a,fl.a,bl.a,vl.a,yl.a,Yl.a,Cl.a,Zl.a,jl.a,kl.a,zl.a,Pl.a,wl.a,xl.a,dn,gn.a,_n.a,pn.a,mn.a,hn.a,fn.a,bn.a,Tn,An]],[3,t.i],t.s]),t._11(5120,t.r,t._9,[[3,t.r]]),t._11(4608,jn.m,jn.l,[t.r,[2,jn.u]]),t._11(5120,t.b,t._0,[]),t._11(5120,t.p,t._6,[]),t._11(5120,t.q,t._7,[]),t._11(4608,a.c,a.q,[jn.d]),t._11(6144,t.D,null,[a.c]),t._11(4608,a.f,Vn.a,[]),t._11(5120,a.d,function(l,n,u,t,e){return[new a.k(l,n),new a.o(u),new a.n(t,e)]},[jn.d,t.u,jn.d,jn.d,a.f]),t._11(4608,a.e,a.e,[a.d,t.u]),t._11(135680,a.m,a.m,[jn.d]),t._11(4608,a.l,a.l,[a.e,a.m]),t._11(6144,t.B,null,[a.l]),t._11(6144,a.p,null,[a.m]),t._11(4608,t.G,t.G,[t.u]),t._11(4608,a.h,a.h,[jn.d]),t._11(4608,a.i,a.i,[jn.d]),t._11(4608,Ql.k,Ql.k,[]),t._11(4608,Ql.c,Ql.c,[]),t._11(4608,F.c,F.c,[]),t._11(4608,F.g,F.b,[]),t._11(5120,F.i,F.j,[]),t._11(4608,F.h,F.h,[F.c,F.g,F.i]),t._11(4608,F.f,F.a,[]),t._11(5120,F.d,F.k,[F.h,F.f]),t._11(4608,Un.a,Un.a,[Ll.a,Il.a]),t._11(4608,en.a,en.a,[Ll.a,Il.a]),t._11(4608,On.a,On.a,[]),t._11(4608,Xl.a,Xl.a,[]),t._11(4608,Hn.a,Hn.a,[Hl.a]),t._11(4608,El.a,El.a,[Il.a,Hl.a,t.u,Rl.a]),t._11(4608,sn.a,sn.a,[Ll.a,Il.a]),t._11(5120,jn.h,Rn.c,[jn.s,[2,jn.a],Il.a]),t._11(4608,jn.g,jn.g,[jn.h]),t._11(5120,En.b,En.d,[Ll.a,En.a]),t._11(5120,wn.a,wn.b,[Ll.a,En.b,jn.g,$n.b,t.i]),t._11(4608,Bn.a,Bn.a,[Ll.a,Il.a,wn.a]),t._11(4608,Jn.a,Jn.a,[Ll.a,Il.a]),t._11(4608,Wn.a,Wn.a,[Ll.a,Il.a,wn.a]),t._11(4608,Xn.a,Xn.a,[Il.a,Hl.a,Rl.a,Ll.a,Bl.l]),t._11(4608,Gn.a,Gn.a,[Ll.a,Il.a]),t._11(4608,Pn.a,Pn.a,[Hl.a,Il.a]),t._11(5120,xn.a,xn.c,[xn.b]),t._11(4608,p.a,p.a,[]),t._11(4608,_.a,_.a,[]),t._11(4608,I.a,I.a,[]),t._11(4608,M.a,M.a,[]),t._11(4608,T.a,T.a,[]),t._11(4608,L.a,L.a,[]),t._11(4608,D.a,D.a,[F.d,en.a,L.a]),t._11(4608,c.a,c.a,[]),t._11(512,jn.b,jn.b,[]),t._11(512,t.k,qn.a,[]),t._11(256,Il.b,{mode:"ios",backButtonText:""},[]),t._11(1024,Kn.a,Kn.b,[]),t._11(1024,Hl.a,Hl.b,[a.b,Kn.a,t.u]),t._11(1024,Il.a,Il.c,[Il.b,Hl.a]),t._11(512,Rl.a,Rl.a,[Hl.a]),t._11(512,yn.a,yn.a,[]),t._11(512,Ll.a,Ll.a,[Il.a,Hl.a,[2,yn.a]]),t._11(512,Bl.l,Bl.l,[Ll.a]),t._11(256,En.a,{links:[{loadChildren:"../pages/_projectsbudget/projectsbudget.module.ngfactory#ProjectsbudgetPageModuleNgFactory",name:"ProjectsbudgetPage",segment:"projectsbudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/analysis/analysis.module.ngfactory#AnalysisPageModuleNgFactory",name:"AnalysisPage",segment:"analysis",priority:"low",defaultHistory:[]},{loadChildren:"../pages/banking/banking.module.ngfactory#BankingPageModuleNgFactory",name:"BankingPage",segment:"banking",priority:"low",defaultHistory:[]},{loadChildren:"../pages/budget/budget.module.ngfactory#BudgetPageModuleNgFactory",name:"BudgetPage",segment:"budget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/cards/cards.module.ngfactory#CardsPageModuleNgFactory",name:"CardsPage",segment:"cards",priority:"low",defaultHistory:[]},{loadChildren:"../pages/credit-card/credit-card.module.ngfactory#CreditCardPageModuleNgFactory",name:"CreditCardPage",segment:"credit-card",priority:"low",defaultHistory:[]},{loadChildren:"../pages/addtransactions/addtransactions.module.ngfactory#AddtransactionsPageModuleNgFactory",name:"AddtransactionsPage",segment:"addtransactions",priority:"low",defaultHistory:[]},{loadChildren:"../pages/current/current.module.ngfactory#CurrentPageModuleNgFactory",name:"CurrentPage",segment:"current",priority:"low",defaultHistory:[]},{loadChildren:"../pages/edittransactions/edittransactions.module.ngfactory#EdittransactionsPageModuleNgFactory",name:"EdittransactionsPage",segment:"edittransactions",priority:"low",defaultHistory:[]},{loadChildren:"../pages/favorites/favorites.module.ngfactory#FavoritesPageModuleNgFactory",name:"FavoritesPage",segment:"favorites",priority:"low",defaultHistory:[]},{loadChildren:"../pages/fin-info/fin-info.module.ngfactory#FinInfoPageModuleNgFactory",name:"FinInfoPage",segment:"fin-info",priority:"low",defaultHistory:[]},{loadChildren:"../pages/fixed-deposit/fixed-deposit.module.ngfactory#FixedDepositPageModuleNgFactory",name:"FixedDepositPage",segment:"fixed-deposit",priority:"low",defaultHistory:[]},{loadChildren:"../pages/incomebudget/incomebudget.module.ngfactory#IncomebudgetPageModuleNgFactory",name:"IncomebudgetPage",segment:"incomebudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/foreign/foreign.module.ngfactory#ForeignPageModuleNgFactory",name:"ForeignPage",segment:"foreign",priority:"low",defaultHistory:[]},{loadChildren:"../pages/forex-card/forex-card.module.ngfactory#ForexCardPageModuleNgFactory",name:"ForexCardPage",segment:"forex-card",priority:"low",defaultHistory:[]},{loadChildren:"../pages/initial/initial.module.ngfactory#InitialPageModuleNgFactory",name:"InitialPage",segment:"initial",priority:"low",defaultHistory:[]},{loadChildren:"../pages/investment/investment.module.ngfactory#InvestmentPageModuleNgFactory",name:"InvestmentPage",segment:"investment",priority:"low",defaultHistory:[]},{loadChildren:"../pages/liabilitiesbudget/liabilitiesbudget.module.ngfactory#LiabilitiesbudgetPageModuleNgFactory",name:"LiabilitiesbudgetPage",segment:"liabilitiesbudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/menu-one/menu-one.module.ngfactory#MenuOnePageModuleNgFactory",name:"MenuOnePage",segment:"menu-one",priority:"low",defaultHistory:[]},{loadChildren:"../pages/menu-two/menu-two.module.ngfactory#MenuTwoPageModuleNgFactory",name:"MenuTwoPage",segment:"menu-two",priority:"low",defaultHistory:[]},{loadChildren:"../pages/naira-card/naira-card.module.ngfactory#NairaCardPageModuleNgFactory",name:"NairaCardPage",segment:"naira-card",priority:"low",defaultHistory:[]},{loadChildren:"../pages/managebudget/managebudget.module.ngfactory#ManagebudgetPageModuleNgFactory",name:"ManagebudgetPage",segment:"managebudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/order-details/order-details.module.ngfactory#OrderDetailsPageModuleNgFactory",name:"OrderDetailsPage",segment:"order-details",priority:"low",defaultHistory:[]},{loadChildren:"../pages/pay-conf/pay-conf.module.ngfactory#PayConfPageModuleNgFactory",name:"PayConfPage",segment:"pay-conf",priority:"low",defaultHistory:[]},{loadChildren:"../pages/prepaid-card/prepaid-card.module.ngfactory#PrepaidCardPageModuleNgFactory",name:"PrepaidCardPage",segment:"prepaid-card",priority:"low",defaultHistory:[]},{loadChildren:"../pages/profile/profile.module.ngfactory#ProfilePageModuleNgFactory",name:"ProfilePage",segment:"profile",priority:"low",defaultHistory:[]},{loadChildren:"../pages/savings/savings.module.ngfactory#SavingsPageModuleNgFactory",name:"SavingsPage",segment:"savings",priority:"low",defaultHistory:[]},{loadChildren:"../pages/signup/signup.module.ngfactory#SignupPageModuleNgFactory",name:"SignupPage",segment:"signup",priority:"low",defaultHistory:[]},{loadChildren:"../pages/splash-one/splash-one.module.ngfactory#SplashOnePageModuleNgFactory",name:"SplashOnePage",segment:"splash-one",priority:"low",defaultHistory:[]},{loadChildren:"../pages/splash-two/splash-two.module.ngfactory#SplashTwoPageModuleNgFactory",name:"SplashTwoPage",segment:"splash-two",priority:"low",defaultHistory:[]},{loadChildren:"../pages/sub-menu-one/sub-menu-one.module.ngfactory#SubMenuOnePageModuleNgFactory",name:"SubMenuOnePage",segment:"sub-menu-one",priority:"low",defaultHistory:[]},{loadChildren:"../pages/sub-menu-two/sub-menu-two.module.ngfactory#SubMenuTwoPageModuleNgFactory",name:"SubMenuTwoPage",segment:"sub-menu-two",priority:"low",defaultHistory:[]},{loadChildren:"../pages/setting/setting.module.ngfactory#SettingPageModuleNgFactory",name:"SettingPage",segment:"setting",priority:"low",defaultHistory:[]},{loadChildren:"../pages/viewbudget/viewbudget.module.ngfactory#ViewbudgetPageModuleNgFactory",name:"ViewbudgetPage",segment:"viewbudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/viewanalytics/viewanalytics.module.ngfactory#ViewanalyticsPageModuleNgFactory",name:"ViewanalyticsPage",segment:"viewanalytics",priority:"low",defaultHistory:[]},{loadChildren:"../pages/viewtransactions/viewtransactions.module.ngfactory#ViewtransactionsPageModuleNgFactory",name:"ViewtransactionsPage",segment:"viewtransactions",priority:"low",defaultHistory:[]},{loadChildren:"../pages/walkthrough/walkthrough.module.ngfactory#WalkthroughPageModuleNgFactory",name:"WalkthroughPage",segment:"walkthrough",priority:"low",defaultHistory:[]},{loadChildren:"../pages/expense/expense.module.ngfactory#ExpensePageModuleNgFactory",name:"ExpensePage",segment:"expense",priority:"low",defaultHistory:[]},{loadChildren:"../pages/expensebudget/expensebudget.module.ngfactory#ExpensebudgetPageModuleNgFactory",name:"ExpensebudgetPage",segment:"expensebudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/liabilities/liabilities.module.ngfactory#LiabilitiesPageModuleNgFactory",name:"LiabilitiesPage",segment:"liabilities",priority:"low",defaultHistory:[]},{loadChildren:"../pages/debtbudget/debtbudget.module.ngfactory#DebtbudgetPageModuleNgFactory",name:"DebtbudgetPage",segment:"debtbudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/projects/projects.module.ngfactory#ProjectsPageModuleNgFactory",name:"ProjectsPage",segment:"projects",priority:"low",defaultHistory:[]},{loadChildren:"../pages/projectbudget/projectbudget.module.ngfactory#ProjectbudgetPageModuleNgFactory",name:"ProjectbudgetPage",segment:"projectbudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/loadsmstransactions/loadsmstransactions.module.ngfactory#LoadsmstransactionsPageModuleNgFactory",name:"LoadsmstransactionsPage",segment:"loadsmstransactions",priority:"low",defaultHistory:[]},{loadChildren:"../pages/sav-inv/sav-inv.module.ngfactory#SavInvPageModuleNgFactory",name:"SavInvPage",segment:"sav-inv",priority:"low",defaultHistory:[]},{loadChildren:"../pages/sav-invbudget/sav-invbudget.module.ngfactory#SavInvbudgetPageModuleNgFactory",name:"SavInvbudgetPage",segment:"sav-invbudget",priority:"low",defaultHistory:[]},{loadChildren:"../pages/dashboard/dashboard.module.ngfactory#DashboardPageModuleNgFactory",name:"DashboardPage",segment:"dashboard",priority:"low",defaultHistory:[]},{loadChildren:"../pages/income/income.module.ngfactory#IncomePageModuleNgFactory",name:"IncomePage",segment:"income",priority:"low",defaultHistory:[]}]},[]),t._11(512,t.h,t.h,[]),t._11(512,Qn.a,Qn.a,[t.h]),t._11(1024,$n.b,$n.c,[Qn.a,t.o]),t._11(1024,t.c,function(l,n,u,t,e,i,o,s,r,c,d,g,_){return[a.s(l),lu.a(n),On.b(u,t),Xn.b(e,i,o,s,r),$n.d(c,d,g,_)]},[[2,t.t],Il.a,Hl.a,Rl.a,Il.a,Hl.a,Rl.a,Ll.a,Bl.l,Il.a,En.a,$n.b,t.u]),t._11(512,t.d,t.d,[[2,t.c]]),t._11(131584,t.f,t.f,[t.u,t.T,t.o,t.k,t.i,t.d]),t._11(512,t.e,t.e,[t.f]),t._11(512,a.a,a.a,[[3,a.a]]),t._11(512,Ql.j,Ql.j,[]),t._11(512,Ql.d,Ql.d,[]),t._11(512,Ql.i,Ql.i,[]),t._11(512,Rn.a,Rn.a,[]),t._11(512,h.a,h.a,[]),t._11(512,Rn.b,Rn.b,[]),t._11(512,b.a,b.a,[]),t._11(512,v.a,v.a,[]),t._11(512,y.a,y.a,[]),t._11(512,Y.a,Y.a,[]),t._11(512,C.a,C.a,[]),t._11(512,Z.a,Z.a,[]),t._11(512,j.a,j.a,[]),t._11(512,k.a,k.a,[]),t._11(512,z.a,z.a,[]),t._11(512,P.a,P.a,[]),t._11(512,w.a,w.a,[]),t._11(512,x.a,x.a,[]),t._11(512,N.a,N.a,[]),t._11(512,E.a,E.a,[]),t._11(512,S.a,S.a,[]),t._11(512,A.a,A.a,[]),t._11(512,V.a,V.a,[]),t._11(512,U.a,U.a,[]),t._11(512,O.a,O.a,[]),t._11(512,H.a,H.a,[]),t._11(512,R.a,R.a,[]),t._11(512,$.a,$.a,[]),t._11(512,B.a,B.a,[]),t._11(512,J.a,J.a,[]),t._11(512,F.e,F.e,[]),t._11(512,g,g,[]),t._11(512,W.a,W.a,[]),t._11(512,X.a,X.a,[]),t._11(512,r.a,r.a,[]),t._11(512,s.a,s.a,[]),t._11(512,o.a,o.a,[]),t._11(512,i.a,i.a,[]),t._11(512,e.a,e.a,[]),t._11(512,G,G,[]),t._11(256,$n.a,nu.a,[]),t._11(256,q.a,f,[]),t._11(256,jn.a,"/",[]),t._11(256,xn.b,null,[])])});Object(t.M)(),Object(a.j)().bootstrapModuleFactory(uu)},62:function(l,n,u){"use strict";u.d(n,"a",function(){return e});u(1);var a=u(150),t=(u(7),u(115)),e=(u(39),function(){function l(l,n,u,a,t){this.navCtrl=l,this.grsp=n,this.alertCtrl=u,this.storage=a,this.navParams=t,this.initialData={id:0,name:"",dob:"",dow:"",pen:"",savInv:"",exp:"",inc:"",ret:"",email:"",pword:"",cpword:""},this.screen="signin",this.login={email:"",pword:""},this.loaded=1}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad SignupPage")},l.prototype.setApiJson=function(){this.apiData={email:this.login.email,pword:this.login.pword}},l.prototype.loginCheck=function(){var l=this;""!=this.login.email&&""!=this.login.pword?(this.setApiJson(),this.loaded=0,console.log(this.apiData),this.grsp.login(this.apiData).then(function(n){if(console.log("data"),console.log(n),console.log(" data"),console.log(n.data),l.apiRes=JSON.parse(n.data),console.log(" apires"),console.log(l.apiRes),l.loaded=1,1==l.apiRes.response){var u=JSON.parse(l.apiRes.loginRes)[0];console.log(u),l.initialData={id:u.user_id,name:u.user_name,dob:u.user_dob,dow:u.user_dow,pen:u.user_pension,savInv:u.user_savInv,exp:u.user_lastMonthExp,inc:u.user_lastMonthInc,ret:u.user_retAge,email:u.user_email,pword:u.user_pword,cpword:u.user_pword},l.storage.set("initialData",l.initialData),console.log(l.initialData),l.alertCtrl.create({title:"Success",subTitle:"Login Successful",buttons:["Dismiss"]}).present(),l.navCtrl.setRoot(t.a)}else{l.alertCtrl.create({title:"Error",subTitle:"Something is wrong, Please try again",buttons:["Dismiss"]}).present()}}).catch(function(n){l.loaded=1,console.log(n),l.alertCtrl.create({title:"Error",subTitle:n,buttons:["Dismiss"]}).present()})):this.alertCtrl.create({title:"Error",subTitle:"Please Enter Correct Login Details",buttons:["Dismiss"]}).present()},l.prototype.signUp=function(){this.navCtrl.push(a.a,{})},l}())},630:function(l,n,u){var a={"./af":337,"./af.js":337,"./ar":338,"./ar-dz":339,"./ar-dz.js":339,"./ar-kw":340,"./ar-kw.js":340,"./ar-ly":341,"./ar-ly.js":341,"./ar-ma":342,"./ar-ma.js":342,"./ar-sa":343,"./ar-sa.js":343,"./ar-tn":344,"./ar-tn.js":344,"./ar.js":338,"./az":345,"./az.js":345,"./be":346,"./be.js":346,"./bg":347,"./bg.js":347,"./bm":348,"./bm.js":348,"./bn":349,"./bn.js":349,"./bo":350,"./bo.js":350,"./br":351,"./br.js":351,"./bs":352,"./bs.js":352,"./ca":353,"./ca.js":353,"./cs":354,"./cs.js":354,"./cv":355,"./cv.js":355,"./cy":356,"./cy.js":356,"./da":357,"./da.js":357,"./de":358,"./de-at":359,"./de-at.js":359,"./de-ch":360,"./de-ch.js":360,"./de.js":358,"./dv":361,"./dv.js":361,"./el":362,"./el.js":362,"./en-au":363,"./en-au.js":363,"./en-ca":364,"./en-ca.js":364,"./en-gb":365,"./en-gb.js":365,"./en-ie":366,"./en-ie.js":366,"./en-il":367,"./en-il.js":367,"./en-nz":368,"./en-nz.js":368,"./eo":369,"./eo.js":369,"./es":370,"./es-do":371,"./es-do.js":371,"./es-us":372,"./es-us.js":372,"./es.js":370,"./et":373,"./et.js":373,"./eu":374,"./eu.js":374,"./fa":375,"./fa.js":375,"./fi":376,"./fi.js":376,"./fo":377,"./fo.js":377,"./fr":378,"./fr-ca":379,"./fr-ca.js":379,"./fr-ch":380,"./fr-ch.js":380,"./fr.js":378,"./fy":381,"./fy.js":381,"./gd":382,"./gd.js":382,"./gl":383,"./gl.js":383,"./gom-latn":384,"./gom-latn.js":384,"./gu":385,"./gu.js":385,"./he":386,"./he.js":386,"./hi":387,"./hi.js":387,"./hr":388,"./hr.js":388,"./hu":389,"./hu.js":389,"./hy-am":390,"./hy-am.js":390,"./id":391,"./id.js":391,"./is":392,"./is.js":392,"./it":393,"./it.js":393,"./ja":394,"./ja.js":394,"./jv":395,"./jv.js":395,"./ka":396,"./ka.js":396,"./kk":397,"./kk.js":397,"./km":398,"./km.js":398,"./kn":399,"./kn.js":399,"./ko":400,"./ko.js":400,"./ky":401,"./ky.js":401,"./lb":402,"./lb.js":402,"./lo":403,"./lo.js":403,"./lt":404,"./lt.js":404,"./lv":405,"./lv.js":405,"./me":406,"./me.js":406,"./mi":407,"./mi.js":407,"./mk":408,"./mk.js":408,"./ml":409,"./ml.js":409,"./mn":410,"./mn.js":410,"./mr":411,"./mr.js":411,"./ms":412,"./ms-my":413,"./ms-my.js":413,"./ms.js":412,"./mt":414,"./mt.js":414,"./my":415,"./my.js":415,"./nb":416,"./nb.js":416,"./ne":417,"./ne.js":417,"./nl":418,"./nl-be":419,"./nl-be.js":419,"./nl.js":418,"./nn":420,"./nn.js":420,"./pa-in":421,"./pa-in.js":421,"./pl":422,"./pl.js":422,"./pt":423,"./pt-br":424,"./pt-br.js":424,"./pt.js":423,"./ro":425,"./ro.js":425,"./ru":426,"./ru.js":426,"./sd":427,"./sd.js":427,"./se":428,"./se.js":428,"./si":429,"./si.js":429,"./sk":430,"./sk.js":430,"./sl":431,"./sl.js":431,"./sq":432,"./sq.js":432,"./sr":433,"./sr-cyrl":434,"./sr-cyrl.js":434,"./sr.js":433,"./ss":435,"./ss.js":435,"./sv":436,"./sv.js":436,"./sw":437,"./sw.js":437,"./ta":438,"./ta.js":438,"./te":439,"./te.js":439,"./tet":440,"./tet.js":440,"./tg":441,"./tg.js":441,"./th":442,"./th.js":442,"./tl-ph":443,"./tl-ph.js":443,"./tlh":444,"./tlh.js":444,"./tr":445,"./tr.js":445,"./tzl":446,"./tzl.js":446,"./tzm":447,"./tzm-latn":448,"./tzm-latn.js":448,"./tzm.js":447,"./ug-cn":449,"./ug-cn.js":449,"./uk":450,"./uk.js":450,"./ur":451,"./ur.js":451,"./uz":452,"./uz-latn":453,"./uz-latn.js":453,"./uz.js":452,"./vi":454,"./vi.js":454,"./x-pseudo":455,"./x-pseudo.js":455,"./yo":456,"./yo.js":456,"./zh-cn":457,"./zh-cn.js":457,"./zh-hk":458,"./zh-hk.js":458,"./zh-tw":459,"./zh-tw.js":459};function t(l){return u(e(l))}function e(l){var n=a[l];if(!(n+1))throw new Error("Cannot find module '"+l+"'.");return n}t.keys=function(){return Object.keys(a)},t.resolve=e,l.exports=t,t.id=630}},[591]);
=======
webpackJsonp([18],{

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashboardPage = (function () {
    function DashboardPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 0;
        this.networth = 0;
        this.assets = 0;
        this.liability = 0;
        this.pension = 0;
        this.amtRet = 0;
        this.ageRet = 0;
        this.drange = false;
        this.loadProgress = 0;
        this.loadProgressAge = 0;
        this.currAge = 0;
        this.finAge = 90;
        // getUserName(){
        //   this.userName = "Tayo";
        // }
        this.anotherSav = 0;
        this.pieValues = [];
        // lineLabels:any;
        this.lineIncomeValues = [];
        this.lineExpenseValues = [];
    }
    // initialData = {
    //   id:0,
    //   name:'',
    //   dob:'',
    //   dow:'',
    //   pen:'',
    //   savInv:'',
    //   exp:'',
    //   inc:'',
    //   ret:'',
    //   // desc:'',
    //   // date:'',
    // };
    DashboardPage.prototype.ionViewWillEnter = function () {
        // this.load();
        var tday = __WEBPACK_IMPORTED_MODULE_6_moment__().format("YYYY-MM-DD").toString();
        // console.log(tday);
        // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
        var yday = __WEBPACK_IMPORTED_MODULE_6_moment__().subtract(1, 'month').format("YYYY-MM-DD").toString();
        // console.log(yday);
        this.end = tday;
        this.start = yday;
        this.load();
        console.log('ionViewWillEnter DashboardPage');
        // this.initialData = this.navParams.get('val');
        this.getUserName();
    };
    DashboardPage.prototype.ionViewDidLoad = function () {
        var tday = __WEBPACK_IMPORTED_MODULE_6_moment__().format("YYYY-MM-DD").toString();
        // console.log(tday);
        // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
        var yday = __WEBPACK_IMPORTED_MODULE_6_moment__().subtract(1, 'month').format("YYYY-MM-DD").toString();
        // console.log(yday);
        this.end = tday;
        this.start = yday;
        this.load();
        console.log('ionViewDidLoad DashboardPage');
        // this.initialData = this.navParams.get('val');
        this.getUserName();
        // this.doNut();
        // this.liNe();
        // this.apiLoad();
        // this.retirement();
        // });
    };
    DashboardPage.prototype.clearInit = function () {
        // this.storage.set('initialData', null);
        // this.navCtrl.push(InitialPage, {
        //   val: 'I am View Ana-lytics'
        // })
    };
    DashboardPage.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
    };
    DashboardPage.prototype.week = function () {
        var weekStart = __WEBPACK_IMPORTED_MODULE_6_moment__().startOf('isoWeek').format('YYYY-MM-DD HH:mm:ss');
        var weekEnd = __WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(weekStart, weekEnd);
    };
    DashboardPage.prototype.month = function () {
        var start = __WEBPACK_IMPORTED_MODULE_6_moment__().startOf('month').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    DashboardPage.prototype.year = function () {
        var start = __WEBPACK_IMPORTED_MODULE_6_moment__().startOf('year').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    DashboardPage.prototype.range = function () {
        this.drange = !this.drange;
    };
    DashboardPage.prototype.getRange = function () {
        this.getTransactionsApi(this.from, this.to);
    };
    DashboardPage.prototype.apiLoad = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        this.grsp.dashboardLoad({ "uId": this.uId }).then(function (data) {
            // console.log(data);
            _this.dash = data;
            _this.assets = _this.numberWithCommas(parseInt(_this.dash.rassets));
            _this.liability = _this.numberWithCommas(parseInt(_this.dash.rliabilities));
            // this.networth = this.dash.networth;
            _this.networth = parseInt(_this.dash.rassets) - parseInt(_this.dash.rliabilities);
            _this.networth = _this.numberWithCommas(_this.networth);
            // this.pension = this.numberWithCommas(parseInt(this.dash.pension));
            // this.amtRet = this.dash.retirementTarget;
            // this.ageRet = this.dash.retirementAge;
            _this.assetTrend = parseInt(_this.dash.rassets) - parseInt(_this.dash.oassets);
            _this.liabTrend = parseInt(_this.dash.rliabilities) - parseInt(_this.dash.oliabilities);
            var onetworth = parseInt(_this.dash.oassets) - parseInt(_this.dash.oliabilities);
            _this.nwTrend = _this.networth - onetworth;
            // console.log(parseInt(this.dash.rassets));
            // console.log(parseInt(this.dash.oassets));
            // console.log(parseInt(this.dash.rliabilities));
            // console.log(parseInt(this.dash.oliabilities));
            // console.log(this.assetTrend);
            // console.log(this.liabTrend);
            // console.log(this.nwTrend);
            _this.donutLabels = _this.dash.donutLabels.split(",");
            // this.donutLabels = this.donutLabels.split(",");
            _this.donutData = _this.dash.donutData.split(",");
            _this.lineLabels = _this.dash.lineLabels.split(",");
            _this.lineIncomeData = _this.dash.lineIncomeData.split(",");
            _this.lineExpenseData = _this.dash.lineExpenseData.split(",");
            _this.doNut();
            _this.liNe();
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.dash.userName,
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    DashboardPage.prototype.retirement = function (time) {
        // let savings = this.savings;
        var savings = this.anotherSav;
        var expenses = this.expenses;
        var wrkingYrs = this.wrkingYrs;
        var retYrs = this.retYrs;
        // let savings = 1000
        var nvalue = savings;
        var r = 0.14;
        var count = 0;
        while (count < wrkingYrs) {
            var temp = savings * Math.pow((1 + r), count);
            // console.log(temp);
            nvalue += temp;
            count++;
        }
        // console.log(nvalue);
        var prannuity;
        prannuity = (1 - (Math.pow((1 + r), (retYrs * -1)))) / r;
        // console.log(prannuity);
        var annuity = (1 / prannuity) * nvalue;
        this.amtRet = this.numberWithCommas(annuity.toFixed(2));
        this.annuity = this.numberWithCommas((annuity / retYrs).toFixed(2));
        // console.log(annuity);
    };
    DashboardPage.prototype.load = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    DashboardPage.prototype.doNut = function () {
        console.log(this.donutLabels);
        this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.doughnutCanvas.nativeElement, {
            type: 'doughnut',
            data: {
                // labels: ["Food", "Utility", "Airtime", "Investments", "Entertainment", "Travel"],
                // labels: this.donutLabels,
                labels: this.pieLabels,
                datasets: [{
                        label: 'User Personal Finance Status',
                        // data: this.donutData,
                        data: this.pieValues,
                        // data: [12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }]
            },
            options: {
                //   scales: {
                //     xAxes: [{
                //         gridLines: {
                //             color: "rgba(0, 0, 0, 0)",
                //         }
                //     }],
                //     yAxes: [{
                //         gridLines: {
                //             color: "rgba(0, 0, 0, 0)",
                //         }   
                //     }]
                // },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            // var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                            // var total = meta.total;
                            var currentValue = dataset.data[tooltipItem.index];
                            // var percentage = parseFloat((currentValue/total*100).toFixed(1));
                            // return currentValue + ' (' + percentage + '%)';
                            return  true ? currentValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : currentValue;
                        },
                    }
                }
            }
        });
    };
    DashboardPage.prototype.liNe = function () {
        this.lineChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                // labels: ["January", "February", "March", "April", "May", "June", "July"],
                labels: this.lineLabels,
                datasets: [
                    {
                        label: "Income",
                        fill: false,
                        lineTension: 0.1,
                        // backgroundColor: "rgba(75,92,192,0.4)",
                        borderColor: "rgba(75,92,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,92,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,92,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 55, 90, 21, 78, 95, 20],
                        data: this.lineIncomeValues,
                        spanGaps: false,
                    },
                    {
                        label: "Expense",
                        fill: false,
                        lineTension: 0.1,
                        // backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.lineExpenseValues,
                        spanGaps: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }]
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return  true ? tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : tooltipItem.yLabel;
                        }
                    }
                }
            }
        });
    };
    DashboardPage.prototype.getUserName = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            if (initialData.name.length < 1 || initialData.dob.length < 1 || initialData.dow.length < 1
                || initialData.pen.length < 1 || initialData.ret.length < 1 || initialData.email.length < 1
                || initialData.pword.length < 1) {
                _this.storage.set('initialData', null);
                var alert_1 = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Invalid Details, Re-Login or Contact Admin',
                    buttons: ['Ok']
                });
                alert_1.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
                return;
            }
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.pension = _this.numberWithCommas(parseInt(initialData.pen));
            _this.apiLoad();
            var ageNow = __WEBPACK_IMPORTED_MODULE_6_moment__().diff(initialData.dob, 'years');
            _this.savings = _this.numberWithCommas(parseInt(initialData.savInv));
            _this.anotherSav = parseInt(initialData.savInv);
            _this.expenses = _this.numberWithCommas(parseInt(initialData.exp));
            _this.wrkingYrs = parseInt(initialData.ret) - ageNow;
            _this.retYrs = 90 - parseInt(initialData.ret);
            var totAge = 90 - ageNow;
            _this.loadProgress = (_this.wrkingYrs / totAge) * 100;
            _this.loadProgressAge = initialData.ret;
            _this.currAge = ageNow;
            _this.getTransactionsApi(_this.start, _this.end);
            _this.retirement();
            _this.ageRet = initialData.ret;
        });
    };
    DashboardPage.prototype.setApiJson = function (start, end) {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "start": start,
            "end": end
        };
        // });
    };
    DashboardPage.prototype.getTransactionsApi = function (start, end) {
        var _this = this;
        this.loaded = 0;
        this.setApiJson(start, end);
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        // console.log(this.apiData);
        this.lineLabels = [];
        // this.lineValues= [];
        this.pieLabels = [];
        this.pieValues = [];
        this.grsp.transactions(this.apiData).then(function (data) {
            // console.log(data);
            _this.trans = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.transactions = JSON.parse(_this.trans.transactions);
            // this.transactions  = this.transactions.filter(tr => tr.trans_bucket == 'Assets');
            // get all
            _this.pieLabels = _this.transactions.map(function (value) { return value.trans_bucket; });
            // console.log(this.transactions);
            // console.log(this.pieLabels);
            // get unique
            _this.pieLabels = _this.pieLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            console.log(_this.pieLabels);
            var isArr = (Array.isArray(_this.pieLabels));
            // console.log(isArr);
            _this.pieLabels.forEach(function (element) {
                // console.log(element);
                var arr = _this.transactions.filter(function (tr) { return tr.trans_bucket == element; });
                // console.log(arr);
                var subArr = arr.map(function (value) { return value.trans_amt; });
                subArr = subArr.map(function (x) { return parseInt(x); });
                // console.log(subArr);
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                // console.log(arrSum);
                _this.pieValues.push(arrSum);
            });
            _this.pieValues.length = _this.pieLabels.length;
            console.log(_this.pieValues);
            _this.doNut();
            // -----------------------------Line graph-----------------------------------
            // get trans again
            _this.transactions = JSON.parse(_this.trans.transactions);
            _this.transactions = _this.transactions.filter(function (tr) { return tr.trans_bucket == 'Income'; });
            _this.lineLabels = _this.transactions.map(function (value) { return value.trans_date; });
            // get unique
            _this.lineLabels = _this.lineLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            // get last 5 days
            _this.lineLabels = _this.lineLabels.slice(-5);
            console.log(_this.lineLabels);
            _this.lineLabels.forEach(function (element) {
                // get all transactions for current iteration
                var arr = _this.transactions.filter(function (tr) { return tr.trans_date == element; });
                // get all amts for all those transactions
                var subArr = arr.map(function (value) { return value.trans_amt; });
                // parse amts to int
                subArr = subArr.map(function (x) { return parseInt(x); });
                // sum them up
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                _this.lineIncomeValues.push(arrSum);
            });
            console.log(_this.lineIncomeValues);
            // -------------------------------------------expenses line-------------------------------------------------
            _this.etransactions = JSON.parse(_this.trans.transactions);
            _this.etransactions = _this.etransactions.filter(function (tr) { return tr.trans_bucket == 'Expenses'; });
            // console.log(this.etransactions);
            _this.lineExpLabels = _this.etransactions.map(function (value) { return value.trans_date; });
            // get unique
            _this.lineExpLabels = _this.lineExpLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            // get last 5 days
            _this.lineExpLabels = _this.lineExpLabels.slice(-5);
            console.log(_this.lineExpLabels);
            _this.lineExpLabels.forEach(function (element) {
                // get all transactions for current iteration
                var arr = _this.etransactions.filter(function (tr) { return tr.trans_date == element; });
                // get all amts for all those transactions
                var subArr = arr.map(function (value) { return value.trans_amt; });
                // parse amts to int
                subArr = subArr.map(function (x) { return parseInt(x); });
                // sum them up
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                _this.lineExpenseValues.push(arrSum);
            });
            console.log(_this.lineExpenseValues);
            _this.liNe();
            _this.loaded = 1;
            alert = _this.alertCtrl.create({
                title: 'No name',
                subTitle: 'no Name' + _this.transactions[0],
                buttons: ['Dismiss']
            });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            console.log(err);
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], DashboardPage.prototype, "doughnutCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], DashboardPage.prototype, "lineCanvas", void 0);
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\dashboard\dashboard.html"*/'<!--\n  Generated template for the DashboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>{{userName}}\'s Dashboard</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>{{userName}}\'s Dashboard</ion-title>\n\n    </ion-navbar>\n\n    <br>\n    <div class="offer" text-center>\n        <!-- <h2>The Science of Delicious.</h2>\n        <p>Amazing coffees from around the world! </p> -->\n        <!-- <ion-grid >\n          <ion-row justify-content-center align-items-center >\n              <button ion-button small >Week</button>\n              <button ion-button small>Month</button>\n              <button ion-button small >Year</button>\n              <button ion-button small (click)="clearInit()">Range</button>\n      \n          </ion-row>\n        </ion-grid> -->\n    </div>\n\n\n  </ion-header>\n\n\n<ion-content padding>\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-grid >\n        <ion-row justify-content-center align-items-center >\n            <button ion-button small (click)="week()" >Week</button>\n        <button ion-button small (click)="month()" >Month</button>\n        <button ion-button small (click)="year()" >Year</button>\n        <button ion-button small (click)="range()" >Range</button>\n  \n    </ion-row>\n  </ion-grid>\n  \n      <ion-card *ngIf="drange">\n          \n        <ion-card-content>\n            <ion-item>\n              <ion-label stacked> From </ion-label>\n              <ion-input type="date" [(ngModel)]="from" name="from" ></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label stacked> To </ion-label>\n              <ion-input type="date" [(ngModel)]="to" name="to" ></ion-input>\n            </ion-item>\n        <button ion-button small (click)="getRange()" >Go</button>\n           \n         </ion-card-content>\n      </ion-card>\n\n\n\n      <ion-card>\n          <!-- <ion-card-header>\n            Doughnut Chart\n          </ion-card-header> -->\n          <ion-card-content>\n            <canvas #doughnutCanvas></canvas>\n          </ion-card-content>\n        </ion-card>\n        <!-- <hr> -->\n        <ion-card>\n          <ion-card-content>\n            <canvas #lineCanvas></canvas>\n          </ion-card-content>\n        </ion-card>\n        \n        \n        \n        <!-- <hr> -->\n        <ion-card>\n          <ion-card-content>\n        \n        <ion-grid>\n          <ion-row>\n        \n            <ion-col col-12>\n              \n              <h1>Networth(NGN): {{networth}} <ion-icon name="{{nwTrend > 0 ? \'arrow-round-up\' : \'arrow-round-down\'}}" color="{{nwTrend > 0 ? \'primary\' : \'danger\'}}"></ion-icon></h1>\n            </ion-col>\n            <ion-col col-4>\n              <h5>Assets(NGN): <br> {{assets}} <ion-icon name="{{assetTrend > 0 ? \'arrow-round-up\' : \'arrow-round-down\'}}" color="{{assetTrend > 0 ? \'primary\' : \'danger\'}}"></ion-icon></h5>\n            </ion-col>\n            <ion-col col-4>\n              <h5>Liability(NGN): <br> {{liability}} <ion-icon name="{{liabTrend > 0 ? \'arrow-round-up\' : \'arrow-round-down\'}}" color="{{liabTrend > 0 ? \'primary\' : \'danger\'}}"></ion-icon></h5>\n            </ion-col>\n            <ion-col col-4>\n              <h5>Pension(NGN): <br> {{pension}} <ion-icon name="arrow-round-up" color="primary"></ion-icon></h5>\n            </ion-col>\n          </ion-row>\n        \n        </ion-grid>\n        \n          </ion-card-content>\n        </ion-card>\n       \n        <ion-card>\n          <ion-card-content>\n            \n            <ion-grid>\n              <ion-row>\n                \n                    <ion-col col-12>\n                        <h1 text-center>Retirement Plan (NGN) </h1>\n                      \n                    </ion-col>\n                    <ion-col col-3>\n                      <h5>Savings: <br/> {{savings}} </h5>\n                    </ion-col>\n                    <ion-col col-3>\n                      <h5>Expenses: <br/> {{expenses}} </h5>\n                    </ion-col>\n                    <ion-col col-6>\n                      <h5>Annuity For {{retYrs}} Years: <br> {{annuity}} </h5>\n                    </ion-col>\n                  </ion-row>\n                \n                </ion-grid>\n          \n            <!-- <progress-bar [progressAge]="loadProgressAge" [progress]="loadProgress" [tright]="finAge" [tleft]="currAge" ></progress-bar> -->\n            <progress-bar [progressAge]="loadProgressAge" [progress]="loadProgress" [tright]="finAge" [tleft]="currAge" ></progress-bar>\n         \n          </ion-card-content>\n        </ion-card>\n        \n        \n        <hr>\n        <ion-card>\n          <ion-card-content>\n            \n            <ion-grid>\n                <ion-row>\n                  \n                \n                  <ion-col col-6>\n                      <h5>Amount to Retire(NGN):\n                      <br>\n                      {{amtRet}}</h5>\n                  </ion-col>\n                  <ion-col col-6>\n                      <h5>Age to Retire(Years):<br/>{{ageRet}}</h5>\n                  </ion-col>\n                </ion-row>\n              \n              </ion-grid>\n        \n          </ion-card-content>\n        </ion-card>\n\n\n\n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\dashboard\dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 165:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 165;

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditCardPageModule", function() { return CreditCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__credit_card__ = __webpack_require__(872);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CreditCardPageModule = (function () {
    function CreditCardPageModule() {
    }
    CreditCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__credit_card__["a" /* CreditCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__credit_card__["a" /* CreditCardPage */]),
            ],
        })
    ], CreditCardPageModule);
    return CreditCardPageModule;
}());

//# sourceMappingURL=credit-card.module.js.map

/***/ }),

/***/ 211:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/_projectsbudget/projectsbudget.module": [
		875,
		17
	],
	"../pages/addtransactions/addtransactions.module": [
		212
	],
	"../pages/analysis/analysis.module": [
		220
	],
	"../pages/banking/banking.module": [
		349
	],
	"../pages/budget/budget.module": [
		351
	],
	"../pages/cards/cards.module": [
		350
	],
	"../pages/credit-card/credit-card.module": [
		166
	],
	"../pages/current/current.module": [
		876,
		16
	],
	"../pages/dashboard/dashboard.module": [
		352
	],
	"../pages/debtbudget/debtbudget.module": [
		877,
		15
	],
	"../pages/edittransactions/edittransactions.module": [
		354
	],
	"../pages/expense/expense.module": [
		878,
		14
	],
	"../pages/expensebudget/expensebudget.module": [
		879,
		13
	],
	"../pages/favorites/favorites.module": [
		356
	],
	"../pages/fin-info/fin-info.module": [
		355
	],
	"../pages/fixed-deposit/fixed-deposit.module": [
		880,
		12
	],
	"../pages/foreign/foreign.module": [
		881,
		11
	],
	"../pages/forex-card/forex-card.module": [
		357
	],
	"../pages/income/income.module": [
		883,
		10
	],
	"../pages/incomebudget/incomebudget.module": [
		882,
		9
	],
	"../pages/initial/initial.module": [
		358
	],
	"../pages/investment/investment.module": [
		359
	],
	"../pages/liabilities/liabilities.module": [
		884,
		8
	],
	"../pages/liabilitiesbudget/liabilitiesbudget.module": [
		885,
		7
	],
	"../pages/loadsmstransactions/loadsmstransactions.module": [
		886,
		6
	],
	"../pages/managebudget/managebudget.module": [
		453
	],
	"../pages/menu-one/menu-one.module": [
		457
	],
	"../pages/menu-two/menu-two.module": [
		454
	],
	"../pages/naira-card/naira-card.module": [
		455
	],
	"../pages/order-details/order-details.module": [
		456
	],
	"../pages/pay-conf/pay-conf.module": [
		887,
		5
	],
	"../pages/prepaid-card/prepaid-card.module": [
		459
	],
	"../pages/profile/profile.module": [
		458
	],
	"../pages/projectbudget/projectbudget.module": [
		888,
		4
	],
	"../pages/projects/projects.module": [
		889,
		3
	],
	"../pages/sav-inv/sav-inv.module": [
		892,
		2
	],
	"../pages/sav-invbudget/sav-invbudget.module": [
		890,
		1
	],
	"../pages/savings/savings.module": [
		891,
		0
	],
	"../pages/setting/setting.module": [
		460
	],
	"../pages/signup/signup.module": [
		461
	],
	"../pages/splash-one/splash-one.module": [
		464
	],
	"../pages/splash-two/splash-two.module": [
		462
	],
	"../pages/sub-menu-one/sub-menu-one.module": [
		463
	],
	"../pages/sub-menu-two/sub-menu-two.module": [
		469
	],
	"../pages/viewanalytics/viewanalytics.module": [
		465
	],
	"../pages/viewbudget/viewbudget.module": [
		466
	],
	"../pages/viewtransactions/viewtransactions.module": [
		467
	],
	"../pages/walkthrough/walkthrough.module": [
		468
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 211;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddtransactionsPageModule", function() { return AddtransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addtransactions__ = __webpack_require__(541);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddtransactionsPageModule = (function () {
    function AddtransactionsPageModule() {
    }
    AddtransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__addtransactions__["a" /* AddtransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__addtransactions__["a" /* AddtransactionsPage */]),
            ],
        })
    ], AddtransactionsPageModule);
    return AddtransactionsPageModule;
}());

//# sourceMappingURL=addtransactions.module.js.map

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalRestServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(3);
// import { HttpClient } from '@angular/common/http';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the GlobalRestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GlobalRestServiceProvider = (function () {
    function GlobalRestServiceProvider(http, alertCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.baseUrl = "http://gaid.io/myLeo";
        console.log('Hello GlobalRestServiceProvider Provider');
    }
    GlobalRestServiceProvider.prototype.showAlert = function (err) {
        // let alert = this.alertCtrl.create({
        //   title: 'Error',
        //   subTitle: 'Something broke and thats on us, Please try again',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
    };
    GlobalRestServiceProvider.prototype.buckets = function () {
        // let buckets = ['grocery','transportation','utilities','vacation','education','entertainment','food','gifts','subscriptions','rent','Banking'];
        var buckets = ['Income', 'Expenses', 'Saving&Investments(Assets)', 'Liabilities', 'Projects (Goals)'];
        return buckets;
    };
    GlobalRestServiceProvider.prototype.test = function (param) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.post(_this.baseUrl + "/test", param).subscribe(function (res) {
                resolve(res.json());
            });
        });
    };
    GlobalRestServiceProvider.prototype.addUser = function (param) {
        // console.log(param);
        // return new Promise(resolve=>{
        //   // this.http.post(`${this.baseUrl}/addUser`,param).subscribe(res=>resolve(res.json()));
        //   this.http.post(`${this.baseUrl}/addUser`,param).subscribe(res=>{
        //     console.log(res);
        //     resolve(res.json());
        var _this = this;
        //   });
        // })
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addUser", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.editUser = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/editUser", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                // this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.dashboardLoad = function (param) {
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/dashboard`,param).subscribe(res=>{
        //     console.log(res);
        //     resolve(res.json());
        //   });
        // })
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/dashboard", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.login = function (param) {
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/addTrans`,param).subscribe(res=>{
        var _this = this;
        //     resolve(res.json());
        //   });
        // })
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/login", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.addBudget = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addBudget", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.addTrans = function (param) {
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/addTrans`,param).subscribe(res=>{
        var _this = this;
        //     resolve(res.json());
        //   });
        // })
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addTrans", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    // editTrans(param){
    //   return new Promise((resolve,reject)=>{
    //     this.http.post(`${this.baseUrl}/editTrans`,param).subscribe(res=>{
    //       console.log(res);
    //       resolve(res.json());
    //     });
    //   })
    // }
    GlobalRestServiceProvider.prototype.editTrans = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/editTrans", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                // this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.buyInvestment = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/buyInvestment", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.addInitialData = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addInitialData", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.analytics = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/analytics", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.fixedDeposits = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/fixedDeposits", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    // mutualFunds(param){
    //   return new Promise(resolve=>{
    //     this.http.post(`${this.baseUrl}/mutualFunds`,param).subscribe(res=>{
    //       resolve(res.json());
    //     });
    //   })
    // }
    // stocks(param){
    //   return new Promise(resolve=>{
    //     this.http.post(`${this.baseUrl}/stocks`,param).subscribe(res=>{
    //       resolve(res.json());
    //     });
    //   })
    // }
    GlobalRestServiceProvider.prototype.transactions = function (param) {
        // console.log(param);
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/transactions`,param).subscribe(res=>{
        //     console.log(res);
        //     resolve(res.json());
        //   });
        // })
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/transactions", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.getTransSumByMonth = function (param) {
        var _this = this;
        // return new Promise(resolve=>{
        //     this.http.post(`${this.baseUrl}/transSum`,param).subscribe(res=>{
        //       console.log(res);
        //       resolve(res.json());
        //     });
        //   })
        //  ==================================================
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/transSum", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.banking = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/banking", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.cards = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/cards", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.investments = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/investments", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.lastDate = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/lastDate", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */]])
    ], GlobalRestServiceProvider);
    return GlobalRestServiceProvider;
}());

//# sourceMappingURL=global-rest-service.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalysisPageModule", function() { return AnalysisPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__analysis__ = __webpack_require__(545);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AnalysisPageModule = (function () {
    function AnalysisPageModule() {
    }
    AnalysisPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__analysis__["a" /* AnalysisPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__analysis__["a" /* AnalysisPage */]),
            ],
        })
    ], AnalysisPageModule);
    return AnalysisPageModule;
}());

//# sourceMappingURL=analysis.module.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InitialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { LandPage } from './../land/land';




/**
 * Generated class for the InitialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InitialPage = (function () {
    function InitialPage(navCtrl, navParams, alertCtrl, storage, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.initialData = {
            id: 0,
            name: '',
            dob: '',
            dow: '',
            pen: '',
            savInv: '',
            exp: '',
            inc: '',
            ret: '',
            email: '',
            pword: '',
            cpword: '',
        };
        this.loaded = 1;
    }
    InitialPage.prototype.IsEmail = function (email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        }
        else {
            return true;
        }
    };
    InitialPage.prototype.ionViewWillEnter = function () {
        this.checkInit();
    };
    InitialPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InitialPage');
        // this.checkInit();
    };
    InitialPage.prototype.nextIndex = function () {
        var currentIndex = this.slides.getActiveIndex();
        this.slides.slideTo(currentIndex + 1, 500);
    };
    InitialPage.prototype.prevIndex = function () {
        var currentIndex = this.slides.getActiveIndex();
        this.slides.slideTo(currentIndex - 1, 500);
    };
    InitialPage.prototype.checkInit = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            if (initialData == null || initialData.id == 0) {
            }
            else {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
                // this.navCtrl.push(DashboardPage, {
                //   // val: 'Jeeje'
                //   val: initialData
                // })
            }
        });
    };
    InitialPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "id": 0,
            "name": this.initialData.name,
            "dob": this.initialData.dob,
            "dow": this.initialData.dow,
            "pen": this.initialData.pen,
            "savInv": this.initialData.savInv,
            "exp": this.initialData.exp,
            "inc": this.initialData.inc,
            "ret": this.initialData.ret,
            "email": this.initialData.email,
            "pword": this.initialData.pword,
        };
        console.log(this.apiData);
        // });this.initialData.namethis.initialData.name
    };
    InitialPage.prototype.logForm = function () {
        var _this = this;
        var alert;
        if (this.initialData.name.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Name is Required',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(1, 500);
            return;
        }
        if (this.initialData.dob.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Date of Birth is Required',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(2, 500);
            return;
        }
        if (this.initialData.dow.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us when you Started Working',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(3, 500);
            return;
        }
        if (this.initialData.pen.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Have in Pension',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(4, 500);
            return;
        }
        if (this.initialData.savInv.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Have in Savings and Investments',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(5, 500);
            return;
        }
        if (this.initialData.exp.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Spent Last Month',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(6, 500);
            return;
        }
        if (this.initialData.inc.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Made Last Month',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(7, 500);
            return;
        }
        if (this.initialData.ret.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'When Would You Like to Retire',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(8, 500);
            return;
        }
        if (this.initialData.email.length < 1 || this.IsEmail(this.initialData.email) == false) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Enter a Valid Email',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(9, 500);
            return;
        }
        if (this.initialData.pword.length < 6) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Enter a Valid Password',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(10, 500);
            return;
        }
        if (this.initialData.pword !== this.initialData.cpword) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Passwords Do Not Match',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(10, 500);
            return;
        }
        this.setApiJson();
        this.loaded = 0;
        // ========================================================
        // this.apiData = this.initialData;
        console.log(this.apiData);
        this.grsp.addUser(this.apiData).then(function (data) {
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                _this.initialData.id = _this.apiRes.uId;
                _this.storage.set('initialData', _this.initialData);
                alert = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Saved User ' + _this.apiRes.uId,
                    buttons: ['Ok']
                });
                // alert.present();
                // ========================================
                alert = _this.alertCtrl.create({
                    title: 'Saved',
                    subTitle: 'Thank You ' + _this.initialData.name.toUpperCase() + ' Your Details Have Been Saved',
                    buttons: ['Ok']
                });
                alert.present();
                // this.navCtrl.push(DashboardPage, {
                //   // val: 'Jeeje'
                //   val: this.initialData
                // })
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
                // ========================================
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Something went wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_1.present();
            }
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // ========================================================
        //  let alert = this.alertCtrl.create({
        //   title: 'Saved',
        //   subTitle: 'Thank You '+this.initialData.name+ ' Your details have been saved',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
        // this.navCtrl.push(LandPage, {
        //   // val: 'Jeeje'
        //   val: this.initialData
        // })
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */])
    ], InitialPage.prototype, "slides", void 0);
    InitialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-initial',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\initial\initial.html"*/'<!--\n  Generated template for the InitialPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="secondary">\n      <!-- <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button> -->\n      <ion-title>Welcome</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="tutorial-page">\n  <ion-slides >\n    <ion-slide>\n      <h1>Welcome, We have been expecting you</h1>\n    \n      <button ion-button small (click)="nextIndex()" >Next</button>\n\n\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <!-- <h1>Tell us your name</h1> -->\n      <ion-item>\n        <ion-label stacked>Tell us your name </ion-label>\n        <ion-input type="text" [(ngModel)]="initialData.name" name="name" ></ion-input>\n      </ion-item>\n      <!-- <hr> -->\n      <button ion-button small (click)="nextIndex()" >Next</button>\n\n\n    </ion-slide>\n\n    <ion-slide style="margin-top: 60px;">\n      <!-- <h1>When were you born</h1> -->\n      <ion-item>\n        <ion-label stacked>When were you born </ion-label>\n        <ion-input type="date" [(ngModel)]="initialData.dob" name="dob" ></ion-input>\n      </ion-item>\n      <!-- <hr> -->\n      <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n      <button ion-button small (click)="nextIndex()" >Next</button>\n\n\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n          <!-- <h1>When did you start work</h1> -->\n        <ion-label stacked>When did you start work </ion-label>\n        <ion-input type="date" [(ngModel)]="initialData.dow" name="dow" ></ion-input>\n      </ion-item>\n      <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n      <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) do you have in your pension account </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.pen" name="pen" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) was your savings and investment last month </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.savInv" name="savInv" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) did you spend in total last month </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.exp" name="exp" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) did you make in total last month </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.inc" name="inc" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>At what age do you intend to retire </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.ret" name="ret" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n\n          <!-- <button ion-button small (click)="logForm()" >Submit</button> -->\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>Email Address </ion-label>\n            <ion-input type="email" [(ngModel)]="initialData.email" name="email" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n\n          <!-- <button ion-button small (click)="logForm()" >Submit</button> -->\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>Password (Minimum of 6 characters) </ion-label>\n            <ion-input type="password" [(ngModel)]="initialData.pword" name="pword" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n\n          <!-- <button ion-button small (click)="logForm()" >Submit</button> -->\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>Confirm Password (Be sure) </ion-label>\n            <ion-input type="password" [(ngModel)]="initialData.cpword" name="cpword" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <!-- <button ion-button small (click)="nextIndex()" >Next</button> -->\n\n          <button ion-button small (click)="logForm()" >Submit</button>\n    </ion-slide>\n\n  </ion-slides>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\initial\initial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], InitialPage);
    return InitialPage;
}());

//# sourceMappingURL=initial.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__initial_initial__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupPage = (function () {
    function SignupPage(navCtrl, grsp, alertCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.grsp = grsp;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.navParams = navParams;
        // @ViewChild(Nav) nav: Nav;
        // rootPage: any;
        this.initialData = {
            id: 0,
            name: '',
            dob: '',
            dow: '',
            pen: '',
            savInv: '',
            exp: '',
            inc: '',
            ret: '',
            email: '',
            pword: '',
            cpword: '',
        };
        this.screen = "signin";
        this.login = {
            email: '',
            pword: '',
        };
        this.loaded = 1;
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.setApiJson = function () {
        this.apiData = {
            "email": this.login.email,
            "pword": this.login.pword
        };
    };
    SignupPage.prototype.loginCheck = function () {
        var _this = this;
        if (this.login.email == '' || this.login.pword == '') {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Enter Correct Login Details',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        this.setApiJson();
        this.loaded = 0;
        console.log(this.apiData);
        this.grsp.login(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var loginRes = JSON.parse(_this.apiRes.loginRes)[0];
                console.log(loginRes);
                _this.initialData = {
                    id: loginRes.user_id,
                    name: loginRes.user_name,
                    dob: loginRes.user_dob,
                    dow: loginRes.user_dow,
                    pen: loginRes.user_pension,
                    savInv: loginRes.user_savInv,
                    exp: loginRes.user_lastMonthExp,
                    inc: loginRes.user_lastMonthInc,
                    ret: loginRes.user_retAge,
                    email: loginRes.user_email,
                    pword: loginRes.user_pword,
                    cpword: loginRes.user_pword,
                };
                _this.storage.set('initialData', _this.initialData);
                console.log(_this.initialData);
                var alert_2 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Login Successful',
                    buttons: ['Dismiss']
                });
                alert_2.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
                // this.activePage = { title: 'Dashboard', component: DashboardPage };
            }
            else {
                var alert_3 = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Something is wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_3.present();
            }
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // =============================================================
        // this.navCtrl.setRoot(DashboardPage);
        // this.activePage = { title: 'Dashboard', component: DashboardPage };
        // this.navCtrl.push(MyApp, {
        //   val: 'I am View Ana-lytics',
        //   page:'Dashboard'
        // })
    };
    SignupPage.prototype.signUp = function () {
        // this.navCtrl.setRoot(InitialPage);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__initial_initial__["a" /* InitialPage */], {});
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\signup\signup.html"*/'<ion-content [ngSwitch]="screen">\n\n    <div class="logo-container" text-center>\n        <img src="assets/imgs/logo.png" alt="Logo">\n\n        <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n            <ion-segment-button value="signin">\n                Sign In\n            </ion-segment-button>\n            <!-- <ion-segment-button value="signup">\n                Sign Up\n            </ion-segment-button> -->\n        </ion-segment>\n    </div>\n\n\n    <div class="form-container" *ngSwitchCase="\'signin\'">\n        <ion-list>\n                <ion-item *ngIf="loaded==0">\n                  <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n               \n                </ion-item>\n                <ion-item>\n                    <ion-input placeholder="Email"  type="email" [(ngModel)]="login.email" name="email" ></ion-input>\n                </ion-item>\n            <!-- <ion-item>\n                <ion-input placeholder="Phone" type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Email" type="email"></ion-input>\n            </ion-item> -->\n            <ion-item>\n                <ion-input placeholder="Password" type="password" [(ngModel)]="login.pword" name="pword" ></ion-input>\n            </ion-item>\n\n            <ion-item text-center no-lines>                \n                No Account? <a  color="primary" (click)="signUp()" >Sign Up</a>\n            </ion-item>\n                \n            </ion-list>\n        <br>\n        <button ion-button full color="primary" (click)="loginCheck()" >Sign In</button>\n\n\n\n\n        \n\n\n\n\n    </div>\n    <!-- <div class="form-container" *ngSwitchCase="\'signup\'">\n        <ion-list>\n            <ion-item>\n                <ion-input placeholder="Username" type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Phone" type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Email" type="email"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Password" type="password"></ion-input>\n            </ion-item>\n        </ion-list>\n\n        <button ion-button full color="primary">Sign Up</button>\n\n    </div> -->\n</ion-content>\n<!-- <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav> -->\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankingPageModule", function() { return BankingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__banking__ = __webpack_require__(548);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BankingPageModule = (function () {
    function BankingPageModule() {
    }
    BankingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__banking__["a" /* BankingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__banking__["a" /* BankingPage */]),
            ],
        })
    ], BankingPageModule);
    return BankingPageModule;
}());

//# sourceMappingURL=banking.module.js.map

/***/ }),

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardsPageModule", function() { return CardsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cards__ = __webpack_require__(549);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CardsPageModule = (function () {
    function CardsPageModule() {
    }
    CardsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cards__["a" /* CardsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cards__["a" /* CardsPage */]),
            ],
        })
    ], CardsPageModule);
    return CardsPageModule;
}());

//# sourceMappingURL=cards.module.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BudgetPageModule", function() { return BudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__budget__ = __webpack_require__(550);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BudgetPageModule = (function () {
    function BudgetPageModule() {
    }
    BudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__budget__["a" /* BudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__budget__["a" /* BudgetPage */]),
            ],
        })
    ], BudgetPageModule);
    return BudgetPageModule;
}());

//# sourceMappingURL=budget.module.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(353);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DashboardPageModule = (function () {
    function DashboardPageModule() {
    }
    DashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
            ],
        })
    ], DashboardPageModule);
    return DashboardPageModule;
}());

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__ = __webpack_require__(551);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */],
                __WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */],
                __WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EdittransactionsPageModule", function() { return EdittransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edittransactions__ = __webpack_require__(552);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EdittransactionsPageModule = (function () {
    function EdittransactionsPageModule() {
    }
    EdittransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__edittransactions__["a" /* EdittransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__edittransactions__["a" /* EdittransactionsPage */]),
            ],
        })
    ], EdittransactionsPageModule);
    return EdittransactionsPageModule;
}());

//# sourceMappingURL=edittransactions.module.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinInfoPageModule", function() { return FinInfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fin_info__ = __webpack_require__(553);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FinInfoPageModule = (function () {
    function FinInfoPageModule() {
    }
    FinInfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fin_info__["a" /* FinInfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__fin_info__["a" /* FinInfoPage */]),
            ],
        })
    ], FinInfoPageModule);
    return FinInfoPageModule;
}());

//# sourceMappingURL=fin-info.module.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoritesPageModule", function() { return FavoritesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__favorites__ = __webpack_require__(554);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FavoritesPageModule = (function () {
    function FavoritesPageModule() {
    }
    FavoritesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__favorites__["a" /* FavoritesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__favorites__["a" /* FavoritesPage */]),
            ],
        })
    ], FavoritesPageModule);
    return FavoritesPageModule;
}());

//# sourceMappingURL=favorites.module.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForexCardPageModule", function() { return ForexCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forex_card__ = __webpack_require__(555);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ForexCardPageModule = (function () {
    function ForexCardPageModule() {
    }
    ForexCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__forex_card__["a" /* ForexCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__forex_card__["a" /* ForexCardPage */]),
            ],
        })
    ], ForexCardPageModule);
    return ForexCardPageModule;
}());

//# sourceMappingURL=forex-card.module.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitialPageModule", function() { return InitialPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__initial__ = __webpack_require__(221);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InitialPageModule = (function () {
    function InitialPageModule() {
    }
    InitialPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__initial__["a" /* InitialPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__initial__["a" /* InitialPage */]),
            ],
        })
    ], InitialPageModule);
    return InitialPageModule;
}());

//# sourceMappingURL=initial.module.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvestmentPageModule", function() { return InvestmentPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__investment__ = __webpack_require__(556);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InvestmentPageModule = (function () {
    function InvestmentPageModule() {
    }
    InvestmentPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__investment__["a" /* InvestmentPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__investment__["a" /* InvestmentPage */]),
            ],
        })
    ], InvestmentPageModule);
    return InvestmentPageModule;
}());

//# sourceMappingURL=investment.module.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaystackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__ = __webpack_require__(361);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { InAppBrowser } from 'ionic-native';

var PaystackPage = (function () {
    function PaystackPage(navCtrl, alertCtrl, navParams, grsp, loading, platform, iab, viewCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.grsp = grsp;
        this.loading = loading;
        this.platform = platform;
        this.iab = iab;
        this.viewCtrl = viewCtrl;
    }
    PaystackPage.prototype.ngOnInit = function () {
        this.price = parseInt(this.navParams.get('price'));
        // this.data= parseInt(this.navParams.get('data'));
        console.log(this.price);
        console.log(this.navParams.get('data'));
        // this.price= 500;
        this.chargeAmount = this.price * 100;
    };
    PaystackPage.prototype.ChargeCard = function () {
        var _this = this;
        var card = this.card_number.value;
        var month = this.expiryMonth.value;
        var cvc = this.cvc.value;
        var year = this.expiryYear.value;
        var amount = this.chargeAmount;
        var email = this.emailValue;
        // let apiData = this.data;
        console.log(card);
        console.log(month);
        console.log(cvc);
        console.log(year);
        console.log(amount);
        console.log(email);
        var loader = this.loading.create({
            content: 'Processing Charge…'
        });
        loader.present();
        this.platform.ready().then(function () {
            if (_this.platform.is('cordova')) {
                // Now safe to use device APIs
                window.window.PaystackPlugin.chargeCard(function (resp) {
                    loader.dismiss();
                    //this.pop.showPayMentAlert("Payment Was Successful", "We will Now Refund Your Balance");
                    console.log('charge successful: ', resp);
                    // console.log(apiData);
                    alert('Payment Was Successful');
                    // this.navCtrl.push("InvestmentPage", {res:"success"} );
                    _this.navCtrl.push("PayConfPage", { res: "success", amt: (amount / 100) });
                }, function (resp) {
                    loader.dismiss();
                    console.log(resp);
                    alert('We Encountered An Error While Charging Your Card');
                    // this.navCtrl.push("InvestmentPage", {res:"failed"} );
                }, {
                    cardNumber: card,
                    expiryMonth: month,
                    expiryYear: year,
                    cvc: cvc,
                    email: email,
                    amountInKobo: amount,
                });
            }
            else {
            }
        });
    };
    PaystackPage.prototype.whatsPaystack = function () {
        var _this = this;
        this.platform.ready().then(function () {
            //  let browser = new InAppBrowser("https://paystack.com/what-is-paystack",'_blank');
            var browser = _this.iab.create("https://paystack.com/what-is-paystack", '_blank');
        });
    };
    PaystackPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("email_add"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "email_add", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("card_number"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "card_number", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("expiryMonth"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "expiryMonth", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("expiryYear"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "expiryYear", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("cvc"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "cvc", void 0);
    PaystackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-paystack',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/'<!--\n  Generated template for the PaystackPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n    \n      <ion-title>Make Payment</ion-title>\n    \n    </ion-navbar>\n  \n  </ion-header>\n  \n  <ion-content  >\n  \n    <div class="image" text-center><img  src="assets/imgs/paystack_logo.png" /></div>\n    \n    <!-- <div class="amount">NGN {{data | number:\'.2\'}}</div> -->\n    <div class="amount">NGN {{data}}</div>\n    \n    <div class="input-area">\n    \n    <ion-list >\n    \n    <ion-item >\n    \n    <ion-input type="text" placeholder="Email Address"  #email_add [(ngModel)]="emailValue"></ion-input>\n    \n    </ion-item>\n    \n    <div class="cardnumber">\n    \n    <ion-label >CARD NUMBER</ion-label>\n    \n    <ion-item >\n    \n    <ion-input type="tel" placeholder="0000000000000000" name="card_number" #card_number [(ngModel)]="cardNumberValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    <ion-grid padding>\n    \n    <ion-row >\n    \n    <ion-col col-6>Expiry date</ion-col>\n    \n    <ion-col col-2> </ion-col>\n    \n    <ion-col col-4> CVV</ion-col>\n    \n    </ion-row>\n    \n    <ion-row no-lines align-items-start>\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel" placeholder="MM" name="expiryMonth" #expiryMonth [(ngModel)]="expiryMonthValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col >\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel"  placeholder="YY" name="expiryYear" #expiryYear [(ngModel)]="expiryYearValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col>\n    \n    <ion-col col-2>\n    \n    </ion-col>\n    \n    <ion-col col-4>\n    \n    <div class="cvv">\n    \n    <ion-item>\n    \n    <ion-input type="tel" placeholder="123" name="cvc" #cvc [(ngModel)]="cvcValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    </ion-col>\n    \n    </ion-row>\n    \n    </ion-grid>\n    \n    <div class="pay-button">\n    \n    <!-- <button ion-button full (click)="ChargeCard()">PAY NGN {{data | number:\'.2\'}} </button> -->\n    <button ion-button full (click)="ChargeCard()">PAY NGN {{price}} </button>\n    \n    </div>\n    \n    </ion-list>\n    \n    </div>\n    \n    <div class="footer">\n    \n    <div class="secured">\n    \n    <ion-icon name="lock" item-start></ion-icon>\n    \n    SECURED BY PAYSTACK\n    \n    </div>\n    \n    <div class="whatspaystack">\n    \n    <button ion-button outline  small round class="app-font-25" (click)="whatsPaystack()"> WHAT IS PAYSTACK?</button>\n    \n    </div>\n    \n    </div>\n    \n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]])
    ], PaystackPage);
    return PaystackPage;
}());

// =======================================================
// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
// /**
//  * Generated class for the PaystackPage page.
//  *
//  * See https://ionicframework.com/docs/components/#navigation for more info on
//  * Ionic pages and navigation.
//  */
// @IonicPage()
// @Component({
//   selector: 'page-paystack',
//template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/'<!--\n  Generated template for the PaystackPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n    \n      <ion-title>Make Payment</ion-title>\n    \n    </ion-navbar>\n  \n  </ion-header>\n  \n  <ion-content  >\n  \n    <div class="image" text-center><img  src="assets/imgs/paystack_logo.png" /></div>\n    \n    <!-- <div class="amount">NGN {{data | number:\'.2\'}}</div> -->\n    <div class="amount">NGN {{data}}</div>\n    \n    <div class="input-area">\n    \n    <ion-list >\n    \n    <ion-item >\n    \n    <ion-input type="text" placeholder="Email Address"  #email_add [(ngModel)]="emailValue"></ion-input>\n    \n    </ion-item>\n    \n    <div class="cardnumber">\n    \n    <ion-label >CARD NUMBER</ion-label>\n    \n    <ion-item >\n    \n    <ion-input type="tel" placeholder="0000000000000000" name="card_number" #card_number [(ngModel)]="cardNumberValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    <ion-grid padding>\n    \n    <ion-row >\n    \n    <ion-col col-6>Expiry date</ion-col>\n    \n    <ion-col col-2> </ion-col>\n    \n    <ion-col col-4> CVV</ion-col>\n    \n    </ion-row>\n    \n    <ion-row no-lines align-items-start>\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel" placeholder="MM" name="expiryMonth" #expiryMonth [(ngModel)]="expiryMonthValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col >\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel"  placeholder="YY" name="expiryYear" #expiryYear [(ngModel)]="expiryYearValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col>\n    \n    <ion-col col-2>\n    \n    </ion-col>\n    \n    <ion-col col-4>\n    \n    <div class="cvv">\n    \n    <ion-item>\n    \n    <ion-input type="tel" placeholder="123" name="cvc" #cvc [(ngModel)]="cvcValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    </ion-col>\n    \n    </ion-row>\n    \n    </ion-grid>\n    \n    <div class="pay-button">\n    \n    <!-- <button ion-button full (click)="ChargeCard()">PAY NGN {{data | number:\'.2\'}} </button> -->\n    <button ion-button full (click)="ChargeCard()">PAY NGN {{price}} </button>\n    \n    </div>\n    \n    </ion-list>\n    \n    </div>\n    \n    <div class="footer">\n    \n    <div class="secured">\n    \n    <ion-icon name="lock" item-start></ion-icon>\n    \n    SECURED BY PAYSTACK\n    \n    </div>\n    \n    <div class="whatspaystack">\n    \n    <button ion-button outline  small round class="app-font-25" (click)="whatsPaystack()"> WHAT IS PAYSTACK?</button>\n    \n    </div>\n    \n    </div>\n    \n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/,
// })
// export class PaystackPage {
//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }
//   ionViewDidLoad() {
//     console.log('ionViewDidLoad PaystackPage');
//   }
// }
//# sourceMappingURL=paystack.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagebudgetPageModule", function() { return ManagebudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__managebudget__ = __webpack_require__(839);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ManagebudgetPageModule = (function () {
    function ManagebudgetPageModule() {
    }
    ManagebudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__managebudget__["a" /* ManagebudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__managebudget__["a" /* ManagebudgetPage */]),
            ],
        })
    ], ManagebudgetPageModule);
    return ManagebudgetPageModule;
}());

//# sourceMappingURL=managebudget.module.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuTwoPageModule", function() { return MenuTwoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_two__ = __webpack_require__(840);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MenuTwoPageModule = (function () {
    function MenuTwoPageModule() {
    }
    MenuTwoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__menu_two__["a" /* MenuTwoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__menu_two__["a" /* MenuTwoPage */]),
            ],
        })
    ], MenuTwoPageModule);
    return MenuTwoPageModule;
}());

//# sourceMappingURL=menu-two.module.js.map

/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NairaCardPageModule", function() { return NairaCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__naira_card__ = __webpack_require__(841);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NairaCardPageModule = (function () {
    function NairaCardPageModule() {
    }
    NairaCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__naira_card__["a" /* NairaCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__naira_card__["a" /* NairaCardPage */]),
            ],
        })
    ], NairaCardPageModule);
    return NairaCardPageModule;
}());

//# sourceMappingURL=naira-card.module.js.map

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailsPageModule", function() { return OrderDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_details__ = __webpack_require__(842);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OrderDetailsPageModule = (function () {
    function OrderDetailsPageModule() {
    }
    OrderDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__order_details__["a" /* OrderDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__order_details__["a" /* OrderDetailsPage */]),
            ],
        })
    ], OrderDetailsPageModule);
    return OrderDetailsPageModule;
}());

//# sourceMappingURL=order-details.module.js.map

/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuOnePageModule", function() { return MenuOnePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_one__ = __webpack_require__(843);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MenuOnePageModule = (function () {
    function MenuOnePageModule() {
    }
    MenuOnePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__menu_one__["a" /* MenuOnePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__menu_one__["a" /* MenuOnePage */]),
            ],
        })
    ], MenuOnePageModule);
    return MenuOnePageModule;
}());

//# sourceMappingURL=menu-one.module.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile__ = __webpack_require__(844);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilePageModule = (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */]),
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrepaidCardPageModule", function() { return PrepaidCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__prepaid_card__ = __webpack_require__(845);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PrepaidCardPageModule = (function () {
    function PrepaidCardPageModule() {
    }
    PrepaidCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__prepaid_card__["a" /* PrepaidCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__prepaid_card__["a" /* PrepaidCardPage */]),
            ],
        })
    ], PrepaidCardPageModule);
    return PrepaidCardPageModule;
}());

//# sourceMappingURL=prepaid-card.module.js.map

/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingPageModule", function() { return SettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__setting__ = __webpack_require__(846);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SettingPageModule = (function () {
    function SettingPageModule() {
    }
    SettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */]),
            ],
        })
    ], SettingPageModule);
    return SettingPageModule;
}());

//# sourceMappingURL=setting.module.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SignupPageModule = (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]),
            ],
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashTwoPageModule", function() { return SplashTwoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__splash_two__ = __webpack_require__(847);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SplashTwoPageModule = (function () {
    function SplashTwoPageModule() {
    }
    SplashTwoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__splash_two__["a" /* SplashTwoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__splash_two__["a" /* SplashTwoPage */]),
            ],
        })
    ], SplashTwoPageModule);
    return SplashTwoPageModule;
}());

//# sourceMappingURL=splash-two.module.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubMenuOnePageModule", function() { return SubMenuOnePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_menu_one__ = __webpack_require__(848);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubMenuOnePageModule = (function () {
    function SubMenuOnePageModule() {
    }
    SubMenuOnePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sub_menu_one__["a" /* SubMenuOnePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sub_menu_one__["a" /* SubMenuOnePage */]),
            ],
        })
    ], SubMenuOnePageModule);
    return SubMenuOnePageModule;
}());

//# sourceMappingURL=sub-menu-one.module.js.map

/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashOnePageModule", function() { return SplashOnePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__splash_one__ = __webpack_require__(849);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SplashOnePageModule = (function () {
    function SplashOnePageModule() {
    }
    SplashOnePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__splash_one__["a" /* SplashOnePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__splash_one__["a" /* SplashOnePage */]),
            ],
        })
    ], SplashOnePageModule);
    return SplashOnePageModule;
}());

//# sourceMappingURL=splash-one.module.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewanalyticsPageModule", function() { return ViewanalyticsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__viewanalytics__ = __webpack_require__(850);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewanalyticsPageModule = (function () {
    function ViewanalyticsPageModule() {
    }
    ViewanalyticsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__viewanalytics__["a" /* ViewanalyticsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__viewanalytics__["a" /* ViewanalyticsPage */]),
            ],
        })
    ], ViewanalyticsPageModule);
    return ViewanalyticsPageModule;
}());

//# sourceMappingURL=viewanalytics.module.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewbudgetPageModule", function() { return ViewbudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__viewbudget__ = __webpack_require__(851);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewbudgetPageModule = (function () {
    function ViewbudgetPageModule() {
    }
    ViewbudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__viewbudget__["a" /* ViewbudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__viewbudget__["a" /* ViewbudgetPage */]),
            ],
        })
    ], ViewbudgetPageModule);
    return ViewbudgetPageModule;
}());

//# sourceMappingURL=viewbudget.module.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewtransactionsPageModule", function() { return ViewtransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__viewtransactions__ = __webpack_require__(852);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewtransactionsPageModule = (function () {
    function ViewtransactionsPageModule() {
    }
    ViewtransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__viewtransactions__["a" /* ViewtransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__viewtransactions__["a" /* ViewtransactionsPage */]),
            ],
        })
    ], ViewtransactionsPageModule);
    return ViewtransactionsPageModule;
}());

//# sourceMappingURL=viewtransactions.module.js.map

/***/ }),

/***/ 468:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkthroughPageModule", function() { return WalkthroughPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__walkthrough__ = __webpack_require__(853);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WalkthroughPageModule = (function () {
    function WalkthroughPageModule() {
    }
    WalkthroughPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__walkthrough__["a" /* WalkthroughPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__walkthrough__["a" /* WalkthroughPage */]),
            ],
        })
    ], WalkthroughPageModule);
    return WalkthroughPageModule;
}());

//# sourceMappingURL=walkthrough.module.js.map

/***/ }),

/***/ 469:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubMenuTwoPageModule", function() { return SubMenuTwoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_menu_two__ = __webpack_require__(854);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubMenuTwoPageModule = (function () {
    function SubMenuTwoPageModule() {
    }
    SubMenuTwoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sub_menu_two__["a" /* SubMenuTwoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sub_menu_two__["a" /* SubMenuTwoPage */]),
            ],
        })
    ], SubMenuTwoPageModule);
    return SubMenuTwoPageModule;
}());

//# sourceMappingURL=sub-menu-two.module.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\home\home.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    </ion-navbar>\n</ion-header>\n\n<ion-content text-center>\n    <img src="assets/imgs/logo-gray.png" alt="Logo">\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(521);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_credit_card_credit_card_module__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_prepaid_card_prepaid_card_module__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_forex_card_forex_card_module__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_naira_card_naira_card_module__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_cards_cards_module__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_paystack_paystack_module__ = __webpack_require__(873);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__(874);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_home_home__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_splash_one_splash_one_module__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_splash_two_splash_two_module__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_signup_signup_module__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_walkthrough_walkthrough_module__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_menu_one_menu_one_module__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_menu_two_menu_two_module__ = __webpack_require__(454);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_sub_menu_one_sub_menu_one_module__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_sub_menu_two_sub_menu_two_module__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_order_details_order_details_module__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_favorites_favorites_module__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_profile_profile_module__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_setting_setting_module__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_chooser__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_http__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_initial_initial_module__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_dashboard_dashboard_module__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_analysis_analysis_module__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_viewanalytics_viewanalytics_module__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_addtransactions_addtransactions_module__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_viewtransactions_viewtransactions_module__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_edittransactions_edittransactions_module__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_investment_investment_module__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__components_components_module__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_budget_budget_module__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_managebudget_managebudget_module__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_viewbudget_viewbudget_module__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_fin_info_fin_info_module__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_banking_banking_module__ = __webpack_require__(349);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























// import { AndroidPermissions } from '@ionic-native/android-permissions';








// import { ViewanalyticsPage } from '../pages/viewanalytics/viewanalytics';





// =======================================================================================================
// import { ProgressBarComponent } from '../components/progress-bar/progress-bar';






// =======================================================================================================
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */], {
                    mode: 'ios',
                    backButtonText: '',
                }, {
                    links: [
                        { loadChildren: '../pages/_projectsbudget/projectsbudget.module#ProjectsbudgetPageModule', name: 'ProjectsbudgetPage', segment: 'projectsbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addtransactions/addtransactions.module#AddtransactionsPageModule', name: 'AddtransactionsPage', segment: 'addtransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/analysis/analysis.module#AnalysisPageModule', name: 'AnalysisPage', segment: 'analysis', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/banking/banking.module#BankingPageModule', name: 'BankingPage', segment: 'banking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cards/cards.module#CardsPageModule', name: 'CardsPage', segment: 'cards', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/credit-card/credit-card.module#CreditCardPageModule', name: 'CreditCardPage', segment: 'credit-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/budget/budget.module#BudgetPageModule', name: 'BudgetPage', segment: 'budget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/current/current.module#CurrentPageModule', name: 'CurrentPage', segment: 'current', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/debtbudget/debtbudget.module#DebtbudgetPageModule', name: 'DebtbudgetPage', segment: 'debtbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edittransactions/edittransactions.module#EdittransactionsPageModule', name: 'EdittransactionsPage', segment: 'edittransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expense/expense.module#ExpensePageModule', name: 'ExpensePage', segment: 'expense', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expensebudget/expensebudget.module#ExpensebudgetPageModule', name: 'ExpensebudgetPage', segment: 'expensebudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fin-info/fin-info.module#FinInfoPageModule', name: 'FinInfoPage', segment: 'fin-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/favorites/favorites.module#FavoritesPageModule', name: 'FavoritesPage', segment: 'favorites', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fixed-deposit/fixed-deposit.module#FixedDepositPageModule', name: 'FixedDepositPage', segment: 'fixed-deposit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/foreign/foreign.module#ForeignPageModule', name: 'ForeignPage', segment: 'foreign', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forex-card/forex-card.module#ForexCardPageModule', name: 'ForexCardPage', segment: 'forex-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/incomebudget/incomebudget.module#IncomebudgetPageModule', name: 'IncomebudgetPage', segment: 'incomebudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/initial/initial.module#InitialPageModule', name: 'InitialPage', segment: 'initial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/income/income.module#IncomePageModule', name: 'IncomePage', segment: 'income', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/investment/investment.module#InvestmentPageModule', name: 'InvestmentPage', segment: 'investment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/liabilities/liabilities.module#LiabilitiesPageModule', name: 'LiabilitiesPage', segment: 'liabilities', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/liabilitiesbudget/liabilitiesbudget.module#LiabilitiesbudgetPageModule', name: 'LiabilitiesbudgetPage', segment: 'liabilitiesbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/managebudget/managebudget.module#ManagebudgetPageModule', name: 'ManagebudgetPage', segment: 'managebudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loadsmstransactions/loadsmstransactions.module#LoadsmstransactionsPageModule', name: 'LoadsmstransactionsPage', segment: 'loadsmstransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu-two/menu-two.module#MenuTwoPageModule', name: 'MenuTwoPage', segment: 'menu-two', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/naira-card/naira-card.module#NairaCardPageModule', name: 'NairaCardPage', segment: 'naira-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order-details/order-details.module#OrderDetailsPageModule', name: 'OrderDetailsPage', segment: 'order-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pay-conf/pay-conf.module#PayConfPageModule', name: 'PayConfPage', segment: 'pay-conf', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu-one/menu-one.module#MenuOnePageModule', name: 'MenuOnePage', segment: 'menu-one', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/projectbudget/projectbudget.module#ProjectbudgetPageModule', name: 'ProjectbudgetPage', segment: 'projectbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/projects/projects.module#ProjectsPageModule', name: 'ProjectsPage', segment: 'projects', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sav-invbudget/sav-invbudget.module#SavInvbudgetPageModule', name: 'SavInvbudgetPage', segment: 'sav-invbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/savings/savings.module#SavingsPageModule', name: 'SavingsPage', segment: 'savings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/prepaid-card/prepaid-card.module#PrepaidCardPageModule', name: 'PrepaidCardPage', segment: 'prepaid-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting.module#SettingPageModule', name: 'SettingPage', segment: 'setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sav-inv/sav-inv.module#SavInvPageModule', name: 'SavInvPage', segment: 'sav-inv', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/splash-two/splash-two.module#SplashTwoPageModule', name: 'SplashTwoPage', segment: 'splash-two', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sub-menu-one/sub-menu-one.module#SubMenuOnePageModule', name: 'SubMenuOnePage', segment: 'sub-menu-one', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/splash-one/splash-one.module#SplashOnePageModule', name: 'SplashOnePage', segment: 'splash-one', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewanalytics/viewanalytics.module#ViewanalyticsPageModule', name: 'ViewanalyticsPage', segment: 'viewanalytics', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewbudget/viewbudget.module#ViewbudgetPageModule', name: 'ViewbudgetPage', segment: 'viewbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewtransactions/viewtransactions.module#ViewtransactionsPageModule', name: 'ViewtransactionsPage', segment: 'viewtransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/walkthrough/walkthrough.module#WalkthroughPageModule', name: 'WalkthroughPage', segment: 'walkthrough', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sub-menu-two/sub-menu-two.module#SubMenuTwoPageModule', name: 'SubMenuTwoPage', segment: 'sub-menu-two', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_33__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_14__pages_splash_one_splash_one_module__["SplashOnePageModule"],
                __WEBPACK_IMPORTED_MODULE_15__pages_splash_two_splash_two_module__["SplashTwoPageModule"],
                __WEBPACK_IMPORTED_MODULE_16__pages_signup_signup_module__["SignupPageModule"],
                __WEBPACK_IMPORTED_MODULE_17__pages_walkthrough_walkthrough_module__["WalkthroughPageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_menu_one_menu_one_module__["MenuOnePageModule"],
                __WEBPACK_IMPORTED_MODULE_19__pages_menu_two_menu_two_module__["MenuTwoPageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_sub_menu_one_sub_menu_one_module__["SubMenuOnePageModule"],
                __WEBPACK_IMPORTED_MODULE_21__pages_sub_menu_two_sub_menu_two_module__["SubMenuTwoPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__pages_order_details_order_details_module__["OrderDetailsPageModule"],
                __WEBPACK_IMPORTED_MODULE_23__pages_favorites_favorites_module__["FavoritesPageModule"],
                __WEBPACK_IMPORTED_MODULE_24__pages_profile_profile_module__["ProfilePageModule"],
                __WEBPACK_IMPORTED_MODULE_25__pages_setting_setting_module__["SettingPageModule"],
                __WEBPACK_IMPORTED_MODULE_31__pages_initial_initial_module__["InitialPageModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_dashboard_dashboard_module__["DashboardPageModule"],
                __WEBPACK_IMPORTED_MODULE_34__pages_analysis_analysis_module__["AnalysisPageModule"],
                __WEBPACK_IMPORTED_MODULE_35__pages_viewanalytics_viewanalytics_module__["ViewanalyticsPageModule"],
                __WEBPACK_IMPORTED_MODULE_36__pages_addtransactions_addtransactions_module__["AddtransactionsPageModule"],
                __WEBPACK_IMPORTED_MODULE_37__pages_viewtransactions_viewtransactions_module__["ViewtransactionsPageModule"],
                __WEBPACK_IMPORTED_MODULE_38__pages_edittransactions_edittransactions_module__["EdittransactionsPageModule"],
                __WEBPACK_IMPORTED_MODULE_39__pages_investment_investment_module__["InvestmentPageModule"],
                __WEBPACK_IMPORTED_MODULE_41__pages_budget_budget_module__["BudgetPageModule"],
                __WEBPACK_IMPORTED_MODULE_42__pages_managebudget_managebudget_module__["ManagebudgetPageModule"],
                __WEBPACK_IMPORTED_MODULE_43__pages_viewbudget_viewbudget_module__["ViewbudgetPageModule"],
                __WEBPACK_IMPORTED_MODULE_40__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_30__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__pages_paystack_paystack_module__["a" /* PaystackPageModule */],
                __WEBPACK_IMPORTED_MODULE_44__pages_fin_info_fin_info_module__["FinInfoPageModule"],
                __WEBPACK_IMPORTED_MODULE_45__pages_banking_banking_module__["BankingPageModule"],
                __WEBPACK_IMPORTED_MODULE_4__pages_cards_cards_module__["CardsPageModule"],
                __WEBPACK_IMPORTED_MODULE_3__pages_naira_card_naira_card_module__["NairaCardPageModule"],
                __WEBPACK_IMPORTED_MODULE_2__pages_forex_card_forex_card_module__["ForexCardPageModule"],
                __WEBPACK_IMPORTED_MODULE_1__pages_prepaid_card_prepaid_card_module__["PrepaidCardPageModule"],
                __WEBPACK_IMPORTED_MODULE_0__pages_credit_card_credit_card_module__["CreditCardPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
            ],
            providers: [
                // AndroidPermissions,
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__["a" /* StatusBar */],
                // NavParams,
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__["a" /* FileTransfer */],
                { provide: __WEBPACK_IMPORTED_MODULE_8__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_26__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 541:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddtransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddtransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddtransactionsPage = (function () {
    function AddtransactionsPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 1;
    }
    AddtransactionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddtransactionsPage');
        this.valName = this.navParams.get('val');
        this.getUserName();
    };
    AddtransactionsPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.buckets = _this.grsp.buckets();
        });
    };
    AddtransactionsPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "type": this.type,
            "desc": this.desc,
            "bucket": this.bucket,
            "amt": this.amt,
            "date": this.date
        };
        // });
    };
    AddtransactionsPage.prototype.saveAddApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        console.log(this.apiData);
        this.grsp.addTrans(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Saved Transaction ' + _this.apiRes.desc,
                    buttons: ['Ok']
                });
                alert_1.present();
                _this.type = '';
                _this.desc = '';
                _this.bucket = '';
                _this.amt = '';
                _this.date = '';
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Failed',
                    subTitle: 'Something went wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_2.present();
            }
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    AddtransactionsPage.prototype.saveAdd = function () {
        this.desc = this.desc ? this.desc : "";
        var alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: 'Successfully Saved Transaction ' + this.desc,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    AddtransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-addtransactions',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\addtransactions\addtransactions.html"*/'<!--\n  Generated template for the AddtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <div class="logo-container" text-center [style.display]="\'block\'">\n        <img src="assets/imgs/logo.png" alt="Logo">\n\n        <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n            <ion-segment-button value="signin">\n                Sign In\n            </ion-segment-button>\n           \n        </ion-segment> -->\n        \n        <ion-navbar>\n          <ion-title>Add Transactions for {{userName}}</ion-title>\n      \n        </ion-navbar>\n\n    </div>\n\n    \n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n  \n  <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n  \n      <ion-item>\n        <ion-label stacked>Transaction Type</ion-label>\n        <ion-select [(ngModel)]="type">\n          <ion-option value="cr">Credit</ion-option>\n          <ion-option value="dr">Debit</ion-option>\n        </ion-select>\n      </ion-item>\n  \n      <ion-item >\n        <ion-label stacked>Description</ion-label>\n        <ion-input type=""  [(ngModel)]="desc"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Bucket</ion-label>\n        <ion-select [(ngModel)]="bucket">\n          <!-- <ion-option value="b1">B1</ion-option> -->\n          <ion-option *ngFor="let x of buckets" [value]="x">{{x}}</ion-option>\n        </ion-select>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Amount</ion-label>\n        <ion-input type="number" [(ngModel)]="amt"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Date</ion-label>\n          <ion-input type="date"  [(ngModel)]="date"></ion-input>\n        </ion-item>\n  \n      <ion-item>\n          <!-- <button ion-button small (click)="saveAdd()">Save</button> -->\n          <button ion-button small (click)="saveAddApi()">Save</button>\n      </ion-item>\n  \n  </ion-content>\n  '/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\addtransactions\addtransactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], AddtransactionsPage);
    return AddtransactionsPage;
}());

//# sourceMappingURL=addtransactions.js.map

/***/ }),

/***/ 545:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnalysisPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AnalysisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AnalysisPage = (function () {
    function AnalysisPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'View Analytics',
                title: 'View Analytics',
                description: 'See How Your Numbers Are Looking',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Add Transaction(s)',
                title: 'Add Transaction(s)',
                description: 'Anything We Might Have Missed?',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'Edit Transaction(s)',
                title: 'Edit Transaction(s)',
                description: 'Because You Have Full Control',
                img: 'assets/imgs/menu/munchies.png'
            },
        ];
    }
    AnalysisPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad AnalysisPage');
    };
    AnalysisPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('ViewanalyticsPage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('AddtransactionsPage', param);
        }
        else {
            var param = { category: 3 };
            this.navCtrl.push('ViewtransactionsPage', param);
        }
    };
    AnalysisPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    AnalysisPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    AnalysisPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-analysis',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\analysis\analysis.html"*/'<!--\n  Generated template for the AnalysisPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Analysis</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\analysis\analysis.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], AnalysisPage);
    return AnalysisPage;
}());

//# sourceMappingURL=analysis.js.map

/***/ }),

/***/ 547:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 222,
	"./af.js": 222,
	"./ar": 223,
	"./ar-dz": 224,
	"./ar-dz.js": 224,
	"./ar-kw": 225,
	"./ar-kw.js": 225,
	"./ar-ly": 226,
	"./ar-ly.js": 226,
	"./ar-ma": 227,
	"./ar-ma.js": 227,
	"./ar-sa": 228,
	"./ar-sa.js": 228,
	"./ar-tn": 229,
	"./ar-tn.js": 229,
	"./ar.js": 223,
	"./az": 230,
	"./az.js": 230,
	"./be": 231,
	"./be.js": 231,
	"./bg": 232,
	"./bg.js": 232,
	"./bm": 233,
	"./bm.js": 233,
	"./bn": 234,
	"./bn.js": 234,
	"./bo": 235,
	"./bo.js": 235,
	"./br": 236,
	"./br.js": 236,
	"./bs": 237,
	"./bs.js": 237,
	"./ca": 238,
	"./ca.js": 238,
	"./cs": 239,
	"./cs.js": 239,
	"./cv": 240,
	"./cv.js": 240,
	"./cy": 241,
	"./cy.js": 241,
	"./da": 242,
	"./da.js": 242,
	"./de": 243,
	"./de-at": 244,
	"./de-at.js": 244,
	"./de-ch": 245,
	"./de-ch.js": 245,
	"./de.js": 243,
	"./dv": 246,
	"./dv.js": 246,
	"./el": 247,
	"./el.js": 247,
	"./en-SG": 248,
	"./en-SG.js": 248,
	"./en-au": 249,
	"./en-au.js": 249,
	"./en-ca": 250,
	"./en-ca.js": 250,
	"./en-gb": 251,
	"./en-gb.js": 251,
	"./en-ie": 252,
	"./en-ie.js": 252,
	"./en-il": 253,
	"./en-il.js": 253,
	"./en-nz": 254,
	"./en-nz.js": 254,
	"./eo": 255,
	"./eo.js": 255,
	"./es": 256,
	"./es-do": 257,
	"./es-do.js": 257,
	"./es-us": 258,
	"./es-us.js": 258,
	"./es.js": 256,
	"./et": 259,
	"./et.js": 259,
	"./eu": 260,
	"./eu.js": 260,
	"./fa": 261,
	"./fa.js": 261,
	"./fi": 262,
	"./fi.js": 262,
	"./fo": 263,
	"./fo.js": 263,
	"./fr": 264,
	"./fr-ca": 265,
	"./fr-ca.js": 265,
	"./fr-ch": 266,
	"./fr-ch.js": 266,
	"./fr.js": 264,
	"./fy": 267,
	"./fy.js": 267,
	"./ga": 268,
	"./ga.js": 268,
	"./gd": 269,
	"./gd.js": 269,
	"./gl": 270,
	"./gl.js": 270,
	"./gom-latn": 271,
	"./gom-latn.js": 271,
	"./gu": 272,
	"./gu.js": 272,
	"./he": 273,
	"./he.js": 273,
	"./hi": 274,
	"./hi.js": 274,
	"./hr": 275,
	"./hr.js": 275,
	"./hu": 276,
	"./hu.js": 276,
	"./hy-am": 277,
	"./hy-am.js": 277,
	"./id": 278,
	"./id.js": 278,
	"./is": 279,
	"./is.js": 279,
	"./it": 280,
	"./it-ch": 281,
	"./it-ch.js": 281,
	"./it.js": 280,
	"./ja": 282,
	"./ja.js": 282,
	"./jv": 283,
	"./jv.js": 283,
	"./ka": 284,
	"./ka.js": 284,
	"./kk": 285,
	"./kk.js": 285,
	"./km": 286,
	"./km.js": 286,
	"./kn": 287,
	"./kn.js": 287,
	"./ko": 288,
	"./ko.js": 288,
	"./ku": 289,
	"./ku.js": 289,
	"./ky": 290,
	"./ky.js": 290,
	"./lb": 291,
	"./lb.js": 291,
	"./lo": 292,
	"./lo.js": 292,
	"./lt": 293,
	"./lt.js": 293,
	"./lv": 294,
	"./lv.js": 294,
	"./me": 295,
	"./me.js": 295,
	"./mi": 296,
	"./mi.js": 296,
	"./mk": 297,
	"./mk.js": 297,
	"./ml": 298,
	"./ml.js": 298,
	"./mn": 299,
	"./mn.js": 299,
	"./mr": 300,
	"./mr.js": 300,
	"./ms": 301,
	"./ms-my": 302,
	"./ms-my.js": 302,
	"./ms.js": 301,
	"./mt": 303,
	"./mt.js": 303,
	"./my": 304,
	"./my.js": 304,
	"./nb": 305,
	"./nb.js": 305,
	"./ne": 306,
	"./ne.js": 306,
	"./nl": 307,
	"./nl-be": 308,
	"./nl-be.js": 308,
	"./nl.js": 307,
	"./nn": 309,
	"./nn.js": 309,
	"./pa-in": 310,
	"./pa-in.js": 310,
	"./pl": 311,
	"./pl.js": 311,
	"./pt": 312,
	"./pt-br": 313,
	"./pt-br.js": 313,
	"./pt.js": 312,
	"./ro": 314,
	"./ro.js": 314,
	"./ru": 315,
	"./ru.js": 315,
	"./sd": 316,
	"./sd.js": 316,
	"./se": 317,
	"./se.js": 317,
	"./si": 318,
	"./si.js": 318,
	"./sk": 319,
	"./sk.js": 319,
	"./sl": 320,
	"./sl.js": 320,
	"./sq": 321,
	"./sq.js": 321,
	"./sr": 322,
	"./sr-cyrl": 323,
	"./sr-cyrl.js": 323,
	"./sr.js": 322,
	"./ss": 324,
	"./ss.js": 324,
	"./sv": 325,
	"./sv.js": 325,
	"./sw": 326,
	"./sw.js": 326,
	"./ta": 327,
	"./ta.js": 327,
	"./te": 328,
	"./te.js": 328,
	"./tet": 329,
	"./tet.js": 329,
	"./tg": 330,
	"./tg.js": 330,
	"./th": 331,
	"./th.js": 331,
	"./tl-ph": 332,
	"./tl-ph.js": 332,
	"./tlh": 333,
	"./tlh.js": 333,
	"./tr": 334,
	"./tr.js": 334,
	"./tzl": 335,
	"./tzl.js": 335,
	"./tzm": 336,
	"./tzm-latn": 337,
	"./tzm-latn.js": 337,
	"./tzm.js": 336,
	"./ug-cn": 338,
	"./ug-cn.js": 338,
	"./uk": 339,
	"./uk.js": 339,
	"./ur": 340,
	"./ur.js": 340,
	"./uz": 341,
	"./uz-latn": 342,
	"./uz-latn.js": 342,
	"./uz.js": 341,
	"./vi": 343,
	"./vi.js": 343,
	"./x-pseudo": 344,
	"./x-pseudo.js": 344,
	"./yo": 345,
	"./yo.js": 345,
	"./zh-cn": 346,
	"./zh-cn.js": 346,
	"./zh-hk": 347,
	"./zh-hk.js": 347,
	"./zh-tw": 348,
	"./zh-tw.js": 348
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 547;

/***/ }),

/***/ 548:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BankingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the BankingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BankingPage = (function () {
    function BankingPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Savings',
                title: 'Savings',
                description: 'Banking Savings',
                img: 'assets/imgs/menu/coffee.png',
                page: 'SavingsPage'
            },
            {
                id: 2,
                name: 'Current',
                title: 'Current',
                description: 'Banking Current',
                img: 'assets/imgs/menu/breakfast.png',
                page: 'CurrentPage'
            },
            {
                id: 3,
                name: 'Fixed Deposit',
                title: 'Fixed Deposit',
                description: 'Banking Fixed Deposit',
                img: 'assets/imgs/menu/munchies.png',
                page: 'FixedDepositPage'
            },
            {
                id: 4,
                name: 'Foreign',
                title: 'Foreign',
                description: 'Banking Foreign',
                img: 'assets/imgs/menu/coffee.png',
                page: 'ForeignPage'
            },
        ];
    }
    BankingPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad BankingPage');
        // alert('work here');
    };
    BankingPage.prototype.bankAction = function (cat) {
        /* Pass category object as a parameter */
        // if(cat.id == 1){
        // let param = { category: 1 };
        this.navCtrl.push(cat.page);
        // }
        // else if(cat.id == 2){
        //   let param = { category: 2 };
        //   this.navCtrl.push('AddtransactionsPage', param );
        // }
        // else{ 
        //   let param = { category: 3 };
        //   this.navCtrl.push('ViewtransactionsPage', param );
        // }
    };
    BankingPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    BankingPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    BankingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-banking',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\banking\banking.html"*/'<!--\n  Generated template for the BankingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title>Banking</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="bankAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\banking\banking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], BankingPage);
    return BankingPage;
}());

//# sourceMappingURL=banking.js.map

/***/ }),

/***/ 549:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CardsPage = (function () {
    function CardsPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Naira',
                title: 'Naira',
                description: 'Naira Cards',
                img: 'assets/imgs/menu/coffee.png',
                page: 'NairaCardPage'
            },
            {
                id: 2,
                name: 'Forex',
                title: 'Forex',
                description: 'Forex Cards',
                img: 'assets/imgs/menu/breakfast.png',
                page: 'ForexCardPage'
            },
            {
                id: 3,
                name: 'Prepaid',
                title: 'Prepaid',
                description: 'Prepaid Cards',
                img: 'assets/imgs/menu/munchies.png',
                page: 'PrepaidCardPage'
            },
            {
                id: 4,
                name: 'Credit',
                title: 'Credit',
                description: 'Credit Cards',
                img: 'assets/imgs/menu/coffee.png',
                page: 'CreditCardPage'
            },
        ];
    }
    CardsPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad CardsPage');
    };
    CardsPage.prototype.cardsAction = function (cat) {
        /* Pass category object as a parameter */
        // if(cat.id == 1){
        // let param = { category: 1 };
        this.navCtrl.push(cat.page);
        // }
        // else if(cat.id == 2){
        //   let param = { category: 2 };
        //   this.navCtrl.push('AddtransactionsPage', param );
        // }
        // else{ 
        //   let param = { category: 3 };
        //   this.navCtrl.push('ViewtransactionsPage', param );
        // }
    };
    CardsPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    CardsPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    CardsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cards',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\cards\cards.html"*/'<!--\n  Generated template for the CardsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title>Cards</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="cardsAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\cards\cards.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], CardsPage);
    return CardsPage;
}());

//# sourceMappingURL=cards.js.map

/***/ }),

/***/ 550:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the BudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BudgetPage = (function () {
    function BudgetPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'View Budget Analytics',
                title: 'View Budget',
                description: 'See How Your Numbers Are Looking',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Manage Budget(s)',
                title: 'Manage Budget(s)',
                description: 'Because You Have Full Control',
                img: 'assets/imgs/menu/munchies.png'
            },
        ];
    }
    BudgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BudgetPage');
    };
    BudgetPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    BudgetPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('ViewbudgetPage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('ManagebudgetPage', param);
        }
    };
    BudgetPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    BudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-budget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\budget\budget.html"*/'<!--\n  Generated template for the BudgetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Budget</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\budget\budget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], BudgetPage);
    return BudgetPage;
}());

//# sourceMappingURL=budget.js.map

/***/ }),

/***/ 551:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the ProgressBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ProgressBarComponent = (function () {
    function ProgressBarComponent() {
        console.log('Hello ProgressBarComponent Component');
        this.text = 'Hello World';
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('progress'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "progress", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('progressAge'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "progressAge", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('tleft'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "tleft", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('tright'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "tright", void 0);
    ProgressBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'progress-bar',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\components\progress-bar\progress-bar.html"*/'<!-- Generated template for the ProgressBarComponent component -->\n<div class="progress-outer">\n  <div class="progress-inner" [style.width]="progress + \'%\'">\n      {{progressAge}} Years\n  </div>\n</div>\n<!-- <div class="progress-label">\n\n  <i class="labelLeft">{{tleft}}Years</i>\n  <i class="labelRight">{{tright}}Years</i>\n</div> -->\n<div class="progress-label">\n\n  <i float-left >{{tleft}} Year(s)</i>\n  <i float-right >{{tright}} Year(s)</i>\n</div>\n<!-- <i text-left></i> -->'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\components\progress-bar\progress-bar.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProgressBarComponent);
    return ProgressBarComponent;
}());

//# sourceMappingURL=progress-bar.js.map

/***/ }),

/***/ 552:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EdittransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the EdittransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EdittransactionsPage = (function () {
    function EdittransactionsPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 1;
    }
    EdittransactionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EdittransactionsPage');
        this.desc = this.navParams.get('trans').trans_desc;
        this.type = this.navParams.get('trans').trans_type;
        this.bucket = this.navParams.get('trans').trans_bucket;
        this.amt = this.navParams.get('trans').trans_amt;
        this.date = this.navParams.get('trans').trans_date.substring(0, 10);
        this.tId = this.navParams.get('trans').trans_id;
        // this.desc = this.navParams.get('trans');
        this.getUserName();
    };
    EdittransactionsPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.buckets = _this.grsp.buckets();
        });
    };
    EdittransactionsPage.prototype.saveEdit = function () {
        this.desc = this.desc ? this.desc : "";
        var alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: 'Successfully Saved Transaction ' + this.desc,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    EdittransactionsPage.prototype.setApiJson = function () {
        this.apiData = {
            "uId": this.uId,
            "type": this.type,
            "desc": this.desc,
            "bucket": this.bucket,
            "amt": this.amt,
            "date": this.date,
            "tid": this.tId
        };
    };
    EdittransactionsPage.prototype.saveEditApi = function () {
        var _this = this;
        this.setApiJson();
        this.loaded = 0;
        console.log(this.apiData);
        this.grsp.editTrans(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Edited Transaction ' + _this.apiRes.desc,
                    buttons: ['Dismiss']
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Sav ',
                    buttons: ['Dismiss']
                });
                alert_2.present();
            }
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    EdittransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edittransactions',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\edittransactions\edittransactions.html"*/'<!--\n  Generated template for the EdittransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--\n  Generated template for the AddtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <div class="logo-container" text-center [style.display]="\'block\'">\n        <img src="assets/imgs/logo.png" alt="Logo">\n\n        <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n            <ion-segment-button value="signin">\n                Sign In\n            </ion-segment-button>\n           \n        </ion-segment> -->\n        \n        <ion-navbar>\n          <!-- <ion-title>Add Transactions for {{userName}}</ion-title> -->\n          <ion-title>Edit {{userName}}\'s Transactions</ion-title>\n\n      \n        </ion-navbar>\n\n    </div>\n\n    \n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n  \n  <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n  \n      <ion-item>\n        <ion-label stacked>Transaction Type</ion-label>\n        <ion-select [(ngModel)]="type">\n          <ion-option value="cr">Credit</ion-option>\n          <ion-option value="dr">Debit</ion-option>\n        </ion-select>\n      </ion-item>\n  \n      <ion-item >\n        <ion-label stacked>Description</ion-label>\n        <ion-input type=""  [(ngModel)]="desc"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Bucket</ion-label>\n        <ion-select [(ngModel)]="bucket">\n          <!-- <ion-option value="b1">B1</ion-option> -->\n          <!-- <ion-option value="b2">B2</ion-option> -->\n          <ion-option *ngFor="let x of buckets" [value]="x">{{x}}</ion-option>\n\n        </ion-select>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Amount</ion-label>\n        <ion-input type="number" [(ngModel)]="amt"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Date</ion-label>\n          <ion-input type="date"  [(ngModel)]="date"></ion-input>\n        </ion-item>\n  \n      <ion-item>\n          <!-- <button ion-button small (click)="saveAdd()">Save</button> -->\n          <button ion-button small (click)="saveEditApi()">Save</button>\n      </ion-item>\n  \n  </ion-content>\n  \n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\edittransactions\edittransactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], EdittransactionsPage);
    return EdittransactionsPage;
}());

//# sourceMappingURL=edittransactions.js.map

/***/ }),

/***/ 553:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FinInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the FinInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FinInfoPage = (function () {
    function FinInfoPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Banking',
                title: 'Banking',
                description: 'Everything You Need to Have Handy on Banking',
                img: 'assets/imgs/menu/coffee.png',
                page: 'BankingPage'
            },
            {
                id: 2,
                name: 'Cards',
                title: 'Cards',
                description: 'Get Clarity on How Your Cards are Managed',
                img: 'assets/imgs/menu/breakfast.png',
                page: 'CardsPage'
            },
        ];
    }
    FinInfoPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad FinInfoPage');
    };
    FinInfoPage.prototype.fiAction = function (cat) {
        /* Pass category object as a parameter */
        // if(cat.id == 1){
        // let param = { category: 1 };
        this.navCtrl.push(cat.page);
        // }
        // else if(cat.id == 2){
        //   let param = { category: 2 };
        //   this.navCtrl.push('AddtransactionsPage', param );
        // }
        // else{ 
        //   let param = { category: 3 };
        //   this.navCtrl.push('ViewtransactionsPage', param );
        // }
    };
    FinInfoPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    FinInfoPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    FinInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fin-info',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\fin-info\fin-info.html"*/'<!--\n  Generated template for the FinInfoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title>Financial Info.</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="fiAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\fin-info\fin-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FinInfoPage);
    return FinInfoPage;
}());

//# sourceMappingURL=fin-info.js.map

/***/ }),

/***/ 554:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FavoritesPage = (function () {
    function FavoritesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = [
            {
                id: 6,
                name: 'limeRefreshers',
                title: 'Lime Refreshers',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/limeRefreshers.png',
                category: 'coffe',
                price: '12',
                likes: '1900',
                isliked: true,
            },
            {
                id: 4,
                name: 'vanillaFrappucino',
                title: 'Vanilla Frappucino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/vanillaFrappucino.png',
                category: 'coffe',
                price: '9.85',
                likes: '2300',
                isliked: true,
            },
            {
                id: 4,
                name: 'espresso',
                title: 'Espresso',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/espresso.png',
                category: 'coffe',
                price: '4.50',
                likes: '1800',
                isliked: true,
            },
            {
                id: 5,
                name: 'icedAmericano',
                title: 'Iced Americano',
                description: 'Locally Roasted',
                img: 'assets/imgs/submenu/icedAmericano.png',
                category: 'coffe',
                price: '10.50',
                likes: '2300',
                isliked: true,
            },
            {
                id: 3,
                name: 'kickFrappe',
                title: 'Kick Frappe',
                description: 'Coffee Kick',
                img: 'assets/imgs/submenu/kickFrappe.png',
                category: 'coffe',
                price: '12.35',
                likes: '4450',
                isliked: true,
            },
            {
                id: 2,
                name: 'caramelFrappe',
                title: 'Caramel Frappe',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/caramelFrappe.png',
                category: 'coffe',
                price: '7.85',
                likes: '3456',
                isliked: true,
            },
            {
                id: 4,
                name: 'cappuccino',
                title: 'Cappuccino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/cappuccino.png',
                category: 'coffe',
                price: '10.65',
                likes: '2300',
                isliked: true,
            },
            {
                id: 1,
                name: 'chocoFrappe',
                title: 'Choco Frappe',
                description: 'Chocolate Whirl',
                img: 'assets/imgs/submenu/chocoFrappe.png',
                category: 'coffe',
                price: '9',
                likes: '3200',
                isliked: true,
            },
        ];
    }
    FavoritesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FavoritesPage');
    };
    FavoritesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-favorites',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\favorites\favorites.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Likes</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-list no-lines no-margin padding-horizontal>\n        <ion-item *ngFor="let item of items">\n            <ion-thumbnail item-start>\n                <img src="{{item.img}}">\n            </ion-thumbnail>\n            <h2>{{ item.title }}</h2>\n            <p>{{ item.description }}</p>\n            <ion-row no-padding>\n                <ion-col no-padding>\n                    <button no-padding ion-button clear small color="primary">\n                  {{ item.price | currency:\'USD\':true:\'1.2\'}}\n                  </button>\n                </ion-col>\n                <ion-col text-right no-padding>\n                    <button no-padding ion-button clear small [color]="item.isliked? \'danger\' : \'light\'" (click)="item.isliked = !item.isliked" icon-end>\n                  <ion-icon name=\'heart\'></ion-icon>\n                  <!-- {{ item.likes | number}} -->\n                </button>\n                </ion-col>\n            </ion-row>\n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\favorites\favorites.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FavoritesPage);
    return FavoritesPage;
}());

//# sourceMappingURL=favorites.js.map

/***/ }),

/***/ 555:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForexCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ForexCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForexCardPage = (function () {
    function ForexCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    ForexCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForexCardPage');
        this.getUserName();
    };
    ForexCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getForexApi();
        });
    };
    ForexCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Foreign"
        };
        // });
    };
    ForexCardPage.prototype.getForexApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.forex = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.forexCrd = JSON.parse(_this.forex.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    ForexCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forex-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\forex-card\forex-card.html"*/'<!--\n  Generated template for the ForexCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Foreign Currency Card</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  \n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of forexCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\forex-card\forex-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ForexCardPage);
    return ForexCardPage;
}());

//# sourceMappingURL=forex-card.js.map

/***/ }),

/***/ 556:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvestmentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__paystack_paystack__ = __webpack_require__(360);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the InvestmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InvestmentPage = (function () {
    function InvestmentPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.showHide = false;
        this.recur = false;
        this.model = {};
        this.loaded = 1;
        this.more = 0;
    }
    InvestmentPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    InvestmentPage.prototype.ionViewDidLoad = function () {
        this.load();
        // let payRes = this.navParams.get('res');
        console.log('ionViewDidLoad InvestmentPage');
        this.valName = this.navParams.get('val');
        this.getUserName();
        //     if (payRes == "success"){
        //       // this.buyInvestApi();
        // console.log(payRes);
        // this.storage.get('investData').then((data) => {
        //   console.log(data);
        //   this.apiData = data;
        //   this.buyInvestApi();
        //   console.log("done");
        // });
        //     }
        //     else{
        //       // alert("Something Went Wrong, Please Contact Admin");
        //     }
    };
    InvestmentPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getInvestmentsApi();
        });
    };
    InvestmentPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        var md = {
            "recurring": this.recur,
            "frequency": this.freq,
            "deductDate": this.deductDate,
        };
        // md = JSON.stringify(md);
        this.apiData = {
            "uId": this.uId,
            "type": "Gaid",
            "prov": "Gaid",
            "amt": this.amount,
            "model": JSON.stringify(md)
        };
        console.log(this.apiData);
        // });
    };
    InvestmentPage.prototype.goPay = function () {
        this.setApiJson();
        var data = {
            price: this.amount
            // data:this.apiData
        };
        console.log(data);
        this.storage.set('investData', this.apiData);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__paystack_paystack__["a" /* PaystackPage */], data);
    };
    InvestmentPage.prototype.buyInvestApi = function () {
        var _this = this;
        // this.setApiJson();
        this.loaded = 0;
        console.log(this.apiData);
        console.log("done");
        this.grsp.buyInvestment(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Saved NGN' + _this.amount + ' of ' + _this.apiRes.prov + ' Investment ',
                    buttons: ['Ok']
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Failed',
                    subTitle: 'Something went wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_2.present();
            }
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        this.storage.set('investData', null);
    };
    InvestmentPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    InvestmentPage.prototype.toggleMore = function () {
        if (this.more == 0) {
            this.more = 1;
        }
        else if (this.more == 1) {
            this.more = 0;
        }
    };
    InvestmentPage.prototype.setInvApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiInvData = {
            "uId": this.uId
        };
        // });
    };
    InvestmentPage.prototype.getInvestmentsApi = function () {
        var _this = this;
        this.setInvApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiInvData);
        this.grsp.investments(this.apiInvData).then(function (data) {
            console.log(data);
            _this.inv = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.investments = JSON.parse(_this.inv.investments);
            _this.investments = _this.investments.reverse();
            _this.totInvest = 0;
            _this.investments.forEach(function (obj) {
                obj.actAmt = _this.numberWithCommas(obj.inv_amt);
                _this.totInvest += parseInt(obj.inv_amt);
            });
            _this.totInvest = _this.numberWithCommas(_this.totInvest);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    // ===============================================
    InvestmentPage.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
    };
    InvestmentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-investment',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\investment\investment.html"*/'<!--\n  Generated template for the InvestmentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n         <ion-icon name="menu"></ion-icon>\n       </button>\n        <!-- <ion-title>Analysis</ion-title> -->\n        <ion-title>{{userName}}\'s Gaid Investment</ion-title>\n    </ion-navbar>\n \n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-item>\n        <ion-label stacked>How much would you like to invest?</ion-label>\n        <ion-input type="number"  [(ngModel)]="amount"></ion-input>\n    </ion-item>\n\n    <ion-item no-lines>\n        <ion-label stacked>Recurring?</ion-label>\n        <ion-checkbox [(ngModel)]="recur"></ion-checkbox>\n    </ion-item>\n   <hr/>\n    <ion-item  *ngIf="recur==true">\n        <ion-label stacked>How frequently?</ion-label>\n        <ion-select [(ngModel)]="freq">\n            <!-- <ion-option value="daily">Daily</ion-option> -->\n            <!-- <ion-option value="weekly">Weekly</ion-option> -->\n            <ion-option value="monthly">Monthly</ion-option>\n            <!-- <ion-option value="yearly">Yearly</ion-option> -->\n        </ion-select>\n    </ion-item>\n\n    <ion-item  *ngIf="freq==\'monthly\' && recur==true" >\n        <ion-label stacked>What Day of the Month?</ion-label>\n        <ion-input type="date"  [(ngModel)]="deductDate"></ion-input>\n    </ion-item>\n  \n    <ion-item no-lines *ngIf="amount">\n        <!-- <button ion-button small (click)="buyInvestApi()">Buy</button> -->\n        <button ion-button small (click)="goPay()">Buy</button>\n    </ion-item>\n  \n\n\n\n    <hr *ngIf="investments">\n    <h5 text-center *ngIf="investments">\n        My Investments\n    </h5>\n    <i *ngIf="investments" padding text-left><b >Total:</b> NGN{{totInvest}}</i>\n    <button *ngIf="investments" ion-button small  float-right (click)="toggleMore()">Show More</button>\n   <div *ngIf="more==1">\n\n       <ion-card  *ngFor="let item of investments">\n           <ion-card-content>\n               <h4>{{item.inv_type}}</h4>\n               <h2>NGN{{item.actAmt}}</h2>\n               <p text-right>{{item.inv_date}}</p>\n            </ion-card-content>\n        </ion-card>\n    </div>\n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\investment\investment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], InvestmentPage);
    return InvestmentPage;
}());

//# sourceMappingURL=investment.js.map

/***/ }),

/***/ 839:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagebudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ManagebudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ManagebudgetPage = (function () {
    function ManagebudgetPage(navCtrl, storage, alertCtrl, navParams, grsp) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.grsp = grsp;
        this.expenses = 50;
        this.savInv = 25;
        this.proj = 25;
        this.debt = 0;
        this.income = 0;
        this.id = 0;
        this.loaded = 1;
    }
    ManagebudgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ManagebudgetPage');
        this.getUserName();
    };
    ManagebudgetPage.prototype.setApiJson = function () {
        this.apiData = {
            "uId": this.id,
            "expenses": this.expenses,
            "savInv": this.savInv,
            "proj": this.proj,
            "debt": this.debt,
            "income": this.income
        };
    };
    // saveAddApi(){
    // this.loaded = 0;
    //   this.setApiJson();
    //   console.log(this.apiData);
    //   this.grsp.addTrans(this.apiData).then(data => {
    //     console.log(data);
    //     this.apiRes = data;
    // this.loaded = 1;
    //     if(this.apiRes.response == true){
    //       let alert = this.alertCtrl.create({
    //         title: 'Success',
    //         subTitle: 'Successfully Saved Transaction '+this.apiRes.desc,
    //         buttons: ['Ok']
    //       });
    //       alert.present();
    //       this.type='';
    //       this.desc='';
    //       this.bucket='';
    //       this.amt='';
    //       this.date='';
    //     }
    //     else{
    //       let alert = this.alertCtrl.create({
    //         title: 'Failed',
    //         subTitle: 'Something went wrong, Please try again',
    //         buttons: ['Dismiss']
    //       });
    //       alert.present();
    //     }
    //   })
    //   .catch((err) => {
    // this.loaded = 1;
    //     let alert = this.alertCtrl.create({
    //       title: 'Error',
    //       subTitle: 'Something broke and thats on us, Please try again',
    //       buttons: ['Dismiss']
    //     });
    //     alert.present();
    //   });
    // }
    ManagebudgetPage.prototype.saveBudget = function () {
        var _this = this;
        if (this.sumAll() == true) {
            console.log("save budget for the month");
            this.loaded = 0;
            this.setApiJson();
            console.log(this.apiData);
            // ============================================================
            this.grsp.addBudget(this.apiData).then(function (data) {
                console.log(data);
                _this.apiRes = data;
                _this.loaded = 1;
                if (_this.apiRes.response == true) {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Success',
                        subTitle: 'Successfully Saved Budget',
                        buttons: ['Ok']
                    });
                    alert_1.present();
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Failed',
                        subTitle: 'Something went wrong, Please try again',
                        buttons: ['Dismiss']
                    });
                    alert_2.present();
                }
            })
                .catch(function (err) {
                _this.loaded = 1;
                var alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Something broke and thats on us, Please try again',
                    buttons: ['Dismiss']
                });
                alert.present();
            });
            // ============================================================
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: 'Note',
                subTitle: 'The Total of Budget Allocation Must Be Equal to 100%',
                buttons: ['Dismiss']
            });
            alert_3.present();
        }
    };
    ManagebudgetPage.prototype.sumAll = function () {
        if (this.type == 'debtor') {
            // this.debt =15;
        }
        else {
            this.debt = 0;
        }
        // console.log(this.type);
        // console.log(this.expenses);
        // console.log(this.savInv);
        // console.log(this.proj);
        // console.log(this.debt);
        if ((this.expenses + this.savInv + this.proj + this.debt) == 100) {
            return true;
        }
        else {
            return false;
        }
    };
    ManagebudgetPage.prototype.help = function () {
        var alert = this.alertCtrl.create({
            title: 'Note',
            subTitle: 'The Budget Must be Set Before The Month Starts',
            buttons: ['Dismiss']
        });
        alert.present();
    };
    ManagebudgetPage.prototype.getUserName = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            _this.userName = initialData.name.toUpperCase();
            _this.id = initialData.id;
        });
    };
    ManagebudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-managebudget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\managebudget\managebudget.html"*/'<!--\n  Generated template for the AddtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <div class="logo-container" text-center [style.display]="\'block\'">\n      <img src="assets/imgs/logo.png" alt="Logo">\n\n      <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n          <ion-segment-button value="signin">\n              Sign In\n          </ion-segment-button>\n         \n      </ion-segment> -->\n      \n      <ion-navbar>\n        <ion-title>{{userName}}\'s Budget</ion-title>\n        \n      </ion-navbar>\n\n  </div>\n\n  \n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n  <!-- <i  click="help()"> <ion-icon color="danger" name="help"></ion-icon> </i> -->\n  <ion-card>\n      <ion-card-content>\n\n  <button ion-button float-right small color="secondary" (click)="help()"><strong><ion-icon color="danger" style="font-size: 45px" name="help"></ion-icon></strong></button>\n\n  \n  <ion-item >\n      <ion-label stacked>Projected Monthly Income(NGN)</ion-label>\n      <ion-input type="number"  [(ngModel)]="income"></ion-input>\n    </ion-item>\n  \n  <ion-item>\n      <ion-label stacked>Are You Servicing Debts ?</ion-label>\n      <ion-select [(ngModel)]="type">\n        <ion-option value="debtor">Yes</ion-option>\n        <ion-option value="ndebtor">No</ion-option>\n      </ion-select>\n    </ion-item>\n    </ion-card-content>\n</ion-card>\n    <br/>\n    <br/>\n  \n    <ion-card>\n        <ion-card-content>\n    \n  <i > Assign Pecentages of Your Income to Each Allocation </i>\n \n   \n    <ion-item >\n      <ion-label stacked>Living Expenses(%)</ion-label>\n      <ion-input type="number"  [(ngModel)]="expenses"></ion-input>\n    </ion-item>\n\n    <!-- <ion-item>\n      <ion-label stacked>Bucket</ion-label>\n      <ion-select [(ngModel)]="bucket">\n        <ion-option *ngFor="let x of buckets" [value]="x">{{x}}</ion-option>\n      </ion-select>\n    </ion-item> -->\n\n    <ion-item>\n      <ion-label stacked>Savings & Investments(%)</ion-label>\n      <ion-input type="number" [(ngModel)]="savInv"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label stacked>Projects(%)</ion-label>\n        <ion-input type="number"  [(ngModel)]="proj"></ion-input>\n      </ion-item>\n    \n      <ion-item *ngIf="type==\'debtor\'">\n      <ion-label stacked>Debt Repayment(%)</ion-label>\n        <ion-input type="number"  [(ngModel)]="debt"></ion-input>\n      </ion-item>\n\n    <ion-item no-lines *ngIf="id > 0"  >\n        <!-- <button ion-button small (click)="saveAdd()">Save</button> -->\n        <button ion-button small (click)="saveBudget()">Save</button>\n    </ion-item>\n  </ion-card-content>\n</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\managebudget\managebudget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], ManagebudgetPage);
    return ManagebudgetPage;
}());

//# sourceMappingURL=managebudget.js.map

/***/ }),

/***/ 840:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuTwoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuTwoPage = (function () {
    function MenuTwoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'coffe',
                title: 'Coffe',
                description: 'Freshly brewed coffee',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'breakfast',
                title: 'Breakfast',
                description: 'Hot & full of flavour.',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'munchies',
                title: 'Munchies',
                description: 'Perfectly baked goodies.',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'sandwiches',
                title: 'Sandwiches',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/sandwiches.png'
            },
            {
                id: 5,
                name: 'tea',
                title: 'Tea',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/tea.png'
            },
            {
                id: 6,
                name: 'pancake',
                title: 'Pancake',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/pancake.png'
            },
        ];
    }
    MenuTwoPage.prototype.goToSubMenu = function (cat) {
        /* Pass category object as a parameter */
        var param = { category: cat };
        this.navCtrl.push('SubMenuTwoPage', param);
    };
    MenuTwoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu-two',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-two\menu-two.html"*/'<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Menu</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="goToSubMenu(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-two\menu-two.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], MenuTwoPage);
    return MenuTwoPage;
}());

//# sourceMappingURL=menu-two.js.map

/***/ }),

/***/ 841:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NairaCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the NairaCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NairaCardPage = (function () {
    function NairaCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    NairaCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NairaCardPage');
        this.getUserName();
    };
    NairaCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getNairaApi();
        });
    };
    NairaCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Naira"
        };
        // });
    };
    NairaCardPage.prototype.getNairaApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.naira = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.nairaCrd = JSON.parse(_this.naira.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    NairaCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-naira-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\naira-card\naira-card.html"*/'<!--\n  Generated template for the NairaCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Naira Debit Card</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of nairaCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\naira-card\naira-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], NairaCardPage);
    return NairaCardPage;
}());

//# sourceMappingURL=naira-card.js.map

/***/ }),

/***/ 842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderDetailsPage = (function () {
    function OrderDetailsPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.order = {
            item: {
                id: 10,
                name: 'chocolateMuffin',
                title: 'Chocolate Muffin',
                description: "Our muffin unites rich, dense chocolate with a gooey caramel center for bliss in every bite. As far as we're concerned, there's no such thing as too much caramel.",
                img: 'assets/imgs/submenu/chocolateMuffin.png',
                category: 'muffin',
                price: '10',
                likes: '1800',
                isliked: true,
            },
            quantity: 2,
        };
    }
    OrderDetailsPage.prototype.placeOrder = function () {
        var toast = this.toastCtrl.create({
            message: 'Placing Order of ' + this.order.quantity + ' ' + this.order.item.title,
            duration: 2500
        });
        toast.present();
    };
    OrderDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order-details',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\order-details\order-details.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Chocolate Muffin</ion-title>\n\n        <button class="right-header-icon" ion-button clear icon-only [color]="order.item.isliked? \'danger\' : \'light\'" (click)="order.item.isliked = !order.item.isliked">\n            <ion-icon name="heart"></ion-icon>\n      </button>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-row>\n        <ion-col text-center padding>\n            <img src="{{ order.item.img }}" alt="">\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col padding-horizontal>\n            <p>{{ order.item.description }}</p>\n        </ion-col>\n    </ion-row>\n    <ion-row text-center>\n        <ion-col>\n            <ion-label>\n                Quantity\n            </ion-label>\n            <h2 (click)="ionSelect.open()">\n                {{ order.quantity }}\n                <ion-icon name="ios-arrow-down"></ion-icon>\n            </h2>\n        </ion-col>\n        <ion-col class="total">\n            <ion-label>\n                Total\n            </ion-label>\n            <h2>\n                {{ (order.quantity * order.item.price) | currency:\'USD\':true:\'1.0-2\'}}\n            </h2>\n        </ion-col>\n    </ion-row>\n    <button ion-button full no-margin color="primary" (click)="placeOrder()"> Place The Order</button>\n\n    <ion-item style="display:none;">\n        <ion-select [(ngModel)]="order.quantity" #ionSelect>\n            <ion-option>1</ion-option>\n            <ion-option>2</ion-option>\n            <ion-option>3</ion-option>\n            <ion-option>4</ion-option>\n            <ion-option>5</ion-option>\n        </ion-select>\n    </ion-item>\n\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\order-details\order-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], OrderDetailsPage);
    return OrderDetailsPage;
}());

//# sourceMappingURL=order-details.js.map

/***/ }),

/***/ 843:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuOnePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuOnePage = (function () {
    function MenuOnePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'coffe',
                title: 'Coffe',
                description: 'Freshly brewed coffee',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'breakfast',
                title: 'Breakfast',
                description: 'Hot & full of flavour.',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'munchies',
                title: 'Munchies',
                description: 'Perfectly baked goodies.',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'sandwiches',
                title: 'Sandwiches',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/sandwiches.png'
            },
            {
                id: 5,
                name: 'tea',
                title: 'Tea',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/tea.png'
            },
            {
                id: 6,
                name: 'pancake',
                title: 'Pancake',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/pancake.png'
            },
        ];
    }
    MenuOnePage.prototype.goToSubMenu = function (cat) {
        /* Pass category object as a parameter */
        var param = { category: cat };
        this.navCtrl.push('SubMenuOnePage', param);
    };
    MenuOnePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu-one',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-one\menu-one.html"*/'<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n  <ion-icon name="menu"></ion-icon>\n</button>\n        <ion-title>Menu</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid no-padding>\n        <ion-row no-padding justify-content-center>\n            <ion-col no-padding col-6 align-self-center *ngFor="let cat of categories">\n                <ion-card (click)="goToSubMenu(cat)">\n                    <img src="{{cat.img}}" />\n                    <ion-card-content>\n                        <ion-card-title>\n                            {{ cat.title }}\n                        </ion-card-title>\n                        <p>{{ cat.description }} </p>\n                    </ion-card-content>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-one\menu-one.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], MenuOnePage);
    return MenuOnePage;
}());

//# sourceMappingURL=menu-one.js.map

/***/ }),

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
    }
    ProfilePage.prototype.changeImage = function () {
        var toast = this.toastCtrl.create({
            message: 'Change Image Button Clicked',
            duration: 2500,
        });
        toast.present();
    };
    ProfilePage.prototype.follow = function () {
        var toast = this.toastCtrl.create({
            message: 'Follow Button Clicked',
            duration: 2500,
        });
        toast.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\profile\profile.html"*/'<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Profile</ion-title>\n    </ion-navbar>\n\n    <div class="profile-name" text-start icon-end>\n        <h2>Alexander Karev</h2>\n        <p icon-end> UI Designer & Coffee Lover\n\n            <button ion-fab icon-only float-right color="secondary" (click)="changeImage()">\n              <ion-icon name="camera"></ion-icon>\n           </button>\n        </p>\n    </div>\n</ion-header>\n\n\n<ion-content padding>\n    <article text-center padding-vertical>\n        Born in Singapore, received my design training in London and New York, and am always striving to create work that combines strategic design with compelling visuals and an attention to detail.\n    </article>\n\n    <ion-grid>\n        <ion-row text-center>\n            <ion-col col3>\n                <h2>0</h2>\n                <p>Orders</p>\n            </ion-col>\n            <ion-col col3>\n                <h2>30</h2>\n                <p>Likes</p>\n            </ion-col>\n            <ion-col col3>\n                <h2>25</h2>\n                <p>Follower</p>\n            </ion-col>\n            <ion-col col3>\n                <h2>60</h2>\n                <p>Following</p>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n    <button ion-button full color="primary" (click)="follow()">\n    Follow\n  </button>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrepaidCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PrepaidCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PrepaidCardPage = (function () {
    function PrepaidCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    PrepaidCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PrepaidCardPage');
        this.getUserName();
    };
    PrepaidCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getPrepApi();
        });
    };
    PrepaidCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Prepaid"
        };
        // });
    };
    PrepaidCardPage.prototype.getPrepApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.prep = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.prepCrd = JSON.parse(_this.prep.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    PrepaidCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-prepaid-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\prepaid-card\prepaid-card.html"*/'<!--\n  Generated template for the PrepaidCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Prepaid Card</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of prepCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\prepaid-card\prepaid-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], PrepaidCardPage);
    return PrepaidCardPage;
}());

//# sourceMappingURL=prepaid-card.js.map

/***/ }),

/***/ 846:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingPage = (function () {
    function SettingPage(navCtrl, alertCtrl, grsp, storage, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.storage = storage;
        this.navParams = navParams;
        this.setting = {};
        this.loaded = 1;
    }
    SettingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingPage');
        // this.valName = this.navParams.get('val');
        this.getUserName();
    };
    SettingPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.setting = {
                name: initialData.name.toUpperCase(),
                dob: initialData.dob,
                dow: initialData.dow,
                pen: initialData.pen,
                savInv: initialData.savInv,
                exp: initialData.exp,
                inc: initialData.inc,
                ret: initialData.ret,
                email: initialData.email,
                pword: initialData.pword,
                cpword: ""
            };
        });
    };
    SettingPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "name": this.setting.name,
            "dob": this.setting.dob,
            "dow": this.setting.dow,
            "pen": this.setting.pen,
            "savInv": this.setting.savInv,
            "exp": this.setting.exp,
            "inc": this.setting.inc,
            "ret": this.setting.ret,
            "email": this.setting.email,
            "pword": this.setting.pword,
        };
        console.log(this.apiData);
        // });this.initialData.namethis.initialData.name
    };
    SettingPage.prototype.editUser = function () {
        var _this = this;
        if (this.setting.pword !== this.setting.cpword) {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Passwords Do Not Match',
                buttons: ['Ok']
            });
            alert_1.present();
            return;
        }
        var alert = this.alertCtrl.create({
            title: 'Confirm Update',
            message: 'Do you Want to Make These Changes?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        console.log('Yes clicked');
                        _this.setApiJson();
                        _this.loaded = 0;
                        console.log(_this.apiData);
                        // =============================================
                        _this.grsp.editUser(_this.apiData).then(function (data) {
                            console.log(data);
                            _this.apiRes = data;
                            _this.loaded = 1;
                            if (_this.apiRes.response == true) {
                                var alert_2 = _this.alertCtrl.create({
                                    title: 'Success',
                                    subTitle: 'Successfully Edited User ' + _this.apiRes.name,
                                    buttons: ['Dismiss']
                                });
                                alert_2.present();
                                // alert = this.alertCtrl.create({
                                //   title: 'Info',
                                //   subTitle: 'You will be logged out now',
                                //   buttons: ['Dismiss']
                                // });
                                // alert.present();
                                // ============================
                                _this.storage.set('initialData', null);
                                // let page = this.pages[this.pages.length-2]
                                var page = { title: 'Sign In', component: 'SignupPage' };
                                _this.navCtrl.setRoot(page.component);
                                // this.activePage = page;
                                // ============================
                            }
                            else {
                                var alert_3 = _this.alertCtrl.create({
                                    title: 'Failed',
                                    subTitle: 'Something Broke ',
                                    buttons: ['Dismiss']
                                });
                                alert_3.present();
                            }
                        }).catch(function (err) {
                            _this.loaded = 1;
                            var alert = _this.alertCtrl.create({
                                title: 'Error',
                                subTitle: 'Something broke and thats on us, Please try again',
                                buttons: ['Dismiss']
                            });
                            alert.present();
                        });
                        // =============================================
                    }
                }
            ]
        });
        alert.present();
    };
    SettingPage.prototype.reset = function () {
        this.getUserName();
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\setting\setting.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n    <ion-icon name="menu"></ion-icon>\n  </button>\n        <ion-title>Setting</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n        <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n    \n    <ion-list no-lines no-margin>\n        <ion-item text-center>\n        <h2>Edit Settings Here</h2>\n        </ion-item>\n        <ion-item>\n            <ion-label> Name</ion-label>\n            <ion-input [(ngModel)]="setting.name" text-right type="text"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> DOB</ion-label>\n            <ion-input [(ngModel)]="setting.dob" text-right type="date"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> DOW</ion-label>\n            <ion-input [(ngModel)]="setting.dow" text-right type="date"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Pension</ion-label>\n            <ion-input [(ngModel)]="setting.pen" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Assets (at signup)</ion-label>\n            <ion-input [(ngModel)]="setting.savInv" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Expenses</ion-label>\n            <ion-input [(ngModel)]="setting.exp" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Income</ion-label>\n            <ion-input [(ngModel)]="setting.inc" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Retirement Age</ion-label>\n            <ion-input [(ngModel)]="setting.ret" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Email</ion-label>\n            <ion-input [(ngModel)]="setting.email" text-right type="email"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Password</ion-label>\n            <ion-input [(ngModel)]="setting.pword" text-right type="password"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Confirm Password</ion-label>\n            <ion-input [(ngModel)]="setting.cpword" text-right type="password"></ion-input>\n        </ion-item>\n\n        <button ion-button color="primary" small (click)="editUser()" >Update</button>\n        <button ion-button small (click)="reset()" >Reset</button>\n        <!-- <ion-list-header>\n            Social Media\n        </ion-list-header>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.facebook" color="primary"></ion-toggle>\n            <ion-label>\n                Facebook\n            </ion-label>\n        </ion-item>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.twitter" color="primary"></ion-toggle>\n            <ion-label>\n                Twitter\n            </ion-label>\n        </ion-item>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.instagram" color="primary"></ion-toggle>\n            <ion-label>\n                Instagram\n            </ion-label>\n        </ion-item>\n        <ion-list-header>\n            Notifications\n        </ion-list-header>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.offers" color="primary"></ion-toggle>\n            <ion-label>\n                Offers\n            </ion-label>\n        </ion-item>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.discount" color="primary"></ion-toggle>\n            <ion-label>\n                New items\n            </ion-label>\n        </ion-item> -->\n    </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\setting\setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 847:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashTwoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SplashTwoPage = (function () {
    function SplashTwoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SplashTwoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-splash-two',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-two\splash-two.html"*/'<ion-content>\n    <img src="assets/imgs/logo.png" alt="">\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-two\splash-two.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SplashTwoPage);
    return SplashTwoPage;
}());

//# sourceMappingURL=splash-two.js.map

/***/ }),

/***/ 848:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMenuOnePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMenuOnePage = (function () {
    function SubMenuOnePage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.specialOffer = {
            oldPrice: 5.5,
            newPrice: 3.55,
            item: 'Espresso'
        };
        this.items = [
            {
                id: 1,
                name: 'chocoFrappe',
                title: 'Choco Frappe',
                description: 'Chocolate Whirl',
                img: 'assets/imgs/submenu/chocoFrappe.png',
                category: 'coffe',
                price: '9',
                likes: '3200',
                isliked: false,
            },
            {
                id: 2,
                name: 'caramelFrappe',
                title: 'Caramel Frappe',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/caramelFrappe.png',
                category: 'coffe',
                price: '7.85',
                likes: '3456',
                isliked: false,
            },
            {
                id: 3,
                name: 'kickFrappe',
                title: 'Kick Frappe',
                description: 'Coffee Kick',
                img: 'assets/imgs/submenu/kickFrappe.png',
                category: 'coffe',
                price: '12.35',
                likes: '4450',
                isliked: false,
            },
            {
                id: 4,
                name: 'cappuccino',
                title: 'Cappuccino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/cappuccino.png',
                category: 'coffe',
                price: '10.65',
                likes: '2300',
                isliked: false,
            },
            {
                id: 5,
                name: 'icedAmericano',
                title: 'Iced Americano',
                description: 'Locally Roasted',
                img: 'assets/imgs/submenu/icedAmericano.png',
                category: 'coffe',
                price: '10.50',
                likes: '2300',
                isliked: false,
            },
            {
                id: 6,
                name: 'limeRefreshers',
                title: 'Lime Refreshers',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/limeRefreshers.png',
                category: 'coffe',
                price: '12',
                likes: '1900',
                isliked: false,
            },
            {
                id: 4,
                name: 'vanillaFrappucino',
                title: 'Vanilla Frappucino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/vanillaFrappucino.png',
                category: 'coffe',
                price: '9.85',
                likes: '2300',
                isliked: false,
            },
            {
                id: 4,
                name: 'espresso',
                title: 'Espresso',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/espresso.png',
                category: 'coffe',
                price: '4.50',
                likes: '1800',
                isliked: false,
            },
        ];
    }
    SubMenuOnePage.prototype.ionViewDidLoad = function () {
        var selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
    };
    SubMenuOnePage.prototype.selectItem = function (item) {
        var toast = this.toastCtrl.create({
            message: 'item ' + item.title + ' Clicked',
            duration: 2500,
        });
        toast.present();
    };
    SubMenuOnePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sub-menu-one',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-one\sub-menu-one.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>Coffee</ion-title>\n    </ion-navbar>\n\n    <br>\n    <div class="offer" text-center>\n        <p>Special Offer</p>\n        <h2>{{ specialOffer.item }}</h2>\n        <p>Blue Ridge Blend</p>\n        <p text-left style="margin-top: 20px;" class="old-price">\n            {{ specialOffer.oldPrice | currency:\'USD\':true:\'1.0-2\'}}\n            <span float-right class="new-price">\n              {{ specialOffer.newPrice | currency:\'USD\':true:\'1.0-2\'}}\n          </span>\n        </p>\n    </div>\n</ion-header>\n\n<ion-content>\n    <ion-list no-lines no-margin>\n        <ion-item *ngFor="let item of items">\n            <ion-thumbnail item-start (click)="selectItem(item)">\n                <img src="{{item.img}}">\n            </ion-thumbnail>\n            <h2 (click)="selectItem(item)">{{ item.title }}</h2>\n            <p (click)="selectItem(item)">{{ item.description }}</p>\n            <ion-row no-padding>\n                <ion-col no-padding>\n                    <button no-padding ion-button clear small color="primary">\n                  {{ item.price | currency:\'USD\':true:\'1.2\'}}\n                  </button>\n                </ion-col>\n                <ion-col (click)="selectItem(item)">\n\n                </ion-col>\n                <ion-col text-right no-padding>\n                    <button no-padding ion-button clear small [color]="item.isliked? \'danger\' : \'light\'" (click)="item.isliked = !item.isliked" icon-start>\n                  <ion-icon name=\'heart\'></ion-icon>\n                  {{ item.likes | number}}\n                </button>\n                </ion-col>\n            </ion-row>\n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-one\sub-menu-one.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], SubMenuOnePage);
    return SubMenuOnePage;
}());

//# sourceMappingURL=sub-menu-one.js.map

/***/ }),

/***/ 849:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashOnePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SplashOnePage = (function () {
    function SplashOnePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SplashOnePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-splash-one',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-one\splash-one.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content text-center>\n    <img src="assets/imgs/logo-gray.png" alt="Logo">\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-one\splash-one.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SplashOnePage);
    return SplashOnePage;
}());

//# sourceMappingURL=splash-one.js.map

/***/ }),

/***/ 850:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewanalyticsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ViewanalyticsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewanalyticsPage = (function () {
    function ViewanalyticsPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Income',
                title: 'Income',
                description: 'See Trends on Your Income Streams',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Expenses',
                title: 'Expenses',
                description: 'Check Out What You Spend on',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'Savings and Investments',
                title: 'Savings and Investments',
                description: 'Take A Look At Your Assets',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'Liabilities',
                title: 'Liabilities',
                description: 'We Have Analysed Your Liabilities',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 5,
                name: 'Projects',
                title: 'Projects',
                description: 'How Close Are You To Your Goals ',
                img: 'assets/imgs/menu/munchies.png'
            },
        ];
    }
    ViewanalyticsPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad ViewanalyticsPage');
    };
    ViewanalyticsPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('IncomePage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('ExpensePage', param);
        }
        else if (cat.id == 3) {
            var param = { category: 3 };
            this.navCtrl.push('SavInvPage', param);
        }
        else if (cat.id == 4) {
            var param = { category: 4 };
            this.navCtrl.push('LiabilitiesPage', param);
        }
        else if (cat.id == 5) {
            var param = { category: 5 };
            this.navCtrl.push('ProjectsPage', param);
        }
    };
    ViewanalyticsPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    ViewanalyticsPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    ViewanalyticsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewanalytics',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewanalytics\viewanalytics.html"*/'<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title> View Analytics</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div> \n  </ion-card>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewanalytics\viewanalytics.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ViewanalyticsPage);
    return ViewanalyticsPage;
}());

//# sourceMappingURL=viewanalytics.js.map

/***/ }),

/***/ 851:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewbudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ViewbudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewbudgetPage = (function () {
    function ViewbudgetPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Living Expenses',
                title: 'Living Expenses',
                description: 'Check Out What You Spent',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Savings and Investments',
                title: 'Savings and Investments',
                description: 'Take A Look At Your Assets',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 3,
                name: 'Projects',
                title: 'Projects',
                description: 'How Close Are You To Your Goals ',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'Debt Repayment',
                title: 'Debt Repayment',
                description: 'See What you Owe',
                img: 'assets/imgs/menu/breakfast.png'
            },
        ];
    }
    ViewbudgetPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad ViewbudgetPage');
    };
    ViewbudgetPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('ExpensebudgetPage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('SavInvbudgetPage', param);
        }
        else if (cat.id == 3) {
            var param = { category: 3 };
            this.navCtrl.push('ProjectbudgetPage', param);
        }
        else if (cat.id == 4) {
            var param = { category: 4 };
            this.navCtrl.push('DebtbudgetPage', param);
        }
        // else if(cat.id == 5){ 
        //   let param = { category: 5 };
        //   this.navCtrl.push('ProjectsbudgetPage', param );
        // }
    };
    ViewbudgetPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    ViewbudgetPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    ViewbudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewbudget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewbudget\viewbudget.html"*/'<!--\n  Generated template for the ViewbudgetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title> View Budget Analytics</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  \n  <ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div> \n    </ion-card>\n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewbudget\viewbudget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ViewbudgetPage);
    return ViewbudgetPage;
}());

//# sourceMappingURL=viewbudget.js.map

/***/ }),

/***/ 852:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewtransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ViewtransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewtransactionsPage = (function () {
    function ViewtransactionsPage(navCtrl, navParams, storage, alertCtrl, grsp, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.toastCtrl = toastCtrl;
        this.loaded = 0;
    }
    ViewtransactionsPage.prototype.ionViewDidLoad = function () {
        // let selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
        this.valName = this.navParams.get('val');
        this.getUserName();
    };
    ViewtransactionsPage.prototype.selectItem = function (item) {
        var toast = this.toastCtrl.create({
            message: 'item ' + item.title + ' Clicked',
            duration: 2500,
        });
        toast.present();
    };
    ViewtransactionsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        // this.getTransactionsApi();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        // if (val && val.trim() != '') {
        //   this.transactions = this.transactions.filter((item) => {
        //     return (
        //     item.trans_bucket.toLowerCase().indexOf(val.toLowerCase()) > -1  || 
        //     item.trans_type.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_bucket_child.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_bucket_grandChild.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_amt.indexOf(val) > -1  || 
        //     item.trans_desc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_date.indexOf(val) > -1  );
        //   })
        // }
        if (val && val.trim() != '') {
            this.transactions = this.transactions.filter(function (item) { return item.trans_bucket.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_type.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_bucket_child.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_bucket_grandChild.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_amt.indexOf(val) > -1 ||
                item.trans_desc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_date.indexOf(val) > -1; });
        }
        console.log(this.transactions);
    };
    ViewtransactionsPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getTransactionsApi(_this.start, _this.end);
        });
    };
    ViewtransactionsPage.prototype.setApiJson = function (start, end) {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "start": start,
            "end": end
        };
        // });
    };
    ViewtransactionsPage.prototype.getTransactionsApi = function (start, end) {
        var _this = this;
        this.setApiJson(start, end);
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.transactions(this.apiData).then(function (data) {
            console.log(data);
            _this.trans = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.transactions = JSON.parse(_this.trans.transactions);
            _this.loaded = 1;
            alert = _this.alertCtrl.create({
                title: 'No name',
                subTitle: 'no Name' + _this.transactions[0],
                buttons: ['Dismiss']
            });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    ViewtransactionsPage.prototype.editTrans = function (trans) {
        this.navCtrl.push('EdittransactionsPage', {
            val: 'I am View Edit-trans',
            trans: trans
        });
    };
    ViewtransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewtransactions',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewtransactions\viewtransactions.html"*/'<!--\n  Generated template for the ViewtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>viewtransactions</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content> -->\n\n<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>{{userName}}\'s Transactions</ion-title>\n    </ion-navbar>\n\n    <!-- <br>\n    <div class="offer" text-center>\n    </div> -->\n</ion-header>\n\n\n<ion-content>\n    <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar> <button ion-button small ><ion-icon name="refresh" (click)="getTransactionsApi()" color="secondary"></ion-icon></button>\n        \n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n    \n\n\n    <ion-list no-lines no-margin>\n            <ion-item *ngFor="let item of transactions">\n                <ion-thumbnail item-start (click)="editTrans(item)">\n                    <img src="assets/imgs/submenu/limeRefreshers.png">\n                </ion-thumbnail>\n                <h2 (click)="editTrans(item)"> {{item.trans_type == \'dr\' ? \'Debit\':\'Credit\'}}</h2>\n                <p (click)="editTrans(item)">{{ item.trans_desc }}</p>\n                <ion-row no-padding>\n                    <ion-col no-padding>\n                        <button no-padding ion-button clear small color="primary">\n                     NGN {{ item.trans_amt | number}}\n                      </button>\n                    </ion-col>\n                    <ion-col (click)="editTrans(item)">\n    \n                    </ion-col>\n                    <ion-col text-right no-padding>\n                        <button no-padding ion-button clear small [color]="\'light\'"  icon-start>\n                            {{item.trans_bucket}}\n                            <ion-icon name=\'trash\'></ion-icon>\n                    </button>\n                    </ion-col>\n                </ion-row>\n            </ion-item>\n        </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewtransactions\viewtransactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], ViewtransactionsPage);
    return ViewtransactionsPage;
}());

//# sourceMappingURL=viewtransactions.js.map

/***/ }),

/***/ 853:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalkthroughPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WalkthroughPage = (function () {
    function WalkthroughPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.slides = [
            {
                title: "Discover",
                description: "The Little Coffee Shop serves specialty coffee, fancy grilled cheese sandwiches, scratch cooking, craft ales, and cider.",
            },
            {
                title: "Taste",
                description: "You like your coffee Rich and Distinctive? Sweet and ight? Maybe Intense and Creamy? no worries, We've got it!",
            },
            {
                title: "Love",
                description: "So You Are a Coffe Lover? So as we! We serve coffee with all the Love in the world. Come Love with us.",
            }
        ];
    }
    WalkthroughPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WalkthroughPage');
    };
    WalkthroughPage.prototype.goToHomePage = function () {
        var toast = this.toastCtrl.create({
            message: 'Get Started Clicked',
            duration: 2500,
        });
        toast.present();
    };
    WalkthroughPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-walkthrough',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\walkthrough\walkthrough.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Walkthrough</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="tutorial-page">\n    <ion-slides pager>\n        <ion-slide *ngFor="let slide of slides">\n            <div class="image-container">\n                <img [src]="\'assets/imgs/logo-gray.png\'" />\n            </div>\n            <h2 text-uppercase class="slide-title" [innerHTML]="slide.title"></h2>\n            <p [innerHTML]="slide.description"></p>\n            <button ion-button color="primary" (click)="goToHomePage()">\n              Get Started!\n            </button>\n        </ion-slide>\n    </ion-slides>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\walkthrough\walkthrough.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], WalkthroughPage);
    return WalkthroughPage;
}());

//# sourceMappingURL=walkthrough.js.map

/***/ }),

/***/ 854:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMenuTwoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMenuTwoPage = (function () {
    function SubMenuTwoPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.items = [
            {
                id: 1,
                name: 'chocoFrappe',
                title: 'Choco Frappe',
                description: 'Chocolate Whirl',
                img: 'assets/imgs/submenu/chocoFrappe.png',
                category: 'coffe',
                price: '9',
                likes: '3200',
                isliked: false,
            },
            {
                id: 2,
                name: 'caramelFrappe',
                title: 'Caramel Frappe',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/caramelFrappe.png',
                category: 'coffe',
                price: '7.85',
                likes: '3456',
                isliked: false,
            },
            {
                id: 3,
                name: 'kickFrappe',
                title: 'Kick Frappe',
                description: 'Coffee Kick',
                img: 'assets/imgs/submenu/kickFrappe.png',
                category: 'coffe',
                price: '12.35',
                likes: '4450',
                isliked: false,
            },
            {
                id: 4,
                name: 'cappuccino',
                title: 'Cappuccino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/cappuccino.png',
                category: 'coffe',
                price: '10.65',
                likes: '2300',
                isliked: false,
            },
            {
                id: 5,
                name: 'icedAmericano',
                title: 'Iced Americano',
                description: 'Locally Roasted',
                img: 'assets/imgs/submenu/icedAmericano.png',
                category: 'coffe',
                price: '10.50',
                likes: '2300',
                isliked: false,
            },
            {
                id: 6,
                name: 'limeRefreshers',
                title: 'Lime Refreshers',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/limeRefreshers.png',
                category: 'coffe',
                price: '12',
                likes: '1900',
                isliked: false,
            },
            {
                id: 4,
                name: 'vanillaFrappucino',
                title: 'Vanilla Frappucino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/vanillaFrappucino.png',
                category: 'coffe',
                price: '9.85',
                likes: '2300',
                isliked: false,
            },
            {
                id: 4,
                name: 'espresso',
                title: 'Espresso',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/espresso.png',
                category: 'coffe',
                price: '4.50',
                likes: '1800',
                isliked: false,
            },
        ];
    }
    SubMenuTwoPage.prototype.ionViewDidLoad = function () {
        var selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
    };
    SubMenuTwoPage.prototype.selectItem = function (item) {
        var toast = this.toastCtrl.create({
            message: 'item ' + item.title + ' Clicked',
            duration: 2500,
        });
        toast.present();
    };
    SubMenuTwoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sub-menu-two',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-two\sub-menu-two.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>Coffee</ion-title>\n    </ion-navbar>\n\n    <br>\n    <div class="offer" text-center>\n        <h2>The Science of Delicious.</h2>\n        <p>Amazing coffees from around the world! </p>\n    </div>\n</ion-header>\n\n\n<ion-content>\n    <ion-grid no-padding>\n        <ion-row no-padding justify-content-center>\n            <ion-col no-padding col-6 align-self-center *ngFor="let item of items">\n                <ion-card>\n                    <ion-card-content>\n                        <ion-card-title (click)="selectItem(item)">\n                            {{ item.title }}\n                        </ion-card-title>\n                        <p (click)="selectItem(item)">{{ item.description }} </p>\n                        <img (click)="selectItem(item)" src="{{item.img}}" />\n                        <ion-row no-padding>\n                            <ion-col no-padding>\n                                <button no-padding ion-button clear small color="primary">\n                              {{ item.price | currency:\'USD\':true:\'1.2\'}}\n                              </button>\n                            </ion-col>\n                            <ion-col (click)="selectItem(item)">\n\n                            </ion-col>\n                            <ion-col text-right no-padding>\n                                <button no-padding ion-button clear small [color]="item.isliked? \'danger\' : \'light\'" (click)="item.isliked = !item.isliked" icon-start>\n                              <ion-icon name=\'heart\'></ion-icon>\n                              {{ item.likes | number}}\n                            </button>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-two\sub-menu-two.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], SubMenuTwoPage);
    return SubMenuTwoPage;
}());

//# sourceMappingURL=sub-menu-two.js.map

/***/ }),

/***/ 872:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreditCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CreditCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreditCardPage = (function () {
    function CreditCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    CreditCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreditCardPage');
        this.getUserName();
    };
    CreditCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getCredApi();
        });
    };
    CreditCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Credit"
        };
        // });
    };
    CreditCardPage.prototype.getCredApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.cred = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.credCrd = JSON.parse(_this.cred.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    CreditCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-credit-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\credit-card\credit-card.html"*/'<!--\n  Generated template for the CreditCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>CreditCard</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of credCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\credit-card\credit-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], CreditCardPage);
    return CreditCardPage;
}());

//# sourceMappingURL=credit-card.js.map

/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaystackPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paystack__ = __webpack_require__(360);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaystackPageModule = (function () {
    function PaystackPageModule() {
    }
    PaystackPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__paystack__["a" /* PaystackPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__paystack__["a" /* PaystackPage */]),
            ],
        })
    ], PaystackPageModule);
    return PaystackPageModule;
}());

//# sourceMappingURL=paystack.module.js.map

/***/ }),

/***/ 874:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = (function () {
    function MyApp(platform, statusBar, storage, 
        // public navParams: NavParams,
        splashScreen) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.storage = storage;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            // { title: 'Splash One', component: 'SplashOnePage' },
            // { title: 'Splash Two', component: 'SplashTwoPage' },
            // { title: 'Walkthrough', component: 'WalkthroughPage' },
            // { title: 'Menu One', component: 'MenuOnePage' },
            // { title: 'Menu Two', component: 'MenuTwoPage' },
            // { title: 'Submenu One', component: 'SubMenuOnePage' },
            // { title: 'Submenu Two', component: 'SubMenuTwoPage' },
            // { title: 'Order', component: 'OrderDetailsPage' },
            // { title: 'Favorites', component: 'FavoritesPage' },
            // { title: 'Profile', component: 'ProfilePage' },
            { title: 'Dashboard', component: 'DashboardPage' },
            // { title: 'Financial Info', component: 'FinInfoPage' },
            { title: 'Gaid Investment', component: 'InvestmentPage' },
            { title: 'Analysis', component: 'AnalysisPage' },
            { title: 'Budget', component: 'BudgetPage' },
            // { title: 'Initial', component: 'InitialPage' },
            { title: 'Setting', component: 'SettingPage' },
            { title: 'Sync', component: 'LoadsmstransactionsPage' },
            { title: 'Logout', component: 'InitialPage' },
        ];
        this.storage.get('initialData').then(function (initialData) {
            if (initialData == null || initialData.id == 0) {
                // let page = this.pages[4]
                var page = { title: 'Sign In', component: 'SignupPage' };
                // let page = this.pages[this.pages.length-3]
                _this.nav.setRoot(page.component);
                _this.activePage = page;
            }
            else {
                var page = _this.pages[1];
                _this.nav.setRoot(page.component);
                _this.activePage = page;
            }
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.load();
        if (page.title == 'Logout') {
            this.storage.set('initialData', null);
            // let page = this.pages[this.pages.length-2]
            var page_1 = { title: 'Sign In', component: 'SignupPage' };
            this.nav.setRoot(page_1.component);
            this.activePage = page_1;
        }
        else {
            this.nav.setRoot(page.component);
            this.activePage = page;
        }
    };
    MyApp.prototype.load = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            if (initialData == null) {
                _this.nav.setRoot('SignupPage');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\app\app.html"*/'<ion-menu [content]="content">\n    <ion-content>\n        <div text-center class="menu-header">\n            <img src="assets/imgs/logo-gray.png" alt="">\n        </div>\n\n        <ion-list no-lines>\n            <ion-item class="menu-item" menuClose *ngFor="let page of pages" [class.highlight]="activePage == page" (click)="openPage(page)">\n                {{ page.title }}\n            </ion-item>\n        </ion-list>\n    </ion-content>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[516]);
//# sourceMappingURL=main.js.map
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
