<<<<<<< HEAD
webpackJsonp([13],{969:function(l,n,a){"use strict";Object.defineProperty(n,"__esModule",{value:!0});var u=a(0),t=(a(1),a(7),a(39),a(560)),e=a(32),r=a(4),o=function(){function l(l,n,a,u,t){this.navCtrl=l,this.navParams=n,this.storage=a,this.alertCtrl=u,this.grsp=t,this.loaded=0,this.barLabels=[],this.barLabelNums=[],this.barLabelYears=[],this.month=r(),this.totalIncome=[],this.barBudgetData=[]}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad ExpensebudgetPage"),this.getUserName(),console.log(this.month)},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.inc=n.inc,l.barSetup()})},l.prototype.setApiJson=function(){this.apiData={uId:this.uId,months:this.barLabelNums.join(),years:this.barLabelYears.join()}},l.prototype.getTransSumByMonth=function(){var l=this;this.loaded=0,this.setApiJson(),this.grsp.getTransSumByMonth(this.apiData).then(function(n){console.log(n),l.retval=n,l.retval=JSON.parse(n.data),console.log(JSON.parse(l.retval.budget));JSON.parse(l.retval.sumIncome);JSON.parse(l.retval.budget).forEach(function(n,a){l.barBudgetData.push((n[0]?n[0].b_expense:50)/100*(n[0]?n[0].b_income:l.inc))}),console.log(l.barBudgetData),l.barActualData=JSON.parse(l.retval.sumExpenses),l.loaded=1,l.lineLabels=l.barLabels,l.lineBudgetData=l.barBudgetData,l.lineActualData=l.barActualData,l.bar(),l.liNe()}).catch(function(n){console.log(n),l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l.prototype.liNe=function(){this.lineChart=new t.Chart(this.lineCanvas.nativeElement,{type:"line",data:{labels:this.lineLabels,datasets:[{label:"Budget(%)",fill:!1,lineTension:.1,backgroundColor:"rgba(75,92,192,0.4)",borderColor:"rgba(75,92,192,1)",borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"rgba(75,92,192,1)",pointBackgroundColor:"#fff",pointBorderWidth:1,pointHoverRadius:5,pointHoverBackgroundColor:"rgba(75,92,192,1)",pointHoverBorderColor:"rgba(220,220,220,1)",pointHoverBorderWidth:2,pointRadius:1,pointHitRadius:10,data:this.lineBudgetData,spanGaps:!1},{label:"Actual(%)",fill:!1,lineTension:.1,backgroundColor:"rgba(75,192,192,0.4)",borderColor:"rgba(75,192,192,1)",borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"rgba(75,192,192,1)",pointBackgroundColor:"#fff",pointBorderWidth:1,pointHoverRadius:5,pointHoverBackgroundColor:"rgba(75,192,192,1)",pointHoverBorderColor:"rgba(220,220,220,1)",pointHoverBorderWidth:2,pointRadius:1,pointHitRadius:10,data:this.lineActualData,spanGaps:!1}]},options:{scales:{xAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}],yAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}]}}})},l.prototype.bar=function(){this.barChart=new t.Chart(this.barCanvas.nativeElement,{type:"bar",data:{labels:this.barLabels,datasets:[{label:"Budget(%)",data:this.barBudgetData,backgroundColor:["rgba(255, 99, 132, 0.2)","rgba(54, 162, 235, 0.2)","rgba(255, 206, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(153, 102, 255, 0.2)","rgba(255, 159, 64, 0.2)"],borderColor:["rgba(255,99,132,1)","rgba(54, 162, 235, 1)","rgba(255, 206, 86, 1)","rgba(75, 192, 192, 1)","rgba(153, 102, 255, 1)","rgba(255, 159, 64, 1)"],borderWidth:1},{label:"Actual(%)",data:this.barActualData,backgroundColor:["rgba(255, 99, 132, 0.2)","rgba(5, 162, 235, 0.2)","rgba(55, 206, 86, 0.2)","rgba(50, 192, 192, 0.2)","rgba(13, 102, 255, 0.2)","rgba(250, 159, 64, 0.2)"],borderColor:["rgba(255,99,132,1)","rgba(54, 162, 235, 1)","rgba(255, 206, 86, 1)","rgba(75, 192, 192, 1)","rgba(153, 102, 255, 1)","rgba(255, 159, 64, 1)"],borderWidth:1}]},options:{scales:{yAxes:[{ticks:{beginAtZero:!0}}]}}})},l.prototype.barSetup=function(){for(var l=6,n=this.month;l>0;)this.barLabels.push(n.format("MMM")),this.barLabelNums.push(n.format("M")),this.barLabelYears.push(n.format("Y")),n=n.subtract(1,"month"),l--;console.log(this.barLabels),console.log(this.barLabelNums),console.log(this.barLabelYears),this.getTransSumByMonth()},l}(),s=function(){return function(){}}(),i=a(551),b=a(552),d=a(553),g=a(554),c=a(555),p=a(556),h=a(557),_=a(558),f=a(559),m=a(57),v=a(51),C=a(3),B=a(26),y=a(6),D=a(33),L=a(19),Y=a(9),Z=a(14),j=a(36),S=a(28),k=a(21),N=a(24),x=a(15),A=a(5),z=a(10),H=a(17),J=a(13),M=a(84),E=a(64),I=a(45),O=a(207),P=a(60),w=a(12),R=a(40),W=a(48),T=u.X({encapsulation:2,styles:[],data:{}});function U(l){return u._22(0,[(l()(),u.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,m.b,m.a)),u.Y(1,114688,null,0,v.a,[C.a,u.j,u.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,u._13(n,1)._paused)})}function V(l){return u._22(0,[u._18(402653184,1,{lineCanvas:0}),u._18(402653184,2,{barCanvas:0}),(l()(),u._20(-1,null,["\n\n\n"])),(l()(),u.Z(3,0,null,null,16,"ion-header",[],null,null,null,null,null)),u.Y(4,16384,null,0,B.a,[C.a,u.j,u.z,[2,y.a]],null,null),(l()(),u._20(-1,null,["\n\n  "])),(l()(),u.Z(6,0,null,null,12,"div",[["class","logo-container"],["text-center",""]],[[4,"display",null]],null,null,null,null)),(l()(),u._20(-1,null,["\n      "])),(l()(),u.Z(8,0,null,null,0,"img",[["alt","Logo"],["src","assets/imgs/logo.png"]],null,null,null,null,null)),(l()(),u._20(-1,null,["\n\n      "])),(l()(),u._20(-1,null,["\n      \n      "])),(l()(),u.Z(11,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,D.b,D.a)),u.Y(12,49152,null,0,L.a,[Y.a,[2,y.a],[2,Z.a],C.a,u.j,u.z],null,null),(l()(),u._20(-1,3,["\n        "])),(l()(),u.Z(14,0,null,3,2,"ion-title",[],null,null,null,j.b,j.a)),u.Y(15,49152,null,0,S.a,[C.a,u.j,u.z,[2,k.a],[2,L.a]],null,null),(l()(),u._20(-1,0,["Expense Budget Performance"])),(l()(),u._20(-1,3,["\n      "])),(l()(),u._20(-1,null,["\n\n  "])),(l()(),u._20(-1,null,["\n\n  \n\n"])),(l()(),u._20(-1,null,["\n\n\n"])),(l()(),u.Z(21,0,null,null,42,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,N.b,N.a)),u.Y(22,4374528,null,0,x.a,[C.a,A.a,z.a,u.j,u.z,Y.a,H.a,u.u,[2,y.a],[2,Z.a]],null,null),(l()(),u._20(-1,1,["\n    "])),(l()(),u.U(16777216,null,1,1,null,U)),u.Y(25,16384,null,0,J.k,[u.I,u.F],{ngIf:[0,"ngIf"]},null),(l()(),u._20(-1,1,["\n\n    "])),(l()(),u.Z(27,0,null,1,7,"ion-grid",[["class","grid"]],null,null,null,null,null)),u.Y(28,16384,null,0,M.a,[],null,null),(l()(),u._20(-1,null,["\n        "])),(l()(),u.Z(30,0,null,null,3,"ion-row",[["align-items-center",""],["class","row"],["justify-content-center",""]],null,null,null,null,null)),u.Y(31,16384,null,0,E.a,[],null,null),(l()(),u._20(-1,null,["\n            \n          "])),(l()(),u._20(-1,null,["\n      \n        "])),(l()(),u._20(-1,null,["\n      "])),(l()(),u._20(-1,1,["\n\n\n\n      "])),(l()(),u.Z(36,0,null,1,12,"ion-card",[],null,null,null,null,null)),u.Y(37,16384,null,0,I.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n        "])),(l()(),u.Z(39,0,null,null,2,"ion-card-header",[],null,null,null,null,null)),u.Y(40,16384,null,0,O.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          Line Chart\n        "])),(l()(),u._20(-1,null,["\n      "])),(l()(),u.Z(43,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),u.Y(44,16384,null,0,P.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(46,0,[[1,0],["lineCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),u._20(-1,null,["\n       "])),(l()(),u._20(-1,null,["\n    "])),(l()(),u._20(-1,1,["\n\n    "])),(l()(),u.Z(50,0,null,1,12,"ion-card",[],null,null,null,null,null)),u.Y(51,16384,null,0,I.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n        \n      "])),(l()(),u.Z(53,0,null,null,2,"ion-card-header",[],null,null,null,null,null)),u.Y(54,16384,null,0,O.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          Bar Chart\n        "])),(l()(),u._20(-1,null,["\n      "])),(l()(),u.Z(57,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),u.Y(58,16384,null,0,P.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(60,0,[[2,0],["barCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),u._20(-1,null,["\n       "])),(l()(),u._20(-1,null,["\n    "])),(l()(),u._20(-1,1,["\n\n"])),(l()(),u._20(-1,null,["\n"]))],function(l,n){l(n,25,0,0==n.component.loaded)},function(l,n){l(n,6,0,"block"),l(n,11,0,u._13(n,12)._hidden,u._13(n,12)._sbPadding),l(n,21,0,u._13(n,22).statusbarPadding,u._13(n,22)._hasRefresher)})}var F=u.V("page-expensebudget",o,function(l){return u._22(0,[(l()(),u.Z(0,0,null,null,1,"page-expensebudget",[],null,null,null,V,T)),u.Y(1,49152,null,0,o,[Z.a,w.a,R.a,W.a,e.a],null,null)],null,null)},{},{},[]),G=a(35),X=a(206),q=a(89);a.d(n,"ExpensebudgetPageModuleNgFactory",function(){return K});var K=u.W(s,[],function(l){return u._10([u._11(512,u.i,u.S,[[8,[i.a,b.a,d.a,g.a,c.a,p.a,h.a,_.a,f.a,F]],[3,u.i],u.s]),u._11(4608,J.m,J.l,[u.r,[2,J.u]]),u._11(4608,G.k,G.k,[]),u._11(4608,G.c,G.c,[]),u._11(512,J.b,J.b,[]),u._11(512,G.j,G.j,[]),u._11(512,G.d,G.d,[]),u._11(512,G.i,G.i,[]),u._11(512,X.a,X.a,[]),u._11(512,X.b,X.b,[]),u._11(512,s,s,[]),u._11(256,q.a,o,[])])})}});
=======
webpackJsonp([13],{

/***/ 879:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ExpensebudgetPageModule", function() { return ExpensebudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__expensebudget__ = __webpack_require__(897);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ExpensebudgetPageModule = (function () {
    function ExpensebudgetPageModule() {
    }
    ExpensebudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__expensebudget__["a" /* ExpensebudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__expensebudget__["a" /* ExpensebudgetPage */]),
            ],
        })
    ], ExpensebudgetPageModule);
    return ExpensebudgetPageModule;
}());

//# sourceMappingURL=expensebudget.module.js.map

/***/ }),

/***/ 897:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExpensebudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ExpensebudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ExpensebudgetPage = (function () {
    function ExpensebudgetPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 0;
        this.barLabels = [];
        this.barLabelNums = [];
        this.barLabelYears = [];
        this.month = __WEBPACK_IMPORTED_MODULE_5_moment__();
        this.totalIncome = [];
        this.barBudgetData = [];
    }
    ExpensebudgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ExpensebudgetPage');
        this.getUserName();
        console.log(this.month);
    };
    ExpensebudgetPage.prototype.getUserName = function () {
        // this.userName = "Tayo";
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.inc = initialData.inc;
            _this.barSetup();
        });
    };
    ExpensebudgetPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "months": this.barLabelNums.join(),
            "years": this.barLabelYears.join(),
        };
        // });
    };
    ExpensebudgetPage.prototype.getTransSumByMonth = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        this.grsp.getTransSumByMonth(this.apiData).then(function (data) {
            console.log(data);
            _this.retval = data;
            console.log(JSON.parse(_this.retval.budget));
            var income = JSON.parse(_this.retval.sumIncome);
            var budget = JSON.parse(_this.retval.budget);
            budget.forEach(function (element, ind) {
                var expPercent = element[0] ? element[0].b_expense : 50;
                // let exp = (expPercent/100) * (element[0] ? element[0].b_income : income[ind]);
                var exp = (expPercent / 100) * (element[0] ? element[0].b_income : _this.inc);
                _this.barBudgetData.push(exp);
            });
            console.log(_this.barBudgetData);
            _this.barActualData = JSON.parse(_this.retval.sumExpenses);
            _this.loaded = 1;
            _this.lineLabels = _this.barLabels;
            _this.lineBudgetData = _this.barBudgetData;
            _this.lineActualData = _this.barActualData;
            _this.bar();
            _this.liNe();
        })
            .catch(function (err) {
            console.log(err);
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    ExpensebudgetPage.prototype.liNe = function () {
        // if (this.toggleVarLine){
        this.lineChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                // labels: ["January", "February", "March", "April", "May", "June", "July"],
                labels: this.lineLabels,
                datasets: [
                    {
                        label: "Budget(%)",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,92,192,0.4)",
                        borderColor: "rgba(75,92,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,92,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,92,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 55, 90, 21, 78, 95, 20],
                        data: this.lineBudgetData,
                        spanGaps: false,
                    },
                    {
                        label: "Actual(%)",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.lineActualData,
                        spanGaps: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }]
                },
            }
        });
        // }
    };
    ExpensebudgetPage.prototype.bar = function () {
        this.barChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.barCanvas.nativeElement, {
            type: 'bar',
            data: {
                // labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                labels: this.barLabels,
                datasets: [{
                        label: 'Budget(%)',
                        // data: [12, 9, 3, 5, 12, 3],
                        data: this.barBudgetData,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Actual(%)',
                        // data: [12, 19, 13, 5, 2, 13],
                        data: this.barActualData,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(5, 162, 235, 0.2)',
                            'rgba(55, 206, 86, 0.2)',
                            'rgba(50, 192, 192, 0.2)',
                            'rgba(13, 102, 255, 0.2)',
                            'rgba(250, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    };
    ExpensebudgetPage.prototype.barSetup = function () {
        var i = 6;
        var tday = this.month;
        while (i > 0) {
            // console.log(tday.format('MMMM'));
            this.barLabels.push(tday.format('MMM'));
            this.barLabelNums.push(tday.format('M'));
            this.barLabelYears.push(tday.format('Y'));
            tday = tday.subtract(1, 'month');
            i--;
        }
        console.log(this.barLabels);
        console.log(this.barLabelNums);
        console.log(this.barLabelYears);
        // this.barLabels = this.barLabels.reverse();
        // this.barLabels.forEach((element, ind) => {
        // });
        this.getTransSumByMonth();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], ExpensebudgetPage.prototype, "lineCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], ExpensebudgetPage.prototype, "barCanvas", void 0);
    ExpensebudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-expensebudget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\expensebudget\expensebudget.html"*/'<!--\n  Generated template for the ExpensebudgetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n<ion-header>\n\n  <div class="logo-container" text-center [style.display]="\'block\'">\n      <img src="assets/imgs/logo.png" alt="Logo">\n\n      <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n          <ion-segment-button value="signin">\n              Sign In\n          </ion-segment-button>\n         \n      </ion-segment> -->\n      \n      <ion-navbar>\n        <ion-title>Expense Budget Performance</ion-title>\n      </ion-navbar>\n\n  </div>\n\n  \n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n    <ion-grid >\n        <ion-row justify-content-center align-items-center >\n            \n          <!-- <button ion-button small (click)="week()" >Week</button>\n            <button ion-button small (click)="month()" >Month</button>\n            <button ion-button small (click)="year()" >Year</button>\n            <button ion-button small (click)="range()" >Range</button> -->\n      \n        </ion-row>\n      </ion-grid>\n\n\n\n      <ion-card>\n        <ion-card-header >\n          Line Chart\n        </ion-card-header>\n      <ion-card-content>\n          <canvas #lineCanvas></canvas>\n       </ion-card-content>\n    </ion-card>\n\n    <ion-card>\n        \n      <ion-card-header >\n          Bar Chart\n        </ion-card-header>\n      <ion-card-content>\n          <canvas #barCanvas></canvas>\n       </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\expensebudget\expensebudget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], ExpensebudgetPage);
    return ExpensebudgetPage;
}());

//# sourceMappingURL=expensebudget.js.map

/***/ })

});
//# sourceMappingURL=13.js.map
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
