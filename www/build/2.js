<<<<<<< HEAD
webpackJsonp([2],{975:function(l,n,a){"use strict";Object.defineProperty(n,"__esModule",{value:!0});var u=a(0),t=(a(1),a(7),a(39),a(560)),e=a(32),o=a(4),s=function(){function l(l,n,a,u,t){this.navCtrl=l,this.navParams=n,this.storage=a,this.alertCtrl=u,this.grsp=t,this.toggleVar=!0,this.toggleVarLine=!0,this.loaded=0,this.drange=!1,this.pieValues=[],this.lineValues=[]}return l.prototype.ionViewDidLoad=function(){var l=o().format("YYYY-MM-DD").toString();console.log(l);var n=o().subtract(1,"month").format("YYYY-MM-DD").toString();console.log(n),this.end=l,this.start=n,console.log("ionViewDidLoad SavInvPage"),this.valName=this.navParams.get("val"),this.getUserName()},l.prototype.doNut=function(){this.toggleVar&&(console.log(this.donutLabels),this.doughnutChart=new t.Chart(this.doughnutCanvas.nativeElement,{type:"doughnut",data:{labels:this.pieLabels,datasets:[{label:"User Personal Finance Status",data:this.pieValues,backgroundColor:["rgba(255, 99, 132, 0.2)","rgba(54, 162, 235, 0.2)","rgba(255, 206, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(153, 102, 255, 0.2)","rgba(255, 159, 64, 0.2)"],hoverBackgroundColor:["#FF6384","#36A2EB","#FFCE56","#FF6384","#36A2EB","#FFCE56"]}]}}))},l.prototype.liNe=function(){this.toggleVarLine&&(this.lineChart=new t.Chart(this.lineCanvas.nativeElement,{type:"line",data:{labels:this.lineLabels,datasets:[{label:"Assets",fill:!1,lineTension:.1,backgroundColor:"rgba(75,92,192,0.4)",borderColor:"rgba(75,92,192,1)",borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"rgba(75,92,192,1)",pointBackgroundColor:"#fff",pointBorderWidth:1,pointHoverRadius:5,pointHoverBackgroundColor:"rgba(75,92,192,1)",pointHoverBorderColor:"rgba(220,220,220,1)",pointHoverBorderWidth:2,pointRadius:1,pointHitRadius:10,data:this.lineValues,spanGaps:!1}]},options:{scales:{xAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}],yAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}]}}}))},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.getTransactionsApi(l.start,l.end)})},l.prototype.week=function(){var l=o().startOf("isoWeek").format("YYYY-MM-DD HH:mm:ss"),n=o().format("YYYY-MM-DD HH:mm:ss");this.getTransactionsApi(l,n)},l.prototype.month=function(){var l=o().startOf("month").format("YYYY-MM-DD HH:mm:ss"),n=o().format("YYYY-MM-DD HH:mm:ss");this.getTransactionsApi(l,n)},l.prototype.year=function(){var l=o().startOf("year").format("YYYY-MM-DD HH:mm:ss"),n=o().format("YYYY-MM-DD HH:mm:ss");this.getTransactionsApi(l,n)},l.prototype.range=function(){this.drange=!this.drange},l.prototype.getRange=function(){this.getTransactionsApi(this.from,this.to)},l.prototype.setApiJson=function(l,n){this.apiData={uId:this.uId,start:l,end:n}},l.prototype.getTransactionsApi=function(l,n){var a=this;this.loaded=0,this.setApiJson(l,n);this.alertCtrl.create({title:"No name",subTitle:"no Name",buttons:["Dismiss"]});console.log(this.apiData),this.lineLabels=[],this.lineValues=[],this.pieLabels=[],this.pieValues=[],this.grsp.transactions(this.apiData).then(function(l){a.trans=l,a.trans=JSON.parse(l.data),a.transactions=JSON.parse(a.trans.transactions),a.transactions=a.transactions.filter(function(l){return"Assets"==l.trans_bucket}),a.pieLabels=a.transactions.map(function(l){return l.trans_bucket_child}),console.log(a.transactions),console.log(a.pieLabels),a.pieLabels=a.pieLabels.filter(function(l,n,a){return a.indexOf(l)===n}),console.log(a.pieLabels);var n=Array.isArray(a.pieLabels);console.log(n),a.pieLabels.forEach(function(l){var n=a.transactions.filter(function(n){return n.trans_bucket_child==l}).map(function(l){return l.trans_amt}),u=(n=n.map(function(l){return parseInt(l)})).reduce(function(l,n){return l+n},0);a.pieValues.push(u)}),console.log(a.pieValues),a.doNut(),a.lineLabels=a.transactions.map(function(l){return l.trans_date}),a.lineLabels=a.lineLabels.filter(function(l,n,a){return a.indexOf(l)===n}),a.lineLabels=a.lineLabels.slice(-5),a.lineLabels.forEach(function(l){var n=a.transactions.filter(function(n){return n.trans_date==l}).map(function(l){return l.trans_amt}),u=(n=n.map(function(l){return parseInt(l)})).reduce(function(l,n){return l+n},0);a.lineValues.push(u)}),console.log(a.lineValues),a.liNe(),a.loaded=1,a.alertCtrl.create({title:"No name",subTitle:"no Name"+a.transactions[0],buttons:["Dismiss"]})}).catch(function(l){a.loaded=1,console.log(l),a.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l}(),i=function(){return function(){}}(),r=a(551),c=a(552),d=a(553),g=a(554),_=a(555),p=a(556),h=a(557),b=a(558),m=a(559),f=a(57),Y=a(51),v=a(3),C=a(45),y=a(60),k=a(54),D=a(34),Z=a(29),j=a(46),L=a(49),M=a(58),z=a(69),V=a(35),A=a(59),H=a(5),N=a(9),I=a(15),F=a(10),P=a(20),T=a(16),S=a(26),w=a(6),B=a(33),E=a(19),O=a(14),U=a(36),R=a(28),x=a(21),J=a(24),W=a(17),G=a(13),X=a(84),q=a(64),K=a(207),Q=a(12),$=a(40),ll=a(48),nl=u.X({encapsulation:2,styles:[],data:{}});function al(l){return u._22(0,[(l()(),u.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,f.b,f.a)),u.Y(1,114688,null,0,Y.a,[v.a,u.j,u.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,u._13(n,1)._paused)})}function ul(l){return u._22(0,[(l()(),u.Z(0,0,null,null,46,"ion-card",[],null,null,null,null,null)),u.Y(1,16384,null,0,C.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          \n      "])),(l()(),u.Z(3,0,null,null,42,"ion-card-content",[],null,null,null,null,null)),u.Y(4,16384,null,0,y.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(6,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),u.Y(7,1097728,null,3,D.a,[Z.a,v.a,u.j,u.z,[2,j.a]],null,null),u._18(335544320,3,{contentLabel:0}),u._18(603979776,4,{_buttons:1}),u._18(603979776,5,{_icons:1}),u.Y(11,16384,null,0,L.a,[],null,null),(l()(),u._20(-1,2,["\n            "])),(l()(),u.Z(13,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),u.Y(14,16384,[[3,4]],0,M.a,[v.a,u.j,u.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),u._20(-1,null,[" From "])),(l()(),u._20(-1,2,["\n            "])),(l()(),u.Z(17,0,null,3,4,"ion-input",[["name","from"],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,a){var u=!0;"ngModelChange"===n&&(u=!1!==(l.component.from=a)&&u);return u},z.b,z.a)),u.Y(18,671744,null,0,V.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),u._17(2048,null,V.f,null,[V.h]),u.Y(20,16384,null,0,V.g,[V.f],null,null),u.Y(21,5423104,null,0,A.a,[v.a,H.a,Z.a,N.a,u.j,u.z,[2,I.a],[2,D.a],[2,V.f],F.a],{type:[0,"type"]},null),(l()(),u._20(-1,2,["\n          "])),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(24,0,null,null,16,"ion-item",[["class","item item-block"]],null,null,null,k.b,k.a)),u.Y(25,1097728,null,3,D.a,[Z.a,v.a,u.j,u.z,[2,j.a]],null,null),u._18(335544320,6,{contentLabel:0}),u._18(603979776,7,{_buttons:1}),u._18(603979776,8,{_icons:1}),u.Y(29,16384,null,0,L.a,[],null,null),(l()(),u._20(-1,2,["\n            "])),(l()(),u.Z(31,0,null,1,2,"ion-label",[["stacked",""]],null,null,null,null,null)),u.Y(32,16384,[[6,4]],0,M.a,[v.a,u.j,u.z,[8,null],[8,""],[8,null],[8,null]],null,null),(l()(),u._20(-1,null,[" To "])),(l()(),u._20(-1,2,["\n            "])),(l()(),u.Z(35,0,null,3,4,"ion-input",[["name","to"],["type","date"]],[[2,"ng-untouched",null],[2,"ng-touched",null],[2,"ng-pristine",null],[2,"ng-dirty",null],[2,"ng-valid",null],[2,"ng-invalid",null],[2,"ng-pending",null]],[[null,"ngModelChange"]],function(l,n,a){var u=!0;"ngModelChange"===n&&(u=!1!==(l.component.to=a)&&u);return u},z.b,z.a)),u.Y(36,671744,null,0,V.h,[[8,null],[8,null],[8,null],[8,null]],{name:[0,"name"],model:[1,"model"]},{update:"ngModelChange"}),u._17(2048,null,V.f,null,[V.h]),u.Y(38,16384,null,0,V.g,[V.f],null,null),u.Y(39,5423104,null,0,A.a,[v.a,H.a,Z.a,N.a,u.j,u.z,[2,I.a],[2,D.a],[2,V.f],F.a],{type:[0,"type"]},null),(l()(),u._20(-1,2,["\n          "])),(l()(),u._20(-1,null,["\n      "])),(l()(),u.Z(42,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,a){var u=!0;"click"===n&&(u=!1!==l.component.getRange()&&u);return u},P.b,P.a)),u.Y(43,1097728,null,0,T.a,[[8,""],v.a,u.j,u.z],{small:[0,"small"]},null),(l()(),u._20(-1,0,["Go"])),(l()(),u._20(-1,null,["\n         \n       "])),(l()(),u._20(-1,null,["\n    "]))],function(l,n){var a=n.component;l(n,18,0,"from",a.from);l(n,21,0,"date");l(n,36,0,"to",a.to);l(n,39,0,"date");l(n,43,0,"")},function(l,n){l(n,17,0,u._13(n,20).ngClassUntouched,u._13(n,20).ngClassTouched,u._13(n,20).ngClassPristine,u._13(n,20).ngClassDirty,u._13(n,20).ngClassValid,u._13(n,20).ngClassInvalid,u._13(n,20).ngClassPending),l(n,35,0,u._13(n,38).ngClassUntouched,u._13(n,38).ngClassTouched,u._13(n,38).ngClassPristine,u._13(n,38).ngClassDirty,u._13(n,38).ngClassValid,u._13(n,38).ngClassInvalid,u._13(n,38).ngClassPending)})}function tl(l){return u._22(0,[u._18(402653184,1,{doughnutCanvas:0}),u._18(402653184,2,{lineCanvas:0}),(l()(),u._20(-1,null,["\n"])),(l()(),u.Z(3,0,null,null,10,"ion-header",[],null,null,null,null,null)),u.Y(4,16384,null,0,S.a,[v.a,u.j,u.z,[2,w.a]],null,null),(l()(),u._20(-1,null,["\n\n    "])),(l()(),u.Z(6,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,B.b,B.a)),u.Y(7,49152,null,0,E.a,[N.a,[2,w.a],[2,O.a],v.a,u.j,u.z],null,null),(l()(),u._20(-1,3,["\n      "])),(l()(),u.Z(9,0,null,3,2,"ion-title",[],null,null,null,U.b,U.a)),u.Y(10,49152,null,0,R.a,[v.a,u.j,u.z,[2,x.a],[2,E.a]],null,null),(l()(),u._20(11,0,["Assets Analytics for ",""])),(l()(),u._20(-1,3,["\n    "])),(l()(),u._20(-1,null,["\n  \n  "])),(l()(),u._20(-1,null,["\n  \n  \n  "])),(l()(),u.Z(15,0,null,null,63,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,J.b,J.a)),u.Y(16,4374528,null,0,I.a,[v.a,H.a,F.a,u.j,u.z,N.a,W.a,u.u,[2,w.a],[2,O.a]],null,null),(l()(),u._20(-1,1,["\n    "])),(l()(),u.U(16777216,null,1,1,null,al)),u.Y(19,16384,null,0,G.k,[u.I,u.F],{ngIf:[0,"ngIf"]},null),(l()(),u._20(-1,1,["\n    \n    "])),(l()(),u.Z(21,0,null,1,24,"ion-grid",[["class","grid"]],null,null,null,null,null)),u.Y(22,16384,null,0,X.a,[],null,null),(l()(),u._20(-1,null,["\n      "])),(l()(),u.Z(24,0,null,null,20,"ion-row",[["align-items-center",""],["class","row"],["justify-content-center",""]],null,null,null,null,null)),u.Y(25,16384,null,0,q.a,[],null,null),(l()(),u._20(-1,null,["\n    "])),(l()(),u._20(-1,null,["\n          "])),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(29,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,a){var u=!0;"click"===n&&(u=!1!==l.component.week()&&u);return u},P.b,P.a)),u.Y(30,1097728,null,0,T.a,[[8,""],v.a,u.j,u.z],{small:[0,"small"]},null),(l()(),u._20(-1,0,["Week"])),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(33,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,a){var u=!0;"click"===n&&(u=!1!==l.component.month()&&u);return u},P.b,P.a)),u.Y(34,1097728,null,0,T.a,[[8,""],v.a,u.j,u.z],{small:[0,"small"]},null),(l()(),u._20(-1,0,["Month"])),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(37,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,a){var u=!0;"click"===n&&(u=!1!==l.component.year()&&u);return u},P.b,P.a)),u.Y(38,1097728,null,0,T.a,[[8,""],v.a,u.j,u.z],{small:[0,"small"]},null),(l()(),u._20(-1,0,["Year"])),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(41,0,null,null,2,"button",[["ion-button",""],["small",""]],null,[[null,"click"]],function(l,n,a){var u=!0;"click"===n&&(u=!1!==l.component.range()&&u);return u},P.b,P.a)),u.Y(42,1097728,null,0,T.a,[[8,""],v.a,u.j,u.z],{small:[0,"small"]},null),(l()(),u._20(-1,0,["Range"])),(l()(),u._20(-1,null,["\n    \n      "])),(l()(),u._20(-1,null,["\n    "])),(l()(),u._20(-1,1,["\n    \n    "])),(l()(),u.U(16777216,null,1,1,null,ul)),u.Y(48,16384,null,0,G.k,[u.I,u.F],{ngIf:[0,"ngIf"]},null),(l()(),u._20(-1,1,["\n    \n        "])),(l()(),u.Z(50,0,null,1,13,"ion-card",[],null,null,null,null,null)),u.Y(51,16384,null,0,C.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n            "])),(l()(),u._20(-1,null,["\n            "])),(l()(),u.Z(54,0,null,null,2,"ion-card-header",[],null,null,null,null,null)),u.Y(55,16384,null,0,K.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n              Donut\n            "])),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(58,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),u.Y(59,16384,null,0,y.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n              "])),(l()(),u.Z(61,0,[[1,0],["doughnutCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),u._20(-1,null,["\n           "])),(l()(),u._20(-1,null,["\n        "])),(l()(),u._20(-1,1,["\n    \n        "])),(l()(),u.Z(65,0,null,1,12,"ion-card",[],null,null,null,null,null)),u.Y(66,16384,null,0,C.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n            "])),(l()(),u.Z(68,0,null,null,2,"ion-card-header",[],null,null,null,null,null)),u.Y(69,16384,null,0,K.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n              Line\n            "])),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(72,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),u.Y(73,16384,null,0,y.a,[v.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n              "])),(l()(),u.Z(75,0,[[2,0],["lineCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),u._20(-1,null,["\n           "])),(l()(),u._20(-1,null,["\n        "])),(l()(),u._20(-1,1,["\n    \n    \n    "])),(l()(),u._20(-1,null,["\n  \n\n\n"]))],function(l,n){var a=n.component;l(n,19,0,0==a.loaded);l(n,30,0,"");l(n,34,0,"");l(n,38,0,"");l(n,42,0,""),l(n,48,0,a.drange)},function(l,n){var a=n.component;l(n,6,0,u._13(n,7)._hidden,u._13(n,7)._sbPadding),l(n,11,0,a.userName),l(n,15,0,u._13(n,16).statusbarPadding,u._13(n,16)._hasRefresher)})}var el=u.V("page-sav-inv",s,function(l){return u._22(0,[(l()(),u.Z(0,0,null,null,1,"page-sav-inv",[],null,null,null,tl,nl)),u.Y(1,49152,null,0,s,[O.a,Q.a,$.a,ll.a,e.a],null,null)],null,null)},{},{},[]),ol=a(206),sl=a(89);a.d(n,"SavInvPageModuleNgFactory",function(){return il});var il=u.W(i,[],function(l){return u._10([u._11(512,u.i,u.S,[[8,[r.a,c.a,d.a,g.a,_.a,p.a,h.a,b.a,m.a,el]],[3,u.i],u.s]),u._11(4608,G.m,G.l,[u.r,[2,G.u]]),u._11(4608,V.k,V.k,[]),u._11(4608,V.c,V.c,[]),u._11(512,G.b,G.b,[]),u._11(512,V.j,V.j,[]),u._11(512,V.d,V.d,[]),u._11(512,V.i,V.i,[]),u._11(512,ol.a,ol.a,[]),u._11(512,ol.b,ol.b,[]),u._11(512,i,i,[]),u._11(256,sl.a,s,[])])})}});
=======
webpackJsonp([2],{

/***/ 892:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavInvPageModule", function() { return SavInvPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sav_inv__ = __webpack_require__(910);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavInvPageModule = (function () {
    function SavInvPageModule() {
    }
    SavInvPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sav_inv__["a" /* SavInvPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sav_inv__["a" /* SavInvPage */]),
            ],
        })
    ], SavInvPageModule);
    return SavInvPageModule;
}());

//# sourceMappingURL=sav-inv.module.js.map

/***/ }),

/***/ 910:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavInvPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SavInvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SavInvPage = (function () {
    function SavInvPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.toggleVar = true;
        this.toggleVarLine = true;
        this.loaded = 0;
        this.drange = false;
        this.pieValues = [];
        this.lineValues = [];
    }
    SavInvPage.prototype.ionViewDidLoad = function () {
        var tday = __WEBPACK_IMPORTED_MODULE_5_moment__().format("YYYY-MM-DD").toString();
        console.log(tday);
        // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
        var yday = __WEBPACK_IMPORTED_MODULE_5_moment__().subtract(1, 'month').format("YYYY-MM-DD").toString();
        console.log(yday);
        this.end = tday;
        this.start = yday;
        console.log('ionViewDidLoad SavInvPage');
        this.valName = this.navParams.get('val');
        this.getUserName();
    };
    SavInvPage.prototype.doNut = function () {
        if (this.toggleVar) {
            console.log(this.donutLabels);
            this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.doughnutCanvas.nativeElement, {
                type: 'doughnut',
                data: {
                    // labels: ["Food", "Utility", "Airtime", "Investments", "Entertainment", "Travel"],
                    // labels: this.donutLabels,
                    labels: this.pieLabels,
                    datasets: [{
                            label: 'User Personal Finance Status',
                            // data: [12, 19, 3, 5, 2, 3],
                            data: this.pieValues,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56",
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56"
                            ]
                        }]
                }
            });
        }
    };
    SavInvPage.prototype.liNe = function () {
        if (this.toggleVarLine) {
            this.lineChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.lineCanvas.nativeElement, {
                type: 'line',
                data: {
                    // labels: ["January", "February", "March", "April", "May", "June", "July"],
                    labels: this.lineLabels,
                    datasets: [
                        {
                            label: "Assets",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(75,92,192,0.4)",
                            borderColor: "rgba(75,92,192,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: "rgba(75,92,192,1)",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(75,92,192,1)",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            // data: [65, 55, 90, 21, 78, 95, 20],
                            data: this.lineValues,
                            spanGaps: false,
                        },
                    ]
                },
                options: {
                    scales: {
                        xAxes: [{
                                gridLines: {
                                    color: "rgba(0, 0, 0, 0)",
                                }
                            }],
                        yAxes: [{
                                gridLines: {
                                    color: "rgba(0, 0, 0, 0)",
                                }
                            }]
                    },
                }
            });
        }
    };
    SavInvPage.prototype.getUserName = function () {
        // this.userName = "Tayo";
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getTransactionsApi(_this.start, _this.end);
        });
    };
    SavInvPage.prototype.week = function () {
        var weekStart = __WEBPACK_IMPORTED_MODULE_5_moment__().startOf('isoWeek').format('YYYY-MM-DD HH:mm:ss');
        var weekEnd = __WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(weekStart, weekEnd);
    };
    SavInvPage.prototype.month = function () {
        var start = __WEBPACK_IMPORTED_MODULE_5_moment__().startOf('month').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    SavInvPage.prototype.year = function () {
        var start = __WEBPACK_IMPORTED_MODULE_5_moment__().startOf('year').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    SavInvPage.prototype.range = function () {
        this.drange = !this.drange;
    };
    SavInvPage.prototype.getRange = function () {
        this.getTransactionsApi(this.from, this.to);
    };
    SavInvPage.prototype.setApiJson = function (start, end) {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "start": start,
            "end": end
        };
        // });
    };
    SavInvPage.prototype.getTransactionsApi = function (start, end) {
        var _this = this;
        this.loaded = 0;
        this.setApiJson(start, end);
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.lineLabels = [];
        this.lineValues = [];
        this.pieLabels = [];
        this.pieValues = [];
        this.grsp.transactions(this.apiData).then(function (data) {
            // console.log(data);
            _this.trans = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.transactions = JSON.parse(_this.trans.transactions);
            _this.transactions = _this.transactions.filter(function (tr) { return tr.trans_bucket == 'Assets'; });
            // get all
            _this.pieLabels = _this.transactions.map(function (value) { return value.trans_bucket_child; });
            console.log(_this.transactions);
            console.log(_this.pieLabels);
            // get unique
            _this.pieLabels = _this.pieLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            console.log(_this.pieLabels);
            var isArr = (Array.isArray(_this.pieLabels));
            console.log(isArr);
            _this.pieLabels.forEach(function (element) {
                // console.log(element);
                var arr = _this.transactions.filter(function (tr) { return tr.trans_bucket_child == element; });
                // console.log(arr);
                var subArr = arr.map(function (value) { return value.trans_amt; });
                subArr = subArr.map(function (x) { return parseInt(x); });
                // console.log(subArr);
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                // console.log(arrSum);
                _this.pieValues.push(arrSum);
            });
            console.log(_this.pieValues);
            _this.doNut();
            // -----------------------------Line graph-----------------------------------
            // get all
            _this.lineLabels = _this.transactions.map(function (value) { return value.trans_date; });
            // get unique
            _this.lineLabels = _this.lineLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            // get last 5 days
            _this.lineLabels = _this.lineLabels.slice(-5);
            _this.lineLabels.forEach(function (element) {
                // get all transactions for current iteration
                var arr = _this.transactions.filter(function (tr) { return tr.trans_date == element; });
                // get all amts for all those transactions
                var subArr = arr.map(function (value) { return value.trans_amt; });
                // parse amts to int
                subArr = subArr.map(function (x) { return parseInt(x); });
                // sum them up
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                _this.lineValues.push(arrSum);
            });
            console.log(_this.lineValues);
            _this.liNe();
            _this.loaded = 1;
            alert = _this.alertCtrl.create({
                title: 'No name',
                subTitle: 'no Name' + _this.transactions[0],
                buttons: ['Dismiss']
            });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            console.log(err);
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], SavInvPage.prototype, "doughnutCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], SavInvPage.prototype, "lineCanvas", void 0);
    SavInvPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sav-inv',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sav-inv\sav-inv.html"*/'<!--\n  Generated template for the SavInvPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n      <ion-title>Assets Analytics for {{userName}}</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n    \n    <ion-grid >\n      <ion-row justify-content-center align-items-center >\n    <!--  -->\n          <!-- <button ion-button small (click)="apitest()">Week</button> -->\n          <button ion-button small (click)="week()" >Week</button>\n          <button ion-button small (click)="month()" >Month</button>\n          <button ion-button small (click)="year()" >Year</button>\n          <button ion-button small (click)="range()" >Range</button>\n    \n      </ion-row>\n    </ion-grid>\n    \n    <ion-card *ngIf="drange">\n          \n      <ion-card-content>\n          <ion-item>\n            <ion-label stacked> From </ion-label>\n            <ion-input type="date" [(ngModel)]="from" name="from" ></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label stacked> To </ion-label>\n            <ion-input type="date" [(ngModel)]="to" name="to" ></ion-input>\n          </ion-item>\n      <button ion-button small (click)="getRange()" >Go</button>\n         \n       </ion-card-content>\n    </ion-card>\n    \n        <ion-card>\n            <!-- <ion-card-header (click)="toggleDonut()"> -->\n            <ion-card-header >\n              Donut\n            </ion-card-header>\n          <ion-card-content>\n              <canvas #doughnutCanvas></canvas>\n           </ion-card-content>\n        </ion-card>\n    \n        <ion-card>\n            <ion-card-header >\n              Line\n            </ion-card-header>\n          <ion-card-content>\n              <canvas #lineCanvas></canvas>\n           </ion-card-content>\n        </ion-card>\n    \n    \n    </ion-content>\n  \n\n\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sav-inv\sav-inv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], SavInvPage);
    return SavInvPage;
}());

//# sourceMappingURL=sav-inv.js.map

/***/ })

});
//# sourceMappingURL=2.js.map
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
