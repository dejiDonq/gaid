<<<<<<< HEAD
webpackJsonp([1],{976:function(l,n,a){"use strict";Object.defineProperty(n,"__esModule",{value:!0});var u=a(0),t=(a(1),a(7),a(39),a(560)),r=a(32),e=a(4),o=function(){function l(l,n,a,u,t){this.navCtrl=l,this.navParams=n,this.storage=a,this.alertCtrl=u,this.grsp=t,this.loaded=0,this.barLabels=[],this.barLabelNums=[],this.barLabelYears=[],this.month=e(),this.totalIncome=[],this.barBudgetData=[]}return l.prototype.ionViewDidLoad=function(){console.log("ionViewDidLoad SavInvbudgetPage"),this.getUserName()},l.prototype.getUserName=function(){var l=this;this.storage.get("initialData").then(function(n){l.userName=n.name.toUpperCase(),l.uId=n.id,l.inc=n.inc,l.barSetup()})},l.prototype.setApiJson=function(){this.apiData={uId:this.uId,months:this.barLabelNums.join(),years:this.barLabelYears.join()}},l.prototype.getTransSumByMonth=function(){var l=this;this.loaded=0,this.setApiJson(),this.grsp.getTransSumByMonth(this.apiData).then(function(n){console.log(n),l.retval=n,l.retval=JSON.parse(n.data),console.log(JSON.parse(l.retval.budget));JSON.parse(l.retval.sumIncome);JSON.parse(l.retval.budget).forEach(function(n,a){l.barBudgetData.push((n[0]?n[0].b_savInv:25)/100*(n[0]?n[0].b_income:l.inc))}),console.log(l.barBudgetData),l.barActualData=JSON.parse(l.retval.sumSavInv),l.loaded=1,l.lineLabels=l.barLabels,l.lineBudgetData=l.barBudgetData,l.lineActualData=l.barActualData,l.bar(),l.liNe()}).catch(function(n){l.loaded=1,l.alertCtrl.create({title:"Error",subTitle:"Something broke and thats on us, Please try again",buttons:["Dismiss"]}).present()})},l.prototype.liNe=function(){this.lineChart=new t.Chart(this.lineCanvas.nativeElement,{type:"line",data:{labels:this.lineLabels,datasets:[{label:"Budget(%)",fill:!1,lineTension:.1,backgroundColor:"rgba(75,92,192,0.4)",borderColor:"rgba(75,92,192,1)",borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"rgba(75,92,192,1)",pointBackgroundColor:"#fff",pointBorderWidth:1,pointHoverRadius:5,pointHoverBackgroundColor:"rgba(75,92,192,1)",pointHoverBorderColor:"rgba(220,220,220,1)",pointHoverBorderWidth:2,pointRadius:1,pointHitRadius:10,data:this.lineBudgetData,spanGaps:!1},{label:"Actual(%)",fill:!1,lineTension:.1,backgroundColor:"rgba(75,192,192,0.4)",borderColor:"rgba(75,192,192,1)",borderCapStyle:"butt",borderDash:[],borderDashOffset:0,borderJoinStyle:"miter",pointBorderColor:"rgba(75,192,192,1)",pointBackgroundColor:"#fff",pointBorderWidth:1,pointHoverRadius:5,pointHoverBackgroundColor:"rgba(75,192,192,1)",pointHoverBorderColor:"rgba(220,220,220,1)",pointHoverBorderWidth:2,pointRadius:1,pointHitRadius:10,data:this.lineActualData,spanGaps:!1}]},options:{scales:{xAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}],yAxes:[{gridLines:{color:"rgba(0, 0, 0, 0)"}}]}}})},l.prototype.bar=function(){this.barChart=new t.Chart(this.barCanvas.nativeElement,{type:"bar",data:{labels:this.barLabels,datasets:[{label:"Budget(%)",data:this.barBudgetData,backgroundColor:["rgba(255, 99, 132, 0.2)","rgba(54, 162, 235, 0.2)","rgba(255, 206, 86, 0.2)","rgba(75, 192, 192, 0.2)","rgba(153, 102, 255, 0.2)","rgba(255, 159, 64, 0.2)"],borderColor:["rgba(255,99,132,1)","rgba(54, 162, 235, 1)","rgba(255, 206, 86, 1)","rgba(75, 192, 192, 1)","rgba(153, 102, 255, 1)","rgba(255, 159, 64, 1)"],borderWidth:1},{label:"Actual(%)",data:this.barActualData,backgroundColor:["rgba(255, 99, 132, 0.2)","rgba(5, 162, 235, 0.2)","rgba(55, 206, 86, 0.2)","rgba(50, 192, 192, 0.2)","rgba(13, 102, 255, 0.2)","rgba(250, 159, 64, 0.2)"],borderColor:["rgba(255,99,132,1)","rgba(54, 162, 235, 1)","rgba(255, 206, 86, 1)","rgba(75, 192, 192, 1)","rgba(153, 102, 255, 1)","rgba(255, 159, 64, 1)"],borderWidth:1}]},options:{scales:{yAxes:[{ticks:{beginAtZero:!0}}]}}})},l.prototype.barSetup=function(){for(var l=6,n=this.month;l>0;)this.barLabels.push(n.format("MMM")),this.barLabelNums.push(n.format("M")),this.barLabelYears.push(n.format("Y")),n=n.subtract(1,"month"),l--;console.log(this.barLabels),console.log(this.barLabelNums),console.log(this.barLabelYears),this.getTransSumByMonth()},l}(),i=function(){return function(){}}(),s=a(551),b=a(552),d=a(553),g=a(554),c=a(555),h=a(556),p=a(557),_=a(558),f=a(559),v=a(57),m=a(51),C=a(3),B=a(26),y=a(6),D=a(33),L=a(19),Y=a(9),S=a(14),Z=a(36),j=a(28),k=a(21),N=a(24),A=a(15),I=a(5),z=a(10),H=a(17),J=a(13),M=a(84),O=a(64),P=a(45),w=a(207),R=a(60),W=a(12),x=a(40),T=a(48),E=u.X({encapsulation:2,styles:[],data:{}});function U(l){return u._22(0,[(l()(),u.Z(0,0,null,null,1,"ion-spinner",[["name","bubbles"],["paused","false"],["text-center",""]],[[2,"spinner-paused",null]],null,null,v.b,v.a)),u.Y(1,114688,null,0,m.a,[C.a,u.j,u.z],{name:[0,"name"],paused:[1,"paused"]},null)],function(l,n){l(n,1,0,"bubbles","false")},function(l,n){l(n,0,0,u._13(n,1)._paused)})}function V(l){return u._22(0,[u._18(402653184,1,{lineCanvas:0}),u._18(402653184,2,{barCanvas:0}),(l()(),u._20(-1,null,["\n\n\n  "])),(l()(),u.Z(3,0,null,null,16,"ion-header",[],null,null,null,null,null)),u.Y(4,16384,null,0,B.a,[C.a,u.j,u.z,[2,y.a]],null,null),(l()(),u._20(-1,null,["\n\n    "])),(l()(),u.Z(6,0,null,null,12,"div",[["class","logo-container"],["text-center",""]],[[4,"display",null]],null,null,null,null)),(l()(),u._20(-1,null,["\n        "])),(l()(),u.Z(8,0,null,null,0,"img",[["alt","Logo"],["src","assets/imgs/logo.png"]],null,null,null,null,null)),(l()(),u._20(-1,null,["\n  \n        "])),(l()(),u._20(-1,null,["\n        \n        "])),(l()(),u.Z(11,0,null,null,6,"ion-navbar",[["class","toolbar"]],[[8,"hidden",0],[2,"statusbar-padding",null]],null,null,D.b,D.a)),u.Y(12,49152,null,0,L.a,[Y.a,[2,y.a],[2,S.a],C.a,u.j,u.z],null,null),(l()(),u._20(-1,3,["\n      "])),(l()(),u.Z(14,0,null,3,2,"ion-title",[],null,null,null,Z.b,Z.a)),u.Y(15,49152,null,0,j.a,[C.a,u.j,u.z,[2,k.a],[2,L.a]],null,null),(l()(),u._20(-1,0,["Assets Budget Performance"])),(l()(),u._20(-1,3,["\n          \n        "])),(l()(),u._20(-1,null,["\n  \n    "])),(l()(),u._20(-1,null,["\n  \n    \n  \n  "])),(l()(),u._20(-1,null,["\n  \n  \n  "])),(l()(),u.Z(21,0,null,null,42,"ion-content",[["padding",""]],[[2,"statusbar-padding",null],[2,"has-refresher",null]],null,null,N.b,N.a)),u.Y(22,4374528,null,0,A.a,[C.a,I.a,z.a,u.j,u.z,Y.a,H.a,u.u,[2,y.a],[2,S.a]],null,null),(l()(),u._20(-1,1,["\n      "])),(l()(),u.U(16777216,null,1,1,null,U)),u.Y(25,16384,null,0,J.k,[u.I,u.F],{ngIf:[0,"ngIf"]},null),(l()(),u._20(-1,1,["\n  \n      "])),(l()(),u.Z(27,0,null,1,7,"ion-grid",[["class","grid"]],null,null,null,null,null)),u.Y(28,16384,null,0,M.a,[],null,null),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(30,0,null,null,3,"ion-row",[["align-items-center",""],["class","row"],["justify-content-center",""]],null,null,null,null,null)),u.Y(31,16384,null,0,O.a,[],null,null),(l()(),u._20(-1,null,["\n              \n            "])),(l()(),u._20(-1,null,["\n        \n          "])),(l()(),u._20(-1,null,["\n        "])),(l()(),u._20(-1,1,["\n  \n\n        "])),(l()(),u.Z(36,0,null,1,12,"ion-card",[],null,null,null,null,null)),u.Y(37,16384,null,0,P.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          "])),(l()(),u.Z(39,0,null,null,2,"ion-card-header",[],null,null,null,null,null)),u.Y(40,16384,null,0,w.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n            Line Chart\n          "])),(l()(),u._20(-1,null,["\n        "])),(l()(),u.Z(43,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),u.Y(44,16384,null,0,R.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n            "])),(l()(),u.Z(46,0,[[1,0],["lineCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),u._20(-1,null,["\n         "])),(l()(),u._20(-1,null,["\n      "])),(l()(),u._20(-1,1,["\n\n  \n      "])),(l()(),u.Z(50,0,null,1,12,"ion-card",[],null,null,null,null,null)),u.Y(51,16384,null,0,P.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n          \n        "])),(l()(),u.Z(53,0,null,null,2,"ion-card-header",[],null,null,null,null,null)),u.Y(54,16384,null,0,w.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n            Bar Chart\n          "])),(l()(),u._20(-1,null,["\n        "])),(l()(),u.Z(57,0,null,null,4,"ion-card-content",[],null,null,null,null,null)),u.Y(58,16384,null,0,R.a,[C.a,u.j,u.z],null,null),(l()(),u._20(-1,null,["\n            "])),(l()(),u.Z(60,0,[[2,0],["barCanvas",1]],null,0,"canvas",[],null,null,null,null,null)),(l()(),u._20(-1,null,["\n         "])),(l()(),u._20(-1,null,["\n      "])),(l()(),u._20(-1,1,["\n  \n  "])),(l()(),u._20(-1,null,["\n"]))],function(l,n){l(n,25,0,0==n.component.loaded)},function(l,n){l(n,6,0,"block"),l(n,11,0,u._13(n,12)._hidden,u._13(n,12)._sbPadding),l(n,21,0,u._13(n,22).statusbarPadding,u._13(n,22)._hasRefresher)})}var F=u.V("page-sav-invbudget",o,function(l){return u._22(0,[(l()(),u.Z(0,0,null,null,1,"page-sav-invbudget",[],null,null,null,V,E)),u.Y(1,49152,null,0,o,[S.a,W.a,x.a,T.a,r.a],null,null)],null,null)},{},{},[]),G=a(35),X=a(206),q=a(89);a.d(n,"SavInvbudgetPageModuleNgFactory",function(){return K});var K=u.W(i,[],function(l){return u._10([u._11(512,u.i,u.S,[[8,[s.a,b.a,d.a,g.a,c.a,h.a,p.a,_.a,f.a,F]],[3,u.i],u.s]),u._11(4608,J.m,J.l,[u.r,[2,J.u]]),u._11(4608,G.k,G.k,[]),u._11(4608,G.c,G.c,[]),u._11(512,J.b,J.b,[]),u._11(512,G.j,G.j,[]),u._11(512,G.d,G.d,[]),u._11(512,G.i,G.i,[]),u._11(512,X.a,X.a,[]),u._11(512,X.b,X.b,[]),u._11(512,i,i,[]),u._11(256,q.a,o,[])])})}});
=======
webpackJsonp([1],{

/***/ 890:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavInvbudgetPageModule", function() { return SavInvbudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sav_invbudget__ = __webpack_require__(908);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavInvbudgetPageModule = (function () {
    function SavInvbudgetPageModule() {
    }
    SavInvbudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sav_invbudget__["a" /* SavInvbudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sav_invbudget__["a" /* SavInvbudgetPage */]),
            ],
        })
    ], SavInvbudgetPageModule);
    return SavInvbudgetPageModule;
}());

//# sourceMappingURL=sav-invbudget.module.js.map

/***/ }),

/***/ 908:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavInvbudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SavInvbudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SavInvbudgetPage = (function () {
    function SavInvbudgetPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 0;
        this.barLabels = [];
        this.barLabelNums = [];
        this.barLabelYears = [];
        this.month = __WEBPACK_IMPORTED_MODULE_5_moment__();
        this.totalIncome = [];
        this.barBudgetData = [];
    }
    SavInvbudgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SavInvbudgetPage');
        this.getUserName();
    };
    SavInvbudgetPage.prototype.getUserName = function () {
        // this.userName = "Tayo";
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.inc = initialData.inc;
            _this.barSetup();
        });
    };
    SavInvbudgetPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "months": this.barLabelNums.join(),
            "years": this.barLabelYears.join(),
        };
        // });
    };
    SavInvbudgetPage.prototype.getTransSumByMonth = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        this.grsp.getTransSumByMonth(this.apiData).then(function (data) {
            console.log(data);
            _this.retval = data;
            console.log(JSON.parse(_this.retval.budget));
            var income = JSON.parse(_this.retval.sumIncome);
            var budget = JSON.parse(_this.retval.budget);
            budget.forEach(function (element, ind) {
                var expPercent = element[0] ? element[0].b_savInv : 25;
                // let exp = (expPercent/100) * (element[0] ? element[0].b_income : income[ind]);
                var exp = (expPercent / 100) * (element[0] ? element[0].b_income : _this.inc);
                _this.barBudgetData.push(exp);
            });
            console.log(_this.barBudgetData);
            _this.barActualData = JSON.parse(_this.retval.sumSavInv);
            _this.loaded = 1;
            _this.lineLabels = _this.barLabels;
            _this.lineBudgetData = _this.barBudgetData;
            _this.lineActualData = _this.barActualData;
            _this.bar();
            _this.liNe();
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    SavInvbudgetPage.prototype.liNe = function () {
        // if (this.toggleVarLine){
        this.lineChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                // labels: ["January", "February", "March", "April", "May", "June", "July"],
                labels: this.lineLabels,
                datasets: [
                    {
                        label: "Budget(%)",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,92,192,0.4)",
                        borderColor: "rgba(75,92,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,92,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,92,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 55, 90, 21, 78, 95, 20],
                        data: this.lineBudgetData,
                        spanGaps: false,
                    },
                    {
                        label: "Actual(%)",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.lineActualData,
                        spanGaps: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }]
                },
            }
        });
        // }
    };
    SavInvbudgetPage.prototype.bar = function () {
        this.barChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.barCanvas.nativeElement, {
            type: 'bar',
            data: {
                // labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                labels: this.barLabels,
                datasets: [{
                        label: 'Budget(%)',
                        data: this.barBudgetData,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Actual(%)',
                        // data: [12, 19, 13, 5, 2, 13],
                        data: this.barActualData,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(5, 162, 235, 0.2)',
                            'rgba(55, 206, 86, 0.2)',
                            'rgba(50, 192, 192, 0.2)',
                            'rgba(13, 102, 255, 0.2)',
                            'rgba(250, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    };
    SavInvbudgetPage.prototype.barSetup = function () {
        var i = 6;
        var tday = this.month;
        while (i > 0) {
            // console.log(tday.format('MMMM'));
            this.barLabels.push(tday.format('MMM'));
            this.barLabelNums.push(tday.format('M'));
            this.barLabelYears.push(tday.format('Y'));
            tday = tday.subtract(1, 'month');
            i--;
        }
        console.log(this.barLabels);
        console.log(this.barLabelNums);
        console.log(this.barLabelYears);
        // this.barLabels = this.barLabels.reverse();
        // this.barLabels.forEach((element, ind) => {
        // });
        this.getTransSumByMonth();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], SavInvbudgetPage.prototype, "lineCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], SavInvbudgetPage.prototype, "barCanvas", void 0);
    SavInvbudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sav-invbudget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sav-invbudget\sav-invbudget.html"*/'<!--\n  Generated template for the SavInvbudgetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n  <ion-header>\n\n    <div class="logo-container" text-center [style.display]="\'block\'">\n        <img src="assets/imgs/logo.png" alt="Logo">\n  \n        <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n            <ion-segment-button value="signin">\n                Sign In\n            </ion-segment-button>\n           \n        </ion-segment> -->\n        \n        <ion-navbar>\n      <ion-title>Assets Budget Performance</ion-title>\n          \n        </ion-navbar>\n  \n    </div>\n  \n    \n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n      <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n  \n      <ion-grid >\n          <ion-row justify-content-center align-items-center >\n              \n            <!-- <button ion-button small (click)="week()" >Week</button>\n              <button ion-button small (click)="month()" >Month</button>\n              <button ion-button small (click)="year()" >Year</button>\n              <button ion-button small (click)="range()" >Range</button> -->\n        \n          </ion-row>\n        </ion-grid>\n  \n\n        <ion-card>\n          <ion-card-header >\n            Line Chart\n          </ion-card-header>\n        <ion-card-content>\n            <canvas #lineCanvas></canvas>\n         </ion-card-content>\n      </ion-card>\n\n  \n      <ion-card>\n          \n        <ion-card-header >\n            Bar Chart\n          </ion-card-header>\n        <ion-card-content>\n            <canvas #barCanvas></canvas>\n         </ion-card-content>\n      </ion-card>\n  \n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sav-invbudget\sav-invbudget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], SavInvbudgetPage);
    return SavInvbudgetPage;
}());

//# sourceMappingURL=sav-invbudget.js.map

/***/ })

});
//# sourceMappingURL=1.js.map
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
