webpackJsonp([12],{

/***/ 880:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FixedDepositPageModule", function() { return FixedDepositPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fixed_deposit__ = __webpack_require__(898);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FixedDepositPageModule = (function () {
    function FixedDepositPageModule() {
    }
    FixedDepositPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fixed_deposit__["a" /* FixedDepositPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__fixed_deposit__["a" /* FixedDepositPage */]),
            ],
        })
    ], FixedDepositPageModule);
    return FixedDepositPageModule;
}());

//# sourceMappingURL=fixed-deposit.module.js.map

/***/ }),

/***/ 898:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FixedDepositPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the FixedDepositPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FixedDepositPage = (function () {
    function FixedDepositPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    FixedDepositPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FixedDepositPage');
        this.getUserName();
    };
    FixedDepositPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getSavingsApi();
        });
    };
    FixedDepositPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Fixed Deposit"
        };
        // });
    };
    FixedDepositPage.prototype.getSavingsApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.banking(this.apiData).then(function (data) {
            console.log(data);
            _this.sav = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.fd = JSON.parse(_this.sav.banking);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    FixedDepositPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fixed-deposit',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\fixed-deposit\fixed-deposit.html"*/'<!--\n  Generated template for the FixedDepositPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Fixed Deposit</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of fd">\n        <ion-card-content>\n<h4>{{item.Banks}}</h4>\n<h2>{{item.Minimum_account_opening_balance}}</h2>\n<p text-right>{{item.Interest_rate}}</p>\n<p text-right>{{item.child_type}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\fixed-deposit\fixed-deposit.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FixedDepositPage);
    return FixedDepositPage;
}());

//# sourceMappingURL=fixed-deposit.js.map

/***/ })

});
//# sourceMappingURL=12.js.map