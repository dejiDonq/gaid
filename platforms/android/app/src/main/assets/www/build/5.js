webpackJsonp([5],{

/***/ 887:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PayConfPageModule", function() { return PayConfPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pay_conf__ = __webpack_require__(905);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PayConfPageModule = (function () {
    function PayConfPageModule() {
    }
    PayConfPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__pay_conf__["a" /* PayConfPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__pay_conf__["a" /* PayConfPage */]),
            ],
        })
    ], PayConfPageModule);
    return PayConfPageModule;
}());

//# sourceMappingURL=pay-conf.module.js.map

/***/ }),

/***/ 905:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PayConfPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the PayConfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PayConfPage = (function () {
    function PayConfPage(navCtrl, grsp, loading, storage, alertCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.grsp = grsp;
        this.loading = loading;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.loaded = 1;
    }
    PayConfPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PayConfPage');
        this.res = this.navParams.get('res');
        this.amount = this.navParams.get('amt');
        this.manageRes();
    };
    PayConfPage.prototype.manageRes = function () {
        var _this = this;
        if (this.res == "success") {
            // this.loadWallet(this.id);
            this.storage.get('investData').then(function (data) {
                console.log(data);
                _this.apiData = data;
                _this.buyInvestApi();
                console.log("done");
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Payment Failed, Please Confirm You were not Debited',
                buttons: ['Dismiss']
            });
            alert_1.present();
            // this.loader.dismiss();
            this.navCtrl.setRoot("InvestmentPage");
        }
    };
    PayConfPage.prototype.buyInvestApi = function () {
        var _this = this;
        // this.setApiJson();
        // this.loaded = 0;
        var loader = this.loading.create({
            content: 'Processing...'
        });
        loader.present();
        console.log(this.apiData);
        console.log("done");
        this.grsp.buyInvestment(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            if (_this.apiRes.response == true) {
                loader.dismiss();
                var alert_2 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Saved NGN' + _this.amount + ' of ' + _this.apiRes.prov + ' Investment ',
                    buttons: ['Ok']
                });
                alert_2.present();
                _this.storage.set('investData', null);
                _this.navCtrl.setRoot("InvestmentPage");
            }
            else {
                var alert_3 = _this.alertCtrl.create({
                    title: 'Failed',
                    subTitle: 'Something went wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_3.present();
            }
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        this.storage.set('investData', null);
    };
    PayConfPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-pay-conf',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\pay-conf\pay-conf.html"*/'<!--\n  Generated template for the PayConfPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Payment Confirmation</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\pay-conf\pay-conf.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], PayConfPage);
    return PayConfPage;
}());

//# sourceMappingURL=pay-conf.js.map

/***/ })

});
//# sourceMappingURL=5.js.map