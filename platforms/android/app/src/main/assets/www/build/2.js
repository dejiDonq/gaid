webpackJsonp([2],{

/***/ 892:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SavInvPageModule", function() { return SavInvPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sav_inv__ = __webpack_require__(910);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SavInvPageModule = (function () {
    function SavInvPageModule() {
    }
    SavInvPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sav_inv__["a" /* SavInvPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sav_inv__["a" /* SavInvPage */]),
            ],
        })
    ], SavInvPageModule);
    return SavInvPageModule;
}());

//# sourceMappingURL=sav-inv.module.js.map

/***/ }),

/***/ 910:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SavInvPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the SavInvPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SavInvPage = (function () {
    function SavInvPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.toggleVar = true;
        this.toggleVarLine = true;
        this.loaded = 0;
        this.drange = false;
        this.pieValues = [];
        this.lineValues = [];
    }
    SavInvPage.prototype.ionViewDidLoad = function () {
        var tday = __WEBPACK_IMPORTED_MODULE_5_moment__().format("YYYY-MM-DD").toString();
        console.log(tday);
        // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
        var yday = __WEBPACK_IMPORTED_MODULE_5_moment__().subtract(1, 'month').format("YYYY-MM-DD").toString();
        console.log(yday);
        this.end = tday;
        this.start = yday;
        console.log('ionViewDidLoad SavInvPage');
        this.valName = this.navParams.get('val');
        this.getUserName();
    };
    SavInvPage.prototype.doNut = function () {
        if (this.toggleVar) {
            console.log(this.donutLabels);
            this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.doughnutCanvas.nativeElement, {
                type: 'doughnut',
                data: {
                    // labels: ["Food", "Utility", "Airtime", "Investments", "Entertainment", "Travel"],
                    // labels: this.donutLabels,
                    labels: this.pieLabels,
                    datasets: [{
                            label: 'User Personal Finance Status',
                            // data: [12, 19, 3, 5, 2, 3],
                            data: this.pieValues,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            hoverBackgroundColor: [
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56",
                                "#FF6384",
                                "#36A2EB",
                                "#FFCE56"
                            ]
                        }]
                }
            });
        }
    };
    SavInvPage.prototype.liNe = function () {
        if (this.toggleVarLine) {
            this.lineChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.lineCanvas.nativeElement, {
                type: 'line',
                data: {
                    // labels: ["January", "February", "March", "April", "May", "June", "July"],
                    labels: this.lineLabels,
                    datasets: [
                        {
                            label: "Assets",
                            fill: false,
                            lineTension: 0.1,
                            backgroundColor: "rgba(75,92,192,0.4)",
                            borderColor: "rgba(75,92,192,1)",
                            borderCapStyle: 'butt',
                            borderDash: [],
                            borderDashOffset: 0.0,
                            borderJoinStyle: 'miter',
                            pointBorderColor: "rgba(75,92,192,1)",
                            pointBackgroundColor: "#fff",
                            pointBorderWidth: 1,
                            pointHoverRadius: 5,
                            pointHoverBackgroundColor: "rgba(75,92,192,1)",
                            pointHoverBorderColor: "rgba(220,220,220,1)",
                            pointHoverBorderWidth: 2,
                            pointRadius: 1,
                            pointHitRadius: 10,
                            // data: [65, 55, 90, 21, 78, 95, 20],
                            data: this.lineValues,
                            spanGaps: false,
                        },
                    ]
                },
                options: {
                    scales: {
                        xAxes: [{
                                gridLines: {
                                    color: "rgba(0, 0, 0, 0)",
                                }
                            }],
                        yAxes: [{
                                gridLines: {
                                    color: "rgba(0, 0, 0, 0)",
                                }
                            }]
                    },
                }
            });
        }
    };
    SavInvPage.prototype.getUserName = function () {
        // this.userName = "Tayo";
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getTransactionsApi(_this.start, _this.end);
        });
    };
    SavInvPage.prototype.week = function () {
        var weekStart = __WEBPACK_IMPORTED_MODULE_5_moment__().startOf('isoWeek').format('YYYY-MM-DD HH:mm:ss');
        var weekEnd = __WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(weekStart, weekEnd);
    };
    SavInvPage.prototype.month = function () {
        var start = __WEBPACK_IMPORTED_MODULE_5_moment__().startOf('month').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    SavInvPage.prototype.year = function () {
        var start = __WEBPACK_IMPORTED_MODULE_5_moment__().startOf('year').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_5_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    SavInvPage.prototype.range = function () {
        this.drange = !this.drange;
    };
    SavInvPage.prototype.getRange = function () {
        this.getTransactionsApi(this.from, this.to);
    };
    SavInvPage.prototype.setApiJson = function (start, end) {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "start": start,
            "end": end
        };
        // });
    };
    SavInvPage.prototype.getTransactionsApi = function (start, end) {
        var _this = this;
        this.loaded = 0;
        this.setApiJson(start, end);
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.lineLabels = [];
        this.lineValues = [];
        this.pieLabels = [];
        this.pieValues = [];
        this.grsp.transactions(this.apiData).then(function (data) {
            // console.log(data);
            _this.trans = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.transactions = JSON.parse(_this.trans.transactions);
            _this.transactions = _this.transactions.filter(function (tr) { return tr.trans_bucket == 'Assets'; });
            // get all
            _this.pieLabels = _this.transactions.map(function (value) { return value.trans_bucket_child; });
            console.log(_this.transactions);
            console.log(_this.pieLabels);
            // get unique
            _this.pieLabels = _this.pieLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            console.log(_this.pieLabels);
            var isArr = (Array.isArray(_this.pieLabels));
            console.log(isArr);
            _this.pieLabels.forEach(function (element) {
                // console.log(element);
                var arr = _this.transactions.filter(function (tr) { return tr.trans_bucket_child == element; });
                // console.log(arr);
                var subArr = arr.map(function (value) { return value.trans_amt; });
                subArr = subArr.map(function (x) { return parseInt(x); });
                // console.log(subArr);
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                // console.log(arrSum);
                _this.pieValues.push(arrSum);
            });
            console.log(_this.pieValues);
            _this.doNut();
            // -----------------------------Line graph-----------------------------------
            // get all
            _this.lineLabels = _this.transactions.map(function (value) { return value.trans_date; });
            // get unique
            _this.lineLabels = _this.lineLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            // get last 5 days
            _this.lineLabels = _this.lineLabels.slice(-5);
            _this.lineLabels.forEach(function (element) {
                // get all transactions for current iteration
                var arr = _this.transactions.filter(function (tr) { return tr.trans_date == element; });
                // get all amts for all those transactions
                var subArr = arr.map(function (value) { return value.trans_amt; });
                // parse amts to int
                subArr = subArr.map(function (x) { return parseInt(x); });
                // sum them up
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                _this.lineValues.push(arrSum);
            });
            console.log(_this.lineValues);
            _this.liNe();
            _this.loaded = 1;
            alert = _this.alertCtrl.create({
                title: 'No name',
                subTitle: 'no Name' + _this.transactions[0],
                buttons: ['Dismiss']
            });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            console.log(err);
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], SavInvPage.prototype, "doughnutCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], SavInvPage.prototype, "lineCanvas", void 0);
    SavInvPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sav-inv',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sav-inv\sav-inv.html"*/'<!--\n  Generated template for the SavInvPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n      <ion-title>Assets Analytics for {{userName}}</ion-title>\n    </ion-navbar>\n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n    \n    <ion-grid >\n      <ion-row justify-content-center align-items-center >\n    <!--  -->\n          <!-- <button ion-button small (click)="apitest()">Week</button> -->\n          <button ion-button small (click)="week()" >Week</button>\n          <button ion-button small (click)="month()" >Month</button>\n          <button ion-button small (click)="year()" >Year</button>\n          <button ion-button small (click)="range()" >Range</button>\n    \n      </ion-row>\n    </ion-grid>\n    \n    <ion-card *ngIf="drange">\n          \n      <ion-card-content>\n          <ion-item>\n            <ion-label stacked> From </ion-label>\n            <ion-input type="date" [(ngModel)]="from" name="from" ></ion-input>\n          </ion-item>\n          <ion-item>\n            <ion-label stacked> To </ion-label>\n            <ion-input type="date" [(ngModel)]="to" name="to" ></ion-input>\n          </ion-item>\n      <button ion-button small (click)="getRange()" >Go</button>\n         \n       </ion-card-content>\n    </ion-card>\n    \n        <ion-card>\n            <!-- <ion-card-header (click)="toggleDonut()"> -->\n            <ion-card-header >\n              Donut\n            </ion-card-header>\n          <ion-card-content>\n              <canvas #doughnutCanvas></canvas>\n           </ion-card-content>\n        </ion-card>\n    \n        <ion-card>\n            <ion-card-header >\n              Line\n            </ion-card-header>\n          <ion-card-content>\n              <canvas #lineCanvas></canvas>\n           </ion-card-content>\n        </ion-card>\n    \n    \n    </ion-content>\n  \n\n\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sav-inv\sav-inv.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], SavInvPage);
    return SavInvPage;
}());

//# sourceMappingURL=sav-inv.js.map

/***/ })

});
//# sourceMappingURL=2.js.map