webpackJsonp([18],{

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DashboardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DashboardPage = (function () {
    function DashboardPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 0;
        this.networth = 0;
        this.assets = 0;
        this.liability = 0;
        this.pension = 0;
        this.amtRet = 0;
        this.ageRet = 0;
        this.drange = false;
        this.loadProgress = 0;
        this.loadProgressAge = 0;
        this.currAge = 0;
        this.finAge = 90;
        // getUserName(){
        //   this.userName = "Tayo";
        // }
        this.anotherSav = 0;
        this.pieValues = [];
        // lineLabels:any;
        this.lineIncomeValues = [];
        this.lineExpenseValues = [];
    }
    // initialData = {
    //   id:0,
    //   name:'',
    //   dob:'',
    //   dow:'',
    //   pen:'',
    //   savInv:'',
    //   exp:'',
    //   inc:'',
    //   ret:'',
    //   // desc:'',
    //   // date:'',
    // };
    DashboardPage.prototype.ionViewWillEnter = function () {
        // this.load();
        var tday = __WEBPACK_IMPORTED_MODULE_6_moment__().format("YYYY-MM-DD").toString();
        // console.log(tday);
        // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
        var yday = __WEBPACK_IMPORTED_MODULE_6_moment__().subtract(1, 'month').format("YYYY-MM-DD").toString();
        // console.log(yday);
        this.end = tday;
        this.start = yday;
        this.load();
        console.log('ionViewWillEnter DashboardPage');
        // this.initialData = this.navParams.get('val');
        this.getUserName();
    };
    DashboardPage.prototype.ionViewDidLoad = function () {
        var tday = __WEBPACK_IMPORTED_MODULE_6_moment__().format("YYYY-MM-DD").toString();
        // console.log(tday);
        // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
        var yday = __WEBPACK_IMPORTED_MODULE_6_moment__().subtract(1, 'month').format("YYYY-MM-DD").toString();
        // console.log(yday);
        this.end = tday;
        this.start = yday;
        this.load();
        console.log('ionViewDidLoad DashboardPage');
        // this.initialData = this.navParams.get('val');
        this.getUserName();
        // this.doNut();
        // this.liNe();
        // this.apiLoad();
        // this.retirement();
        // });
    };
    DashboardPage.prototype.clearInit = function () {
        // this.storage.set('initialData', null);
        // this.navCtrl.push(InitialPage, {
        //   val: 'I am View Ana-lytics'
        // })
    };
    DashboardPage.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
    };
    DashboardPage.prototype.week = function () {
        var weekStart = __WEBPACK_IMPORTED_MODULE_6_moment__().startOf('isoWeek').format('YYYY-MM-DD HH:mm:ss');
        var weekEnd = __WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(weekStart, weekEnd);
    };
    DashboardPage.prototype.month = function () {
        var start = __WEBPACK_IMPORTED_MODULE_6_moment__().startOf('month').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    DashboardPage.prototype.year = function () {
        var start = __WEBPACK_IMPORTED_MODULE_6_moment__().startOf('year').format('YYYY-MM-DD HH:mm:ss');
        var end = __WEBPACK_IMPORTED_MODULE_6_moment__().format('YYYY-MM-DD HH:mm:ss');
        this.getTransactionsApi(start, end);
    };
    DashboardPage.prototype.range = function () {
        this.drange = !this.drange;
    };
    DashboardPage.prototype.getRange = function () {
        this.getTransactionsApi(this.from, this.to);
    };
    DashboardPage.prototype.apiLoad = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        this.grsp.dashboardLoad({ "uId": this.uId }).then(function (data) {
            // console.log(data);
            _this.dash = data;
            _this.assets = _this.numberWithCommas(parseInt(_this.dash.rassets));
            _this.liability = _this.numberWithCommas(parseInt(_this.dash.rliabilities));
            // this.networth = this.dash.networth;
            _this.networth = parseInt(_this.dash.rassets) - parseInt(_this.dash.rliabilities);
            _this.networth = _this.numberWithCommas(_this.networth);
            // this.pension = this.numberWithCommas(parseInt(this.dash.pension));
            // this.amtRet = this.dash.retirementTarget;
            // this.ageRet = this.dash.retirementAge;
            _this.assetTrend = parseInt(_this.dash.rassets) - parseInt(_this.dash.oassets);
            _this.liabTrend = parseInt(_this.dash.rliabilities) - parseInt(_this.dash.oliabilities);
            var onetworth = parseInt(_this.dash.oassets) - parseInt(_this.dash.oliabilities);
            _this.nwTrend = _this.networth - onetworth;
            // console.log(parseInt(this.dash.rassets));
            // console.log(parseInt(this.dash.oassets));
            // console.log(parseInt(this.dash.rliabilities));
            // console.log(parseInt(this.dash.oliabilities));
            // console.log(this.assetTrend);
            // console.log(this.liabTrend);
            // console.log(this.nwTrend);
            _this.donutLabels = _this.dash.donutLabels.split(",");
            // this.donutLabels = this.donutLabels.split(",");
            _this.donutData = _this.dash.donutData.split(",");
            _this.lineLabels = _this.dash.lineLabels.split(",");
            _this.lineIncomeData = _this.dash.lineIncomeData.split(",");
            _this.lineExpenseData = _this.dash.lineExpenseData.split(",");
            _this.doNut();
            _this.liNe();
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.dash.userName,
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    DashboardPage.prototype.retirement = function (time) {
        // let savings = this.savings;
        var savings = this.anotherSav;
        var expenses = this.expenses;
        var wrkingYrs = this.wrkingYrs;
        var retYrs = this.retYrs;
        // let savings = 1000
        var nvalue = savings;
        var r = 0.14;
        var count = 0;
        while (count < wrkingYrs) {
            var temp = savings * Math.pow((1 + r), count);
            // console.log(temp);
            nvalue += temp;
            count++;
        }
        // console.log(nvalue);
        var prannuity;
        prannuity = (1 - (Math.pow((1 + r), (retYrs * -1)))) / r;
        // console.log(prannuity);
        var annuity = (1 / prannuity) * nvalue;
        this.amtRet = this.numberWithCommas(annuity.toFixed(2));
        this.annuity = this.numberWithCommas((annuity / retYrs).toFixed(2));
        // console.log(annuity);
    };
    DashboardPage.prototype.load = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    DashboardPage.prototype.doNut = function () {
        console.log(this.donutLabels);
        this.doughnutChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.doughnutCanvas.nativeElement, {
            type: 'doughnut',
            data: {
                // labels: ["Food", "Utility", "Airtime", "Investments", "Entertainment", "Travel"],
                // labels: this.donutLabels,
                labels: this.pieLabels,
                datasets: [{
                        label: 'User Personal Finance Status',
                        // data: this.donutData,
                        data: this.pieValues,
                        // data: [12, 19, 3, 5, 2, 3],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56",
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }]
            },
            options: {
                //   scales: {
                //     xAxes: [{
                //         gridLines: {
                //             color: "rgba(0, 0, 0, 0)",
                //         }
                //     }],
                //     yAxes: [{
                //         gridLines: {
                //             color: "rgba(0, 0, 0, 0)",
                //         }   
                //     }]
                // },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem, data) {
                            var dataset = data.datasets[tooltipItem.datasetIndex];
                            // var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                            // var total = meta.total;
                            var currentValue = dataset.data[tooltipItem.index];
                            // var percentage = parseFloat((currentValue/total*100).toFixed(1));
                            // return currentValue + ' (' + percentage + '%)';
                            return  true ? currentValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : currentValue;
                        },
                    }
                }
            }
        });
    };
    DashboardPage.prototype.liNe = function () {
        this.lineChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                // labels: ["January", "February", "March", "April", "May", "June", "July"],
                labels: this.lineLabels,
                datasets: [
                    {
                        label: "Income",
                        fill: false,
                        lineTension: 0.1,
                        // backgroundColor: "rgba(75,92,192,0.4)",
                        borderColor: "rgba(75,92,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,92,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,92,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 55, 90, 21, 78, 95, 20],
                        data: this.lineIncomeValues,
                        spanGaps: false,
                    },
                    {
                        label: "Expense",
                        fill: false,
                        lineTension: 0.1,
                        // backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.lineExpenseValues,
                        spanGaps: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }]
                },
                tooltips: {
                    callbacks: {
                        label: function (tooltipItem) {
                            return  true ? tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : tooltipItem.yLabel;
                        }
                    }
                }
            }
        });
    };
    DashboardPage.prototype.getUserName = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            if (initialData.name.length < 1 || initialData.dob.length < 1 || initialData.dow.length < 1
                || initialData.pen.length < 1 || initialData.ret.length < 1 || initialData.email.length < 1
                || initialData.pword.length < 1) {
                _this.storage.set('initialData', null);
                var alert_1 = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Invalid Details, Re-Login or Contact Admin',
                    buttons: ['Ok']
                });
                alert_1.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__signup_signup__["a" /* SignupPage */]);
                return;
            }
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.pension = _this.numberWithCommas(parseInt(initialData.pen));
            _this.apiLoad();
            var ageNow = __WEBPACK_IMPORTED_MODULE_6_moment__().diff(initialData.dob, 'years');
            _this.savings = _this.numberWithCommas(parseInt(initialData.savInv));
            _this.anotherSav = parseInt(initialData.savInv);
            _this.expenses = _this.numberWithCommas(parseInt(initialData.exp));
            _this.wrkingYrs = parseInt(initialData.ret) - ageNow;
            _this.retYrs = 90 - parseInt(initialData.ret);
            var totAge = 90 - ageNow;
            _this.loadProgress = (_this.wrkingYrs / totAge) * 100;
            _this.loadProgressAge = initialData.ret;
            _this.currAge = ageNow;
            _this.getTransactionsApi(_this.start, _this.end);
            _this.retirement();
            _this.ageRet = initialData.ret;
        });
    };
    DashboardPage.prototype.setApiJson = function (start, end) {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "start": start,
            "end": end
        };
        // });
    };
    DashboardPage.prototype.getTransactionsApi = function (start, end) {
        var _this = this;
        this.loaded = 0;
        this.setApiJson(start, end);
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        // console.log(this.apiData);
        this.lineLabels = [];
        // this.lineValues= [];
        this.pieLabels = [];
        this.pieValues = [];
        this.grsp.transactions(this.apiData).then(function (data) {
            // console.log(data);
            _this.trans = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.transactions = JSON.parse(_this.trans.transactions);
            // this.transactions  = this.transactions.filter(tr => tr.trans_bucket == 'Assets');
            // get all
            _this.pieLabels = _this.transactions.map(function (value) { return value.trans_bucket; });
            // console.log(this.transactions);
            // console.log(this.pieLabels);
            // get unique
            _this.pieLabels = _this.pieLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            console.log(_this.pieLabels);
            var isArr = (Array.isArray(_this.pieLabels));
            // console.log(isArr);
            _this.pieLabels.forEach(function (element) {
                // console.log(element);
                var arr = _this.transactions.filter(function (tr) { return tr.trans_bucket == element; });
                // console.log(arr);
                var subArr = arr.map(function (value) { return value.trans_amt; });
                subArr = subArr.map(function (x) { return parseInt(x); });
                // console.log(subArr);
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                // console.log(arrSum);
                _this.pieValues.push(arrSum);
            });
            _this.pieValues.length = _this.pieLabels.length;
            console.log(_this.pieValues);
            _this.doNut();
            // -----------------------------Line graph-----------------------------------
            // get trans again
            _this.transactions = JSON.parse(_this.trans.transactions);
            _this.transactions = _this.transactions.filter(function (tr) { return tr.trans_bucket == 'Income'; });
            _this.lineLabels = _this.transactions.map(function (value) { return value.trans_date; });
            // get unique
            _this.lineLabels = _this.lineLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            // get last 5 days
            _this.lineLabels = _this.lineLabels.slice(-5);
            console.log(_this.lineLabels);
            _this.lineLabels.forEach(function (element) {
                // get all transactions for current iteration
                var arr = _this.transactions.filter(function (tr) { return tr.trans_date == element; });
                // get all amts for all those transactions
                var subArr = arr.map(function (value) { return value.trans_amt; });
                // parse amts to int
                subArr = subArr.map(function (x) { return parseInt(x); });
                // sum them up
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                _this.lineIncomeValues.push(arrSum);
            });
            console.log(_this.lineIncomeValues);
            // -------------------------------------------expenses line-------------------------------------------------
            _this.etransactions = JSON.parse(_this.trans.transactions);
            _this.etransactions = _this.etransactions.filter(function (tr) { return tr.trans_bucket == 'Expenses'; });
            // console.log(this.etransactions);
            _this.lineExpLabels = _this.etransactions.map(function (value) { return value.trans_date; });
            // get unique
            _this.lineExpLabels = _this.lineExpLabels.filter(function (v, i, a) { return a.indexOf(v) === i; });
            // get last 5 days
            _this.lineExpLabels = _this.lineExpLabels.slice(-5);
            console.log(_this.lineExpLabels);
            _this.lineExpLabels.forEach(function (element) {
                // get all transactions for current iteration
                var arr = _this.etransactions.filter(function (tr) { return tr.trans_date == element; });
                // get all amts for all those transactions
                var subArr = arr.map(function (value) { return value.trans_amt; });
                // parse amts to int
                subArr = subArr.map(function (x) { return parseInt(x); });
                // sum them up
                var arrSum = subArr.reduce(function (a, b) { return a + b; }, 0);
                _this.lineExpenseValues.push(arrSum);
            });
            console.log(_this.lineExpenseValues);
            _this.liNe();
            _this.loaded = 1;
            alert = _this.alertCtrl.create({
                title: 'No name',
                subTitle: 'no Name' + _this.transactions[0],
                buttons: ['Dismiss']
            });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            console.log(err);
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('doughnutCanvas'),
        __metadata("design:type", Object)
    ], DashboardPage.prototype, "doughnutCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], DashboardPage.prototype, "lineCanvas", void 0);
    DashboardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-dashboard',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\dashboard\dashboard.html"*/'<!--\n  Generated template for the DashboardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>{{userName}}\'s Dashboard</ion-title>\n  </ion-navbar>\n\n</ion-header> -->\n\n<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n    <ion-title>{{userName}}\'s Dashboard</ion-title>\n\n    </ion-navbar>\n\n    <br>\n    <div class="offer" text-center>\n        <!-- <h2>The Science of Delicious.</h2>\n        <p>Amazing coffees from around the world! </p> -->\n        <!-- <ion-grid >\n          <ion-row justify-content-center align-items-center >\n              <button ion-button small >Week</button>\n              <button ion-button small>Month</button>\n              <button ion-button small >Year</button>\n              <button ion-button small (click)="clearInit()">Range</button>\n      \n          </ion-row>\n        </ion-grid> -->\n    </div>\n\n\n  </ion-header>\n\n\n<ion-content padding>\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-grid >\n        <ion-row justify-content-center align-items-center >\n            <button ion-button small (click)="week()" >Week</button>\n        <button ion-button small (click)="month()" >Month</button>\n        <button ion-button small (click)="year()" >Year</button>\n        <button ion-button small (click)="range()" >Range</button>\n  \n    </ion-row>\n  </ion-grid>\n  \n      <ion-card *ngIf="drange">\n          \n        <ion-card-content>\n            <ion-item>\n              <ion-label stacked> From </ion-label>\n              <ion-input type="date" [(ngModel)]="from" name="from" ></ion-input>\n            </ion-item>\n            <ion-item>\n              <ion-label stacked> To </ion-label>\n              <ion-input type="date" [(ngModel)]="to" name="to" ></ion-input>\n            </ion-item>\n        <button ion-button small (click)="getRange()" >Go</button>\n           \n         </ion-card-content>\n      </ion-card>\n\n\n\n      <ion-card>\n          <!-- <ion-card-header>\n            Doughnut Chart\n          </ion-card-header> -->\n          <ion-card-content>\n            <canvas #doughnutCanvas></canvas>\n          </ion-card-content>\n        </ion-card>\n        <!-- <hr> -->\n        <ion-card>\n          <ion-card-content>\n            <canvas #lineCanvas></canvas>\n          </ion-card-content>\n        </ion-card>\n        \n        \n        \n        <!-- <hr> -->\n        <ion-card>\n          <ion-card-content>\n        \n        <ion-grid>\n          <ion-row>\n        \n            <ion-col col-12>\n              \n              <h1>Networth(NGN): {{networth}} <ion-icon name="{{nwTrend > 0 ? \'arrow-round-up\' : \'arrow-round-down\'}}" color="{{nwTrend > 0 ? \'primary\' : \'danger\'}}"></ion-icon></h1>\n            </ion-col>\n            <ion-col col-4>\n              <h5>Assets(NGN): <br> {{assets}} <ion-icon name="{{assetTrend > 0 ? \'arrow-round-up\' : \'arrow-round-down\'}}" color="{{assetTrend > 0 ? \'primary\' : \'danger\'}}"></ion-icon></h5>\n            </ion-col>\n            <ion-col col-4>\n              <h5>Liability(NGN): <br> {{liability}} <ion-icon name="{{liabTrend > 0 ? \'arrow-round-up\' : \'arrow-round-down\'}}" color="{{liabTrend > 0 ? \'primary\' : \'danger\'}}"></ion-icon></h5>\n            </ion-col>\n            <ion-col col-4>\n              <h5>Pension(NGN): <br> {{pension}} <ion-icon name="arrow-round-up" color="primary"></ion-icon></h5>\n            </ion-col>\n          </ion-row>\n        \n        </ion-grid>\n        \n          </ion-card-content>\n        </ion-card>\n       \n        <ion-card>\n          <ion-card-content>\n            \n            <ion-grid>\n              <ion-row>\n                \n                    <ion-col col-12>\n                        <h1 text-center>Retirement Plan (NGN) </h1>\n                      \n                    </ion-col>\n                    <ion-col col-3>\n                      <h5>Savings: <br/> {{savings}} </h5>\n                    </ion-col>\n                    <ion-col col-3>\n                      <h5>Expenses: <br/> {{expenses}} </h5>\n                    </ion-col>\n                    <ion-col col-6>\n                      <h5>Annuity For {{retYrs}} Years: <br> {{annuity}} </h5>\n                    </ion-col>\n                  </ion-row>\n                \n                </ion-grid>\n          \n            <!-- <progress-bar [progressAge]="loadProgressAge" [progress]="loadProgress" [tright]="finAge" [tleft]="currAge" ></progress-bar> -->\n            <progress-bar [progressAge]="loadProgressAge" [progress]="loadProgress" [tright]="finAge" [tleft]="currAge" ></progress-bar>\n         \n          </ion-card-content>\n        </ion-card>\n        \n        \n        <hr>\n        <ion-card>\n          <ion-card-content>\n            \n            <ion-grid>\n                <ion-row>\n                  \n                \n                  <ion-col col-6>\n                      <h5>Amount to Retire(NGN):\n                      <br>\n                      {{amtRet}}</h5>\n                  </ion-col>\n                  <ion-col col-6>\n                      <h5>Age to Retire(Years):<br/>{{ageRet}}</h5>\n                  </ion-col>\n                </ion-row>\n              \n              </ion-grid>\n        \n          </ion-card-content>\n        </ion-card>\n\n\n\n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\dashboard\dashboard.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], DashboardPage);
    return DashboardPage;
}());

//# sourceMappingURL=dashboard.js.map

/***/ }),

/***/ 165:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 165;

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditCardPageModule", function() { return CreditCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__credit_card__ = __webpack_require__(872);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CreditCardPageModule = (function () {
    function CreditCardPageModule() {
    }
    CreditCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__credit_card__["a" /* CreditCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__credit_card__["a" /* CreditCardPage */]),
            ],
        })
    ], CreditCardPageModule);
    return CreditCardPageModule;
}());

//# sourceMappingURL=credit-card.module.js.map

/***/ }),

/***/ 211:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/_projectsbudget/projectsbudget.module": [
		875,
		17
	],
	"../pages/addtransactions/addtransactions.module": [
		212
	],
	"../pages/analysis/analysis.module": [
		220
	],
	"../pages/banking/banking.module": [
		349
	],
	"../pages/budget/budget.module": [
		351
	],
	"../pages/cards/cards.module": [
		350
	],
	"../pages/credit-card/credit-card.module": [
		166
	],
	"../pages/current/current.module": [
		876,
		16
	],
	"../pages/dashboard/dashboard.module": [
		352
	],
	"../pages/debtbudget/debtbudget.module": [
		877,
		15
	],
	"../pages/edittransactions/edittransactions.module": [
		354
	],
	"../pages/expense/expense.module": [
		878,
		14
	],
	"../pages/expensebudget/expensebudget.module": [
		879,
		13
	],
	"../pages/favorites/favorites.module": [
		356
	],
	"../pages/fin-info/fin-info.module": [
		355
	],
	"../pages/fixed-deposit/fixed-deposit.module": [
		880,
		12
	],
	"../pages/foreign/foreign.module": [
		881,
		11
	],
	"../pages/forex-card/forex-card.module": [
		357
	],
	"../pages/income/income.module": [
		883,
		10
	],
	"../pages/incomebudget/incomebudget.module": [
		882,
		9
	],
	"../pages/initial/initial.module": [
		358
	],
	"../pages/investment/investment.module": [
		359
	],
	"../pages/liabilities/liabilities.module": [
		884,
		8
	],
	"../pages/liabilitiesbudget/liabilitiesbudget.module": [
		885,
		7
	],
	"../pages/loadsmstransactions/loadsmstransactions.module": [
		886,
		6
	],
	"../pages/managebudget/managebudget.module": [
		453
	],
	"../pages/menu-one/menu-one.module": [
		457
	],
	"../pages/menu-two/menu-two.module": [
		454
	],
	"../pages/naira-card/naira-card.module": [
		455
	],
	"../pages/order-details/order-details.module": [
		456
	],
	"../pages/pay-conf/pay-conf.module": [
		887,
		5
	],
	"../pages/prepaid-card/prepaid-card.module": [
		459
	],
	"../pages/profile/profile.module": [
		458
	],
	"../pages/projectbudget/projectbudget.module": [
		888,
		4
	],
	"../pages/projects/projects.module": [
		889,
		3
	],
	"../pages/sav-inv/sav-inv.module": [
		892,
		2
	],
	"../pages/sav-invbudget/sav-invbudget.module": [
		890,
		1
	],
	"../pages/savings/savings.module": [
		891,
		0
	],
	"../pages/setting/setting.module": [
		460
	],
	"../pages/signup/signup.module": [
		461
	],
	"../pages/splash-one/splash-one.module": [
		464
	],
	"../pages/splash-two/splash-two.module": [
		462
	],
	"../pages/sub-menu-one/sub-menu-one.module": [
		463
	],
	"../pages/sub-menu-two/sub-menu-two.module": [
		469
	],
	"../pages/viewanalytics/viewanalytics.module": [
		465
	],
	"../pages/viewbudget/viewbudget.module": [
		466
	],
	"../pages/viewtransactions/viewtransactions.module": [
		467
	],
	"../pages/walkthrough/walkthrough.module": [
		468
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 211;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 212:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddtransactionsPageModule", function() { return AddtransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addtransactions__ = __webpack_require__(541);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddtransactionsPageModule = (function () {
    function AddtransactionsPageModule() {
    }
    AddtransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__addtransactions__["a" /* AddtransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__addtransactions__["a" /* AddtransactionsPage */]),
            ],
        })
    ], AddtransactionsPageModule);
    return AddtransactionsPageModule;
}());

//# sourceMappingURL=addtransactions.module.js.map

/***/ }),

/***/ 22:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalRestServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_do__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_catch__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(3);
// import { HttpClient } from '@angular/common/http';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/*
  Generated class for the GlobalRestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var GlobalRestServiceProvider = (function () {
    function GlobalRestServiceProvider(http, alertCtrl) {
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.baseUrl = "http://gaid.io/myLeo";
        console.log('Hello GlobalRestServiceProvider Provider');
    }
    GlobalRestServiceProvider.prototype.showAlert = function (err) {
        // let alert = this.alertCtrl.create({
        //   title: 'Error',
        //   subTitle: 'Something broke and thats on us, Please try again',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
    };
    GlobalRestServiceProvider.prototype.buckets = function () {
        // let buckets = ['grocery','transportation','utilities','vacation','education','entertainment','food','gifts','subscriptions','rent','Banking'];
        var buckets = ['Income', 'Expenses', 'Saving&Investments(Assets)', 'Liabilities', 'Projects (Goals)'];
        return buckets;
    };
    GlobalRestServiceProvider.prototype.test = function (param) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.post(_this.baseUrl + "/test", param).subscribe(function (res) {
                resolve(res.json());
            });
        });
    };
    GlobalRestServiceProvider.prototype.addUser = function (param) {
        // console.log(param);
        // return new Promise(resolve=>{
        //   // this.http.post(`${this.baseUrl}/addUser`,param).subscribe(res=>resolve(res.json()));
        //   this.http.post(`${this.baseUrl}/addUser`,param).subscribe(res=>{
        //     console.log(res);
        //     resolve(res.json());
        var _this = this;
        //   });
        // })
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addUser", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.editUser = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/editUser", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                // this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.dashboardLoad = function (param) {
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/dashboard`,param).subscribe(res=>{
        //     console.log(res);
        //     resolve(res.json());
        //   });
        // })
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/dashboard", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.login = function (param) {
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/addTrans`,param).subscribe(res=>{
        var _this = this;
        //     resolve(res.json());
        //   });
        // })
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/login", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.addBudget = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addBudget", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.addTrans = function (param) {
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/addTrans`,param).subscribe(res=>{
        var _this = this;
        //     resolve(res.json());
        //   });
        // })
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addTrans", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    // editTrans(param){
    //   return new Promise((resolve,reject)=>{
    //     this.http.post(`${this.baseUrl}/editTrans`,param).subscribe(res=>{
    //       console.log(res);
    //       resolve(res.json());
    //     });
    //   })
    // }
    GlobalRestServiceProvider.prototype.editTrans = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/editTrans", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                // this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.buyInvestment = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/buyInvestment", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.addInitialData = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/addInitialData", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.analytics = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/analytics", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.fixedDeposits = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/fixedDeposits", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    // mutualFunds(param){
    //   return new Promise(resolve=>{
    //     this.http.post(`${this.baseUrl}/mutualFunds`,param).subscribe(res=>{
    //       resolve(res.json());
    //     });
    //   })
    // }
    // stocks(param){
    //   return new Promise(resolve=>{
    //     this.http.post(`${this.baseUrl}/stocks`,param).subscribe(res=>{
    //       resolve(res.json());
    //     });
    //   })
    // }
    GlobalRestServiceProvider.prototype.transactions = function (param) {
        // console.log(param);
        // return new Promise(resolve=>{
        //   this.http.post(`${this.baseUrl}/transactions`,param).subscribe(res=>{
        //     console.log(res);
        //     resolve(res.json());
        //   });
        // })
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/transactions", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.getTransSumByMonth = function (param) {
        var _this = this;
        // return new Promise(resolve=>{
        //     this.http.post(`${this.baseUrl}/transSum`,param).subscribe(res=>{
        //       console.log(res);
        //       resolve(res.json());
        //     });
        //   })
        //  ==================================================
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/transSum", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.banking = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/banking", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.cards = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/cards", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.investments = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/investments", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider.prototype.lastDate = function (param) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.http.post(_this.baseUrl + "/lastDate", param).map(function (res) { return res.json(); })
                .subscribe(function (data) { resolve(data); }, function (err) {
                _this.showAlert(err);
                console.log(err);
                reject(err);
            });
        });
    };
    GlobalRestServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["a" /* AlertController */]])
    ], GlobalRestServiceProvider);
    return GlobalRestServiceProvider;
}());

//# sourceMappingURL=global-rest-service.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AnalysisPageModule", function() { return AnalysisPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__analysis__ = __webpack_require__(545);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AnalysisPageModule = (function () {
    function AnalysisPageModule() {
    }
    AnalysisPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__analysis__["a" /* AnalysisPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__analysis__["a" /* AnalysisPage */]),
            ],
        })
    ], AnalysisPageModule);
    return AnalysisPageModule;
}());

//# sourceMappingURL=analysis.module.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InitialPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__ = __webpack_require__(116);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { LandPage } from './../land/land';




/**
 * Generated class for the InitialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InitialPage = (function () {
    function InitialPage(navCtrl, navParams, alertCtrl, storage, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.initialData = {
            id: 0,
            name: '',
            dob: '',
            dow: '',
            pen: '',
            savInv: '',
            exp: '',
            inc: '',
            ret: '',
            email: '',
            pword: '',
            cpword: '',
        };
        this.loaded = 1;
    }
    InitialPage.prototype.IsEmail = function (email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        }
        else {
            return true;
        }
    };
    InitialPage.prototype.ionViewWillEnter = function () {
        this.checkInit();
    };
    InitialPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InitialPage');
        // this.checkInit();
    };
    InitialPage.prototype.nextIndex = function () {
        var currentIndex = this.slides.getActiveIndex();
        this.slides.slideTo(currentIndex + 1, 500);
    };
    InitialPage.prototype.prevIndex = function () {
        var currentIndex = this.slides.getActiveIndex();
        this.slides.slideTo(currentIndex - 1, 500);
    };
    InitialPage.prototype.checkInit = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            if (initialData == null || initialData.id == 0) {
            }
            else {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
                // this.navCtrl.push(DashboardPage, {
                //   // val: 'Jeeje'
                //   val: initialData
                // })
            }
        });
    };
    InitialPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "id": 0,
            "name": this.initialData.name,
            "dob": this.initialData.dob,
            "dow": this.initialData.dow,
            "pen": this.initialData.pen,
            "savInv": this.initialData.savInv,
            "exp": this.initialData.exp,
            "inc": this.initialData.inc,
            "ret": this.initialData.ret,
            "email": this.initialData.email,
            "pword": this.initialData.pword,
        };
        console.log(this.apiData);
        // });this.initialData.namethis.initialData.name
    };
    InitialPage.prototype.logForm = function () {
        var _this = this;
        var alert;
        if (this.initialData.name.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Name is Required',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(1, 500);
            return;
        }
        if (this.initialData.dob.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Date of Birth is Required',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(2, 500);
            return;
        }
        if (this.initialData.dow.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us when you Started Working',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(3, 500);
            return;
        }
        if (this.initialData.pen.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Have in Pension',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(4, 500);
            return;
        }
        if (this.initialData.savInv.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Have in Savings and Investments',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(5, 500);
            return;
        }
        if (this.initialData.exp.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Spent Last Month',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(6, 500);
            return;
        }
        if (this.initialData.inc.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Tell us How Much You Made Last Month',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(7, 500);
            return;
        }
        if (this.initialData.ret.length < 1) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'When Would You Like to Retire',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(8, 500);
            return;
        }
        if (this.initialData.email.length < 1 || this.IsEmail(this.initialData.email) == false) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Enter a Valid Email',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(9, 500);
            return;
        }
        if (this.initialData.pword.length < 6) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Enter a Valid Password',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(10, 500);
            return;
        }
        if (this.initialData.pword !== this.initialData.cpword) {
            alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Passwords Do Not Match',
                buttons: ['Ok']
            });
            alert.present();
            this.slides.slideTo(10, 500);
            return;
        }
        this.setApiJson();
        this.loaded = 0;
        // ========================================================
        // this.apiData = this.initialData;
        console.log(this.apiData);
        this.grsp.addUser(this.apiData).then(function (data) {
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                _this.initialData.id = _this.apiRes.uId;
                _this.storage.set('initialData', _this.initialData);
                alert = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Saved User ' + _this.apiRes.uId,
                    buttons: ['Ok']
                });
                // alert.present();
                // ========================================
                alert = _this.alertCtrl.create({
                    title: 'Saved',
                    subTitle: 'Thank You ' + _this.initialData.name.toUpperCase() + ' Your Details Have Been Saved',
                    buttons: ['Ok']
                });
                alert.present();
                // this.navCtrl.push(DashboardPage, {
                //   // val: 'Jeeje'
                //   val: this.initialData
                // })
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__dashboard_dashboard__["a" /* DashboardPage */]);
                // ========================================
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Something went wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_1.present();
            }
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // ========================================================
        //  let alert = this.alertCtrl.create({
        //   title: 'Saved',
        //   subTitle: 'Thank You '+this.initialData.name+ ' Your details have been saved',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
        // this.navCtrl.push(LandPage, {
        //   // val: 'Jeeje'
        //   val: this.initialData
        // })
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Slides */])
    ], InitialPage.prototype, "slides", void 0);
    InitialPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-initial',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\initial\initial.html"*/'<!--\n  Generated template for the InitialPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="secondary">\n      <!-- <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button> -->\n      <ion-title>Welcome</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="tutorial-page">\n  <ion-slides >\n    <ion-slide>\n      <h1>Welcome, We have been expecting you</h1>\n    \n      <button ion-button small (click)="nextIndex()" >Next</button>\n\n\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <!-- <h1>Tell us your name</h1> -->\n      <ion-item>\n        <ion-label stacked>Tell us your name </ion-label>\n        <ion-input type="text" [(ngModel)]="initialData.name" name="name" ></ion-input>\n      </ion-item>\n      <!-- <hr> -->\n      <button ion-button small (click)="nextIndex()" >Next</button>\n\n\n    </ion-slide>\n\n    <ion-slide style="margin-top: 60px;">\n      <!-- <h1>When were you born</h1> -->\n      <ion-item>\n        <ion-label stacked>When were you born </ion-label>\n        <ion-input type="date" [(ngModel)]="initialData.dob" name="dob" ></ion-input>\n      </ion-item>\n      <!-- <hr> -->\n      <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n      <button ion-button small (click)="nextIndex()" >Next</button>\n\n\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n          <!-- <h1>When did you start work</h1> -->\n        <ion-label stacked>When did you start work </ion-label>\n        <ion-input type="date" [(ngModel)]="initialData.dow" name="dow" ></ion-input>\n      </ion-item>\n      <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n      <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) do you have in your pension account </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.pen" name="pen" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) was your savings and investment last month </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.savInv" name="savInv" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) did you spend in total last month </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.exp" name="exp" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>How much (Approx) did you make in total last month </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.inc" name="inc" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n    </ion-slide>\n    \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>At what age do you intend to retire </ion-label>\n            <ion-input type="number" [(ngModel)]="initialData.ret" name="ret" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n\n          <!-- <button ion-button small (click)="logForm()" >Submit</button> -->\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>Email Address </ion-label>\n            <ion-input type="email" [(ngModel)]="initialData.email" name="email" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n\n          <!-- <button ion-button small (click)="logForm()" >Submit</button> -->\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>Password (Minimum of 6 characters) </ion-label>\n            <ion-input type="password" [(ngModel)]="initialData.pword" name="pword" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <button ion-button small (click)="nextIndex()" >Next</button>\n\n          <!-- <button ion-button small (click)="logForm()" >Submit</button> -->\n    </ion-slide>\n   \n    <ion-slide style="margin-top: 60px;">\n      <ion-item>\n            <ion-label stacked>Confirm Password (Be sure) </ion-label>\n            <ion-input type="password" [(ngModel)]="initialData.cpword" name="cpword" ></ion-input>\n          </ion-item>\n          <button ion-button color="primary" small (click)="prevIndex()" >Previous</button>\n          <!-- <button ion-button small (click)="nextIndex()" >Next</button> -->\n\n          <button ion-button small (click)="logForm()" >Submit</button>\n    </ion-slide>\n\n  </ion-slides>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\initial\initial.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], InitialPage);
    return InitialPage;
}());

//# sourceMappingURL=initial.js.map

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__initial_initial__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var SignupPage = (function () {
    function SignupPage(navCtrl, grsp, alertCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.grsp = grsp;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.navParams = navParams;
        // @ViewChild(Nav) nav: Nav;
        // rootPage: any;
        this.initialData = {
            id: 0,
            name: '',
            dob: '',
            dow: '',
            pen: '',
            savInv: '',
            exp: '',
            inc: '',
            ret: '',
            email: '',
            pword: '',
            cpword: '',
        };
        this.screen = "signin";
        this.login = {
            email: '',
            pword: '',
        };
        this.loaded = 1;
    }
    SignupPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SignupPage');
    };
    SignupPage.prototype.setApiJson = function () {
        this.apiData = {
            "email": this.login.email,
            "pword": this.login.pword
        };
    };
    SignupPage.prototype.loginCheck = function () {
        var _this = this;
        if (this.login.email == '' || this.login.pword == '') {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Please Enter Correct Login Details',
                buttons: ['Dismiss']
            });
            alert_1.present();
            return;
        }
        this.setApiJson();
        this.loaded = 0;
        console.log(this.apiData);
        this.grsp.login(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var loginRes = JSON.parse(_this.apiRes.loginRes)[0];
                console.log(loginRes);
                _this.initialData = {
                    id: loginRes.user_id,
                    name: loginRes.user_name,
                    dob: loginRes.user_dob,
                    dow: loginRes.user_dow,
                    pen: loginRes.user_pension,
                    savInv: loginRes.user_savInv,
                    exp: loginRes.user_lastMonthExp,
                    inc: loginRes.user_lastMonthInc,
                    ret: loginRes.user_retAge,
                    email: loginRes.user_email,
                    pword: loginRes.user_pword,
                    cpword: loginRes.user_pword,
                };
                _this.storage.set('initialData', _this.initialData);
                console.log(_this.initialData);
                var alert_2 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Login Successful',
                    buttons: ['Dismiss']
                });
                alert_2.present();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__dashboard_dashboard__["a" /* DashboardPage */]);
                // this.activePage = { title: 'Dashboard', component: DashboardPage };
            }
            else {
                var alert_3 = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Something is wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_3.present();
            }
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // =============================================================
        // this.navCtrl.setRoot(DashboardPage);
        // this.activePage = { title: 'Dashboard', component: DashboardPage };
        // this.navCtrl.push(MyApp, {
        //   val: 'I am View Ana-lytics',
        //   page:'Dashboard'
        // })
    };
    SignupPage.prototype.signUp = function () {
        // this.navCtrl.setRoot(InitialPage);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_0__initial_initial__["a" /* InitialPage */], {});
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\signup\signup.html"*/'<ion-content [ngSwitch]="screen">\n\n    <div class="logo-container" text-center>\n        <img src="assets/imgs/logo.png" alt="Logo">\n\n        <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n            <ion-segment-button value="signin">\n                Sign In\n            </ion-segment-button>\n            <!-- <ion-segment-button value="signup">\n                Sign Up\n            </ion-segment-button> -->\n        </ion-segment>\n    </div>\n\n\n    <div class="form-container" *ngSwitchCase="\'signin\'">\n        <ion-list>\n                <ion-item *ngIf="loaded==0">\n                  <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n               \n                </ion-item>\n                <ion-item>\n                    <ion-input placeholder="Email"  type="email" [(ngModel)]="login.email" name="email" ></ion-input>\n                </ion-item>\n            <!-- <ion-item>\n                <ion-input placeholder="Phone" type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Email" type="email"></ion-input>\n            </ion-item> -->\n            <ion-item>\n                <ion-input placeholder="Password" type="password" [(ngModel)]="login.pword" name="pword" ></ion-input>\n            </ion-item>\n\n            <ion-item text-center no-lines>                \n                No Account? <a  color="primary" (click)="signUp()" >Sign Up</a>\n            </ion-item>\n                \n            </ion-list>\n        <br>\n        <button ion-button full color="primary" (click)="loginCheck()" >Sign In</button>\n\n\n\n\n        \n\n\n\n\n    </div>\n    <!-- <div class="form-container" *ngSwitchCase="\'signup\'">\n        <ion-list>\n            <ion-item>\n                <ion-input placeholder="Username" type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Phone" type="text"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Email" type="email"></ion-input>\n            </ion-item>\n            <ion-item>\n                <ion-input placeholder="Password" type="password"></ion-input>\n            </ion-item>\n        </ion-list>\n\n        <button ion-button full color="primary">Sign Up</button>\n\n    </div> -->\n</ion-content>\n<!-- <ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav> -->\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavParams */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 349:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankingPageModule", function() { return BankingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__banking__ = __webpack_require__(548);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BankingPageModule = (function () {
    function BankingPageModule() {
    }
    BankingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__banking__["a" /* BankingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__banking__["a" /* BankingPage */]),
            ],
        })
    ], BankingPageModule);
    return BankingPageModule;
}());

//# sourceMappingURL=banking.module.js.map

/***/ }),

/***/ 350:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CardsPageModule", function() { return CardsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cards__ = __webpack_require__(549);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CardsPageModule = (function () {
    function CardsPageModule() {
    }
    CardsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__cards__["a" /* CardsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__cards__["a" /* CardsPage */]),
            ],
        })
    ], CardsPageModule);
    return CardsPageModule;
}());

//# sourceMappingURL=cards.module.js.map

/***/ }),

/***/ 351:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BudgetPageModule", function() { return BudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__budget__ = __webpack_require__(550);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var BudgetPageModule = (function () {
    function BudgetPageModule() {
    }
    BudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__budget__["a" /* BudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__budget__["a" /* BudgetPage */]),
            ],
        })
    ], BudgetPageModule);
    return BudgetPageModule;
}());

//# sourceMappingURL=budget.module.js.map

/***/ }),

/***/ 352:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardPageModule", function() { return DashboardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__dashboard__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_components_module__ = __webpack_require__(353);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DashboardPageModule = (function () {
    function DashboardPageModule() {
    }
    DashboardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__dashboard__["a" /* DashboardPage */]),
                __WEBPACK_IMPORTED_MODULE_3__components_components_module__["a" /* ComponentsModule */],
            ],
        })
    ], DashboardPageModule);
    return DashboardPageModule;
}());

//# sourceMappingURL=dashboard.module.js.map

/***/ }),

/***/ 353:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__ = __webpack_require__(551);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ComponentsModule = (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */],
                __WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */],
                __WEBPACK_IMPORTED_MODULE_1__progress_bar_progress_bar__["a" /* ProgressBarComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EdittransactionsPageModule", function() { return EdittransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edittransactions__ = __webpack_require__(552);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EdittransactionsPageModule = (function () {
    function EdittransactionsPageModule() {
    }
    EdittransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__edittransactions__["a" /* EdittransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__edittransactions__["a" /* EdittransactionsPage */]),
            ],
        })
    ], EdittransactionsPageModule);
    return EdittransactionsPageModule;
}());

//# sourceMappingURL=edittransactions.module.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FinInfoPageModule", function() { return FinInfoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fin_info__ = __webpack_require__(553);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FinInfoPageModule = (function () {
    function FinInfoPageModule() {
    }
    FinInfoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__fin_info__["a" /* FinInfoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__fin_info__["a" /* FinInfoPage */]),
            ],
        })
    ], FinInfoPageModule);
    return FinInfoPageModule;
}());

//# sourceMappingURL=fin-info.module.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FavoritesPageModule", function() { return FavoritesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__favorites__ = __webpack_require__(554);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FavoritesPageModule = (function () {
    function FavoritesPageModule() {
    }
    FavoritesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__favorites__["a" /* FavoritesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__favorites__["a" /* FavoritesPage */]),
            ],
        })
    ], FavoritesPageModule);
    return FavoritesPageModule;
}());

//# sourceMappingURL=favorites.module.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ForexCardPageModule", function() { return ForexCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forex_card__ = __webpack_require__(555);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ForexCardPageModule = (function () {
    function ForexCardPageModule() {
    }
    ForexCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__forex_card__["a" /* ForexCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__forex_card__["a" /* ForexCardPage */]),
            ],
        })
    ], ForexCardPageModule);
    return ForexCardPageModule;
}());

//# sourceMappingURL=forex-card.module.js.map

/***/ }),

/***/ 358:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitialPageModule", function() { return InitialPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__initial__ = __webpack_require__(221);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InitialPageModule = (function () {
    function InitialPageModule() {
    }
    InitialPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__initial__["a" /* InitialPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__initial__["a" /* InitialPage */]),
            ],
        })
    ], InitialPageModule);
    return InitialPageModule;
}());

//# sourceMappingURL=initial.module.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvestmentPageModule", function() { return InvestmentPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__investment__ = __webpack_require__(556);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InvestmentPageModule = (function () {
    function InvestmentPageModule() {
    }
    InvestmentPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__investment__["a" /* InvestmentPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__investment__["a" /* InvestmentPage */]),
            ],
        })
    ], InvestmentPageModule);
    return InvestmentPageModule;
}());

//# sourceMappingURL=investment.module.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaystackPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__ = __webpack_require__(361);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { InAppBrowser } from 'ionic-native';

var PaystackPage = (function () {
    function PaystackPage(navCtrl, alertCtrl, navParams, grsp, loading, platform, iab, viewCtrl) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.grsp = grsp;
        this.loading = loading;
        this.platform = platform;
        this.iab = iab;
        this.viewCtrl = viewCtrl;
    }
    PaystackPage.prototype.ngOnInit = function () {
        this.price = parseInt(this.navParams.get('price'));
        // this.data= parseInt(this.navParams.get('data'));
        console.log(this.price);
        console.log(this.navParams.get('data'));
        // this.price= 500;
        this.chargeAmount = this.price * 100;
    };
    PaystackPage.prototype.ChargeCard = function () {
        var _this = this;
        var card = this.card_number.value;
        var month = this.expiryMonth.value;
        var cvc = this.cvc.value;
        var year = this.expiryYear.value;
        var amount = this.chargeAmount;
        var email = this.emailValue;
        // let apiData = this.data;
        console.log(card);
        console.log(month);
        console.log(cvc);
        console.log(year);
        console.log(amount);
        console.log(email);
        var loader = this.loading.create({
            content: 'Processing Charge…'
        });
        loader.present();
        this.platform.ready().then(function () {
            if (_this.platform.is('cordova')) {
                // Now safe to use device APIs
                window.window.PaystackPlugin.chargeCard(function (resp) {
                    loader.dismiss();
                    //this.pop.showPayMentAlert("Payment Was Successful", "We will Now Refund Your Balance");
                    console.log('charge successful: ', resp);
                    // console.log(apiData);
                    alert('Payment Was Successful');
                    // this.navCtrl.push("InvestmentPage", {res:"success"} );
                    _this.navCtrl.push("PayConfPage", { res: "success", amt: (amount / 100) });
                }, function (resp) {
                    loader.dismiss();
                    console.log(resp);
                    alert('We Encountered An Error While Charging Your Card');
                    // this.navCtrl.push("InvestmentPage", {res:"failed"} );
                }, {
                    cardNumber: card,
                    expiryMonth: month,
                    expiryYear: year,
                    cvc: cvc,
                    email: email,
                    amountInKobo: amount,
                });
            }
            else {
            }
        });
    };
    PaystackPage.prototype.whatsPaystack = function () {
        var _this = this;
        this.platform.ready().then(function () {
            //  let browser = new InAppBrowser("https://paystack.com/what-is-paystack",'_blank');
            var browser = _this.iab.create("https://paystack.com/what-is-paystack", '_blank');
        });
    };
    PaystackPage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("email_add"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "email_add", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("card_number"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "card_number", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("expiryMonth"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "expiryMonth", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("expiryYear"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "expiryYear", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("cvc"),
        __metadata("design:type", Object)
    ], PaystackPage.prototype, "cvc", void 0);
    PaystackPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-paystack',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/'<!--\n  Generated template for the PaystackPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n    \n      <ion-title>Make Payment</ion-title>\n    \n    </ion-navbar>\n  \n  </ion-header>\n  \n  <ion-content  >\n  \n    <div class="image" text-center><img  src="assets/imgs/paystack_logo.png" /></div>\n    \n    <!-- <div class="amount">NGN {{data | number:\'.2\'}}</div> -->\n    <div class="amount">NGN {{data}}</div>\n    \n    <div class="input-area">\n    \n    <ion-list >\n    \n    <ion-item >\n    \n    <ion-input type="text" placeholder="Email Address"  #email_add [(ngModel)]="emailValue"></ion-input>\n    \n    </ion-item>\n    \n    <div class="cardnumber">\n    \n    <ion-label >CARD NUMBER</ion-label>\n    \n    <ion-item >\n    \n    <ion-input type="tel" placeholder="0000000000000000" name="card_number" #card_number [(ngModel)]="cardNumberValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    <ion-grid padding>\n    \n    <ion-row >\n    \n    <ion-col col-6>Expiry date</ion-col>\n    \n    <ion-col col-2> </ion-col>\n    \n    <ion-col col-4> CVV</ion-col>\n    \n    </ion-row>\n    \n    <ion-row no-lines align-items-start>\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel" placeholder="MM" name="expiryMonth" #expiryMonth [(ngModel)]="expiryMonthValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col >\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel"  placeholder="YY" name="expiryYear" #expiryYear [(ngModel)]="expiryYearValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col>\n    \n    <ion-col col-2>\n    \n    </ion-col>\n    \n    <ion-col col-4>\n    \n    <div class="cvv">\n    \n    <ion-item>\n    \n    <ion-input type="tel" placeholder="123" name="cvc" #cvc [(ngModel)]="cvcValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    </ion-col>\n    \n    </ion-row>\n    \n    </ion-grid>\n    \n    <div class="pay-button">\n    \n    <!-- <button ion-button full (click)="ChargeCard()">PAY NGN {{data | number:\'.2\'}} </button> -->\n    <button ion-button full (click)="ChargeCard()">PAY NGN {{price}} </button>\n    \n    </div>\n    \n    </ion-list>\n    \n    </div>\n    \n    <div class="footer">\n    \n    <div class="secured">\n    \n    <ion-icon name="lock" item-start></ion-icon>\n    \n    SECURED BY PAYSTACK\n    \n    </div>\n    \n    <div class="whatspaystack">\n    \n    <button ion-button outline  small round class="app-font-25" (click)="whatsPaystack()"> WHAT IS PAYSTACK?</button>\n    \n    </div>\n    \n    </div>\n    \n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */]])
    ], PaystackPage);
    return PaystackPage;
}());

// =======================================================
// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';
// /**
//  * Generated class for the PaystackPage page.
//  *
//  * See https://ionicframework.com/docs/components/#navigation for more info on
//  * Ionic pages and navigation.
//  */
// @IonicPage()
// @Component({
//   selector: 'page-paystack',
//template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/'<!--\n  Generated template for the PaystackPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n    \n      <ion-title>Make Payment</ion-title>\n    \n    </ion-navbar>\n  \n  </ion-header>\n  \n  <ion-content  >\n  \n    <div class="image" text-center><img  src="assets/imgs/paystack_logo.png" /></div>\n    \n    <!-- <div class="amount">NGN {{data | number:\'.2\'}}</div> -->\n    <div class="amount">NGN {{data}}</div>\n    \n    <div class="input-area">\n    \n    <ion-list >\n    \n    <ion-item >\n    \n    <ion-input type="text" placeholder="Email Address"  #email_add [(ngModel)]="emailValue"></ion-input>\n    \n    </ion-item>\n    \n    <div class="cardnumber">\n    \n    <ion-label >CARD NUMBER</ion-label>\n    \n    <ion-item >\n    \n    <ion-input type="tel" placeholder="0000000000000000" name="card_number" #card_number [(ngModel)]="cardNumberValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    <ion-grid padding>\n    \n    <ion-row >\n    \n    <ion-col col-6>Expiry date</ion-col>\n    \n    <ion-col col-2> </ion-col>\n    \n    <ion-col col-4> CVV</ion-col>\n    \n    </ion-row>\n    \n    <ion-row no-lines align-items-start>\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel" placeholder="MM" name="expiryMonth" #expiryMonth [(ngModel)]="expiryMonthValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col >\n    \n    <ion-col col-3>\n    \n    <ion-item no-padding>\n    \n    <ion-input type="tel"  placeholder="YY" name="expiryYear" #expiryYear [(ngModel)]="expiryYearValue"></ion-input>\n    \n    </ion-item>\n    \n    </ion-col>\n    \n    <ion-col col-2>\n    \n    </ion-col>\n    \n    <ion-col col-4>\n    \n    <div class="cvv">\n    \n    <ion-item>\n    \n    <ion-input type="tel" placeholder="123" name="cvc" #cvc [(ngModel)]="cvcValue"></ion-input>\n    \n    </ion-item>\n    \n    </div>\n    \n    </ion-col>\n    \n    </ion-row>\n    \n    </ion-grid>\n    \n    <div class="pay-button">\n    \n    <!-- <button ion-button full (click)="ChargeCard()">PAY NGN {{data | number:\'.2\'}} </button> -->\n    <button ion-button full (click)="ChargeCard()">PAY NGN {{price}} </button>\n    \n    </div>\n    \n    </ion-list>\n    \n    </div>\n    \n    <div class="footer">\n    \n    <div class="secured">\n    \n    <ion-icon name="lock" item-start></ion-icon>\n    \n    SECURED BY PAYSTACK\n    \n    </div>\n    \n    <div class="whatspaystack">\n    \n    <button ion-button outline  small round class="app-font-25" (click)="whatsPaystack()"> WHAT IS PAYSTACK?</button>\n    \n    </div>\n    \n    </div>\n    \n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\paystack\paystack.html"*/,
// })
// export class PaystackPage {
//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }
//   ionViewDidLoad() {
//     console.log('ionViewDidLoad PaystackPage');
//   }
// }
//# sourceMappingURL=paystack.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManagebudgetPageModule", function() { return ManagebudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__managebudget__ = __webpack_require__(839);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ManagebudgetPageModule = (function () {
    function ManagebudgetPageModule() {
    }
    ManagebudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__managebudget__["a" /* ManagebudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__managebudget__["a" /* ManagebudgetPage */]),
            ],
        })
    ], ManagebudgetPageModule);
    return ManagebudgetPageModule;
}());

//# sourceMappingURL=managebudget.module.js.map

/***/ }),

/***/ 454:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuTwoPageModule", function() { return MenuTwoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_two__ = __webpack_require__(840);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MenuTwoPageModule = (function () {
    function MenuTwoPageModule() {
    }
    MenuTwoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__menu_two__["a" /* MenuTwoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__menu_two__["a" /* MenuTwoPage */]),
            ],
        })
    ], MenuTwoPageModule);
    return MenuTwoPageModule;
}());

//# sourceMappingURL=menu-two.module.js.map

/***/ }),

/***/ 455:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NairaCardPageModule", function() { return NairaCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__naira_card__ = __webpack_require__(841);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var NairaCardPageModule = (function () {
    function NairaCardPageModule() {
    }
    NairaCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__naira_card__["a" /* NairaCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__naira_card__["a" /* NairaCardPage */]),
            ],
        })
    ], NairaCardPageModule);
    return NairaCardPageModule;
}());

//# sourceMappingURL=naira-card.module.js.map

/***/ }),

/***/ 456:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "OrderDetailsPageModule", function() { return OrderDetailsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__order_details__ = __webpack_require__(842);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var OrderDetailsPageModule = (function () {
    function OrderDetailsPageModule() {
    }
    OrderDetailsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__order_details__["a" /* OrderDetailsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__order_details__["a" /* OrderDetailsPage */]),
            ],
        })
    ], OrderDetailsPageModule);
    return OrderDetailsPageModule;
}());

//# sourceMappingURL=order-details.module.js.map

/***/ }),

/***/ 457:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuOnePageModule", function() { return MenuOnePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_one__ = __webpack_require__(843);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MenuOnePageModule = (function () {
    function MenuOnePageModule() {
    }
    MenuOnePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__menu_one__["a" /* MenuOnePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__menu_one__["a" /* MenuOnePage */]),
            ],
        })
    ], MenuOnePageModule);
    return MenuOnePageModule;
}());

//# sourceMappingURL=menu-one.module.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilePageModule", function() { return ProfilePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__profile__ = __webpack_require__(844);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProfilePageModule = (function () {
    function ProfilePageModule() {
    }
    ProfilePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__profile__["a" /* ProfilePage */]),
            ],
        })
    ], ProfilePageModule);
    return ProfilePageModule;
}());

//# sourceMappingURL=profile.module.js.map

/***/ }),

/***/ 459:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrepaidCardPageModule", function() { return PrepaidCardPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__prepaid_card__ = __webpack_require__(845);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PrepaidCardPageModule = (function () {
    function PrepaidCardPageModule() {
    }
    PrepaidCardPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__prepaid_card__["a" /* PrepaidCardPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__prepaid_card__["a" /* PrepaidCardPage */]),
            ],
        })
    ], PrepaidCardPageModule);
    return PrepaidCardPageModule;
}());

//# sourceMappingURL=prepaid-card.module.js.map

/***/ }),

/***/ 460:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingPageModule", function() { return SettingPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__setting__ = __webpack_require__(846);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SettingPageModule = (function () {
    function SettingPageModule() {
    }
    SettingPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__setting__["a" /* SettingPage */]),
            ],
        })
    ], SettingPageModule);
    return SettingPageModule;
}());

//# sourceMappingURL=setting.module.js.map

/***/ }),

/***/ 461:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SignupPageModule = (function () {
    function SignupPageModule() {
    }
    SignupPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__signup__["a" /* SignupPage */]),
            ],
        })
    ], SignupPageModule);
    return SignupPageModule;
}());

//# sourceMappingURL=signup.module.js.map

/***/ }),

/***/ 462:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashTwoPageModule", function() { return SplashTwoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__splash_two__ = __webpack_require__(847);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SplashTwoPageModule = (function () {
    function SplashTwoPageModule() {
    }
    SplashTwoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__splash_two__["a" /* SplashTwoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__splash_two__["a" /* SplashTwoPage */]),
            ],
        })
    ], SplashTwoPageModule);
    return SplashTwoPageModule;
}());

//# sourceMappingURL=splash-two.module.js.map

/***/ }),

/***/ 463:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubMenuOnePageModule", function() { return SubMenuOnePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_menu_one__ = __webpack_require__(848);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubMenuOnePageModule = (function () {
    function SubMenuOnePageModule() {
    }
    SubMenuOnePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sub_menu_one__["a" /* SubMenuOnePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sub_menu_one__["a" /* SubMenuOnePage */]),
            ],
        })
    ], SubMenuOnePageModule);
    return SubMenuOnePageModule;
}());

//# sourceMappingURL=sub-menu-one.module.js.map

/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SplashOnePageModule", function() { return SplashOnePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__splash_one__ = __webpack_require__(849);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SplashOnePageModule = (function () {
    function SplashOnePageModule() {
    }
    SplashOnePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__splash_one__["a" /* SplashOnePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__splash_one__["a" /* SplashOnePage */]),
            ],
        })
    ], SplashOnePageModule);
    return SplashOnePageModule;
}());

//# sourceMappingURL=splash-one.module.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewanalyticsPageModule", function() { return ViewanalyticsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__viewanalytics__ = __webpack_require__(850);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewanalyticsPageModule = (function () {
    function ViewanalyticsPageModule() {
    }
    ViewanalyticsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__viewanalytics__["a" /* ViewanalyticsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__viewanalytics__["a" /* ViewanalyticsPage */]),
            ],
        })
    ], ViewanalyticsPageModule);
    return ViewanalyticsPageModule;
}());

//# sourceMappingURL=viewanalytics.module.js.map

/***/ }),

/***/ 466:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewbudgetPageModule", function() { return ViewbudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__viewbudget__ = __webpack_require__(851);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewbudgetPageModule = (function () {
    function ViewbudgetPageModule() {
    }
    ViewbudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__viewbudget__["a" /* ViewbudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__viewbudget__["a" /* ViewbudgetPage */]),
            ],
        })
    ], ViewbudgetPageModule);
    return ViewbudgetPageModule;
}());

//# sourceMappingURL=viewbudget.module.js.map

/***/ }),

/***/ 467:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewtransactionsPageModule", function() { return ViewtransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__viewtransactions__ = __webpack_require__(852);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewtransactionsPageModule = (function () {
    function ViewtransactionsPageModule() {
    }
    ViewtransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__viewtransactions__["a" /* ViewtransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__viewtransactions__["a" /* ViewtransactionsPage */]),
            ],
        })
    ], ViewtransactionsPageModule);
    return ViewtransactionsPageModule;
}());

//# sourceMappingURL=viewtransactions.module.js.map

/***/ }),

/***/ 468:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkthroughPageModule", function() { return WalkthroughPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__walkthrough__ = __webpack_require__(853);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var WalkthroughPageModule = (function () {
    function WalkthroughPageModule() {
    }
    WalkthroughPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__walkthrough__["a" /* WalkthroughPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__walkthrough__["a" /* WalkthroughPage */]),
            ],
        })
    ], WalkthroughPageModule);
    return WalkthroughPageModule;
}());

//# sourceMappingURL=walkthrough.module.js.map

/***/ }),

/***/ 469:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SubMenuTwoPageModule", function() { return SubMenuTwoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sub_menu_two__ = __webpack_require__(854);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SubMenuTwoPageModule = (function () {
    function SubMenuTwoPageModule() {
    }
    SubMenuTwoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sub_menu_two__["a" /* SubMenuTwoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sub_menu_two__["a" /* SubMenuTwoPage */]),
            ],
        })
    ], SubMenuTwoPageModule);
    return SubMenuTwoPageModule;
}());

//# sourceMappingURL=sub-menu-two.module.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HomePage = (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\home\home.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    </ion-navbar>\n</ion-header>\n\n<ion-content text-center>\n    <img src="assets/imgs/logo-gray.png" alt="Logo">\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(517);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(521);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 521:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_credit_card_credit_card_module__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_prepaid_card_prepaid_card_module__ = __webpack_require__(459);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_forex_card_forex_card_module__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_naira_card_naira_card_module__ = __webpack_require__(455);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_cards_cards_module__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_paystack_paystack_module__ = __webpack_require__(873);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__(874);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_home_home__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_splash_one_splash_one_module__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_splash_two_splash_two_module__ = __webpack_require__(462);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_signup_signup_module__ = __webpack_require__(461);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_walkthrough_walkthrough_module__ = __webpack_require__(468);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_menu_one_menu_one_module__ = __webpack_require__(457);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_menu_two_menu_two_module__ = __webpack_require__(454);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_sub_menu_one_sub_menu_one_module__ = __webpack_require__(463);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_sub_menu_two_sub_menu_two_module__ = __webpack_require__(469);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_order_details_order_details_module__ = __webpack_require__(456);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_favorites_favorites_module__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_profile_profile_module__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_setting_setting_module__ = __webpack_require__(460);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_chooser__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__ = __webpack_require__(515);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_http__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_initial_initial_module__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_dashboard_dashboard_module__ = __webpack_require__(352);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_analysis_analysis_module__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_viewanalytics_viewanalytics_module__ = __webpack_require__(465);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_addtransactions_addtransactions_module__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_viewtransactions_viewtransactions_module__ = __webpack_require__(467);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_edittransactions_edittransactions_module__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_investment_investment_module__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__components_components_module__ = __webpack_require__(353);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_budget_budget_module__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_managebudget_managebudget_module__ = __webpack_require__(453);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_viewbudget_viewbudget_module__ = __webpack_require__(466);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_fin_info_fin_info_module__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_banking_banking_module__ = __webpack_require__(349);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























// import { AndroidPermissions } from '@ionic-native/android-permissions';








// import { ViewanalyticsPage } from '../pages/viewanalytics/viewanalytics';





// =======================================================================================================
// import { ProgressBarComponent } from '../components/progress-bar/progress-bar';






// =======================================================================================================
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_8__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */], {
                    mode: 'ios',
                    backButtonText: '',
                }, {
                    links: [
                        { loadChildren: '../pages/_projectsbudget/projectsbudget.module#ProjectsbudgetPageModule', name: 'ProjectsbudgetPage', segment: 'projectsbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addtransactions/addtransactions.module#AddtransactionsPageModule', name: 'AddtransactionsPage', segment: 'addtransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/analysis/analysis.module#AnalysisPageModule', name: 'AnalysisPage', segment: 'analysis', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/banking/banking.module#BankingPageModule', name: 'BankingPage', segment: 'banking', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cards/cards.module#CardsPageModule', name: 'CardsPage', segment: 'cards', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/credit-card/credit-card.module#CreditCardPageModule', name: 'CreditCardPage', segment: 'credit-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/budget/budget.module#BudgetPageModule', name: 'BudgetPage', segment: 'budget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/current/current.module#CurrentPageModule', name: 'CurrentPage', segment: 'current', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dashboard/dashboard.module#DashboardPageModule', name: 'DashboardPage', segment: 'dashboard', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/debtbudget/debtbudget.module#DebtbudgetPageModule', name: 'DebtbudgetPage', segment: 'debtbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/edittransactions/edittransactions.module#EdittransactionsPageModule', name: 'EdittransactionsPage', segment: 'edittransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expense/expense.module#ExpensePageModule', name: 'ExpensePage', segment: 'expense', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/expensebudget/expensebudget.module#ExpensebudgetPageModule', name: 'ExpensebudgetPage', segment: 'expensebudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fin-info/fin-info.module#FinInfoPageModule', name: 'FinInfoPage', segment: 'fin-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/favorites/favorites.module#FavoritesPageModule', name: 'FavoritesPage', segment: 'favorites', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/fixed-deposit/fixed-deposit.module#FixedDepositPageModule', name: 'FixedDepositPage', segment: 'fixed-deposit', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/foreign/foreign.module#ForeignPageModule', name: 'ForeignPage', segment: 'foreign', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forex-card/forex-card.module#ForexCardPageModule', name: 'ForexCardPage', segment: 'forex-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/incomebudget/incomebudget.module#IncomebudgetPageModule', name: 'IncomebudgetPage', segment: 'incomebudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/initial/initial.module#InitialPageModule', name: 'InitialPage', segment: 'initial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/income/income.module#IncomePageModule', name: 'IncomePage', segment: 'income', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/investment/investment.module#InvestmentPageModule', name: 'InvestmentPage', segment: 'investment', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/liabilities/liabilities.module#LiabilitiesPageModule', name: 'LiabilitiesPage', segment: 'liabilities', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/liabilitiesbudget/liabilitiesbudget.module#LiabilitiesbudgetPageModule', name: 'LiabilitiesbudgetPage', segment: 'liabilitiesbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/managebudget/managebudget.module#ManagebudgetPageModule', name: 'ManagebudgetPage', segment: 'managebudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/loadsmstransactions/loadsmstransactions.module#LoadsmstransactionsPageModule', name: 'LoadsmstransactionsPage', segment: 'loadsmstransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu-two/menu-two.module#MenuTwoPageModule', name: 'MenuTwoPage', segment: 'menu-two', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/naira-card/naira-card.module#NairaCardPageModule', name: 'NairaCardPage', segment: 'naira-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/order-details/order-details.module#OrderDetailsPageModule', name: 'OrderDetailsPage', segment: 'order-details', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/pay-conf/pay-conf.module#PayConfPageModule', name: 'PayConfPage', segment: 'pay-conf', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu-one/menu-one.module#MenuOnePageModule', name: 'MenuOnePage', segment: 'menu-one', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/projectbudget/projectbudget.module#ProjectbudgetPageModule', name: 'ProjectbudgetPage', segment: 'projectbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/projects/projects.module#ProjectsPageModule', name: 'ProjectsPage', segment: 'projects', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sav-invbudget/sav-invbudget.module#SavInvbudgetPageModule', name: 'SavInvbudgetPage', segment: 'sav-invbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/savings/savings.module#SavingsPageModule', name: 'SavingsPage', segment: 'savings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/prepaid-card/prepaid-card.module#PrepaidCardPageModule', name: 'PrepaidCardPage', segment: 'prepaid-card', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/setting/setting.module#SettingPageModule', name: 'SettingPage', segment: 'setting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sav-inv/sav-inv.module#SavInvPageModule', name: 'SavInvPage', segment: 'sav-inv', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/splash-two/splash-two.module#SplashTwoPageModule', name: 'SplashTwoPage', segment: 'splash-two', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sub-menu-one/sub-menu-one.module#SubMenuOnePageModule', name: 'SubMenuOnePage', segment: 'sub-menu-one', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/splash-one/splash-one.module#SplashOnePageModule', name: 'SplashOnePage', segment: 'splash-one', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewanalytics/viewanalytics.module#ViewanalyticsPageModule', name: 'ViewanalyticsPage', segment: 'viewanalytics', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewbudget/viewbudget.module#ViewbudgetPageModule', name: 'ViewbudgetPage', segment: 'viewbudget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewtransactions/viewtransactions.module#ViewtransactionsPageModule', name: 'ViewtransactionsPage', segment: 'viewtransactions', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/walkthrough/walkthrough.module#WalkthroughPageModule', name: 'WalkthroughPage', segment: 'walkthrough', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sub-menu-two/sub-menu-two.module#SubMenuTwoPageModule', name: 'SubMenuTwoPage', segment: 'sub-menu-two', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_33__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_14__pages_splash_one_splash_one_module__["SplashOnePageModule"],
                __WEBPACK_IMPORTED_MODULE_15__pages_splash_two_splash_two_module__["SplashTwoPageModule"],
                __WEBPACK_IMPORTED_MODULE_16__pages_signup_signup_module__["SignupPageModule"],
                __WEBPACK_IMPORTED_MODULE_17__pages_walkthrough_walkthrough_module__["WalkthroughPageModule"],
                __WEBPACK_IMPORTED_MODULE_18__pages_menu_one_menu_one_module__["MenuOnePageModule"],
                __WEBPACK_IMPORTED_MODULE_19__pages_menu_two_menu_two_module__["MenuTwoPageModule"],
                __WEBPACK_IMPORTED_MODULE_20__pages_sub_menu_one_sub_menu_one_module__["SubMenuOnePageModule"],
                __WEBPACK_IMPORTED_MODULE_21__pages_sub_menu_two_sub_menu_two_module__["SubMenuTwoPageModule"],
                __WEBPACK_IMPORTED_MODULE_22__pages_order_details_order_details_module__["OrderDetailsPageModule"],
                __WEBPACK_IMPORTED_MODULE_23__pages_favorites_favorites_module__["FavoritesPageModule"],
                __WEBPACK_IMPORTED_MODULE_24__pages_profile_profile_module__["ProfilePageModule"],
                __WEBPACK_IMPORTED_MODULE_25__pages_setting_setting_module__["SettingPageModule"],
                __WEBPACK_IMPORTED_MODULE_31__pages_initial_initial_module__["InitialPageModule"],
                __WEBPACK_IMPORTED_MODULE_32__pages_dashboard_dashboard_module__["DashboardPageModule"],
                __WEBPACK_IMPORTED_MODULE_34__pages_analysis_analysis_module__["AnalysisPageModule"],
                __WEBPACK_IMPORTED_MODULE_35__pages_viewanalytics_viewanalytics_module__["ViewanalyticsPageModule"],
                __WEBPACK_IMPORTED_MODULE_36__pages_addtransactions_addtransactions_module__["AddtransactionsPageModule"],
                __WEBPACK_IMPORTED_MODULE_37__pages_viewtransactions_viewtransactions_module__["ViewtransactionsPageModule"],
                __WEBPACK_IMPORTED_MODULE_38__pages_edittransactions_edittransactions_module__["EdittransactionsPageModule"],
                __WEBPACK_IMPORTED_MODULE_39__pages_investment_investment_module__["InvestmentPageModule"],
                __WEBPACK_IMPORTED_MODULE_41__pages_budget_budget_module__["BudgetPageModule"],
                __WEBPACK_IMPORTED_MODULE_42__pages_managebudget_managebudget_module__["ManagebudgetPageModule"],
                __WEBPACK_IMPORTED_MODULE_43__pages_viewbudget_viewbudget_module__["ViewbudgetPageModule"],
                __WEBPACK_IMPORTED_MODULE_40__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_30__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_6__pages_paystack_paystack_module__["a" /* PaystackPageModule */],
                __WEBPACK_IMPORTED_MODULE_44__pages_fin_info_fin_info_module__["FinInfoPageModule"],
                __WEBPACK_IMPORTED_MODULE_45__pages_banking_banking_module__["BankingPageModule"],
                __WEBPACK_IMPORTED_MODULE_4__pages_cards_cards_module__["CardsPageModule"],
                __WEBPACK_IMPORTED_MODULE_3__pages_naira_card_naira_card_module__["NairaCardPageModule"],
                __WEBPACK_IMPORTED_MODULE_2__pages_forex_card_forex_card_module__["ForexCardPageModule"],
                __WEBPACK_IMPORTED_MODULE_1__pages_prepaid_card_prepaid_card_module__["PrepaidCardPageModule"],
                __WEBPACK_IMPORTED_MODULE_0__pages_credit_card_credit_card_module__["CreditCardPageModule"]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_13__pages_home_home__["a" /* HomePage */],
            ],
            providers: [
                // AndroidPermissions,
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_status_bar__["a" /* StatusBar */],
                // NavParams,
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_file_chooser__["a" /* FileChooser */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_file_transfer__["a" /* FileTransfer */],
                { provide: __WEBPACK_IMPORTED_MODULE_8__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_26__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 541:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddtransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddtransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddtransactionsPage = (function () {
    function AddtransactionsPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 1;
    }
    AddtransactionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddtransactionsPage');
        this.valName = this.navParams.get('val');
        this.getUserName();
    };
    AddtransactionsPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.buckets = _this.grsp.buckets();
        });
    };
    AddtransactionsPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "type": this.type,
            "desc": this.desc,
            "bucket": this.bucket,
            "amt": this.amt,
            "date": this.date
        };
        // });
    };
    AddtransactionsPage.prototype.saveAddApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        console.log(this.apiData);
        this.grsp.addTrans(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Saved Transaction ' + _this.apiRes.desc,
                    buttons: ['Ok']
                });
                alert_1.present();
                _this.type = '';
                _this.desc = '';
                _this.bucket = '';
                _this.amt = '';
                _this.date = '';
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Failed',
                    subTitle: 'Something went wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_2.present();
            }
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    AddtransactionsPage.prototype.saveAdd = function () {
        this.desc = this.desc ? this.desc : "";
        var alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: 'Successfully Saved Transaction ' + this.desc,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    AddtransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-addtransactions',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\addtransactions\addtransactions.html"*/'<!--\n  Generated template for the AddtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <div class="logo-container" text-center [style.display]="\'block\'">\n        <img src="assets/imgs/logo.png" alt="Logo">\n\n        <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n            <ion-segment-button value="signin">\n                Sign In\n            </ion-segment-button>\n           \n        </ion-segment> -->\n        \n        <ion-navbar>\n          <ion-title>Add Transactions for {{userName}}</ion-title>\n      \n        </ion-navbar>\n\n    </div>\n\n    \n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n  \n  <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n  \n      <ion-item>\n        <ion-label stacked>Transaction Type</ion-label>\n        <ion-select [(ngModel)]="type">\n          <ion-option value="cr">Credit</ion-option>\n          <ion-option value="dr">Debit</ion-option>\n        </ion-select>\n      </ion-item>\n  \n      <ion-item >\n        <ion-label stacked>Description</ion-label>\n        <ion-input type=""  [(ngModel)]="desc"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Bucket</ion-label>\n        <ion-select [(ngModel)]="bucket">\n          <!-- <ion-option value="b1">B1</ion-option> -->\n          <ion-option *ngFor="let x of buckets" [value]="x">{{x}}</ion-option>\n        </ion-select>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Amount</ion-label>\n        <ion-input type="number" [(ngModel)]="amt"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Date</ion-label>\n          <ion-input type="date"  [(ngModel)]="date"></ion-input>\n        </ion-item>\n  \n      <ion-item>\n          <!-- <button ion-button small (click)="saveAdd()">Save</button> -->\n          <button ion-button small (click)="saveAddApi()">Save</button>\n      </ion-item>\n  \n  </ion-content>\n  '/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\addtransactions\addtransactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], AddtransactionsPage);
    return AddtransactionsPage;
}());

//# sourceMappingURL=addtransactions.js.map

/***/ }),

/***/ 545:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnalysisPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AnalysisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AnalysisPage = (function () {
    function AnalysisPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'View Analytics',
                title: 'View Analytics',
                description: 'See How Your Numbers Are Looking',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Add Transaction(s)',
                title: 'Add Transaction(s)',
                description: 'Anything We Might Have Missed?',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'Edit Transaction(s)',
                title: 'Edit Transaction(s)',
                description: 'Because You Have Full Control',
                img: 'assets/imgs/menu/munchies.png'
            },
        ];
    }
    AnalysisPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad AnalysisPage');
    };
    AnalysisPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('ViewanalyticsPage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('AddtransactionsPage', param);
        }
        else {
            var param = { category: 3 };
            this.navCtrl.push('ViewtransactionsPage', param);
        }
    };
    AnalysisPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    AnalysisPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    AnalysisPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-analysis',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\analysis\analysis.html"*/'<!--\n  Generated template for the AnalysisPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Analysis</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\analysis\analysis.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], AnalysisPage);
    return AnalysisPage;
}());

//# sourceMappingURL=analysis.js.map

/***/ }),

/***/ 547:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 222,
	"./af.js": 222,
	"./ar": 223,
	"./ar-dz": 224,
	"./ar-dz.js": 224,
	"./ar-kw": 225,
	"./ar-kw.js": 225,
	"./ar-ly": 226,
	"./ar-ly.js": 226,
	"./ar-ma": 227,
	"./ar-ma.js": 227,
	"./ar-sa": 228,
	"./ar-sa.js": 228,
	"./ar-tn": 229,
	"./ar-tn.js": 229,
	"./ar.js": 223,
	"./az": 230,
	"./az.js": 230,
	"./be": 231,
	"./be.js": 231,
	"./bg": 232,
	"./bg.js": 232,
	"./bm": 233,
	"./bm.js": 233,
	"./bn": 234,
	"./bn.js": 234,
	"./bo": 235,
	"./bo.js": 235,
	"./br": 236,
	"./br.js": 236,
	"./bs": 237,
	"./bs.js": 237,
	"./ca": 238,
	"./ca.js": 238,
	"./cs": 239,
	"./cs.js": 239,
	"./cv": 240,
	"./cv.js": 240,
	"./cy": 241,
	"./cy.js": 241,
	"./da": 242,
	"./da.js": 242,
	"./de": 243,
	"./de-at": 244,
	"./de-at.js": 244,
	"./de-ch": 245,
	"./de-ch.js": 245,
	"./de.js": 243,
	"./dv": 246,
	"./dv.js": 246,
	"./el": 247,
	"./el.js": 247,
	"./en-SG": 248,
	"./en-SG.js": 248,
	"./en-au": 249,
	"./en-au.js": 249,
	"./en-ca": 250,
	"./en-ca.js": 250,
	"./en-gb": 251,
	"./en-gb.js": 251,
	"./en-ie": 252,
	"./en-ie.js": 252,
	"./en-il": 253,
	"./en-il.js": 253,
	"./en-nz": 254,
	"./en-nz.js": 254,
	"./eo": 255,
	"./eo.js": 255,
	"./es": 256,
	"./es-do": 257,
	"./es-do.js": 257,
	"./es-us": 258,
	"./es-us.js": 258,
	"./es.js": 256,
	"./et": 259,
	"./et.js": 259,
	"./eu": 260,
	"./eu.js": 260,
	"./fa": 261,
	"./fa.js": 261,
	"./fi": 262,
	"./fi.js": 262,
	"./fo": 263,
	"./fo.js": 263,
	"./fr": 264,
	"./fr-ca": 265,
	"./fr-ca.js": 265,
	"./fr-ch": 266,
	"./fr-ch.js": 266,
	"./fr.js": 264,
	"./fy": 267,
	"./fy.js": 267,
	"./ga": 268,
	"./ga.js": 268,
	"./gd": 269,
	"./gd.js": 269,
	"./gl": 270,
	"./gl.js": 270,
	"./gom-latn": 271,
	"./gom-latn.js": 271,
	"./gu": 272,
	"./gu.js": 272,
	"./he": 273,
	"./he.js": 273,
	"./hi": 274,
	"./hi.js": 274,
	"./hr": 275,
	"./hr.js": 275,
	"./hu": 276,
	"./hu.js": 276,
	"./hy-am": 277,
	"./hy-am.js": 277,
	"./id": 278,
	"./id.js": 278,
	"./is": 279,
	"./is.js": 279,
	"./it": 280,
	"./it-ch": 281,
	"./it-ch.js": 281,
	"./it.js": 280,
	"./ja": 282,
	"./ja.js": 282,
	"./jv": 283,
	"./jv.js": 283,
	"./ka": 284,
	"./ka.js": 284,
	"./kk": 285,
	"./kk.js": 285,
	"./km": 286,
	"./km.js": 286,
	"./kn": 287,
	"./kn.js": 287,
	"./ko": 288,
	"./ko.js": 288,
	"./ku": 289,
	"./ku.js": 289,
	"./ky": 290,
	"./ky.js": 290,
	"./lb": 291,
	"./lb.js": 291,
	"./lo": 292,
	"./lo.js": 292,
	"./lt": 293,
	"./lt.js": 293,
	"./lv": 294,
	"./lv.js": 294,
	"./me": 295,
	"./me.js": 295,
	"./mi": 296,
	"./mi.js": 296,
	"./mk": 297,
	"./mk.js": 297,
	"./ml": 298,
	"./ml.js": 298,
	"./mn": 299,
	"./mn.js": 299,
	"./mr": 300,
	"./mr.js": 300,
	"./ms": 301,
	"./ms-my": 302,
	"./ms-my.js": 302,
	"./ms.js": 301,
	"./mt": 303,
	"./mt.js": 303,
	"./my": 304,
	"./my.js": 304,
	"./nb": 305,
	"./nb.js": 305,
	"./ne": 306,
	"./ne.js": 306,
	"./nl": 307,
	"./nl-be": 308,
	"./nl-be.js": 308,
	"./nl.js": 307,
	"./nn": 309,
	"./nn.js": 309,
	"./pa-in": 310,
	"./pa-in.js": 310,
	"./pl": 311,
	"./pl.js": 311,
	"./pt": 312,
	"./pt-br": 313,
	"./pt-br.js": 313,
	"./pt.js": 312,
	"./ro": 314,
	"./ro.js": 314,
	"./ru": 315,
	"./ru.js": 315,
	"./sd": 316,
	"./sd.js": 316,
	"./se": 317,
	"./se.js": 317,
	"./si": 318,
	"./si.js": 318,
	"./sk": 319,
	"./sk.js": 319,
	"./sl": 320,
	"./sl.js": 320,
	"./sq": 321,
	"./sq.js": 321,
	"./sr": 322,
	"./sr-cyrl": 323,
	"./sr-cyrl.js": 323,
	"./sr.js": 322,
	"./ss": 324,
	"./ss.js": 324,
	"./sv": 325,
	"./sv.js": 325,
	"./sw": 326,
	"./sw.js": 326,
	"./ta": 327,
	"./ta.js": 327,
	"./te": 328,
	"./te.js": 328,
	"./tet": 329,
	"./tet.js": 329,
	"./tg": 330,
	"./tg.js": 330,
	"./th": 331,
	"./th.js": 331,
	"./tl-ph": 332,
	"./tl-ph.js": 332,
	"./tlh": 333,
	"./tlh.js": 333,
	"./tr": 334,
	"./tr.js": 334,
	"./tzl": 335,
	"./tzl.js": 335,
	"./tzm": 336,
	"./tzm-latn": 337,
	"./tzm-latn.js": 337,
	"./tzm.js": 336,
	"./ug-cn": 338,
	"./ug-cn.js": 338,
	"./uk": 339,
	"./uk.js": 339,
	"./ur": 340,
	"./ur.js": 340,
	"./uz": 341,
	"./uz-latn": 342,
	"./uz-latn.js": 342,
	"./uz.js": 341,
	"./vi": 343,
	"./vi.js": 343,
	"./x-pseudo": 344,
	"./x-pseudo.js": 344,
	"./yo": 345,
	"./yo.js": 345,
	"./zh-cn": 346,
	"./zh-cn.js": 346,
	"./zh-hk": 347,
	"./zh-hk.js": 347,
	"./zh-tw": 348,
	"./zh-tw.js": 348
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 547;

/***/ }),

/***/ 548:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BankingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the BankingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BankingPage = (function () {
    function BankingPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Savings',
                title: 'Savings',
                description: 'Banking Savings',
                img: 'assets/imgs/menu/coffee.png',
                page: 'SavingsPage'
            },
            {
                id: 2,
                name: 'Current',
                title: 'Current',
                description: 'Banking Current',
                img: 'assets/imgs/menu/breakfast.png',
                page: 'CurrentPage'
            },
            {
                id: 3,
                name: 'Fixed Deposit',
                title: 'Fixed Deposit',
                description: 'Banking Fixed Deposit',
                img: 'assets/imgs/menu/munchies.png',
                page: 'FixedDepositPage'
            },
            {
                id: 4,
                name: 'Foreign',
                title: 'Foreign',
                description: 'Banking Foreign',
                img: 'assets/imgs/menu/coffee.png',
                page: 'ForeignPage'
            },
        ];
    }
    BankingPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad BankingPage');
        // alert('work here');
    };
    BankingPage.prototype.bankAction = function (cat) {
        /* Pass category object as a parameter */
        // if(cat.id == 1){
        // let param = { category: 1 };
        this.navCtrl.push(cat.page);
        // }
        // else if(cat.id == 2){
        //   let param = { category: 2 };
        //   this.navCtrl.push('AddtransactionsPage', param );
        // }
        // else{ 
        //   let param = { category: 3 };
        //   this.navCtrl.push('ViewtransactionsPage', param );
        // }
    };
    BankingPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    BankingPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    BankingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-banking',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\banking\banking.html"*/'<!--\n  Generated template for the BankingPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title>Banking</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="bankAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\banking\banking.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], BankingPage);
    return BankingPage;
}());

//# sourceMappingURL=banking.js.map

/***/ }),

/***/ 549:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CardsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CardsPage = (function () {
    function CardsPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Naira',
                title: 'Naira',
                description: 'Naira Cards',
                img: 'assets/imgs/menu/coffee.png',
                page: 'NairaCardPage'
            },
            {
                id: 2,
                name: 'Forex',
                title: 'Forex',
                description: 'Forex Cards',
                img: 'assets/imgs/menu/breakfast.png',
                page: 'ForexCardPage'
            },
            {
                id: 3,
                name: 'Prepaid',
                title: 'Prepaid',
                description: 'Prepaid Cards',
                img: 'assets/imgs/menu/munchies.png',
                page: 'PrepaidCardPage'
            },
            {
                id: 4,
                name: 'Credit',
                title: 'Credit',
                description: 'Credit Cards',
                img: 'assets/imgs/menu/coffee.png',
                page: 'CreditCardPage'
            },
        ];
    }
    CardsPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad CardsPage');
    };
    CardsPage.prototype.cardsAction = function (cat) {
        /* Pass category object as a parameter */
        // if(cat.id == 1){
        // let param = { category: 1 };
        this.navCtrl.push(cat.page);
        // }
        // else if(cat.id == 2){
        //   let param = { category: 2 };
        //   this.navCtrl.push('AddtransactionsPage', param );
        // }
        // else{ 
        //   let param = { category: 3 };
        //   this.navCtrl.push('ViewtransactionsPage', param );
        // }
    };
    CardsPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    CardsPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    CardsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cards',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\cards\cards.html"*/'<!--\n  Generated template for the CardsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title>Cards</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="cardsAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\cards\cards.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], CardsPage);
    return CardsPage;
}());

//# sourceMappingURL=cards.js.map

/***/ }),

/***/ 550:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the BudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BudgetPage = (function () {
    function BudgetPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'View Budget Analytics',
                title: 'View Budget',
                description: 'See How Your Numbers Are Looking',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Manage Budget(s)',
                title: 'Manage Budget(s)',
                description: 'Because You Have Full Control',
                img: 'assets/imgs/menu/munchies.png'
            },
        ];
    }
    BudgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad BudgetPage');
    };
    BudgetPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    BudgetPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('ViewbudgetPage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('ManagebudgetPage', param);
        }
    };
    BudgetPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    BudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-budget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\budget\budget.html"*/'<!--\n  Generated template for the BudgetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Budget</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div>\n    </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\budget\budget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], BudgetPage);
    return BudgetPage;
}());

//# sourceMappingURL=budget.js.map

/***/ }),

/***/ 551:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProgressBarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

/**
 * Generated class for the ProgressBarComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
var ProgressBarComponent = (function () {
    function ProgressBarComponent() {
        console.log('Hello ProgressBarComponent Component');
        this.text = 'Hello World';
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('progress'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "progress", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('progressAge'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "progressAge", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('tleft'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "tleft", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('tright'),
        __metadata("design:type", Object)
    ], ProgressBarComponent.prototype, "tright", void 0);
    ProgressBarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'progress-bar',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\components\progress-bar\progress-bar.html"*/'<!-- Generated template for the ProgressBarComponent component -->\n<div class="progress-outer">\n  <div class="progress-inner" [style.width]="progress + \'%\'">\n      {{progressAge}} Years\n  </div>\n</div>\n<!-- <div class="progress-label">\n\n  <i class="labelLeft">{{tleft}}Years</i>\n  <i class="labelRight">{{tright}}Years</i>\n</div> -->\n<div class="progress-label">\n\n  <i float-left >{{tleft}} Year(s)</i>\n  <i float-right >{{tright}} Year(s)</i>\n</div>\n<!-- <i text-left></i> -->'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\components\progress-bar\progress-bar.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], ProgressBarComponent);
    return ProgressBarComponent;
}());

//# sourceMappingURL=progress-bar.js.map

/***/ }),

/***/ 552:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EdittransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the EdittransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EdittransactionsPage = (function () {
    function EdittransactionsPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 1;
    }
    EdittransactionsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EdittransactionsPage');
        this.desc = this.navParams.get('trans').trans_desc;
        this.type = this.navParams.get('trans').trans_type;
        this.bucket = this.navParams.get('trans').trans_bucket;
        this.amt = this.navParams.get('trans').trans_amt;
        this.date = this.navParams.get('trans').trans_date.substring(0, 10);
        this.tId = this.navParams.get('trans').trans_id;
        // this.desc = this.navParams.get('trans');
        this.getUserName();
    };
    EdittransactionsPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.buckets = _this.grsp.buckets();
        });
    };
    EdittransactionsPage.prototype.saveEdit = function () {
        this.desc = this.desc ? this.desc : "";
        var alert = this.alertCtrl.create({
            title: 'Success',
            subTitle: 'Successfully Saved Transaction ' + this.desc,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    EdittransactionsPage.prototype.setApiJson = function () {
        this.apiData = {
            "uId": this.uId,
            "type": this.type,
            "desc": this.desc,
            "bucket": this.bucket,
            "amt": this.amt,
            "date": this.date,
            "tid": this.tId
        };
    };
    EdittransactionsPage.prototype.saveEditApi = function () {
        var _this = this;
        this.setApiJson();
        this.loaded = 0;
        console.log(this.apiData);
        this.grsp.editTrans(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Edited Transaction ' + _this.apiRes.desc,
                    buttons: ['Dismiss']
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Sav ',
                    buttons: ['Dismiss']
                });
                alert_2.present();
            }
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    EdittransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edittransactions',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\edittransactions\edittransactions.html"*/'<!--\n  Generated template for the EdittransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!--\n  Generated template for the AddtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <div class="logo-container" text-center [style.display]="\'block\'">\n        <img src="assets/imgs/logo.png" alt="Logo">\n\n        <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n            <ion-segment-button value="signin">\n                Sign In\n            </ion-segment-button>\n           \n        </ion-segment> -->\n        \n        <ion-navbar>\n          <!-- <ion-title>Add Transactions for {{userName}}</ion-title> -->\n          <ion-title>Edit {{userName}}\'s Transactions</ion-title>\n\n      \n        </ion-navbar>\n\n    </div>\n\n    \n  \n  </ion-header>\n  \n  \n  <ion-content padding>\n  \n  <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n  \n      <ion-item>\n        <ion-label stacked>Transaction Type</ion-label>\n        <ion-select [(ngModel)]="type">\n          <ion-option value="cr">Credit</ion-option>\n          <ion-option value="dr">Debit</ion-option>\n        </ion-select>\n      </ion-item>\n  \n      <ion-item >\n        <ion-label stacked>Description</ion-label>\n        <ion-input type=""  [(ngModel)]="desc"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Bucket</ion-label>\n        <ion-select [(ngModel)]="bucket">\n          <!-- <ion-option value="b1">B1</ion-option> -->\n          <!-- <ion-option value="b2">B2</ion-option> -->\n          <ion-option *ngFor="let x of buckets" [value]="x">{{x}}</ion-option>\n\n        </ion-select>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Amount</ion-label>\n        <ion-input type="number" [(ngModel)]="amt"></ion-input>\n      </ion-item>\n  \n      <ion-item>\n        <ion-label stacked>Date</ion-label>\n          <ion-input type="date"  [(ngModel)]="date"></ion-input>\n        </ion-item>\n  \n      <ion-item>\n          <!-- <button ion-button small (click)="saveAdd()">Save</button> -->\n          <button ion-button small (click)="saveEditApi()">Save</button>\n      </ion-item>\n  \n  </ion-content>\n  \n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\edittransactions\edittransactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], EdittransactionsPage);
    return EdittransactionsPage;
}());

//# sourceMappingURL=edittransactions.js.map

/***/ }),

/***/ 553:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FinInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the FinInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FinInfoPage = (function () {
    function FinInfoPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Banking',
                title: 'Banking',
                description: 'Everything You Need to Have Handy on Banking',
                img: 'assets/imgs/menu/coffee.png',
                page: 'BankingPage'
            },
            {
                id: 2,
                name: 'Cards',
                title: 'Cards',
                description: 'Get Clarity on How Your Cards are Managed',
                img: 'assets/imgs/menu/breakfast.png',
                page: 'CardsPage'
            },
        ];
    }
    FinInfoPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad FinInfoPage');
    };
    FinInfoPage.prototype.fiAction = function (cat) {
        /* Pass category object as a parameter */
        // if(cat.id == 1){
        // let param = { category: 1 };
        this.navCtrl.push(cat.page);
        // }
        // else if(cat.id == 2){
        //   let param = { category: 2 };
        //   this.navCtrl.push('AddtransactionsPage', param );
        // }
        // else{ 
        //   let param = { category: 3 };
        //   this.navCtrl.push('ViewtransactionsPage', param );
        // }
    };
    FinInfoPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    FinInfoPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    FinInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-fin-info',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\fin-info\fin-info.html"*/'<!--\n  Generated template for the FinInfoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title>Financial Info.</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="fiAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div>\n  </ion-card>\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\fin-info\fin-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FinInfoPage);
    return FinInfoPage;
}());

//# sourceMappingURL=fin-info.js.map

/***/ }),

/***/ 554:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FavoritesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FavoritesPage = (function () {
    function FavoritesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.items = [
            {
                id: 6,
                name: 'limeRefreshers',
                title: 'Lime Refreshers',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/limeRefreshers.png',
                category: 'coffe',
                price: '12',
                likes: '1900',
                isliked: true,
            },
            {
                id: 4,
                name: 'vanillaFrappucino',
                title: 'Vanilla Frappucino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/vanillaFrappucino.png',
                category: 'coffe',
                price: '9.85',
                likes: '2300',
                isliked: true,
            },
            {
                id: 4,
                name: 'espresso',
                title: 'Espresso',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/espresso.png',
                category: 'coffe',
                price: '4.50',
                likes: '1800',
                isliked: true,
            },
            {
                id: 5,
                name: 'icedAmericano',
                title: 'Iced Americano',
                description: 'Locally Roasted',
                img: 'assets/imgs/submenu/icedAmericano.png',
                category: 'coffe',
                price: '10.50',
                likes: '2300',
                isliked: true,
            },
            {
                id: 3,
                name: 'kickFrappe',
                title: 'Kick Frappe',
                description: 'Coffee Kick',
                img: 'assets/imgs/submenu/kickFrappe.png',
                category: 'coffe',
                price: '12.35',
                likes: '4450',
                isliked: true,
            },
            {
                id: 2,
                name: 'caramelFrappe',
                title: 'Caramel Frappe',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/caramelFrappe.png',
                category: 'coffe',
                price: '7.85',
                likes: '3456',
                isliked: true,
            },
            {
                id: 4,
                name: 'cappuccino',
                title: 'Cappuccino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/cappuccino.png',
                category: 'coffe',
                price: '10.65',
                likes: '2300',
                isliked: true,
            },
            {
                id: 1,
                name: 'chocoFrappe',
                title: 'Choco Frappe',
                description: 'Chocolate Whirl',
                img: 'assets/imgs/submenu/chocoFrappe.png',
                category: 'coffe',
                price: '9',
                likes: '3200',
                isliked: true,
            },
        ];
    }
    FavoritesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FavoritesPage');
    };
    FavoritesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-favorites',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\favorites\favorites.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Likes</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-list no-lines no-margin padding-horizontal>\n        <ion-item *ngFor="let item of items">\n            <ion-thumbnail item-start>\n                <img src="{{item.img}}">\n            </ion-thumbnail>\n            <h2>{{ item.title }}</h2>\n            <p>{{ item.description }}</p>\n            <ion-row no-padding>\n                <ion-col no-padding>\n                    <button no-padding ion-button clear small color="primary">\n                  {{ item.price | currency:\'USD\':true:\'1.2\'}}\n                  </button>\n                </ion-col>\n                <ion-col text-right no-padding>\n                    <button no-padding ion-button clear small [color]="item.isliked? \'danger\' : \'light\'" (click)="item.isliked = !item.isliked" icon-end>\n                  <ion-icon name=\'heart\'></ion-icon>\n                  <!-- {{ item.likes | number}} -->\n                </button>\n                </ion-col>\n            </ion-row>\n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\favorites\favorites.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], FavoritesPage);
    return FavoritesPage;
}());

//# sourceMappingURL=favorites.js.map

/***/ }),

/***/ 555:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForexCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ForexCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForexCardPage = (function () {
    function ForexCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    ForexCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForexCardPage');
        this.getUserName();
    };
    ForexCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getForexApi();
        });
    };
    ForexCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Foreign"
        };
        // });
    };
    ForexCardPage.prototype.getForexApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.forex = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.forexCrd = JSON.parse(_this.forex.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    ForexCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forex-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\forex-card\forex-card.html"*/'<!--\n  Generated template for the ForexCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Foreign Currency Card</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  \n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of forexCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\forex-card\forex-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ForexCardPage);
    return ForexCardPage;
}());

//# sourceMappingURL=forex-card.js.map

/***/ }),

/***/ 556:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvestmentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__signup_signup__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__paystack_paystack__ = __webpack_require__(360);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the InvestmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InvestmentPage = (function () {
    function InvestmentPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.showHide = false;
        this.recur = false;
        this.model = {};
        this.loaded = 1;
        this.more = 0;
    }
    InvestmentPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    InvestmentPage.prototype.ionViewDidLoad = function () {
        this.load();
        // let payRes = this.navParams.get('res');
        console.log('ionViewDidLoad InvestmentPage');
        this.valName = this.navParams.get('val');
        this.getUserName();
        //     if (payRes == "success"){
        //       // this.buyInvestApi();
        // console.log(payRes);
        // this.storage.get('investData').then((data) => {
        //   console.log(data);
        //   this.apiData = data;
        //   this.buyInvestApi();
        //   console.log("done");
        // });
        //     }
        //     else{
        //       // alert("Something Went Wrong, Please Contact Admin");
        //     }
    };
    InvestmentPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getInvestmentsApi();
        });
    };
    InvestmentPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        var md = {
            "recurring": this.recur,
            "frequency": this.freq,
            "deductDate": this.deductDate,
        };
        // md = JSON.stringify(md);
        this.apiData = {
            "uId": this.uId,
            "type": "Gaid",
            "prov": "Gaid",
            "amt": this.amount,
            "model": JSON.stringify(md)
        };
        console.log(this.apiData);
        // });
    };
    InvestmentPage.prototype.goPay = function () {
        this.setApiJson();
        var data = {
            price: this.amount
            // data:this.apiData
        };
        console.log(data);
        this.storage.set('investData', this.apiData);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__paystack_paystack__["a" /* PaystackPage */], data);
    };
    InvestmentPage.prototype.buyInvestApi = function () {
        var _this = this;
        // this.setApiJson();
        this.loaded = 0;
        console.log(this.apiData);
        console.log("done");
        this.grsp.buyInvestment(this.apiData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            _this.loaded = 1;
            if (_this.apiRes.response == true) {
                var alert_1 = _this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Saved NGN' + _this.amount + ' of ' + _this.apiRes.prov + ' Investment ',
                    buttons: ['Ok']
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: 'Failed',
                    subTitle: 'Something went wrong, Please try again',
                    buttons: ['Dismiss']
                });
                alert_2.present();
            }
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        this.storage.set('investData', null);
    };
    InvestmentPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    InvestmentPage.prototype.toggleMore = function () {
        if (this.more == 0) {
            this.more = 1;
        }
        else if (this.more == 1) {
            this.more = 0;
        }
    };
    InvestmentPage.prototype.setInvApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiInvData = {
            "uId": this.uId
        };
        // });
    };
    InvestmentPage.prototype.getInvestmentsApi = function () {
        var _this = this;
        this.setInvApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiInvData);
        this.grsp.investments(this.apiInvData).then(function (data) {
            console.log(data);
            _this.inv = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.investments = JSON.parse(_this.inv.investments);
            _this.investments = _this.investments.reverse();
            _this.totInvest = 0;
            _this.investments.forEach(function (obj) {
                obj.actAmt = _this.numberWithCommas(obj.inv_amt);
                _this.totInvest += parseInt(obj.inv_amt);
            });
            _this.totInvest = _this.numberWithCommas(_this.totInvest);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    // ===============================================
    InvestmentPage.prototype.numberWithCommas = function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
    };
    InvestmentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-investment',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\investment\investment.html"*/'<!--\n  Generated template for the InvestmentPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n         <ion-icon name="menu"></ion-icon>\n       </button>\n        <!-- <ion-title>Analysis</ion-title> -->\n        <ion-title>{{userName}}\'s Gaid Investment</ion-title>\n    </ion-navbar>\n \n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-item>\n        <ion-label stacked>How much would you like to invest?</ion-label>\n        <ion-input type="number"  [(ngModel)]="amount"></ion-input>\n    </ion-item>\n\n    <ion-item no-lines>\n        <ion-label stacked>Recurring?</ion-label>\n        <ion-checkbox [(ngModel)]="recur"></ion-checkbox>\n    </ion-item>\n   <hr/>\n    <ion-item  *ngIf="recur==true">\n        <ion-label stacked>How frequently?</ion-label>\n        <ion-select [(ngModel)]="freq">\n            <!-- <ion-option value="daily">Daily</ion-option> -->\n            <!-- <ion-option value="weekly">Weekly</ion-option> -->\n            <ion-option value="monthly">Monthly</ion-option>\n            <!-- <ion-option value="yearly">Yearly</ion-option> -->\n        </ion-select>\n    </ion-item>\n\n    <ion-item  *ngIf="freq==\'monthly\' && recur==true" >\n        <ion-label stacked>What Day of the Month?</ion-label>\n        <ion-input type="date"  [(ngModel)]="deductDate"></ion-input>\n    </ion-item>\n  \n    <ion-item no-lines *ngIf="amount">\n        <!-- <button ion-button small (click)="buyInvestApi()">Buy</button> -->\n        <button ion-button small (click)="goPay()">Buy</button>\n    </ion-item>\n  \n\n\n\n    <hr *ngIf="investments">\n    <h5 text-center *ngIf="investments">\n        My Investments\n    </h5>\n    <i *ngIf="investments" padding text-left><b >Total:</b> NGN{{totInvest}}</i>\n    <button *ngIf="investments" ion-button small  float-right (click)="toggleMore()">Show More</button>\n   <div *ngIf="more==1">\n\n       <ion-card  *ngFor="let item of investments">\n           <ion-card-content>\n               <h4>{{item.inv_type}}</h4>\n               <h2>NGN{{item.actAmt}}</h2>\n               <p text-right>{{item.inv_date}}</p>\n            </ion-card-content>\n        </ion-card>\n    </div>\n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\investment\investment.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], InvestmentPage);
    return InvestmentPage;
}());

//# sourceMappingURL=investment.js.map

/***/ }),

/***/ 839:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ManagebudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ManagebudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ManagebudgetPage = (function () {
    function ManagebudgetPage(navCtrl, storage, alertCtrl, navParams, grsp) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.navParams = navParams;
        this.grsp = grsp;
        this.expenses = 50;
        this.savInv = 25;
        this.proj = 25;
        this.debt = 0;
        this.income = 0;
        this.id = 0;
        this.loaded = 1;
    }
    ManagebudgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ManagebudgetPage');
        this.getUserName();
    };
    ManagebudgetPage.prototype.setApiJson = function () {
        this.apiData = {
            "uId": this.id,
            "expenses": this.expenses,
            "savInv": this.savInv,
            "proj": this.proj,
            "debt": this.debt,
            "income": this.income
        };
    };
    // saveAddApi(){
    // this.loaded = 0;
    //   this.setApiJson();
    //   console.log(this.apiData);
    //   this.grsp.addTrans(this.apiData).then(data => {
    //     console.log(data);
    //     this.apiRes = data;
    // this.loaded = 1;
    //     if(this.apiRes.response == true){
    //       let alert = this.alertCtrl.create({
    //         title: 'Success',
    //         subTitle: 'Successfully Saved Transaction '+this.apiRes.desc,
    //         buttons: ['Ok']
    //       });
    //       alert.present();
    //       this.type='';
    //       this.desc='';
    //       this.bucket='';
    //       this.amt='';
    //       this.date='';
    //     }
    //     else{
    //       let alert = this.alertCtrl.create({
    //         title: 'Failed',
    //         subTitle: 'Something went wrong, Please try again',
    //         buttons: ['Dismiss']
    //       });
    //       alert.present();
    //     }
    //   })
    //   .catch((err) => {
    // this.loaded = 1;
    //     let alert = this.alertCtrl.create({
    //       title: 'Error',
    //       subTitle: 'Something broke and thats on us, Please try again',
    //       buttons: ['Dismiss']
    //     });
    //     alert.present();
    //   });
    // }
    ManagebudgetPage.prototype.saveBudget = function () {
        var _this = this;
        if (this.sumAll() == true) {
            console.log("save budget for the month");
            this.loaded = 0;
            this.setApiJson();
            console.log(this.apiData);
            // ============================================================
            this.grsp.addBudget(this.apiData).then(function (data) {
                console.log(data);
                _this.apiRes = data;
                _this.loaded = 1;
                if (_this.apiRes.response == true) {
                    var alert_1 = _this.alertCtrl.create({
                        title: 'Success',
                        subTitle: 'Successfully Saved Budget',
                        buttons: ['Ok']
                    });
                    alert_1.present();
                }
                else {
                    var alert_2 = _this.alertCtrl.create({
                        title: 'Failed',
                        subTitle: 'Something went wrong, Please try again',
                        buttons: ['Dismiss']
                    });
                    alert_2.present();
                }
            })
                .catch(function (err) {
                _this.loaded = 1;
                var alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: 'Something broke and thats on us, Please try again',
                    buttons: ['Dismiss']
                });
                alert.present();
            });
            // ============================================================
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: 'Note',
                subTitle: 'The Total of Budget Allocation Must Be Equal to 100%',
                buttons: ['Dismiss']
            });
            alert_3.present();
        }
    };
    ManagebudgetPage.prototype.sumAll = function () {
        if (this.type == 'debtor') {
            // this.debt =15;
        }
        else {
            this.debt = 0;
        }
        // console.log(this.type);
        // console.log(this.expenses);
        // console.log(this.savInv);
        // console.log(this.proj);
        // console.log(this.debt);
        if ((this.expenses + this.savInv + this.proj + this.debt) == 100) {
            return true;
        }
        else {
            return false;
        }
    };
    ManagebudgetPage.prototype.help = function () {
        var alert = this.alertCtrl.create({
            title: 'Note',
            subTitle: 'The Budget Must be Set Before The Month Starts',
            buttons: ['Dismiss']
        });
        alert.present();
    };
    ManagebudgetPage.prototype.getUserName = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            _this.userName = initialData.name.toUpperCase();
            _this.id = initialData.id;
        });
    };
    ManagebudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-managebudget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\managebudget\managebudget.html"*/'<!--\n  Generated template for the AddtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <div class="logo-container" text-center [style.display]="\'block\'">\n      <img src="assets/imgs/logo.png" alt="Logo">\n\n      <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n          <ion-segment-button value="signin">\n              Sign In\n          </ion-segment-button>\n         \n      </ion-segment> -->\n      \n      <ion-navbar>\n        <ion-title>{{userName}}\'s Budget</ion-title>\n        \n      </ion-navbar>\n\n  </div>\n\n  \n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n  <!-- <i  click="help()"> <ion-icon color="danger" name="help"></ion-icon> </i> -->\n  <ion-card>\n      <ion-card-content>\n\n  <button ion-button float-right small color="secondary" (click)="help()"><strong><ion-icon color="danger" style="font-size: 45px" name="help"></ion-icon></strong></button>\n\n  \n  <ion-item >\n      <ion-label stacked>Projected Monthly Income(NGN)</ion-label>\n      <ion-input type="number"  [(ngModel)]="income"></ion-input>\n    </ion-item>\n  \n  <ion-item>\n      <ion-label stacked>Are You Servicing Debts ?</ion-label>\n      <ion-select [(ngModel)]="type">\n        <ion-option value="debtor">Yes</ion-option>\n        <ion-option value="ndebtor">No</ion-option>\n      </ion-select>\n    </ion-item>\n    </ion-card-content>\n</ion-card>\n    <br/>\n    <br/>\n  \n    <ion-card>\n        <ion-card-content>\n    \n  <i > Assign Pecentages of Your Income to Each Allocation </i>\n \n   \n    <ion-item >\n      <ion-label stacked>Living Expenses(%)</ion-label>\n      <ion-input type="number"  [(ngModel)]="expenses"></ion-input>\n    </ion-item>\n\n    <!-- <ion-item>\n      <ion-label stacked>Bucket</ion-label>\n      <ion-select [(ngModel)]="bucket">\n        <ion-option *ngFor="let x of buckets" [value]="x">{{x}}</ion-option>\n      </ion-select>\n    </ion-item> -->\n\n    <ion-item>\n      <ion-label stacked>Savings & Investments(%)</ion-label>\n      <ion-input type="number" [(ngModel)]="savInv"></ion-input>\n    </ion-item>\n\n    <ion-item>\n      <ion-label stacked>Projects(%)</ion-label>\n        <ion-input type="number"  [(ngModel)]="proj"></ion-input>\n      </ion-item>\n    \n      <ion-item *ngIf="type==\'debtor\'">\n      <ion-label stacked>Debt Repayment(%)</ion-label>\n        <ion-input type="number"  [(ngModel)]="debt"></ion-input>\n      </ion-item>\n\n    <ion-item no-lines *ngIf="id > 0"  >\n        <!-- <button ion-button small (click)="saveAdd()">Save</button> -->\n        <button ion-button small (click)="saveBudget()">Save</button>\n    </ion-item>\n  </ion-card-content>\n</ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\managebudget\managebudget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], ManagebudgetPage);
    return ManagebudgetPage;
}());

//# sourceMappingURL=managebudget.js.map

/***/ }),

/***/ 840:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuTwoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuTwoPage = (function () {
    function MenuTwoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'coffe',
                title: 'Coffe',
                description: 'Freshly brewed coffee',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'breakfast',
                title: 'Breakfast',
                description: 'Hot & full of flavour.',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'munchies',
                title: 'Munchies',
                description: 'Perfectly baked goodies.',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'sandwiches',
                title: 'Sandwiches',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/sandwiches.png'
            },
            {
                id: 5,
                name: 'tea',
                title: 'Tea',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/tea.png'
            },
            {
                id: 6,
                name: 'pancake',
                title: 'Pancake',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/pancake.png'
            },
        ];
    }
    MenuTwoPage.prototype.goToSubMenu = function (cat) {
        /* Pass category object as a parameter */
        var param = { category: cat };
        this.navCtrl.push('SubMenuTwoPage', param);
    };
    MenuTwoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu-two',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-two\menu-two.html"*/'<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Menu</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="goToSubMenu(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div>\n    </ion-card>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-two\menu-two.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], MenuTwoPage);
    return MenuTwoPage;
}());

//# sourceMappingURL=menu-two.js.map

/***/ }),

/***/ 841:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NairaCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the NairaCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NairaCardPage = (function () {
    function NairaCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    NairaCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad NairaCardPage');
        this.getUserName();
    };
    NairaCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getNairaApi();
        });
    };
    NairaCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Naira"
        };
        // });
    };
    NairaCardPage.prototype.getNairaApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.naira = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.nairaCrd = JSON.parse(_this.naira.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    NairaCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-naira-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\naira-card\naira-card.html"*/'<!--\n  Generated template for the NairaCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Naira Debit Card</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of nairaCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\naira-card\naira-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], NairaCardPage);
    return NairaCardPage;
}());

//# sourceMappingURL=naira-card.js.map

/***/ }),

/***/ 842:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderDetailsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderDetailsPage = (function () {
    function OrderDetailsPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.order = {
            item: {
                id: 10,
                name: 'chocolateMuffin',
                title: 'Chocolate Muffin',
                description: "Our muffin unites rich, dense chocolate with a gooey caramel center for bliss in every bite. As far as we're concerned, there's no such thing as too much caramel.",
                img: 'assets/imgs/submenu/chocolateMuffin.png',
                category: 'muffin',
                price: '10',
                likes: '1800',
                isliked: true,
            },
            quantity: 2,
        };
    }
    OrderDetailsPage.prototype.placeOrder = function () {
        var toast = this.toastCtrl.create({
            message: 'Placing Order of ' + this.order.quantity + ' ' + this.order.item.title,
            duration: 2500
        });
        toast.present();
    };
    OrderDetailsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order-details',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\order-details\order-details.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Chocolate Muffin</ion-title>\n\n        <button class="right-header-icon" ion-button clear icon-only [color]="order.item.isliked? \'danger\' : \'light\'" (click)="order.item.isliked = !order.item.isliked">\n            <ion-icon name="heart"></ion-icon>\n      </button>\n    </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n    <ion-row>\n        <ion-col text-center padding>\n            <img src="{{ order.item.img }}" alt="">\n        </ion-col>\n    </ion-row>\n    <ion-row>\n        <ion-col padding-horizontal>\n            <p>{{ order.item.description }}</p>\n        </ion-col>\n    </ion-row>\n    <ion-row text-center>\n        <ion-col>\n            <ion-label>\n                Quantity\n            </ion-label>\n            <h2 (click)="ionSelect.open()">\n                {{ order.quantity }}\n                <ion-icon name="ios-arrow-down"></ion-icon>\n            </h2>\n        </ion-col>\n        <ion-col class="total">\n            <ion-label>\n                Total\n            </ion-label>\n            <h2>\n                {{ (order.quantity * order.item.price) | currency:\'USD\':true:\'1.0-2\'}}\n            </h2>\n        </ion-col>\n    </ion-row>\n    <button ion-button full no-margin color="primary" (click)="placeOrder()"> Place The Order</button>\n\n    <ion-item style="display:none;">\n        <ion-select [(ngModel)]="order.quantity" #ionSelect>\n            <ion-option>1</ion-option>\n            <ion-option>2</ion-option>\n            <ion-option>3</ion-option>\n            <ion-option>4</ion-option>\n            <ion-option>5</ion-option>\n        </ion-select>\n    </ion-item>\n\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\order-details\order-details.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], OrderDetailsPage);
    return OrderDetailsPage;
}());

//# sourceMappingURL=order-details.js.map

/***/ }),

/***/ 843:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuOnePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MenuOnePage = (function () {
    function MenuOnePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'coffe',
                title: 'Coffe',
                description: 'Freshly brewed coffee',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'breakfast',
                title: 'Breakfast',
                description: 'Hot & full of flavour.',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'munchies',
                title: 'Munchies',
                description: 'Perfectly baked goodies.',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'sandwiches',
                title: 'Sandwiches',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/sandwiches.png'
            },
            {
                id: 5,
                name: 'tea',
                title: 'Tea',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/tea.png'
            },
            {
                id: 6,
                name: 'pancake',
                title: 'Pancake',
                description: 'Fresh, healthy and tasty.',
                img: 'assets/imgs/menu/pancake.png'
            },
        ];
    }
    MenuOnePage.prototype.goToSubMenu = function (cat) {
        /* Pass category object as a parameter */
        var param = { category: cat };
        this.navCtrl.push('SubMenuOnePage', param);
    };
    MenuOnePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-menu-one',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-one\menu-one.html"*/'<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n  <ion-icon name="menu"></ion-icon>\n</button>\n        <ion-title>Menu</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <ion-grid no-padding>\n        <ion-row no-padding justify-content-center>\n            <ion-col no-padding col-6 align-self-center *ngFor="let cat of categories">\n                <ion-card (click)="goToSubMenu(cat)">\n                    <img src="{{cat.img}}" />\n                    <ion-card-content>\n                        <ion-card-title>\n                            {{ cat.title }}\n                        </ion-card-title>\n                        <p>{{ cat.description }} </p>\n                    </ion-card-content>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\menu-one\menu-one.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], MenuOnePage);
    return MenuOnePage;
}());

//# sourceMappingURL=menu-one.js.map

/***/ }),

/***/ 844:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProfilePage = (function () {
    function ProfilePage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
    }
    ProfilePage.prototype.changeImage = function () {
        var toast = this.toastCtrl.create({
            message: 'Change Image Button Clicked',
            duration: 2500,
        });
        toast.present();
    };
    ProfilePage.prototype.follow = function () {
        var toast = this.toastCtrl.create({
            message: 'Follow Button Clicked',
            duration: 2500,
        });
        toast.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\profile\profile.html"*/'<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title>Profile</ion-title>\n    </ion-navbar>\n\n    <div class="profile-name" text-start icon-end>\n        <h2>Alexander Karev</h2>\n        <p icon-end> UI Designer & Coffee Lover\n\n            <button ion-fab icon-only float-right color="secondary" (click)="changeImage()">\n              <ion-icon name="camera"></ion-icon>\n           </button>\n        </p>\n    </div>\n</ion-header>\n\n\n<ion-content padding>\n    <article text-center padding-vertical>\n        Born in Singapore, received my design training in London and New York, and am always striving to create work that combines strategic design with compelling visuals and an attention to detail.\n    </article>\n\n    <ion-grid>\n        <ion-row text-center>\n            <ion-col col3>\n                <h2>0</h2>\n                <p>Orders</p>\n            </ion-col>\n            <ion-col col3>\n                <h2>30</h2>\n                <p>Likes</p>\n            </ion-col>\n            <ion-col col3>\n                <h2>25</h2>\n                <p>Follower</p>\n            </ion-col>\n            <ion-col col3>\n                <h2>60</h2>\n                <p>Following</p>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n\n    <button ion-button full color="primary" (click)="follow()">\n    Follow\n  </button>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrepaidCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the PrepaidCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PrepaidCardPage = (function () {
    function PrepaidCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    PrepaidCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PrepaidCardPage');
        this.getUserName();
    };
    PrepaidCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getPrepApi();
        });
    };
    PrepaidCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Prepaid"
        };
        // });
    };
    PrepaidCardPage.prototype.getPrepApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.prep = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.prepCrd = JSON.parse(_this.prep.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    PrepaidCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-prepaid-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\prepaid-card\prepaid-card.html"*/'<!--\n  Generated template for the PrepaidCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Prepaid Card</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of prepCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\prepaid-card\prepaid-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], PrepaidCardPage);
    return PrepaidCardPage;
}());

//# sourceMappingURL=prepaid-card.js.map

/***/ }),

/***/ 846:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingPage = (function () {
    function SettingPage(navCtrl, alertCtrl, grsp, storage, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.storage = storage;
        this.navParams = navParams;
        this.setting = {};
        this.loaded = 1;
    }
    SettingPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SettingPage');
        // this.valName = this.navParams.get('val');
        this.getUserName();
    };
    SettingPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.setting = {
                name: initialData.name.toUpperCase(),
                dob: initialData.dob,
                dow: initialData.dow,
                pen: initialData.pen,
                savInv: initialData.savInv,
                exp: initialData.exp,
                inc: initialData.inc,
                ret: initialData.ret,
                email: initialData.email,
                pword: initialData.pword,
                cpword: ""
            };
        });
    };
    SettingPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "name": this.setting.name,
            "dob": this.setting.dob,
            "dow": this.setting.dow,
            "pen": this.setting.pen,
            "savInv": this.setting.savInv,
            "exp": this.setting.exp,
            "inc": this.setting.inc,
            "ret": this.setting.ret,
            "email": this.setting.email,
            "pword": this.setting.pword,
        };
        console.log(this.apiData);
        // });this.initialData.namethis.initialData.name
    };
    SettingPage.prototype.editUser = function () {
        var _this = this;
        if (this.setting.pword !== this.setting.cpword) {
            var alert_1 = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Passwords Do Not Match',
                buttons: ['Ok']
            });
            alert_1.present();
            return;
        }
        var alert = this.alertCtrl.create({
            title: 'Confirm Update',
            message: 'Do you Want to Make These Changes?',
            buttons: [
                {
                    text: 'No',
                    role: 'cancel',
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        console.log('Yes clicked');
                        _this.setApiJson();
                        _this.loaded = 0;
                        console.log(_this.apiData);
                        // =============================================
                        _this.grsp.editUser(_this.apiData).then(function (data) {
                            console.log(data);
                            _this.apiRes = data;
                            _this.loaded = 1;
                            if (_this.apiRes.response == true) {
                                var alert_2 = _this.alertCtrl.create({
                                    title: 'Success',
                                    subTitle: 'Successfully Edited User ' + _this.apiRes.name,
                                    buttons: ['Dismiss']
                                });
                                alert_2.present();
                                // alert = this.alertCtrl.create({
                                //   title: 'Info',
                                //   subTitle: 'You will be logged out now',
                                //   buttons: ['Dismiss']
                                // });
                                // alert.present();
                                // ============================
                                _this.storage.set('initialData', null);
                                // let page = this.pages[this.pages.length-2]
                                var page = { title: 'Sign In', component: 'SignupPage' };
                                _this.navCtrl.setRoot(page.component);
                                // this.activePage = page;
                                // ============================
                            }
                            else {
                                var alert_3 = _this.alertCtrl.create({
                                    title: 'Failed',
                                    subTitle: 'Something Broke ',
                                    buttons: ['Dismiss']
                                });
                                alert_3.present();
                            }
                        }).catch(function (err) {
                            _this.loaded = 1;
                            var alert = _this.alertCtrl.create({
                                title: 'Error',
                                subTitle: 'Something broke and thats on us, Please try again',
                                buttons: ['Dismiss']
                            });
                            alert.present();
                        });
                        // =============================================
                    }
                }
            ]
        });
        alert.present();
    };
    SettingPage.prototype.reset = function () {
        this.getUserName();
    };
    SettingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-setting',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\setting\setting.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n    <ion-icon name="menu"></ion-icon>\n  </button>\n        <ion-title>Setting</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n        <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n    \n    <ion-list no-lines no-margin>\n        <ion-item text-center>\n        <h2>Edit Settings Here</h2>\n        </ion-item>\n        <ion-item>\n            <ion-label> Name</ion-label>\n            <ion-input [(ngModel)]="setting.name" text-right type="text"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> DOB</ion-label>\n            <ion-input [(ngModel)]="setting.dob" text-right type="date"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> DOW</ion-label>\n            <ion-input [(ngModel)]="setting.dow" text-right type="date"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Pension</ion-label>\n            <ion-input [(ngModel)]="setting.pen" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Assets (at signup)</ion-label>\n            <ion-input [(ngModel)]="setting.savInv" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Expenses</ion-label>\n            <ion-input [(ngModel)]="setting.exp" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Income</ion-label>\n            <ion-input [(ngModel)]="setting.inc" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Retirement Age</ion-label>\n            <ion-input [(ngModel)]="setting.ret" text-right type="number"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Email</ion-label>\n            <ion-input [(ngModel)]="setting.email" text-right type="email"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Password</ion-label>\n            <ion-input [(ngModel)]="setting.pword" text-right type="password"></ion-input>\n        </ion-item>\n        <ion-item>\n            <ion-label> Confirm Password</ion-label>\n            <ion-input [(ngModel)]="setting.cpword" text-right type="password"></ion-input>\n        </ion-item>\n\n        <button ion-button color="primary" small (click)="editUser()" >Update</button>\n        <button ion-button small (click)="reset()" >Reset</button>\n        <!-- <ion-list-header>\n            Social Media\n        </ion-list-header>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.facebook" color="primary"></ion-toggle>\n            <ion-label>\n                Facebook\n            </ion-label>\n        </ion-item>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.twitter" color="primary"></ion-toggle>\n            <ion-label>\n                Twitter\n            </ion-label>\n        </ion-item>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.instagram" color="primary"></ion-toggle>\n            <ion-label>\n                Instagram\n            </ion-label>\n        </ion-item>\n        <ion-list-header>\n            Notifications\n        </ion-list-header>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.offers" color="primary"></ion-toggle>\n            <ion-label>\n                Offers\n            </ion-label>\n        </ion-item>\n        <ion-item>\n            <ion-toggle [(ngModel)]="setting.discount" color="primary"></ion-toggle>\n            <ion-label>\n                New items\n            </ion-label>\n        </ion-item> -->\n    </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\setting\setting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SettingPage);
    return SettingPage;
}());

//# sourceMappingURL=setting.js.map

/***/ }),

/***/ 847:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashTwoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SplashTwoPage = (function () {
    function SplashTwoPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SplashTwoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-splash-two',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-two\splash-two.html"*/'<ion-content>\n    <img src="assets/imgs/logo.png" alt="">\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-two\splash-two.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SplashTwoPage);
    return SplashTwoPage;
}());

//# sourceMappingURL=splash-two.js.map

/***/ }),

/***/ 848:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMenuOnePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMenuOnePage = (function () {
    function SubMenuOnePage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.specialOffer = {
            oldPrice: 5.5,
            newPrice: 3.55,
            item: 'Espresso'
        };
        this.items = [
            {
                id: 1,
                name: 'chocoFrappe',
                title: 'Choco Frappe',
                description: 'Chocolate Whirl',
                img: 'assets/imgs/submenu/chocoFrappe.png',
                category: 'coffe',
                price: '9',
                likes: '3200',
                isliked: false,
            },
            {
                id: 2,
                name: 'caramelFrappe',
                title: 'Caramel Frappe',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/caramelFrappe.png',
                category: 'coffe',
                price: '7.85',
                likes: '3456',
                isliked: false,
            },
            {
                id: 3,
                name: 'kickFrappe',
                title: 'Kick Frappe',
                description: 'Coffee Kick',
                img: 'assets/imgs/submenu/kickFrappe.png',
                category: 'coffe',
                price: '12.35',
                likes: '4450',
                isliked: false,
            },
            {
                id: 4,
                name: 'cappuccino',
                title: 'Cappuccino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/cappuccino.png',
                category: 'coffe',
                price: '10.65',
                likes: '2300',
                isliked: false,
            },
            {
                id: 5,
                name: 'icedAmericano',
                title: 'Iced Americano',
                description: 'Locally Roasted',
                img: 'assets/imgs/submenu/icedAmericano.png',
                category: 'coffe',
                price: '10.50',
                likes: '2300',
                isliked: false,
            },
            {
                id: 6,
                name: 'limeRefreshers',
                title: 'Lime Refreshers',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/limeRefreshers.png',
                category: 'coffe',
                price: '12',
                likes: '1900',
                isliked: false,
            },
            {
                id: 4,
                name: 'vanillaFrappucino',
                title: 'Vanilla Frappucino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/vanillaFrappucino.png',
                category: 'coffe',
                price: '9.85',
                likes: '2300',
                isliked: false,
            },
            {
                id: 4,
                name: 'espresso',
                title: 'Espresso',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/espresso.png',
                category: 'coffe',
                price: '4.50',
                likes: '1800',
                isliked: false,
            },
        ];
    }
    SubMenuOnePage.prototype.ionViewDidLoad = function () {
        var selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
    };
    SubMenuOnePage.prototype.selectItem = function (item) {
        var toast = this.toastCtrl.create({
            message: 'item ' + item.title + ' Clicked',
            duration: 2500,
        });
        toast.present();
    };
    SubMenuOnePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sub-menu-one',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-one\sub-menu-one.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>Coffee</ion-title>\n    </ion-navbar>\n\n    <br>\n    <div class="offer" text-center>\n        <p>Special Offer</p>\n        <h2>{{ specialOffer.item }}</h2>\n        <p>Blue Ridge Blend</p>\n        <p text-left style="margin-top: 20px;" class="old-price">\n            {{ specialOffer.oldPrice | currency:\'USD\':true:\'1.0-2\'}}\n            <span float-right class="new-price">\n              {{ specialOffer.newPrice | currency:\'USD\':true:\'1.0-2\'}}\n          </span>\n        </p>\n    </div>\n</ion-header>\n\n<ion-content>\n    <ion-list no-lines no-margin>\n        <ion-item *ngFor="let item of items">\n            <ion-thumbnail item-start (click)="selectItem(item)">\n                <img src="{{item.img}}">\n            </ion-thumbnail>\n            <h2 (click)="selectItem(item)">{{ item.title }}</h2>\n            <p (click)="selectItem(item)">{{ item.description }}</p>\n            <ion-row no-padding>\n                <ion-col no-padding>\n                    <button no-padding ion-button clear small color="primary">\n                  {{ item.price | currency:\'USD\':true:\'1.2\'}}\n                  </button>\n                </ion-col>\n                <ion-col (click)="selectItem(item)">\n\n                </ion-col>\n                <ion-col text-right no-padding>\n                    <button no-padding ion-button clear small [color]="item.isliked? \'danger\' : \'light\'" (click)="item.isliked = !item.isliked" icon-start>\n                  <ion-icon name=\'heart\'></ion-icon>\n                  {{ item.likes | number}}\n                </button>\n                </ion-col>\n            </ion-row>\n        </ion-item>\n    </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-one\sub-menu-one.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], SubMenuOnePage);
    return SubMenuOnePage;
}());

//# sourceMappingURL=sub-menu-one.js.map

/***/ }),

/***/ 849:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SplashOnePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SplashOnePage = (function () {
    function SplashOnePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SplashOnePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-splash-one',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-one\splash-one.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content text-center>\n    <img src="assets/imgs/logo-gray.png" alt="Logo">\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\splash-one\splash-one.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], SplashOnePage);
    return SplashOnePage;
}());

//# sourceMappingURL=splash-one.js.map

/***/ }),

/***/ 850:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewanalyticsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ViewanalyticsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewanalyticsPage = (function () {
    function ViewanalyticsPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Income',
                title: 'Income',
                description: 'See Trends on Your Income Streams',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Expenses',
                title: 'Expenses',
                description: 'Check Out What You Spend on',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 3,
                name: 'Savings and Investments',
                title: 'Savings and Investments',
                description: 'Take A Look At Your Assets',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'Liabilities',
                title: 'Liabilities',
                description: 'We Have Analysed Your Liabilities',
                img: 'assets/imgs/menu/breakfast.png'
            },
            {
                id: 5,
                name: 'Projects',
                title: 'Projects',
                description: 'How Close Are You To Your Goals ',
                img: 'assets/imgs/menu/munchies.png'
            },
        ];
    }
    ViewanalyticsPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad ViewanalyticsPage');
    };
    ViewanalyticsPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('IncomePage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('ExpensePage', param);
        }
        else if (cat.id == 3) {
            var param = { category: 3 };
            this.navCtrl.push('SavInvPage', param);
        }
        else if (cat.id == 4) {
            var param = { category: 4 };
            this.navCtrl.push('LiabilitiesPage', param);
        }
        else if (cat.id == 5) {
            var param = { category: 5 };
            this.navCtrl.push('ProjectsPage', param);
        }
    };
    ViewanalyticsPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    ViewanalyticsPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    ViewanalyticsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewanalytics',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewanalytics\viewanalytics.html"*/'<ion-header>\n  <ion-navbar color="tertiary">\n      <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n      <ion-title> View Analytics</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n      <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n      <div class="card-title">{{ cat.title }}</div>\n      <div class="card-subtitle">{{ cat.description }}</div> \n  </ion-card>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewanalytics\viewanalytics.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ViewanalyticsPage);
    return ViewanalyticsPage;
}());

//# sourceMappingURL=viewanalytics.js.map

/***/ }),

/***/ 851:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewbudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__signup_signup__ = __webpack_require__(33);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ViewbudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewbudgetPage = (function () {
    function ViewbudgetPage(navCtrl, storage, navParams) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.navParams = navParams;
        this.categories = [
            {
                id: 1,
                name: 'Living Expenses',
                title: 'Living Expenses',
                description: 'Check Out What You Spent',
                img: 'assets/imgs/menu/coffee.png'
            },
            {
                id: 2,
                name: 'Savings and Investments',
                title: 'Savings and Investments',
                description: 'Take A Look At Your Assets',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 3,
                name: 'Projects',
                title: 'Projects',
                description: 'How Close Are You To Your Goals ',
                img: 'assets/imgs/menu/munchies.png'
            },
            {
                id: 4,
                name: 'Debt Repayment',
                title: 'Debt Repayment',
                description: 'See What you Owe',
                img: 'assets/imgs/menu/breakfast.png'
            },
        ];
    }
    ViewbudgetPage.prototype.ionViewDidLoad = function () {
        this.load();
        console.log('ionViewDidLoad ViewbudgetPage');
    };
    ViewbudgetPage.prototype.analysisAction = function (cat) {
        /* Pass category object as a parameter */
        if (cat.id == 1) {
            var param = { category: 1 };
            this.navCtrl.push('ExpensebudgetPage', param);
        }
        else if (cat.id == 2) {
            var param = { category: 2 };
            this.navCtrl.push('SavInvbudgetPage', param);
        }
        else if (cat.id == 3) {
            var param = { category: 3 };
            this.navCtrl.push('ProjectbudgetPage', param);
        }
        else if (cat.id == 4) {
            var param = { category: 4 };
            this.navCtrl.push('DebtbudgetPage', param);
        }
        // else if(cat.id == 5){ 
        //   let param = { category: 5 };
        //   this.navCtrl.push('ProjectsbudgetPage', param );
        // }
    };
    ViewbudgetPage.prototype.ionViewWillEnter = function () {
        this.load();
    };
    ViewbudgetPage.prototype.load = function () {
        var _this = this;
        // this.storage.set('initialData', '');
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            // console.log(initialData.id);
            if (initialData == null) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__signup_signup__["a" /* SignupPage */]);
            }
            else if (initialData.id == 0) {
            }
        });
    };
    ViewbudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewbudget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewbudget\viewbudget.html"*/'<!--\n  Generated template for the ViewbudgetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n        <ion-icon name="menu"></ion-icon>\n      </button>\n        <ion-title> View Budget Analytics</ion-title>\n    </ion-navbar>\n  </ion-header>\n  \n  \n  <ion-content>\n    <ion-card *ngFor="let cat of categories" (click)="analysisAction(cat)">\n        <div class="img" [style.background]="\'url(\' + cat.img + \')\'"></div>\n        <div class="card-title">{{ cat.title }}</div>\n        <div class="card-subtitle">{{ cat.description }}</div> \n    </ion-card>\n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewbudget\viewbudget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], ViewbudgetPage);
    return ViewbudgetPage;
}());

//# sourceMappingURL=viewbudget.js.map

/***/ }),

/***/ 852:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewtransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ViewtransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewtransactionsPage = (function () {
    function ViewtransactionsPage(navCtrl, navParams, storage, alertCtrl, grsp, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.toastCtrl = toastCtrl;
        this.loaded = 0;
    }
    ViewtransactionsPage.prototype.ionViewDidLoad = function () {
        // let selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
        this.valName = this.navParams.get('val');
        this.getUserName();
    };
    ViewtransactionsPage.prototype.selectItem = function (item) {
        var toast = this.toastCtrl.create({
            message: 'item ' + item.title + ' Clicked',
            duration: 2500,
        });
        toast.present();
    };
    ViewtransactionsPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        // this.getTransactionsApi();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        // if (val && val.trim() != '') {
        //   this.transactions = this.transactions.filter((item) => {
        //     return (
        //     item.trans_bucket.toLowerCase().indexOf(val.toLowerCase()) > -1  || 
        //     item.trans_type.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_bucket_child.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_bucket_grandChild.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_amt.indexOf(val) > -1  || 
        //     item.trans_desc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
        //     item.trans_date.indexOf(val) > -1  );
        //   })
        // }
        if (val && val.trim() != '') {
            this.transactions = this.transactions.filter(function (item) { return item.trans_bucket.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_type.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_bucket_child.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_bucket_grandChild.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_amt.indexOf(val) > -1 ||
                item.trans_desc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
                item.trans_date.indexOf(val) > -1; });
        }
        console.log(this.transactions);
    };
    ViewtransactionsPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getTransactionsApi(_this.start, _this.end);
        });
    };
    ViewtransactionsPage.prototype.setApiJson = function (start, end) {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "start": start,
            "end": end
        };
        // });
    };
    ViewtransactionsPage.prototype.getTransactionsApi = function (start, end) {
        var _this = this;
        this.setApiJson(start, end);
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.transactions(this.apiData).then(function (data) {
            console.log(data);
            _this.trans = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.transactions = JSON.parse(_this.trans.transactions);
            _this.loaded = 1;
            alert = _this.alertCtrl.create({
                title: 'No name',
                subTitle: 'no Name' + _this.transactions[0],
                buttons: ['Dismiss']
            });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    ViewtransactionsPage.prototype.editTrans = function (trans) {
        this.navCtrl.push('EdittransactionsPage', {
            val: 'I am View Edit-trans',
            trans: trans
        });
    };
    ViewtransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-viewtransactions',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewtransactions\viewtransactions.html"*/'<!--\n  Generated template for the ViewtransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<!-- <ion-header>\n\n  <ion-navbar>\n    <ion-title>viewtransactions</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content> -->\n\n<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>{{userName}}\'s Transactions</ion-title>\n    </ion-navbar>\n\n    <!-- <br>\n    <div class="offer" text-center>\n    </div> -->\n</ion-header>\n\n\n<ion-content>\n    <ion-searchbar (ionInput)="getItems($event)"></ion-searchbar> <button ion-button small ><ion-icon name="refresh" (click)="getTransactionsApi()" color="secondary"></ion-icon></button>\n        \n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n    \n\n\n    <ion-list no-lines no-margin>\n            <ion-item *ngFor="let item of transactions">\n                <ion-thumbnail item-start (click)="editTrans(item)">\n                    <img src="assets/imgs/submenu/limeRefreshers.png">\n                </ion-thumbnail>\n                <h2 (click)="editTrans(item)"> {{item.trans_type == \'dr\' ? \'Debit\':\'Credit\'}}</h2>\n                <p (click)="editTrans(item)">{{ item.trans_desc }}</p>\n                <ion-row no-padding>\n                    <ion-col no-padding>\n                        <button no-padding ion-button clear small color="primary">\n                     NGN {{ item.trans_amt | number}}\n                      </button>\n                    </ion-col>\n                    <ion-col (click)="editTrans(item)">\n    \n                    </ion-col>\n                    <ion-col text-right no-padding>\n                        <button no-padding ion-button clear small [color]="\'light\'"  icon-start>\n                            {{item.trans_bucket}}\n                            <ion-icon name=\'trash\'></ion-icon>\n                    </button>\n                    </ion-col>\n                </ion-row>\n            </ion-item>\n        </ion-list>\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\viewtransactions\viewtransactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], ViewtransactionsPage);
    return ViewtransactionsPage;
}());

//# sourceMappingURL=viewtransactions.js.map

/***/ }),

/***/ 853:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WalkthroughPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var WalkthroughPage = (function () {
    function WalkthroughPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.slides = [
            {
                title: "Discover",
                description: "The Little Coffee Shop serves specialty coffee, fancy grilled cheese sandwiches, scratch cooking, craft ales, and cider.",
            },
            {
                title: "Taste",
                description: "You like your coffee Rich and Distinctive? Sweet and ight? Maybe Intense and Creamy? no worries, We've got it!",
            },
            {
                title: "Love",
                description: "So You Are a Coffe Lover? So as we! We serve coffee with all the Love in the world. Come Love with us.",
            }
        ];
    }
    WalkthroughPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WalkthroughPage');
    };
    WalkthroughPage.prototype.goToHomePage = function () {
        var toast = this.toastCtrl.create({
            message: 'Get Started Clicked',
            duration: 2500,
        });
        toast.present();
    };
    WalkthroughPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-walkthrough',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\walkthrough\walkthrough.html"*/'<ion-header>\n    <ion-navbar color="secondary">\n        <button ion-button menuToggle>\n          <ion-icon name="menu"></ion-icon>\n        </button>\n        <ion-title>Walkthrough</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content class="tutorial-page">\n    <ion-slides pager>\n        <ion-slide *ngFor="let slide of slides">\n            <div class="image-container">\n                <img [src]="\'assets/imgs/logo-gray.png\'" />\n            </div>\n            <h2 text-uppercase class="slide-title" [innerHTML]="slide.title"></h2>\n            <p [innerHTML]="slide.description"></p>\n            <button ion-button color="primary" (click)="goToHomePage()">\n              Get Started!\n            </button>\n        </ion-slide>\n    </ion-slides>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\walkthrough\walkthrough.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], WalkthroughPage);
    return WalkthroughPage;
}());

//# sourceMappingURL=walkthrough.js.map

/***/ }),

/***/ 854:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SubMenuTwoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SubMenuTwoPage = (function () {
    function SubMenuTwoPage(navCtrl, navParams, toastCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.items = [
            {
                id: 1,
                name: 'chocoFrappe',
                title: 'Choco Frappe',
                description: 'Chocolate Whirl',
                img: 'assets/imgs/submenu/chocoFrappe.png',
                category: 'coffe',
                price: '9',
                likes: '3200',
                isliked: false,
            },
            {
                id: 2,
                name: 'caramelFrappe',
                title: 'Caramel Frappe',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/caramelFrappe.png',
                category: 'coffe',
                price: '7.85',
                likes: '3456',
                isliked: false,
            },
            {
                id: 3,
                name: 'kickFrappe',
                title: 'Kick Frappe',
                description: 'Coffee Kick',
                img: 'assets/imgs/submenu/kickFrappe.png',
                category: 'coffe',
                price: '12.35',
                likes: '4450',
                isliked: false,
            },
            {
                id: 4,
                name: 'cappuccino',
                title: 'Cappuccino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/cappuccino.png',
                category: 'coffe',
                price: '10.65',
                likes: '2300',
                isliked: false,
            },
            {
                id: 5,
                name: 'icedAmericano',
                title: 'Iced Americano',
                description: 'Locally Roasted',
                img: 'assets/imgs/submenu/icedAmericano.png',
                category: 'coffe',
                price: '10.50',
                likes: '2300',
                isliked: false,
            },
            {
                id: 6,
                name: 'limeRefreshers',
                title: 'Lime Refreshers',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/limeRefreshers.png',
                category: 'coffe',
                price: '12',
                likes: '1900',
                isliked: false,
            },
            {
                id: 4,
                name: 'vanillaFrappucino',
                title: 'Vanilla Frappucino',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/vanillaFrappucino.png',
                category: 'coffe',
                price: '9.85',
                likes: '2300',
                isliked: false,
            },
            {
                id: 4,
                name: 'espresso',
                title: 'Espresso',
                description: 'Decaf Colombia',
                img: 'assets/imgs/submenu/espresso.png',
                category: 'coffe',
                price: '4.50',
                likes: '1800',
                isliked: false,
            },
        ];
    }
    SubMenuTwoPage.prototype.ionViewDidLoad = function () {
        var selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
    };
    SubMenuTwoPage.prototype.selectItem = function (item) {
        var toast = this.toastCtrl.create({
            message: 'item ' + item.title + ' Clicked',
            duration: 2500,
        });
        toast.present();
    };
    SubMenuTwoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sub-menu-two',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-two\sub-menu-two.html"*/'<ion-header>\n    <ion-navbar>\n        <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n        <ion-title>Coffee</ion-title>\n    </ion-navbar>\n\n    <br>\n    <div class="offer" text-center>\n        <h2>The Science of Delicious.</h2>\n        <p>Amazing coffees from around the world! </p>\n    </div>\n</ion-header>\n\n\n<ion-content>\n    <ion-grid no-padding>\n        <ion-row no-padding justify-content-center>\n            <ion-col no-padding col-6 align-self-center *ngFor="let item of items">\n                <ion-card>\n                    <ion-card-content>\n                        <ion-card-title (click)="selectItem(item)">\n                            {{ item.title }}\n                        </ion-card-title>\n                        <p (click)="selectItem(item)">{{ item.description }} </p>\n                        <img (click)="selectItem(item)" src="{{item.img}}" />\n                        <ion-row no-padding>\n                            <ion-col no-padding>\n                                <button no-padding ion-button clear small color="primary">\n                              {{ item.price | currency:\'USD\':true:\'1.2\'}}\n                              </button>\n                            </ion-col>\n                            <ion-col (click)="selectItem(item)">\n\n                            </ion-col>\n                            <ion-col text-right no-padding>\n                                <button no-padding ion-button clear small [color]="item.isliked? \'danger\' : \'light\'" (click)="item.isliked = !item.isliked" icon-start>\n                              <ion-icon name=\'heart\'></ion-icon>\n                              {{ item.likes | number}}\n                            </button>\n                            </ion-col>\n                        </ion-row>\n                    </ion-card-content>\n                </ion-card>\n            </ion-col>\n        </ion-row>\n    </ion-grid>\n</ion-content>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\sub-menu-two\sub-menu-two.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */]])
    ], SubMenuTwoPage);
    return SubMenuTwoPage;
}());

//# sourceMappingURL=sub-menu-two.js.map

/***/ }),

/***/ 872:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreditCardPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CreditCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreditCardPage = (function () {
    function CreditCardPage(navCtrl, alertCtrl, storage, grsp, navParams) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.grsp = grsp;
        this.navParams = navParams;
        this.loaded = 1;
    }
    CreditCardPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreditCardPage');
        this.getUserName();
    };
    CreditCardPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getCredApi();
        });
    };
    CreditCardPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "grp": "Credit"
        };
        // });
    };
    CreditCardPage.prototype.getCredApi = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        var alert = this.alertCtrl.create({
            title: 'No name',
            subTitle: 'no Name',
            buttons: ['Dismiss']
        });
        console.log(this.apiData);
        this.grsp.cards(this.apiData).then(function (data) {
            console.log(data);
            _this.cred = data;
            // this.transactions = this.trans.transactions.split(",");
            _this.credCrd = JSON.parse(_this.cred.cards);
            _this.loaded = 1;
            // alert = this.alertCtrl.create({
            //   title: 'No name',
            //   subTitle: 'no Name'+ this.investments[0],
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        }).catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
        // this.Carr = data;
        // console.log(data);
    };
    CreditCardPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-credit-card',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\credit-card\credit-card.html"*/'<!--\n  Generated template for the CreditCardPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>CreditCard</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n    <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n\n\n    <ion-card *ngFor="let item of credCrd">\n        <ion-card-content>\n          <img src="{{item.logo}}" alt="">\n<h4>{{item.card_banks}}</h4>\n<h2>{{item.ATM_withdrawal_abroad}}</h2>\n<p text-right>{{item.local_POS_withdrawals_limit}}</p>\n<p text-right>{{item.local_ATM_withdrawals_limit}}</p>\n        </ion-card-content>\n    </ion-card>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\credit-card\credit-card.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], CreditCardPage);
    return CreditCardPage;
}());

//# sourceMappingURL=credit-card.js.map

/***/ }),

/***/ 873:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaystackPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__paystack__ = __webpack_require__(360);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PaystackPageModule = (function () {
    function PaystackPageModule() {
    }
    PaystackPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__paystack__["a" /* PaystackPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__paystack__["a" /* PaystackPage */]),
            ],
        })
    ], PaystackPageModule);
    return PaystackPageModule;
}());

//# sourceMappingURL=paystack.module.js.map

/***/ }),

/***/ 874:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(509);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_home_home__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(13);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = (function () {
    function MyApp(platform, statusBar, storage, 
        // public navParams: NavParams,
        splashScreen) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.storage = storage;
        this.splashScreen = splashScreen;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */];
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_4__pages_home_home__["a" /* HomePage */] },
            // { title: 'Splash One', component: 'SplashOnePage' },
            // { title: 'Splash Two', component: 'SplashTwoPage' },
            // { title: 'Walkthrough', component: 'WalkthroughPage' },
            // { title: 'Menu One', component: 'MenuOnePage' },
            // { title: 'Menu Two', component: 'MenuTwoPage' },
            // { title: 'Submenu One', component: 'SubMenuOnePage' },
            // { title: 'Submenu Two', component: 'SubMenuTwoPage' },
            // { title: 'Order', component: 'OrderDetailsPage' },
            // { title: 'Favorites', component: 'FavoritesPage' },
            // { title: 'Profile', component: 'ProfilePage' },
            { title: 'Dashboard', component: 'DashboardPage' },
            // { title: 'Financial Info', component: 'FinInfoPage' },
            { title: 'Gaid Investment', component: 'InvestmentPage' },
            { title: 'Analysis', component: 'AnalysisPage' },
            { title: 'Budget', component: 'BudgetPage' },
            // { title: 'Initial', component: 'InitialPage' },
            { title: 'Setting', component: 'SettingPage' },
            { title: 'Sync', component: 'LoadsmstransactionsPage' },
            { title: 'Logout', component: 'InitialPage' },
        ];
        this.storage.get('initialData').then(function (initialData) {
            if (initialData == null || initialData.id == 0) {
                // let page = this.pages[4]
                var page = { title: 'Sign In', component: 'SignupPage' };
                // let page = this.pages[this.pages.length-3]
                _this.nav.setRoot(page.component);
                _this.activePage = page;
            }
            else {
                var page = _this.pages[1];
                _this.nav.setRoot(page.component);
                _this.activePage = page;
            }
        });
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.load();
        if (page.title == 'Logout') {
            this.storage.set('initialData', null);
            // let page = this.pages[this.pages.length-2]
            var page_1 = { title: 'Sign In', component: 'SignupPage' };
            this.nav.setRoot(page_1.component);
            this.activePage = page_1;
        }
        else {
            this.nav.setRoot(page.component);
            this.activePage = page;
        }
    };
    MyApp.prototype.load = function () {
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            console.log(initialData);
            if (initialData == null) {
                _this.nav.setRoot('SignupPage');
            }
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\app\app.html"*/'<ion-menu [content]="content">\n    <ion-content>\n        <div text-center class="menu-header">\n            <img src="assets/imgs/logo-gray.png" alt="">\n        </div>\n\n        <ion-list no-lines>\n            <ion-item class="menu-item" menuClose *ngFor="let page of pages" [class.highlight]="activePage == page" (click)="openPage(page)">\n                {{ page.title }}\n            </ion-item>\n        </ion-list>\n    </ion-content>\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[516]);
//# sourceMappingURL=main.js.map