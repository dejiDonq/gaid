webpackJsonp([6],{

/***/ 886:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadsmstransactionsPageModule", function() { return LoadsmstransactionsPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__loadsmstransactions__ = __webpack_require__(904);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoadsmstransactionsPageModule = (function () {
    function LoadsmstransactionsPageModule() {
    }
    LoadsmstransactionsPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__loadsmstransactions__["a" /* LoadsmstransactionsPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__loadsmstransactions__["a" /* LoadsmstransactionsPage */]),
            ],
        })
    ], LoadsmstransactionsPageModule);
    return LoadsmstransactionsPageModule;
}());

//# sourceMappingURL=loadsmstransactions.module.js.map

/***/ }),

/***/ 904:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoadsmstransactionsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_chooser__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(514);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__ = __webpack_require__(515);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { AndroidPermissions } from '@ionic-native/android-permissions';





/**
 * Generated class for the LoadsmstransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoadsmstransactionsPage = (function () {
    function LoadsmstransactionsPage(fileChooser, file, transfer, navCtrl, grsp, storage, 
        // public androidPermissions: AndroidPermissions,
        toastCtrl, platform, navParams) {
        this.fileChooser = fileChooser;
        this.file = file;
        this.transfer = transfer;
        this.navCtrl = navCtrl;
        this.grsp = grsp;
        this.storage = storage;
        this.toastCtrl = toastCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.messages = [];
        this.bucket = {
            income: ["Salary", "Allowances", "Dividend", "Rental Income", "Interest Income", "Other Income"],
            expenses: ["Food", "Groceries", "Transportation", "Taxi", "Bus", "Fuel", "Car Maintenance", "Utilies",
                "Electricity", "Phone Bills", "Airtime", "Cable", "Waste", "Cook", "Cleaner",
                "Laundry", "Rent", "Entertainment", "Knowledge", "Wife", "Education", "Subscriptions", "Shopping",
                "Children", "Parents", "Siblings", "Charity", "Vacation", "Personal", "Miscellaneous"],
            savingsAndInvestments: ["Equities", "Mutual Funds", "Real Estate", "Other Investments"],
            projects: ["New Apartment", "Party"],
            liabilities: ["Debts"]
        };
        this.feedback = 1;
    }
    LoadsmstransactionsPage.prototype.LoadFile = function () {
        var _this = this;
        this.fileChooser.open().then(function (uri) {
            console.log(uri);
            var fileTransfer = _this.transfer.create();
            var name = Math.floor((new Date).getTime() / 1000);
            console.log(name);
            var options1 = {
                fileKey: 'file',
                fileName: name + "_" + _this.uId + "",
                headers: {}
            };
            _this.feedback = 0;
            // fileTransfer.upload(uri, 'https://ampersandacademy.com/ionic/upload.php', options1)
            fileTransfer.upload(uri, 'http://gaid.io/myLeo/uploadExcel', options1)
                .then(function (data) {
                // success
                console.log("success");
                console.log(data);
                console.log(data.response);
                _this.feedback = 1;
                var dres = data.response;
                var lastfour = dres.substr(dres.length - 4);
                if (lastfour == "done") {
                    _this.uploadRes = "Done";
                    var toast = _this.toastCtrl.create({
                        message: 'Transactions Loaded and Saved Successfully',
                        duration: 4000,
                    });
                    toast.present();
                }
            }, function (err) {
                // error
                console.log("error" + JSON.stringify(err));
                console.log(err);
            });
            console.log("toh");
        });
    };
    // paystack(){
    //   let data = {
    //     price:"5000"
    //     };
    //   this.navCtrl.push(PaystackPage, data );
    // }
    LoadsmstransactionsPage.prototype.ionViewDidLoad = function () {
        this.buckets = this.grsp.buckets();
        console.log(this.buckets);
        console.log('ionViewDidLoad LoadsmstransactionsPage');
        this.getUserName();
        // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
        //   success => {
        //     console.log('Permission granted');
        //     // this.ReadListSMS();
        //   },
        // err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
        // );
        // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);
    };
    LoadsmstransactionsPage.prototype.setAddApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiAddData = {
            "uId": this.uId,
            "type": this.type,
            "desc": this.desc,
            "bucket": this.bucketFin,
            "amt": this.amt,
            "date": this.date
        };
        // });
        console.log(this.apiAddData);
    };
    LoadsmstransactionsPage.prototype.saveAddApi = function () {
        // this.loaded = 0;
        var _this = this;
        this.setAddApiJson();
        console.log(this.apiAddData);
        this.grsp.addTrans(this.apiAddData).then(function (data) {
            console.log(data);
            _this.apiRes = data;
            // this.loaded = 1;
            if (_this.apiRes.response == true) {
                var toast = _this.toastCtrl.create({
                    message: 'Transaction ' + _this.apiRes.desc + ' Saved Successfully',
                    duration: 2500,
                });
                toast.present();
                _this.type = '';
                _this.desc = '';
                _this.bucket = '';
                _this.amt = '';
                _this.date = '';
            }
            else {
                console.log('failed');
            }
        })
            .catch(function (err) {
            console.log('failed catch');
        });
    };
    // =============================================================================
    LoadsmstransactionsPage.prototype.getUserName = function () {
        var _this = this;
        // this.userName = "Tayo";
        this.storage.get('initialData').then(function (initialData) {
            // this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.getLastDateApi();
        });
    };
    LoadsmstransactionsPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId
        };
        // });
    };
    LoadsmstransactionsPage.prototype.getLastDateApi = function () {
        var _this = this;
        this.setApiJson();
        this.grsp.lastDate(this.apiData).then(function (data) {
            console.log(data);
            _this.lastDate = data;
            _this.lastDate = _this.lastDate.lastDate;
            _this.lastDate = new Date(_this.lastDate).valueOf();
            console.log(_this.lastDate);
            // this.ReadListSMS();
        }).catch(function (err) {
            // this.loaded = 1;
            // let alert = this.alertCtrl.create({
            //   title: 'Error',
            //   subTitle: 'Something broke and thats on us, Please try again',
            //   buttons: ['Dismiss']
            // });
            // alert.present();
        });
    };
    // ReadListSMS()
    // {
    // this.platform.ready().then((readySource) => {
    // let filter = {
    //        box : 'inbox', // 'inbox' (default), 'sent', 'draft'
    //        address : 'GTBank', // sender's phone number
    //       //  address : '+2348097239462', // sender's phone number
    //        indexFrom : 0, // start from index 0
    //        maxCount : 10 // count of SMS to return each time
    //             };
    //        if(SMS) SMS.listSMS(filter, (ListSms)=>{
    //         this.messages=ListSms;
    //         // alert ("sms is in/loaded");
    //         this.messages = this.messages.filter(msg => msg.date - this.lastDate > 500 ).reverse();
    //         // var sms = ListSms[ListSms.length-1];
    //         this.smsCount = this.messages.length;
    //         console.log(this.messages);
    //            console.log("Sms",ListSms);
    //           },
    //           Error=>{
    //           console.log('error list sms: ' + Error);
    //           });
    //     });
    // }
    LoadsmstransactionsPage.prototype.ionViewDidEnter = function () {
        // this.platform.ready().then((readySource) => {
        // if(SMS) SMS.startWatch(()=>{
        //           //  alert('watching started');
        //         }, Error=>{
        //        alert('failed to start watching');
        //    });
        //   document.addEventListener('onSMSArrive', (e:any)=>{
        //        var sms = e.data;
        //        if(sms.address == "+2348097239462" ){
        //         if(!sms.body.includes("commission")){
        //           let body = sms.body.replace(/\r\n/, "\n").split("\n");
        //           this.acct = body[0].substring(body[0].indexOf(":")).trim().substr(1);
        //           this.amt = body[1].substring(body[1].indexOf(":"), body[1].length - 2).trim().substr(1);
        //           this.type = (body[1].slice(-3).trim() == "DR") ? "DEBIT":"CREDIT";
        //           this.desc = body[2].substring(body[2].indexOf(":")).trim().substr(1);
        //           this.bal = body[body.length-1].substring(body[body.length-1].indexOf(":")).trim().substr(1);
        //           var arr;
        //           var n;
        //           this.bucketFin = this.desc;
        //          console.log(this.bal);
        //         }
        //        }
        //        });
        //     });
    };
    LoadsmstransactionsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-loadsmstransactions',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\loadsmstransactions\loadsmstransactions.html"*/'<!--\n  Generated template for the LoadsmstransactionsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar color="tertiary">\n        <button ion-button menuToggle>\n         <ion-icon name="menu"></ion-icon>\n       </button>\n        <!-- <ion-title>Analysis</ion-title> -->\n    <ion-title>Upload to Server</ion-title>\n    <!-- <ion-title>{{userName}}\'s Gaid Investment</ion-title> -->\n    </ion-navbar>\n\n  <!-- <ion-navbar>\n    <ion-title>Upload to Server</ion-title>\n  </ion-navbar> -->\n\n</ion-header>\n\n\n<ion-content padding>\n  <!-- <p color="danger" style="color:red">Please Import Whole Month\'s Data at Once to Avoid Irregularities</p> -->\n  <h4 style="color:red">Notice!!!</h4>\n  <ul style="color:red">\n  \n  <li>  Currently you can only upload your GTBank account statement. </li>\n  <li> Login to the internet banking on desktop, and download account statement for any period of time or send to your mail through the app. </li>\n  <li> Then upload here. </li>\n  <li> It is advisable to do it monthly</li>\n  </ul>\n  <!-- add instructions for upload here -->\n  <ion-spinner text-center *ngIf="feedback==0" name="bubbles" paused="false"></ion-spinner>\n\n  <button ion-button color="success" *ngIf="uId" block (click)="LoadFile()" round>Select File to Upload</button>\n  <!-- <p *ngIf="feedback==1">Success!!!</p> -->\n  <!-- <p>{{uId}}</p> -->\n\n\n    <ion-list>\n        <ion-item *ngFor="let x of messages">\n        <ion-avatar item-start>\n        <button ion-button color="danger" (click)="LoadMsg(x)" round>{{x.address}}</button>\n      </ion-avatar>\n      <h2>{{x.address}}</h2>\n      <p>{{x.body}}</p>\n      <!-- <p>"splitBody(x.body)"</p> -->\n    </ion-item>\n  </ion-list>\n  \n  <!-- <button ion-button color="danger" (click)="paystack()" round>pay</button> -->\n<!-- <hr>\n      <h2>Welcome to myLeo!</h2>\n  <p>\n    Acct: {{acct}}\n  </p>\n  <p>\n    Amt: {{amt}}\n  </p>\n  <p>\n    Type: {{type}}\n  </p>\n  <p>\n    Desc: {{desc}}\n  </p>\n  <p>\n    Bal: {{bal}}\n  </p>\n  <p>\n    Bucket: {{bucketFin}}\n  </p>\n  <p>\n    Date: {{date}}\n  </p>\n  <p>\n    LDate: {{lastDate}}\n  </p> -->\n  <p >\n        {{uploadRes}}\n   <!-- {{smsCount}} New Messages Found, <button ion-button color="primary" *ngIf="smsCount > 0" (click)="syncSMS(messages)" round>Sync</button> -->\n  </p>\n\n</ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\loadsmstransactions\loadsmstransactions.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__ionic_native_file_chooser__["a" /* FileChooser */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */]])
    ], LoadsmstransactionsPage);
    return LoadsmstransactionsPage;
}());

//# sourceMappingURL=loadsmstransactions.js.map

/***/ })

});
//# sourceMappingURL=6.js.map