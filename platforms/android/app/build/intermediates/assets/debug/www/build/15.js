webpackJsonp([15],{

/***/ 877:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DebtbudgetPageModule", function() { return DebtbudgetPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__debtbudget__ = __webpack_require__(895);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DebtbudgetPageModule = (function () {
    function DebtbudgetPageModule() {
    }
    DebtbudgetPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__debtbudget__["a" /* DebtbudgetPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__debtbudget__["a" /* DebtbudgetPage */]),
            ],
        })
    ], DebtbudgetPageModule);
    return DebtbudgetPageModule;
}());

//# sourceMappingURL=debtbudget.module.js.map

/***/ }),

/***/ 895:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DebtbudgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_chart_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_chart_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the DebtbudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DebtbudgetPage = (function () {
    function DebtbudgetPage(navCtrl, navParams, storage, alertCtrl, grsp) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.grsp = grsp;
        this.loaded = 0;
        this.barLabels = [];
        this.barLabelNums = [];
        this.barLabelYears = [];
        this.month = __WEBPACK_IMPORTED_MODULE_5_moment__();
        this.totalIncome = [];
        this.barBudgetData = [];
    }
    DebtbudgetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DebtbudgetPage');
        this.getUserName();
    };
    DebtbudgetPage.prototype.getUserName = function () {
        // this.userName = "Tayo";
        var _this = this;
        this.storage.get('initialData').then(function (initialData) {
            _this.userName = initialData.name.toUpperCase();
            _this.uId = initialData.id;
            _this.inc = initialData.inc;
            _this.barSetup();
        });
    };
    DebtbudgetPage.prototype.setApiJson = function () {
        // this.storage.get('initialData').then((initialData) => {
        this.apiData = {
            "uId": this.uId,
            "months": this.barLabelNums.join(),
            "years": this.barLabelYears.join(),
        };
        // });
    };
    DebtbudgetPage.prototype.getTransSumByMonth = function () {
        var _this = this;
        this.loaded = 0;
        this.setApiJson();
        this.grsp.getTransSumByMonth(this.apiData).then(function (data) {
            console.log(data);
            _this.retval = data;
            console.log(JSON.parse(_this.retval.budget));
            var income = JSON.parse(_this.retval.sumIncome);
            var budget = JSON.parse(_this.retval.budget);
            budget.forEach(function (element, ind) {
                var expPercent = element[0] ? element[0].b_debt : 0;
                // let exp = (expPercent/100) * (element[0] ? element[0].b_income : income[ind]);
                var exp = (expPercent / 100) * (element[0] ? element[0].b_income : _this.inc);
                _this.barBudgetData.push(exp);
            });
            console.log(_this.barBudgetData);
            _this.barActualData = JSON.parse(_this.retval.sumLiab);
            _this.loaded = 1;
            _this.lineLabels = _this.barLabels;
            _this.lineBudgetData = _this.barBudgetData;
            _this.lineActualData = _this.barActualData;
            _this.bar();
            _this.liNe();
        })
            .catch(function (err) {
            _this.loaded = 1;
            var alert = _this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
            });
            alert.present();
        });
    };
    DebtbudgetPage.prototype.liNe = function () {
        // if (this.toggleVarLine){
        this.lineChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.lineCanvas.nativeElement, {
            type: 'line',
            data: {
                // labels: ["January", "February", "March", "April", "May", "June", "July"],
                labels: this.lineLabels,
                datasets: [
                    {
                        label: "Budget(%)",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,92,192,0.4)",
                        borderColor: "rgba(75,92,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,92,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,92,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 55, 90, 21, 78, 95, 20],
                        data: this.lineBudgetData,
                        spanGaps: false,
                    },
                    {
                        label: "Actual(%)",
                        fill: false,
                        lineTension: 0.1,
                        backgroundColor: "rgba(75,192,192,0.4)",
                        borderColor: "rgba(75,192,192,1)",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "rgba(75,192,192,1)",
                        pointBackgroundColor: "#fff",
                        pointBorderWidth: 1,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "rgba(75,192,192,1)",
                        pointHoverBorderColor: "rgba(220,220,220,1)",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        // data: [65, 59, 80, 81, 56, 55, 40],
                        data: this.lineActualData,
                        spanGaps: false,
                    }
                ]
            },
            options: {
                scales: {
                    xAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }]
                },
            }
        });
        // }
    };
    DebtbudgetPage.prototype.bar = function () {
        this.barChart = new __WEBPACK_IMPORTED_MODULE_3_chart_js__["Chart"](this.barCanvas.nativeElement, {
            type: 'bar',
            data: {
                // labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                labels: this.barLabels,
                datasets: [{
                        label: 'Budget(%)',
                        data: this.barBudgetData,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Actual(%)',
                        // data: [12, 19, 13, 5, 2, 13],
                        data: this.barActualData,
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(5, 162, 235, 0.2)',
                            'rgba(55, 206, 86, 0.2)',
                            'rgba(50, 192, 192, 0.2)',
                            'rgba(13, 102, 255, 0.2)',
                            'rgba(250, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                }
            }
        });
    };
    DebtbudgetPage.prototype.barSetup = function () {
        var i = 6;
        var tday = this.month;
        while (i > 0) {
            // console.log(tday.format('MMMM'));
            this.barLabels.push(tday.format('MMM'));
            this.barLabelNums.push(tday.format('M'));
            this.barLabelYears.push(tday.format('Y'));
            tday = tday.subtract(1, 'month');
            i--;
        }
        console.log(this.barLabels);
        console.log(this.barLabelNums);
        console.log(this.barLabelYears);
        // this.barLabels = this.barLabels.reverse();
        // this.barLabels.forEach((element, ind) => {
        // });
        this.getTransSumByMonth();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('lineCanvas'),
        __metadata("design:type", Object)
    ], DebtbudgetPage.prototype, "lineCanvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('barCanvas'),
        __metadata("design:type", Object)
    ], DebtbudgetPage.prototype, "barCanvas", void 0);
    DebtbudgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-debtbudget',template:/*ion-inline-start:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\debtbudget\debtbudget.html"*/'<!--\n  Generated template for the DebtbudgetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <div class="logo-container" text-center [style.display]="\'block\'">\n      <img src="assets/imgs/logo.png" alt="Logo">\n\n      <!-- <ion-segment [(ngModel)]="screen" mode="md" align-self-end text-uppercase text-center color="secondary">\n          <ion-segment-button value="signin">\n              Sign In\n          </ion-segment-button>\n         \n      </ion-segment> -->\n      \n      <ion-navbar>\n    <ion-title>Debt Budget Performance</ion-title>\n        \n      </ion-navbar>\n\n  </div>\n\n  \n\n</ion-header>\n  \n  \n  <ion-content padding>\n      <ion-spinner text-center *ngIf="loaded==0" name="bubbles" paused="false"></ion-spinner>\n  \n      <ion-grid >\n          <ion-row justify-content-center align-items-center >\n              \n            <!-- <button ion-button small (click)="week()" >Week</button>\n              <button ion-button small (click)="month()" >Month</button>\n              <button ion-button small (click)="year()" >Year</button>\n              <button ion-button small (click)="range()" >Range</button> -->\n        \n          </ion-row>\n        </ion-grid>\n        \n        <ion-card>\n          <ion-card-header >\n            Line Chart\n          </ion-card-header>\n        <ion-card-content>\n            <canvas #lineCanvas></canvas>\n         </ion-card-content>\n      </ion-card>\n  \n      <ion-card>\n          \n        <ion-card-header >\n            Bar Chart\n          </ion-card-header>\n        <ion-card-content>\n            <canvas #barCanvas></canvas>\n         </ion-card-content>\n      </ion-card>\n  \n  </ion-content>\n'/*ion-inline-end:"C:\Users\abdurraheem.abdul-ma\Downloads\personal stuff\leonine\littleCoffeeShop\src\pages\debtbudget\debtbudget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_global_rest_service_global_rest_service__["a" /* GlobalRestServiceProvider */]])
    ], DebtbudgetPage);
    return DebtbudgetPage;
}());

//# sourceMappingURL=debtbudget.js.map

/***/ })

});
//# sourceMappingURL=15.js.map