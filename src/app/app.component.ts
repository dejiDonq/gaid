import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { Storage } from '@ionic/storage';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  activePage: any;
  toLoad: any;
  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, 
    public statusBar: StatusBar,     
    private storage: Storage,
    // public navParams: NavParams,
    public splashScreen: SplashScreen) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      // { title: 'Splash One', component: 'SplashOnePage' },
      // { title: 'Splash Two', component: 'SplashTwoPage' },
      // { title: 'Walkthrough', component: 'WalkthroughPage' },
      // { title: 'Menu One', component: 'MenuOnePage' },
      // { title: 'Menu Two', component: 'MenuTwoPage' },
      // { title: 'Submenu One', component: 'SubMenuOnePage' },
      // { title: 'Submenu Two', component: 'SubMenuTwoPage' },
      // { title: 'Order', component: 'OrderDetailsPage' },
      // { title: 'Favorites', component: 'FavoritesPage' },
      // { title: 'Profile', component: 'ProfilePage' },
      { title: 'Dashboard', component: 'DashboardPage' },
      // { title: 'Financial Info', component: 'FinInfoPage' },
      { title: 'Gaid Investment', component: 'InvestmentPage' },
      { title: 'Analysis', component: 'AnalysisPage' },
      { title: 'Budget', component: 'BudgetPage' },
      // { title: 'Initial', component: 'InitialPage' },
      { title: 'Setting', component: 'SettingPage' },        
      { title: 'Sync', component: 'LoadsmstransactionsPage' },        
      { title: 'Logout', component: 'InitialPage' },
    ];



    this.storage.get('initialData').then((initialData) => {

      if(initialData == null || initialData.id == 0){

        // let page = this.pages[4]
        let page =  { title: 'Sign In', component: 'SignupPage' };

        // let page = this.pages[this.pages.length-3]

        this.nav.setRoot(page.component);

        this.activePage = page;

      }
      else{
        
        let page = this.pages[1]

        this.nav.setRoot(page.component);

        this.activePage = page;

      }

    });


  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }



  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario

    this.load();

    if(page.title == 'Logout'){

    this.storage.set('initialData', null);

    // let page = this.pages[this.pages.length-2]

    let page =  { title: 'Sign In', component: 'SignupPage' };

        this.nav.setRoot(page.component);

        this.activePage = page;

    }
    else{
      

      this.nav.setRoot(page.component);
      this.activePage = page;
    }
  }




  load(){
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);

      if (initialData == null){
        this.nav.setRoot('SignupPage');

      }

     
    });

  }




}
