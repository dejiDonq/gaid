import { CreditCardPageModule } from './../pages/credit-card/credit-card.module';
import { PrepaidCardPageModule } from './../pages/prepaid-card/prepaid-card.module';
import { ForexCardPageModule } from './../pages/forex-card/forex-card.module';
import { NairaCardPageModule } from './../pages/naira-card/naira-card.module';
import { CardsPageModule } from './../pages/cards/cards.module';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { PaystackPageModule } from './../pages/paystack/paystack.module';

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SplashOnePageModule } from './../pages/splash-one/splash-one.module';
import { SplashTwoPageModule } from '../pages/splash-two/splash-two.module';
import { SignupPageModule } from '../pages/signup/signup.module';
import { WalkthroughPageModule } from '../pages/walkthrough/walkthrough.module';
import { MenuOnePageModule } from './../pages/menu-one/menu-one.module';
import { MenuTwoPageModule } from './../pages/menu-two/menu-two.module';
import { SubMenuOnePageModule } from './../pages/sub-menu-one/sub-menu-one.module';
import { SubMenuTwoPageModule } from '../pages/sub-menu-two/sub-menu-two.module';
import { OrderDetailsPageModule } from '../pages/order-details/order-details.module';
import { FavoritesPageModule } from '../pages/favorites/favorites.module';
import { ProfilePageModule } from './../pages/profile/profile.module';
import { SettingPageModule } from './../pages/setting/setting.module';
import { GlobalRestServiceProvider } from '../providers/global-rest-service/global-rest-service';

// import { AndroidPermissions } from '@ionic-native/android-permissions';

import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';

// =======================================================================================================
import { DashboardPage } from './../pages/dashboard/dashboard';
import { HttpModule } from '@angular/http';
<<<<<<< HEAD
import { HTTP } from '@ionic-native/http';

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
import { InitialPageModule } from '../pages/initial/initial.module';
import { DashboardPageModule } from '../pages/dashboard/dashboard.module';
import { IonicStorageModule } from '@ionic/storage';
import { AnalysisPageModule } from '../pages/analysis/analysis.module';
// import { ViewanalyticsPage } from '../pages/viewanalytics/viewanalytics';
import { ViewanalyticsPageModule } from '../pages/viewanalytics/viewanalytics.module';
import { AddtransactionsPageModule } from '../pages/addtransactions/addtransactions.module';
import { ViewtransactionsPageModule } from '../pages/viewtransactions/viewtransactions.module';
import { EdittransactionsPageModule } from '../pages/edittransactions/edittransactions.module';
import { InvestmentPageModule } from '../pages/investment/investment.module';

// =======================================================================================================
// import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { ComponentsModule } from '../components/components.module';
import { BudgetPageModule } from '../pages/budget/budget.module';
import { ManagebudgetPageModule } from '../pages/managebudget/managebudget.module';
import { ViewbudgetPageModule } from './../pages/viewbudget/viewbudget.module';
import { FinInfoPageModule } from '../pages/fin-info/fin-info.module';
import { BankingPageModule } from '../pages/banking/banking.module';
// =======================================================================================================

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    // ===========================
    // ProgressBarComponent,
    // ===========================
    // DashboardPage
    
    // ===========================
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      mode:'ios',
      backButtonText: '',
    }),
    IonicStorageModule.forRoot(),
    SplashOnePageModule,
    SplashTwoPageModule,
    SignupPageModule,
    WalkthroughPageModule,
    MenuOnePageModule,
    MenuTwoPageModule,
    SubMenuOnePageModule,
    SubMenuTwoPageModule,
    OrderDetailsPageModule,
    FavoritesPageModule,
    ProfilePageModule,
    SettingPageModule,
    InitialPageModule,
    DashboardPageModule,
    AnalysisPageModule,
    ViewanalyticsPageModule,
    AddtransactionsPageModule,
    ViewtransactionsPageModule,
    EdittransactionsPageModule,
    InvestmentPageModule,
    BudgetPageModule,
    ManagebudgetPageModule,
    ViewbudgetPageModule,
    ComponentsModule,
    HttpModule,
    PaystackPageModule,
    FinInfoPageModule,
    BankingPageModule,
    CardsPageModule,
    NairaCardPageModule,
    ForexCardPageModule,
    PrepaidCardPageModule,
    CreditCardPageModule


    


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    // ===========================
    // DashboardPage

    // ===========================
  ],
  providers: [
    // AndroidPermissions,
    StatusBar,
    // NavParams,
    SplashScreen,
    FileChooser,
    File,
    FileTransfer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    GlobalRestServiceProvider,
<<<<<<< HEAD
    HTTP,
=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
    InAppBrowser
  ]
})
export class AppModule {}
