import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';
import { Storage } from '@ionic/storage';

import { SignupPage } from '../signup/signup';
import { PaystackPage } from '../paystack/paystack';



/**
 * Generated class for the InvestmentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-investment',
  templateUrl: 'investment.html',
})
export class InvestmentPage {

  valName: any;
  userName: any;
  // type: any;
  // prov: any;
  amount: any;
  showHide:any = false;
  uId:any;
  freq:any;
  deductDate:any;
  recur:any = false;


  model:any= {};

  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
    private storage: Storage,
    public grsp: GlobalRestServiceProvider,
    public navParams: NavParams) {
  }

  ionViewWillEnter(){
    this.load();
  
  }

  ionViewDidLoad() {

    this.load();
    // let payRes = this.navParams.get('res');

    console.log('ionViewDidLoad InvestmentPage');
    this.valName = this.navParams.get('val');
    this.getUserName();

//     if (payRes == "success"){
//       // this.buyInvestApi();
// console.log(payRes);

// this.storage.get('investData').then((data) => {
//   console.log(data);

//   this.apiData = data;

//   this.buyInvestApi();

//   console.log("done");


// });


//     }
//     else{
//       // alert("Something Went Wrong, Please Contact Admin");
//     }

    
   

  }

  totInvest:any;
<<<<<<< HEAD
  initialData:any;
=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298

  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
<<<<<<< HEAD
      this.initialData = initialData;
=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
      this.getInvestmentsApi();

    });
  }



  apiData:any;

  setApiJson(){
    // this.storage.get('initialData').then((initialData) => {

    let md = {
      "recurring": this.recur,
      "frequency": this.freq,
      "deductDate": this.deductDate,
    };

    // md = JSON.stringify(md);

      this.apiData = {
      "uId":this.uId,
      "type":"Gaid",
      "prov":"Gaid",
      "amt":this.amount,
      "model": JSON.stringify(md)
    };

    console.log(this.apiData);
  // });

  }



  apiRes:any;
  loaded:any=1;


  goPay(){
    this.setApiJson();
    
    let data = {

<<<<<<< HEAD
      price:this.amount,
      // data:this.apiData
      email:this.initialData.email
=======
      price:this.amount
      // data:this.apiData

>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
      
      };

      console.log(data);
      this.storage.set('investData', this.apiData);
  
    this.navCtrl.push(PaystackPage, data );
  }




  buyInvestApi(){

    // this.setApiJson();
    this.loaded = 0;


    console.log(this.apiData);
  console.log("done");


    this.grsp.buyInvestment(this.apiData).then(data => {

      console.log(data);

      this.apiRes = data;
<<<<<<< HEAD
      this.apiRes = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298

      this.loaded = 1;

      if(this.apiRes.response == true){

        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Successfully Saved NGN'+this.amount+' of '+this.apiRes.prov+' Investment ',
          buttons: ['Ok']
        });
        alert.present();

      }
      else{

        let alert = this.alertCtrl.create({
          title: 'Failed',
          subTitle: 'Something went wrong, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();

      }

    }).catch((err) => {
      this.loaded = 1;
  
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Something broke and thats on us, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();
      });


      this.storage.set('investData', null);


  }




  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }



  // ===============================================
  apiInvData:any;
  more:any = 0;


  toggleMore(){
    if(this.more == 0){
      this.more = 1;
    }
    else if(this.more == 1){
      this.more = 0;
    }
  }

  setInvApiJson(){

    // this.storage.get('initialData').then((initialData) => {

    this.apiInvData = {
      "uId":this.uId
    };

    // });

  }


  inv:any;
  investments:any;

  getInvestmentsApi(){

    this.setInvApiJson();

      let alert = this.alertCtrl.create({
        title: 'No name',
        subTitle: 'no Name',
        buttons: ['Dismiss']
      });

    console.log(this.apiInvData);

      this.grsp.investments(this.apiInvData).then(data => {
        console.log(data);

        this.inv = data;
<<<<<<< HEAD
      this.inv = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
        // this.transactions = this.trans.transactions.split(",");
        this.investments = JSON.parse(this.inv.investments);
        this.investments = this.investments.reverse();



        this.totInvest = 0;
        this.investments.forEach((obj)=>{
          obj.actAmt = this.numberWithCommas(obj.inv_amt);
          this.totInvest += parseInt(obj.inv_amt);
        });

        this.totInvest = this.numberWithCommas(this.totInvest);

        this.loaded = 1;

        // alert = this.alertCtrl.create({
        //   title: 'No name',
        //   subTitle: 'no Name'+ this.investments[0],
        //   buttons: ['Dismiss']
        // });
        // alert.present();

        }).catch((err) => {
          this.loaded = 1;
    
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Something broke and thats on us, Please try again',
            buttons: ['Dismiss']
          });
          alert.present();
        });
        // this.Carr = data;
        // console.log(data);

    }

  // ===============================================
  numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
  }
  // ===============================================


}

