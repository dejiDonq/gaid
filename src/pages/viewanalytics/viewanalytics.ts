import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';

import { Storage } from '@ionic/storage';



import { SignupPage } from '../signup/signup';



/**
 * Generated class for the ViewanalyticsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewanalytics',
  templateUrl: 'viewanalytics.html',
})
export class ViewanalyticsPage {

  categories = [
    {
      id: 1,
      name: 'Income',
      title: 'Income',
      description: 'See Trends on Your Income Streams',
      img: 'assets/imgs/menu/coffee.png'
    },
    {
      id: 2,
      name: 'Expenses',
      title: 'Expenses',
      description: 'Check Out What You Spend on',
      img: 'assets/imgs/menu/breakfast.png'
    },
    {
      id: 3,
      name: 'Savings and Investments',
      title: 'Savings and Investments',
      description: 'Take A Look At Your Assets',
      img: 'assets/imgs/menu/munchies.png'
    },
    {
      id: 4,
      name: 'Liabilities',
      title: 'Liabilities',
      description: 'We Have Analysed Your Liabilities',
      img: 'assets/imgs/menu/breakfast.png'
    },
    {
      id: 5,
      name: 'Projects',
      title: 'Projects',
      description: 'How Close Are You To Your Goals ',
      img: 'assets/imgs/menu/munchies.png'
    },
   
  ];

  constructor(public navCtrl: NavController,
    private storage: Storage,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {

    this.load();

    console.log('ionViewDidLoad ViewanalyticsPage');
  }


  analysisAction(cat) {
    /* Pass category object as a parameter */
    if(cat.id == 1){
      let param = { category: 1 };
      this.navCtrl.push('IncomePage', param );
    }
    else if(cat.id == 2){
      let param = { category: 2 };
      this.navCtrl.push('ExpensePage', param );
    }
    else if(cat.id == 3){ 
      let param = { category: 3 };
      this.navCtrl.push('SavInvPage', param );
    }
    else if(cat.id == 4){ 
      let param = { category: 4 };
      this.navCtrl.push('LiabilitiesPage', param );
    }
    else if(cat.id == 5){ 
      let param = { category: 5 };
      this.navCtrl.push('ProjectsPage', param );
    }
  
  }


  ionViewWillEnter(){
    this.load();
  
  }


  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }



}
