import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewanalyticsPage } from './viewanalytics';

@NgModule({
  declarations: [
    ViewanalyticsPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewanalyticsPage),
  ],
})
export class ViewanalyticsPageModule {}
