import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IncomebudgetPage } from './incomebudget';

@NgModule({
  declarations: [
    IncomebudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(IncomebudgetPage),
  ],
})
export class IncomebudgetPageModule {}
