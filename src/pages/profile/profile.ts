import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) {}


  changeImage(){
    let toast = this.toastCtrl.create({
      message: 'Change Image Button Clicked',
      duration: 2500,
    });
    toast.present();
  }

  follow(){
    let toast = this.toastCtrl.create({
      message: 'Follow Button Clicked',
      duration: 2500,
    });
    toast.present();
  }

}
