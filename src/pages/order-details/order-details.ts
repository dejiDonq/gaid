import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-order-details',
  templateUrl: 'order-details.html',
})
export class OrderDetailsPage {

  order = {
    item: {
      id: 10,
      name: 'chocolateMuffin',
      title: 'Chocolate Muffin',
      description: "Our muffin unites rich, dense chocolate with a gooey caramel center for bliss in every bite. As far as we're concerned, there's no such thing as too much caramel.",
      img: 'assets/imgs/submenu/chocolateMuffin.png',
      category: 'muffin',
      price: '10',
      likes: '1800',
      isliked: true,
    },
    quantity: 2,
  };
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) { }

  placeOrder(){
    let toast = this.toastCtrl.create({
      message: 'Placing Order of ' + this.order.quantity + ' ' + this.order.item.title,
      duration: 2500
    });

    toast.present();
  }

}
