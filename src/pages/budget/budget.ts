import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignupPage } from '../signup/signup';

import { Storage } from '@ionic/storage';


/**
 * Generated class for the BudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-budget',
  templateUrl: 'budget.html',
})
export class BudgetPage {

  categories = [
    {
      id: 1,
      name: 'View Budget Analytics',
      title: 'View Budget',
      description: 'See How Your Numbers Are Looking',
      img: 'assets/imgs/menu/coffee.png'
    },
   
    {
      id: 2,
      name: 'Manage Budget(s)',
      title: 'Manage Budget(s)',
      description: 'Because You Have Full Control',
      img: 'assets/imgs/menu/munchies.png'
    },
   
  ];

  constructor(public navCtrl: NavController, 
    private storage: Storage,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BudgetPage');
  }

  ionViewWillEnter(){
    this.load();
  
  }

  analysisAction(cat) {
    /* Pass category object as a parameter */
    if(cat.id == 1){
      let param = { category: 1 };
      this.navCtrl.push('ViewbudgetPage', param );
    }
    else if(cat.id == 2){
      let param = { category: 2 };
      this.navCtrl.push('ManagebudgetPage', param );
    }
   
  
  }



  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }


}
