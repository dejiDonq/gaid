import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavInvPage } from './sav-inv';

@NgModule({
  declarations: [
    SavInvPage,
  ],
  imports: [
    IonicPageModule.forChild(SavInvPage),
  ],
})
export class SavInvPageModule {}
