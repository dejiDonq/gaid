import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController} from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { Chart } from 'chart.js';

import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';


import * as moment from 'moment';

/**
 * Generated class for the IncomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-income',
  templateUrl: 'income.html',
})
export class IncomePage {


  valName: any;
  userName: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private alertCtrl: AlertController,
    public grsp: GlobalRestServiceProvider) {
  }

  ionViewDidLoad() {

    let tday = moment().format("YYYY-MM-DD").toString();
    console.log(tday);
    
    // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
    let yday = moment().subtract(1, 'month').format("YYYY-MM-DD").toString();
    console.log(yday);

    this.end = tday;
    this.start = yday;

    console.log('ionViewDidLoad IncomePage');
    this.valName = this.navParams.get('val');

    this.getUserName();


    let now = moment().format('LLLL');
   console.log(now);

  }

  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any;

  toggleVar:any = true;
  toggleVarLine:any = true;



dash:any;
donutLabels:any;
donutData:any;
lineIncomeData:any;
lineExpenseData:any;

loaded:any = 0;





  doNut(){
    if(this.toggleVar){
    console.log(this.donutLabels);

    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
          // labels: ["Food", "Utility", "Airtime", "Investments", "Entertainment", "Travel"],
          // labels: this.donutLabels,
          labels: this.pieLabels,
          datasets: [{
              label: 'User Personal Finance Status',
              // data: [12, 19, 3, 5, 2, 3],
              data: this.pieValues,
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56",
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ]
          }]
      }

  });

  }
  }




  liNe(){
    if (this.toggleVarLine){

    this.lineChart = new Chart(this.lineCanvas.nativeElement, {

      type: 'line',
      data: {
          // labels: ["January", "February", "March", "April", "May", "June", "July"],
          labels: this.lineLabels,
          datasets: [
              {
                  label: "Income",
                  fill: false,
                  lineTension: 0.1,
                  backgroundColor: "rgba(75,92,192,0.4)",
                  borderColor: "rgba(75,92,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(75,92,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(75,92,192,1)",
                  pointHoverBorderColor: "rgba(220,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  // data: [65, 55, 90, 21, 78, 95, 20],
                  data: this.lineValues,
                  spanGaps: false,
              },
              // {
              //     label: "Expense",
              //     fill: false,
              //     lineTension: 0.1,
              //     backgroundColor: "rgba(75,192,192,0.4)",
              //     borderColor: "rgba(75,192,192,1)",
              //     borderCapStyle: 'butt',
              //     borderDash: [],
              //     borderDashOffset: 0.0,
              //     borderJoinStyle: 'miter',
              //     pointBorderColor: "rgba(75,192,192,1)",
              //     pointBackgroundColor: "#fff",
              //     pointBorderWidth: 1,
              //     pointHoverRadius: 5,
              //     pointHoverBackgroundColor: "rgba(75,192,192,1)",
              //     pointHoverBorderColor: "rgba(220,220,220,1)",
              //     pointHoverBorderWidth: 2,
              //     pointRadius: 1,
              //     pointHitRadius: 10,
              //     // data: [65, 59, 80, 81, 56, 55, 40],
              //     data: this.lineExpenseData,
              //     spanGaps: false,
              // }
          ]
      },
      options:{
           scales: {
          xAxes: [{
              gridLines: {
                  color: "rgba(0, 0, 0, 0)",
              }
          }],
          yAxes: [{
              gridLines: {
                  color: "rgba(0, 0, 0, 0)",
              }   
          }]
        },
      }

  });
  }
  }




  getUserName(){
    // this.userName = "Tayo";

    this.storage.get('initialData').then((initialData) => {
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
      this.getTransactionsApi(this.start,this.end);
    });


  }


  week(){
    let weekStart = moment().startOf('isoWeek').format('YYYY-MM-DD HH:mm:ss');
    let weekEnd = moment().format('YYYY-MM-DD HH:mm:ss');
    this.getTransactionsApi(weekStart, weekEnd);

  }
 
  month(){
    let start = moment().startOf('month').format('YYYY-MM-DD HH:mm:ss');
    let end   = moment().format('YYYY-MM-DD HH:mm:ss');

    this.getTransactionsApi(start, end);

  }
 
  year(){
    let start = moment().startOf('year').format('YYYY-MM-DD HH:mm:ss');
    let end   = moment().format('YYYY-MM-DD HH:mm:ss');

    this.getTransactionsApi(start, end);

  }
 
  range(){

    this.drange = !this.drange;

  }


  getRange(){
    this.getTransactionsApi(this.from, this.to);

  }


// =================================================================
apiData:any;
drange:any=false;
uId:any;
from:any;
to:any;
transactions:any;

start:any;
end:any;

pieLabels:any;
pieValues:any = [];

lineLabels:any;
lineValues:any = [];


  setApiJson(start?,end?){

    // this.storage.get('initialData').then((initialData) => {

      this.apiData = {
        "uId":this.uId,
        "start": start,
        "end": end
      };

    // });

  }



  trans:any;

  getTransactionsApi(start?, end?){
    this.loaded = 0;


    this.setApiJson(start,end);

      let alert = this.alertCtrl.create({
        title: 'No name',
        subTitle: 'no Name',
        buttons: ['Dismiss']
      });

    console.log(this.apiData);

    this.lineLabels= [];
    this.lineValues= [];
    this.pieLabels= [];
    this.pieValues= [];

      this.grsp.transactions(this.apiData).then(data => {
        // console.log(data);

        this.trans = data;
<<<<<<< HEAD
      this.trans = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
        // this.transactions = this.trans.transactions.split(",");
        this.transactions = JSON.parse(this.trans.transactions);
        this.transactions  = this.transactions.filter(tr => tr.trans_bucket == 'Income');
        // get all
        this.pieLabels =  this.transactions.map(value => value.trans_bucket_child);
        console.log(this.transactions);
        console.log(this.pieLabels);
        // get unique
        this.pieLabels =  this.pieLabels.filter((v, i, a) => a.indexOf(v) === i); 
        console.log(this.pieLabels);
        let isArr = (Array.isArray(this.pieLabels));

        console.log(isArr);


        this.pieLabels.forEach((element) => {
          // console.log(element);
          let arr = this.transactions.filter(tr => tr.trans_bucket_child == element);
          // console.log(arr);
          
          let subArr = arr.map(value => value.trans_amt);

          subArr =  subArr.map(x => parseInt(x));

          // console.log(subArr);
          
          let arrSum = subArr.reduce((a, b) => a + b, 0);      
          // console.log(arrSum);
          
          this.pieValues.push(arrSum);

        });

        console.log(this.pieValues);


      this.doNut();

// -----------------------------Line graph-----------------------------------

// get all
this.lineLabels =  this.transactions.map(value => value.trans_date);
// get unique
this.lineLabels =  this.lineLabels.filter((v, i, a) => a.indexOf(v) === i);
// get last 5 days
     this.lineLabels = this.lineLabels.slice(-5);
     
      this.lineLabels.forEach((element) => {
        // get all transactions for current iteration
        let arr = this.transactions.filter(tr => tr.trans_date == element);
        // get all amts for all those transactions
          let subArr = arr.map(value => value.trans_amt);
        // parse amts to int
          subArr =  subArr.map(x => parseInt(x));
        // sum them up
          let arrSum = subArr.reduce((a, b) => a + b, 0);      

        this.lineValues.push(arrSum);
        });

        console.log(this.lineValues);


        this.liNe();









        this.loaded = 1;

        alert = this.alertCtrl.create({
          title: 'No name',
          subTitle: 'no Name'+ this.transactions[0],
          buttons: ['Dismiss']
        });
        // alert.present();

        }).catch((err) => {
          this.loaded = 1;

          console.log(err);
    
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Something broke and thats on us, Please try again',
            buttons: ['Dismiss']
          });
          alert.present();
        });
        // this.Carr = data;
        // console.log(data);

    }
// =================================================================






 

}
