import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';

import { Storage } from '@ionic/storage';

/**
 * Generated class for the ForexCardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forex-card',
  templateUrl: 'forex-card.html',
})
export class ForexCardPage {

  uId: any;

  constructor(public navCtrl: NavController, 
    private alertCtrl: AlertController,
    private storage: Storage,
    public grsp: GlobalRestServiceProvider,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForexCardPage');
    this.getUserName();

  }


  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      // this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
      this.getForexApi();

    });
  }


  setApiJson(){

    // this.storage.get('initialData').then((initialData) => {

    this.apiData = {
      "grp":"Foreign"
    };

    // });

  }

  apiData:any;
  forex:any;
  forexCrd:any;
  loaded:any=1;




  getForexApi(){

    this.loaded = 0;


    this.setApiJson();

      let alert = this.alertCtrl.create({
        title: 'No name',
        subTitle: 'no Name',
        buttons: ['Dismiss']
      });

    console.log(this.apiData);

      this.grsp.cards(this.apiData).then(data => {
        console.log(data);

        this.forex = data;
<<<<<<< HEAD
      this.forex = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
        // this.transactions = this.trans.transactions.split(",");
        this.forexCrd = JSON.parse(this.forex.cards);

        this.loaded = 1;

        // alert = this.alertCtrl.create({
        //   title: 'No name',
        //   subTitle: 'no Name'+ this.investments[0],
        //   buttons: ['Dismiss']
        // });
        // alert.present();

        }).catch((err) => {
          this.loaded = 1;
    
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Something broke and thats on us, Please try again',
            buttons: ['Dismiss']
          });
          alert.present();
        });
        // this.Carr = data;
        // console.log(data);

    }




}
