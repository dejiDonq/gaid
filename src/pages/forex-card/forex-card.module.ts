import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ForexCardPage } from './forex-card';

@NgModule({
  declarations: [
    ForexCardPage,
  ],
  imports: [
    IonicPageModule.forChild(ForexCardPage),
  ],
})
export class ForexCardPageModule {}
