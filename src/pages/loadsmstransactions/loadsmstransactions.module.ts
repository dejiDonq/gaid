import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoadsmstransactionsPage } from './loadsmstransactions';

@NgModule({
  declarations: [
    LoadsmstransactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(LoadsmstransactionsPage),
  ],
})
export class LoadsmstransactionsPageModule {}
