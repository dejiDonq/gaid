import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
// import { AndroidPermissions } from '@ionic-native/android-permissions';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';
import { Storage } from '@ionic/storage';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
// import { PaystackPage } from '../paystack/paystack';


declare var SMS:any;


/**
 * Generated class for the LoadsmstransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-loadsmstransactions',
  templateUrl: 'loadsmstransactions.html',
})
export class LoadsmstransactionsPage {

  constructor(
    public fileChooser: FileChooser,
    public file: File,
    private transfer: FileTransfer,
    public navCtrl: NavController, 
    public grsp: GlobalRestServiceProvider,
    private storage: Storage,
    // public androidPermissions: AndroidPermissions,
    public toastCtrl: ToastController,
    public platform:Platform,
    public navParams: NavParams) {
  }


  messages:any=[];
  buckets:any;
  bucketFin:any;
  acct:any;
  amt:any;
  type:any;
  desc:any;
  bal:any;
  date:any;
  bucket:any = {
    income:[ "Salary", "Allowances", "Dividend", "Rental Income", "Interest Income", "Other Income" ],
    expenses:[ "Food", "Groceries", "Transportation", "Taxi", "Bus", "Fuel", "Car Maintenance", "Utilies",
               "Electricity", "Phone Bills", "Airtime", "Cable", "Waste", "Cook", "Cleaner",
            "Laundry","Rent","Entertainment","Knowledge","Wife", "Education", "Subscriptions", "Shopping",
              "Children","Parents","Siblings","Charity","Vacation","Personal","Miscellaneous" ],
    savingsAndInvestments:[ "Equities","Mutual Funds","Real Estate","Other Investments" ],
    projects:["New Apartment","Party"],
    liabilities:["Debts"]
  };

smsCount:any;

feedback:any=1;

uploadRes:any;


LoadFile(){
  this.fileChooser.open().then((uri)=>{
    console.log(uri);

    const fileTransfer: FileTransferObject = this.transfer.create();

    var name = Math.floor((new Date).getTime()/1000);

    console.log(name);

    let options1: FileUploadOptions = {
       fileKey: 'file',
       fileName: name+"_"+this.uId+"",
       headers: {}
    
    }

    this.feedback=0;


    // fileTransfer.upload(uri, 'https://ampersandacademy.com/ionic/upload.php', options1)
    fileTransfer.upload(uri, 'http://gaid.io/myLeo/uploadExcel', options1)
 .then((data) => {
   // success
   console.log("success");
   console.log(data);
   console.log(data.response);
   this.feedback = 1;

   let dres = data.response;

   let lastfour = dres.substr(dres.length - 4);

   if(lastfour == "done"){

    this.uploadRes = "Done";
     
    let toast = this.toastCtrl.create({
      message: 'Transactions Loaded and Saved Successfully',
      duration: 4000,
    });
    toast.present();

       
      }


  }, (err) => {
    // error
    console.log("error"+JSON.stringify(err));
    console.log(err);
 });


 console.log("toh");
     
  });
}




// paystack(){

//   let data = {

//     price:"5000"
    
//     };

//   this.navCtrl.push(PaystackPage, data );
// }



  ionViewDidLoad() {



    this.buckets = this.grsp.buckets();
    console.log(this.buckets);

    console.log('ionViewDidLoad LoadsmstransactionsPage');

        this.getUserName();
    // this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_SMS).then(
    //   success => {


    //     console.log('Permission granted');
    //     // this.ReadListSMS();

    //   },
    // err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_SMS)
    // );
  
    // this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.READ_SMS]);

  }



  // async syncSMS(msgs){
  //   // console.log(msgs);
  //       // msgs.forEach((element) => {
  //       for (var element of msgs){
  //           // console.log(element);

  //       await this.LoadMsg(element);
  //       }
  // //  });
  // }

//   LoadMsg(sms){

//     // if(sms.address == "g" ){

//       if(!(sms.body.includes("commission"))){

//         let body = sms.body.replace(/\r\n/, "\n").split("\n");
//         this.acct = body[0].substring(body[0].indexOf(":")).trim().substr(1);
//         this.amt = body[1].substring(body[1].indexOf(":"), body[1].length - 2).trim().substr(1);
//         this.type = (body[1].slice(-3).trim() == "DR") ? "DEBIT":"CREDIT";
//         this.desc = body[2].substring(body[2].indexOf(":")).trim().substr(1);
//         this.bal = body[3].substring(body[3].indexOf(":")).trim().substr(1);

//         var number = Number(this.bal.replace(/[^0-9.-]+/g,""));

//         // console.log(this.buckets);
//         // console.log(body);
//         // console.log(number);
        
//         let arr = this.desc.split(' ');
        
//         // console.log(arr);

//         // arr.forEach((element) => {

//           for (var i = 0; i < arr.length; ++i){

//             let element = arr[i];

//           let ind = this.buckets.indexOf(element);

//           if (ind < 0){
//             this.bucketFin = 'no match';
//           }
//           else{
//             this.bucketFin = this.buckets[ind];
//             break;
//           }
//         }


//         var timestamp = sms.date;
// var d = new Date(timestamp);

//  this.date =   d.getFullYear() + "-" + 
//     ("00" + (d.getMonth() + 1)).slice(-2) + "-" + 
//     ("00" + d.getDate()).slice(-2) + " " + 
//     ("00" + d.getHours()).slice(-2) + ":" + 
//     ("00" + d.getMinutes()).slice(-2) + ":" + 
//     ("00" + d.getSeconds()).slice(-2);


//         // });

//         // console.log(this.bucketFin);

//         this.saveAddApi();
//         console.log(sms);


//       }

//     // }

//   }



// =============================================================================

apiAddData:any;

setAddApiJson(){

  // this.storage.get('initialData').then((initialData) => {

this.apiAddData = {
    "uId":this.uId,
    "type":this.type,
    "desc":this.desc,
    "bucket":this.bucketFin,
    "amt":this.amt,
    "date":this.date
  };

  // });

  console.log(this.apiAddData);


}

apiRes:any;


saveAddApi(){

  // this.loaded = 0;
  
      this.setAddApiJson();
  
      console.log(this.apiAddData);
  
      this.grsp.addTrans(this.apiAddData).then(data => {
  
        console.log(data);
  
        this.apiRes = data;
<<<<<<< HEAD
      this.apiRes = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
  // this.loaded = 1;
        if(this.apiRes.response == true){
  
          let toast = this.toastCtrl.create({
            message: 'Transaction ' + this.apiRes.desc + ' Saved Successfully',
            duration: 2500,
          });
          toast.present();
  
  
          this.type='';
          this.desc='';
          this.bucket='';
          this.amt='';
          this.date='';
  
        }
        else{
  
         console.log('failed');
  
        }
  
      })
      .catch((err) => {
  
        console.log('failed catch');

       
      });
  
  
    }
// =============================================================================









  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      // this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
    this.getLastDateApi();


    });
  }


  apiData:any;


  setApiJson(){

    // this.storage.get('initialData').then((initialData) => {

    this.apiData = {
      "uId":this.uId
    };

    // });

  }

  // lastDate:any =  1538728610485;
  lastDate:any;
  uId

  getLastDateApi(){

    this.setApiJson();

    this.grsp.lastDate(this.apiData).then(data => {
      console.log(data);

      this.lastDate = data;
<<<<<<< HEAD
      this.lastDate = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
      this.lastDate = this.lastDate.lastDate;

      this.lastDate = new Date(this.lastDate).valueOf();
      console.log(this.lastDate);

      // this.ReadListSMS();

  

      }).catch((err) => {
        // this.loaded = 1;
  
        // let alert = this.alertCtrl.create({
        //   title: 'Error',
        //   subTitle: 'Something broke and thats on us, Please try again',
        //   buttons: ['Dismiss']
        // });
        // alert.present();
      });
  }


  // ReadListSMS()
  // {

  // this.platform.ready().then((readySource) => {

  // let filter = {
  //        box : 'inbox', // 'inbox' (default), 'sent', 'draft'

  //        address : 'GTBank', // sender's phone number
  //       //  address : '+2348097239462', // sender's phone number


  //        indexFrom : 0, // start from index 0
  //        maxCount : 10 // count of SMS to return each time
  //             };

  //        if(SMS) SMS.listSMS(filter, (ListSms)=>{
  //         this.messages=ListSms;
  //         // alert ("sms is in/loaded");
  //         this.messages = this.messages.filter(msg => msg.date - this.lastDate > 500 ).reverse();
  //         // var sms = ListSms[ListSms.length-1];

  //         this.smsCount = this.messages.length;

  //         console.log(this.messages);

  //            console.log("Sms",ListSms);
  //           },

  //           Error=>{
  //           console.log('error list sms: ' + Error);
  //           });

  //     });
  // }



  ionViewDidEnter()
  {

  // this.platform.ready().then((readySource) => {

  // if(SMS) SMS.startWatch(()=>{
  //           //  alert('watching started');
  //         }, Error=>{
  //        alert('failed to start watching');
  //    });

  //   document.addEventListener('onSMSArrive', (e:any)=>{
  //        var sms = e.data;
  //        if(sms.address == "+2348097239462" ){

  //         if(!sms.body.includes("commission")){


  //           let body = sms.body.replace(/\r\n/, "\n").split("\n");
  //           this.acct = body[0].substring(body[0].indexOf(":")).trim().substr(1);
  //           this.amt = body[1].substring(body[1].indexOf(":"), body[1].length - 2).trim().substr(1);
  //           this.type = (body[1].slice(-3).trim() == "DR") ? "DEBIT":"CREDIT";
  //           this.desc = body[2].substring(body[2].indexOf(":")).trim().substr(1);
  //           this.bal = body[body.length-1].substring(body[body.length-1].indexOf(":")).trim().substr(1);

  //           var arr;
  //           var n;

  //           this.bucketFin = this.desc;

  //          console.log(this.bal);


  //         }


  //        }

  //        });

  //     });
  }




}
