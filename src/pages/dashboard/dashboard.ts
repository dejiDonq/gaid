import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

import { Storage } from '@ionic/storage';
import { Chart } from 'chart.js';

import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';
import { SignupPage } from '../signup/signup';

import * as moment from 'moment';


/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  @ViewChild('doughnutCanvas') doughnutCanvas;
  doughnutChart: any;
  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private alertCtrl: AlertController,
    public grsp: GlobalRestServiceProvider

    ) {
  }

valName:any;

userName:any;
loaded:any = 0;

networth:any = 0;
assets:any = 0;
liability:any = 0;
pension:any = 0;
amtRet:any = 0;
ageRet:any = 0;

initialData:any;

// initialData = {
//   id:0,
//   name:'',
//   dob:'',
//   dow:'',
//   pen:'',
//   savInv:'',
//   exp:'',
//   inc:'',
//   ret:'',
//   // desc:'',
//   // date:'',
// };

ionViewWillEnter(){
  // this.load();

  let tday = moment().format("YYYY-MM-DD").toString();
    // console.log(tday);
    
    // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
    let yday = moment().subtract(1, 'month').format("YYYY-MM-DD").toString();
    // console.log(yday);

    this.end = tday;
    this.start = yday;


    this.load();

    console.log('ionViewWillEnter DashboardPage');
    // this.initialData = this.navParams.get('val');

    this.getUserName();


}

  ionViewDidLoad() {

    
    let tday = moment().format("YYYY-MM-DD").toString();
    // console.log(tday);
    
    // let yday = moment().subtract(1, 'month').endOf('month').format("YYYY-MM-DD").toString();
    let yday = moment().subtract(1, 'month').format("YYYY-MM-DD").toString();
    // console.log(yday);

    this.end = tday;
    this.start = yday;


    this.load();

    console.log('ionViewDidLoad DashboardPage');
    // this.initialData = this.navParams.get('val');

    this.getUserName();

    // this.doNut();
    // this.liNe();
    // this.apiLoad();

    // this.retirement();



    // });


  }


  clearInit(){
    // this.storage.set('initialData', null);

    // this.navCtrl.push(InitialPage, {
    //   val: 'I am View Ana-lytics'
    // })

  }


   numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : x;
  }



  week(){
    let weekStart = moment().startOf('isoWeek').format('YYYY-MM-DD HH:mm:ss');
    let weekEnd = moment().format('YYYY-MM-DD HH:mm:ss');
    this.getTransactionsApi(weekStart, weekEnd);

  }
 
  month(){
    let start = moment().startOf('month').format('YYYY-MM-DD HH:mm:ss');
    let end   = moment().format('YYYY-MM-DD HH:mm:ss');

    this.getTransactionsApi(start, end);

  }
 
  year(){
    let start = moment().startOf('year').format('YYYY-MM-DD HH:mm:ss');
    let end   = moment().format('YYYY-MM-DD HH:mm:ss');

    this.getTransactionsApi(start, end);

  }
 
  range(){

    this.drange = !this.drange;

  }


  getRange(){
    this.getTransactionsApi(this.from, this.to);

  }


dash:any;
donutLabels:any;
donutData:any;
lineLabels:any;
lineIncomeData:any;
lineExpenseData:any;

drange:any=false;
from:any;
to:any;
start:any;
end:any;

assetTrend:any;
liabTrend:any;
nwTrend:any;


apiLoad(){
    let alert = this.alertCtrl.create({
      title: 'No name',
      subTitle: 'no Name',
      buttons: ['Dismiss']
    });

    this.grsp.dashboardLoad({"uId":this.uId}).then(data => {
      // console.log(data);

<<<<<<< HEAD
      // this.dash = data;

      this.dash = JSON.parse(data.data);

=======
      this.dash = data;
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298

      this.assets = this.numberWithCommas(parseInt(this.dash.rassets));
      this.liability = this.numberWithCommas(parseInt(this.dash.rliabilities));
      // this.networth = this.dash.networth;
      this.networth = parseInt(this.dash.rassets) - parseInt(this.dash.rliabilities);
      this.networth = this.numberWithCommas(this.networth);
      // this.pension = this.numberWithCommas(parseInt(this.dash.pension));
      // this.amtRet = this.dash.retirementTarget;
      // this.ageRet = this.dash.retirementAge;

      this.assetTrend = parseInt(this.dash.rassets) - parseInt(this.dash.oassets);
      this.liabTrend = parseInt(this.dash.rliabilities) - parseInt(this.dash.oliabilities);

      let onetworth = parseInt(this.dash.oassets) - parseInt(this.dash.oliabilities);

      this.nwTrend = this.networth - onetworth;

      // console.log(parseInt(this.dash.rassets));
      // console.log(parseInt(this.dash.oassets));
      // console.log(parseInt(this.dash.rliabilities));
      // console.log(parseInt(this.dash.oliabilities));
      // console.log(this.assetTrend);
      // console.log(this.liabTrend);
      // console.log(this.nwTrend);

      

      this.donutLabels = this.dash.donutLabels.split(",");
      // this.donutLabels = this.donutLabels.split(",");

      this.donutData = this.dash.donutData.split(",");
      this.lineLabels = this.dash.lineLabels.split(",");
      this.lineIncomeData = this.dash.lineIncomeData.split(",");
      this.lineExpenseData = this.dash.lineExpenseData.split(",");

    this.doNut();
    this.liNe();

this.loaded = 1;

      // alert = this.alertCtrl.create({
      //   title: 'No name',
      //   subTitle: 'no Name'+ this.dash.userName,
      //   buttons: ['Dismiss']
      // });
      // alert.present();

      })
      .catch((err) => {
        this.loaded = 1;
        
              let alert = this.alertCtrl.create({
                title: 'Error',
                subTitle: 'Something broke and thats on us, Please try again',
                buttons: ['Dismiss']
              });
              alert.present();
            });
      // this.Carr = data;
      // console.log(data);

  }




   savings:any;
   expenses:any;
   wrkingYrs:any;
   retYrs:any;
   annuity:any;


  retirement(time?){
    // let savings = this.savings;
    let savings = this.anotherSav;
    let expenses = this.expenses;
    let wrkingYrs = this.wrkingYrs;
    let retYrs = this.retYrs;
    // let savings = 1000
    let nvalue = savings
    let r = 0.14;

    let count = 0;

    while(count < wrkingYrs){
      
        let temp = savings * Math.pow((1+r), count);
        // console.log(temp);
        nvalue += temp;

        count ++;

      }
      // console.log(nvalue);
      
      let prannuity;
      
      prannuity = (1-(Math.pow((1+r), (retYrs*-1)))) / r;
      
      // console.log(prannuity);

      let annuity = (1/prannuity) * nvalue;

      this.amtRet = this.numberWithCommas(annuity.toFixed(2));


      this.annuity = this.numberWithCommas((annuity/retYrs).toFixed(2));

      // console.log(annuity);

  }
  

  loadProgress:any = 0;
  loadProgressAge:any = 0;
  currAge:any = 0;
  finAge:any = 90;


  load(){
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }






  doNut(){
    console.log(this.donutLabels);
    this.doughnutChart = new Chart(this.doughnutCanvas.nativeElement, {

      type: 'doughnut',
      data: {
          // labels: ["Food", "Utility", "Airtime", "Investments", "Entertainment", "Travel"],
          // labels: this.donutLabels,
          labels: this.pieLabels,
          datasets: [{
              label: 'User Personal Finance Status',
              // data: this.donutData,
              data: this.pieValues,
              // data: [12, 19, 3, 5, 2, 3],
              backgroundColor: [
                  'rgba(255, 99, 132, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(255, 206, 86, 0.2)',
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(153, 102, 255, 0.2)',
                  'rgba(255, 159, 64, 0.2)'
              ],
              hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56",
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56"
              ]
          }]
      },
      options:{
      //   scales: {
      //     xAxes: [{
      //         gridLines: {
      //             color: "rgba(0, 0, 0, 0)",
      //         }
      //     }],
      //     yAxes: [{
      //         gridLines: {
      //             color: "rgba(0, 0, 0, 0)",
      //         }   
      //     }]
      // },
        tooltips: {
          callbacks: {
            label: function(tooltipItem, data) {
              var dataset = data.datasets[tooltipItem.datasetIndex];
              // var meta = dataset._meta[Object.keys(dataset._meta)[0]];
              // var total = meta.total;
              var currentValue = dataset.data[tooltipItem.index];
              // var percentage = parseFloat((currentValue/total*100).toFixed(1));
              // return currentValue + ' (' + percentage + '%)';
              return "NGN" + currentValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? currentValue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : currentValue;

            },
            // title: function(tooltipItem, data) {
            //   return data.labels[tooltipItem[0].index];
            // }
          }
        }
      }
     

  });

  }

  liNe(){

    this.lineChart = new Chart(this.lineCanvas.nativeElement, {

      type: 'line',
      data: {
          // labels: ["January", "February", "March", "April", "May", "June", "July"],
          labels: this.lineLabels,
          datasets: [
              {
                  label: "Income",
                  fill: false,
                  lineTension: 0.1,
                  // backgroundColor: "rgba(75,92,192,0.4)",
                  borderColor: "rgba(75,92,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(75,92,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(75,92,192,1)",
                  pointHoverBorderColor: "rgba(220,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  // data: [65, 55, 90, 21, 78, 95, 20],
                  data: this.lineIncomeValues,
                  spanGaps: false,
              },
              {
                  label: "Expense",
                  fill: false,
                  lineTension: 0.1,
                  // backgroundColor: "rgba(75,192,192,0.4)",
                  borderColor: "rgba(75,192,192,1)",
                  borderCapStyle: 'butt',
                  borderDash: [],
                  borderDashOffset: 0.0,
                  borderJoinStyle: 'miter',
                  pointBorderColor: "rgba(75,192,192,1)",
                  pointBackgroundColor: "#fff",
                  pointBorderWidth: 1,
                  pointHoverRadius: 5,
                  pointHoverBackgroundColor: "rgba(75,192,192,1)",
                  pointHoverBorderColor: "rgba(220,220,220,1)",
                  pointHoverBorderWidth: 2,
                  pointRadius: 1,
                  pointHitRadius: 10,
                  // data: [65, 59, 80, 81, 56, 55, 40],
                  data: this.lineExpenseValues,
                  spanGaps: false,
              }
          ]
      },
      options: {
        scales: {
          xAxes: [{
              gridLines: {
                  color: "rgba(0, 0, 0, 0)",
              }
          }],
          yAxes: [{
              gridLines: {
                  color: "rgba(0, 0, 0, 0)",
              }   
          }]
      },
        tooltips: {
        callbacks: {
            label: function(tooltipItem) {
                return "NGN" + tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") ? tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") : tooltipItem.yLabel;
            }
        }
    }
  }

  });
  }

  // getUserName(){
  //   this.userName = "Tayo";
  // }
  anotherSav:any = 0;

  getUserName(){
    
    this.storage.get('initialData').then((initialData) => {
<<<<<<< HEAD
      console.log(initialData);
=======

      console.log(initialData);
      if(initialData.name.length < 1 || initialData.dob.length < 1  || initialData.dow.length < 1 
        || initialData.pen.length < 1 || initialData.ret.length < 1 || initialData.email.length < 1 
        || initialData.pword.length < 1 ){

    this.storage.set('initialData', null);


          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Invalid Details, Re-Login or Contact Admin',
            buttons: ['Ok']
          });
            alert.present();


    
this.navCtrl.setRoot(SignupPage);

    
          return;

      }

>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
      this.pension = this.numberWithCommas(parseInt(initialData.pen));
    this.apiLoad();


      let ageNow  = moment().diff(initialData.dob, 'years');


      this.savings = this.numberWithCommas(parseInt(initialData.savInv));
      this.anotherSav = parseInt(initialData.savInv);
      this.expenses = this.numberWithCommas(parseInt(initialData.exp));
      this.wrkingYrs =  parseInt(initialData.ret) - ageNow;
      this.retYrs = 90 - parseInt(initialData.ret);

      let totAge = 90 - ageNow;

    this.loadProgress = (this.wrkingYrs/totAge) * 100;
    this.loadProgressAge = initialData.ret;

      this.currAge = ageNow;

      this.getTransactionsApi(this.start,this.end);

      this.retirement();

      this.ageRet = initialData.ret;



    });
  }










  // =================================================================
apiData:any;
uId:any;
transactions:any;
etransactions:any;

lineExpLabels:any;

pieLabels:any;
pieValues:any = [];

// lineLabels:any;
lineIncomeValues:any = [];
lineExpenseValues:any = [];


setApiJson(start?,end?){

    // this.storage.get('initialData').then((initialData) => {

      this.apiData = {
        "uId":this.uId,
        "start": start,
        "end": end
      };

    // });

  }



  trans:any;

  getTransactionsApi(start?, end?){

    this.loaded = 0;


    this.setApiJson(start,end);

      let alert = this.alertCtrl.create({
        title: 'No name',
        subTitle: 'no Name',
        buttons: ['Dismiss']
      });

    // console.log(this.apiData);

    this.lineLabels= [];
    // this.lineValues= [];
    this.pieLabels= [];
    this.pieValues= [];


      this.grsp.transactions(this.apiData).then(data => {
        // console.log(data);

<<<<<<< HEAD
        // this.trans = data;
      this.trans = JSON.parse(data.data);

=======
        this.trans = data;
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
        // this.transactions = this.trans.transactions.split(",");
        this.transactions = JSON.parse(this.trans.transactions);
        // this.transactions  = this.transactions.filter(tr => tr.trans_bucket == 'Assets');
        // get all
        this.pieLabels =  this.transactions.map(value => value.trans_bucket);
        // console.log(this.transactions);
        // console.log(this.pieLabels);
        // get unique
        this.pieLabels =  this.pieLabels.filter((v, i, a) => a.indexOf(v) === i); 
        console.log(this.pieLabels);
        let isArr = (Array.isArray(this.pieLabels));

        // console.log(isArr);


        this.pieLabels.forEach((element) => {
          // console.log(element);
          let arr = this.transactions.filter(tr => tr.trans_bucket == element);
          // console.log(arr);
          
          let subArr = arr.map(value => value.trans_amt);

          subArr =  subArr.map(x => parseInt(x));

          // console.log(subArr);
          
          let arrSum = subArr.reduce((a, b) => a + b, 0);      
          // console.log(arrSum);
          
          this.pieValues.push(arrSum);

        });


        this.pieValues.length = this.pieLabels.length;

        console.log(this.pieValues);


      this.doNut();

// -----------------------------Line graph-----------------------------------
// get trans again
this.transactions = JSON.parse(this.trans.transactions);
  this.transactions  = this.transactions.filter(tr => tr.trans_bucket == 'Income');
 
this.lineLabels =  this.transactions.map(value => value.trans_date);
// get unique
this.lineLabels =  this.lineLabels.filter((v, i, a) => a.indexOf(v) === i);
// get last 5 days
     this.lineLabels = this.lineLabels.slice(-5);

     console.log(this.lineLabels);

     
      this.lineLabels.forEach((element) => {
        // get all transactions for current iteration
        let arr = this.transactions.filter(tr => tr.trans_date == element);
        // get all amts for all those transactions
          let subArr = arr.map(value => value.trans_amt);
        // parse amts to int
          subArr =  subArr.map(x => parseInt(x));
        // sum them up
          let arrSum = subArr.reduce((a, b) => a + b, 0);      

        this.lineIncomeValues.push(arrSum);
        });

        console.log(this.lineIncomeValues);
// -------------------------------------------expenses line-------------------------------------------------
this.etransactions = JSON.parse(this.trans.transactions);

this.etransactions  = this.etransactions.filter(tr => tr.trans_bucket == 'Expenses');
// console.log(this.etransactions);


this.lineExpLabels =  this.etransactions.map(value => value.trans_date);
// get unique
this.lineExpLabels =  this.lineExpLabels.filter((v, i, a) => a.indexOf(v) === i);
// get last 5 days
     this.lineExpLabels = this.lineExpLabels.slice(-5);

     console.log(this.lineExpLabels);
     
      this.lineExpLabels.forEach((element) => {
        // get all transactions for current iteration
        let arr = this.etransactions.filter(tr => tr.trans_date == element);
        // get all amts for all those transactions
          let subArr = arr.map(value => value.trans_amt);
        // parse amts to int
          subArr =  subArr.map(x => parseInt(x));
        // sum them up
          let arrSum = subArr.reduce((a, b) => a + b, 0);      

        this.lineExpenseValues.push(arrSum);
        });

        console.log(this.lineExpenseValues);


        this.liNe();









        this.loaded = 1;

        alert = this.alertCtrl.create({
          title: 'No name',
          subTitle: 'no Name'+ this.transactions[0],
          buttons: ['Dismiss']
        });
        // alert.present();

        }).catch((err) => {
          this.loaded = 1;

          console.log(err);
    
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Something broke and thats on us, Please try again',
            buttons: ['Dismiss']
          });
          alert.present();
        });
        // this.Carr = data;
        // console.log(data);

    }
// =================================================================

}
