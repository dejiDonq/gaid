import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the AnalysisPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-analysis',
  templateUrl: 'analysis.html',
})
export class AnalysisPage {
  categories = [
    {
      id: 1,
      name: 'View Analytics',
      title: 'View Analytics',
      description: 'See How Your Numbers Are Looking',
      img: 'assets/imgs/menu/coffee.png'
    },
    {
      id: 2,
      name: 'Add Transaction(s)',
      title: 'Add Transaction(s)',
      description: 'Anything We Might Have Missed?',
      img: 'assets/imgs/menu/breakfast.png'
    },
    {
      id: 3,
      name: 'Edit Transaction(s)',
      title: 'Edit Transaction(s)',
      description: 'Because You Have Full Control',
      img: 'assets/imgs/menu/munchies.png'
    },
   
  ];

  constructor(public navCtrl: NavController,
    private storage: Storage,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {

    this.load();

    console.log('ionViewDidLoad AnalysisPage');
  }


  analysisAction(cat) {
    /* Pass category object as a parameter */
    if(cat.id == 1){
      let param = { category: 1 };
      this.navCtrl.push('ViewanalyticsPage', param );
    }
    else if(cat.id == 2){
      let param = { category: 2 };
      this.navCtrl.push('AddtransactionsPage', param );
    }
    else{ 
      let param = { category: 3 };
      this.navCtrl.push('ViewtransactionsPage', param );
    }
  
  }


  ionViewWillEnter(){
    this.load();
  
  }


  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }



}
