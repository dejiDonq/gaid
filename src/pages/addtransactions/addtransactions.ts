import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';
import { Storage } from '@ionic/storage';



/**
 * Generated class for the AddtransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addtransactions',
  templateUrl: 'addtransactions.html',
})
export class AddtransactionsPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private alertCtrl: AlertController,
    public grsp: GlobalRestServiceProvider) {
  }


  valName:any;
  userName:any;
  type:any;
  desc:any;
  bucket:any;
  amt:any;
  date:any;
  uId:any;
  buckets: any;

  

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddtransactionsPage');
    this.valName = this.navParams.get('val');
    this.getUserName();

  }


  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;

      this.buckets = this.grsp.buckets();

    });
  }


  apiData:any;

  setApiJson(){

    // this.storage.get('initialData').then((initialData) => {

this.apiData = {
      "uId":this.uId,
      "type":this.type,
      "desc":this.desc,
      "bucket":this.bucket,
      "amt":this.amt,
      "date":this.date
    };

    // });



  }

  apiRes:any;
  loaded:any= 1;




  saveAddApi(){

this.loaded = 0;

    this.setApiJson();

    console.log(this.apiData);

    this.grsp.addTrans(this.apiData).then(data => {

      console.log(data);

<<<<<<< HEAD
      // this.apiRes = data;
      this.apiRes = JSON.parse(data.data);

=======
      this.apiRes = data;
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
this.loaded = 1;
      if(this.apiRes.response == true){

        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Successfully Saved Transaction '+this.apiRes.desc,
          buttons: ['Ok']
        });
        alert.present();


        this.type='';
        this.desc='';
        this.bucket='';
        this.amt='';
        this.date='';

      }
      else{

        let alert = this.alertCtrl.create({
          title: 'Failed',
          subTitle: 'Something went wrong, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();

      }

    })
    .catch((err) => {
this.loaded = 1;

      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Something broke and thats on us, Please try again',
        buttons: ['Dismiss']
      });
      alert.present();
    });


  }



  saveAdd(){
    this.desc = this.desc ? this.desc:"";
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'Successfully Saved Transaction '+this.desc,
      buttons: ['Dismiss']
    });
    alert.present();
  }

}
