import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddtransactionsPage } from './addtransactions';

@NgModule({
  declarations: [
    AddtransactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(AddtransactionsPage),
  ],
})
export class AddtransactionsPageModule {}
