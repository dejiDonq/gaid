import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DebtbudgetPage } from './debtbudget';

@NgModule({
  declarations: [
    DebtbudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(DebtbudgetPage),
  ],
})
export class DebtbudgetPageModule {}
