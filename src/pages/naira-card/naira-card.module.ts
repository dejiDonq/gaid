import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NairaCardPage } from './naira-card';

@NgModule({
  declarations: [
    NairaCardPage,
  ],
  imports: [
    IonicPageModule.forChild(NairaCardPage),
  ],
})
export class NairaCardPageModule {}
