import { InitialPage } from './../initial/initial';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController } from 'ionic-angular';
import { DashboardPage } from '../dashboard/dashboard';
import { MyApp } from '../../app/app.component';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';
import { Storage } from '@ionic/storage';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  // @ViewChild(Nav) nav: Nav;

  // rootPage: any;


  initialData = {
    id:0,
    name:'',
    dob:'',
    dow:'',
    pen:'',
    savInv:'',
    exp:'',
    inc:'',
    ret:'',
    email:'',
    pword:'',
    cpword:'',
    // date:'',
  };


  activePage: any;

  screen = "signin";
  constructor(public navCtrl: NavController,
    public grsp: GlobalRestServiceProvider,
    private alertCtrl: AlertController,
    private storage: Storage,
    public navParams: NavParams) {}

  login = {
    email:'',
    pword:'',
  };

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }


  apiData:any;

  setApiJson(){
    this.apiData = {
      "email":this.login.email,
      "pword":this.login.pword
    };
  }

  apiRes:any;
  loaded:any=1;


  loginCheck(){

    if(this.login.email == '' || this.login.pword == ''){

      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Enter Correct Login Details',
        buttons: ['Dismiss']
      });
      alert.present();

      return
    }

    this.setApiJson();
    this.loaded = 0;
    console.log(this.apiData);


    this.grsp.login(this.apiData).then(data => {
 
<<<<<<< HEAD
      console.log('data');
      console.log(data);
      console.log(' data');
      console.log(data.data);
      
      this.apiRes = JSON.parse(data.data);
      
      console.log(' apires');
      console.log(this.apiRes);
      // console.log(JSON.parse(this.apiRes).loginRes);
=======
      console.log(data);

      this.apiRes = data;
      
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
      this.loaded = 1;

      if(this.apiRes.response == true){

        let loginRes = JSON.parse(this.apiRes.loginRes)[0];

console.log(loginRes);


        this.initialData = {
          id:loginRes.user_id,
          name:loginRes.user_name,
          dob:loginRes.user_dob,
          dow:loginRes.user_dow,
          pen:loginRes.user_pension,
          savInv:loginRes.user_savInv,
          exp:loginRes.user_lastMonthExp,
          inc:loginRes.user_lastMonthInc,
          ret:loginRes.user_retAge,
          email:loginRes.user_email,
          pword:loginRes.user_pword,
          cpword:loginRes.user_pword,
          // date:'',
        };

this.storage.set('initialData', this.initialData);

console.log(this.initialData);



        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Login Successful',
          buttons: ['Dismiss']
        });
        alert.present();

        this.navCtrl.setRoot(DashboardPage);

        // this.activePage = { title: 'Dashboard', component: DashboardPage };

      }
      else{

        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Something is wrong, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();

      }

    }).catch((err) => {
     this.loaded = 1;
<<<<<<< HEAD
 console.log(err);
       let alert = this.alertCtrl.create({
         title: 'Error',
         subTitle: err,
         //  subTitle: err+'Something broke and thats on us, Please try again',
=======
 
       let alert = this.alertCtrl.create({
         title: 'Error',
         subTitle: 'Something broke and thats on us, Please try again',
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
         buttons: ['Dismiss']
       });
       alert.present();
     });


    // =============================================================


    // this.navCtrl.setRoot(DashboardPage);

    // this.activePage = { title: 'Dashboard', component: DashboardPage };
    // this.navCtrl.push(MyApp, {
    //   val: 'I am View Ana-lytics',
    //   page:'Dashboard'
    // })


  }

  signUp(){

    // this.navCtrl.setRoot(InitialPage);

    this.navCtrl.push(InitialPage, {})

  }

}
