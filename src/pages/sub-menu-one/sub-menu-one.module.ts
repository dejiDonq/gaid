import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubMenuOnePage } from './sub-menu-one';

@NgModule({
  declarations: [
    SubMenuOnePage,
  ],
  imports: [
    IonicPageModule.forChild(SubMenuOnePage),
  ],
})
export class SubMenuOnePageModule {}
