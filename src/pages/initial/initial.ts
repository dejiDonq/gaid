import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

// import { LandPage } from './../land/land';


import { Storage } from '@ionic/storage';

import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';
import { DashboardPage } from './../dashboard/dashboard';
import { Slides } from 'ionic-angular';


/**
 * Generated class for the InitialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-initial',
  templateUrl: 'initial.html',
})
export class InitialPage {

  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private storage: Storage,
    public grsp: GlobalRestServiceProvider
    ) {
  }

<<<<<<< HEAD
=======

  IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if(!regex.test(email)) {
      return false;
    }else{
      return true;
    }
  }

>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
  initialData = {
    id:0,
    name:'',
    dob:'',
    dow:'',
    pen:'',
    savInv:'',
    exp:'',
    inc:'',
    ret:'',
    email:'',
    pword:'',
    cpword:'',
    // date:'',
  };

  ionViewWillEnter(){
    this.checkInit();
  
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad InitialPage');
    // this.checkInit();
  }

  nextIndex(){
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex+1, 500);
  }
  
  prevIndex(){
    let currentIndex = this.slides.getActiveIndex();
    this.slides.slideTo(currentIndex-1, 500);
  }

  checkInit(){
    this.storage.get('initialData').then((initialData) => {

      if(initialData == null || initialData.id == 0){

      }
      else{

        this.navCtrl.setRoot(DashboardPage);
        // this.navCtrl.push(DashboardPage, {
        //   // val: 'Jeeje'
        //   val: initialData
        // })

      }

    });
  }

  apiRes:any;

  apiData:any;
  loaded:any = 1;


  setApiJson(){
    // this.storage.get('initialData').then((initialData) => {

      this.apiData = {
        "id":0,
        "name":this.initialData.name,
        "dob":this.initialData.dob,
        "dow":this.initialData.dow,
        "pen":this.initialData.pen,
        "savInv":this.initialData.savInv,
        "exp":this.initialData.exp,
        "inc":this.initialData.inc,
        "ret":this.initialData.ret,
        "email":this.initialData.email,
        "pword":this.initialData.pword,
        // "cpword":this.initialData.cpword,
        // date:'',
      };
      console.log(this.apiData);

  // });this.initialData.namethis.initialData.name

  }



  logForm(){


    let alert;


<<<<<<< HEAD
=======
    if(this.initialData.name.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Name is Required',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(1, 500);

      return;

    }
   
    if(this.initialData.dob.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Date of Birth is Required',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(2, 500);

      return;

    }
   
    if(this.initialData.dow.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Tell us when you Started Working',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(3, 500);

      return;

    }
   
    if(this.initialData.pen.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Tell us How Much You Have in Pension',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(4, 500);

      return;

    }
    
    if(this.initialData.savInv.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Tell us How Much You Have in Savings and Investments',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(5, 500);

      return;

    }
   
    if(this.initialData.exp.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Tell us How Much You Spent Last Month',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(6, 500);

      return;

    }
   
    if(this.initialData.inc.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Tell us How Much You Made Last Month',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(7, 500);

      return;

    }
   
    if(this.initialData.ret.length < 1 ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'When Would You Like to Retire',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(8, 500);

      return;

    }
   
    if(this.initialData.email.length < 1 || this.IsEmail(this.initialData.email) == false ){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Enter a Valid Email',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(9, 500);

      return;

    }


    if(this.initialData.pword.length < 6){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Please Enter a Valid Password',
        buttons: ['Ok']
      });
        alert.present();

        this.slides.slideTo(10, 500);

      return;

    }

>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
    if(this.initialData.pword !== this.initialData.cpword){

      alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Passwords Do Not Match',
        buttons: ['Ok']
      });
        alert.present();

<<<<<<< HEAD
        this.prevIndex();
=======
        this.slides.slideTo(10, 500);
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298

      return;

    }

    this.setApiJson();
    this.loaded = 0;


    // ========================================================
    // this.apiData = this.initialData;
    console.log(this.apiData);
    this.grsp.addUser(this.apiData).then(data => {


      this.apiRes = data;
<<<<<<< HEAD
      this.apiRes = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
      this.loaded = 1;

      if(this.apiRes.response == true){

        this.initialData.id = this.apiRes.uId;
this.storage.set('initialData', this.initialData);

         alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Successfully Saved User '+this.apiRes.uId,
          buttons: ['Ok']
        });
        // alert.present();
// ========================================


 alert = this.alertCtrl.create({
  title: 'Saved',
  subTitle: 'Thank You '+this.initialData.name.toUpperCase()+ ' Your Details Have Been Saved',
  buttons: ['Ok']
});
alert.present();

// this.navCtrl.push(DashboardPage, {
//   // val: 'Jeeje'
//   val: this.initialData
// })
this.navCtrl.setRoot(DashboardPage);


// ========================================


      }
      else{

        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Something went wrong, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();

      }

    })
    .catch((err) => {
      this.loaded = 1;
      
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Something broke and thats on us, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();
      });
    // ========================================================


        //  let alert = this.alertCtrl.create({
        //   title: 'Saved',
        //   subTitle: 'Thank You '+this.initialData.name+ ' Your details have been saved',
        //   buttons: ['Dismiss']
        // });
        // alert.present();

        // this.navCtrl.push(LandPage, {
        //   // val: 'Jeeje'
        //   val: this.initialData
        // })


  }

}
