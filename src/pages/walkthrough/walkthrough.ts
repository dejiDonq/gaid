import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-walkthrough',
  templateUrl: 'walkthrough.html',
})
export class WalkthroughPage {

  slides = [
    {
      title: "Discover",
      description: "The Little Coffee Shop serves specialty coffee, fancy grilled cheese sandwiches, scratch cooking, craft ales, and cider.",
    },
    {
      title: "Taste",
      description: "You like your coffee Rich and Distinctive? Sweet and ight? Maybe Intense and Creamy? no worries, We've got it!",
    },
    {
      title: "Love",
      description: "So You Are a Coffe Lover? So as we! We serve coffee with all the Love in the world. Come Love with us.",
    }
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad WalkthroughPage');
  }
  goToHomePage() {
    let toast = this.toastCtrl.create({
      message: 'Get Started Clicked',
      duration: 2500,
    });
    toast.present();
  }

}
