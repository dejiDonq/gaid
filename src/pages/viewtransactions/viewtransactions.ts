import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController } from 'ionic-angular';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';

import { Storage } from '@ionic/storage';


/**
 * Generated class for the ViewtransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewtransactions',
  templateUrl: 'viewtransactions.html',
})
export class ViewtransactionsPage {

 
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    private storage: Storage,
    private alertCtrl: AlertController,
    public grsp: GlobalRestServiceProvider,
    public toastCtrl: ToastController) { }


  valName:any;
  userName:any;
  uId:any;
  transactions:any;
  loaded:any = 0;

  ionViewDidLoad() {
    // let selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
    this.valName = this.navParams.get('val');

    this.getUserName();
  }

  selectItem(item) {
    let toast = this.toastCtrl.create({
      message: 'item ' + item.title + ' Clicked',
      duration: 2500,
    });
    toast.present();
  }




  getItems(ev: any) {
    // Reset items back to all of the items
    // this.getTransactionsApi();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    // if (val && val.trim() != '') {
    //   this.transactions = this.transactions.filter((item) => {
    //     return (
    //     item.trans_bucket.toLowerCase().indexOf(val.toLowerCase()) > -1  || 
    //     item.trans_type.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
    //     item.trans_bucket_child.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
    //     item.trans_bucket_grandChild.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
    //     item.trans_amt.indexOf(val) > -1  || 
    //     item.trans_desc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
    //     item.trans_date.indexOf(val) > -1  );
        
    //   })
    // }
      if (val && val.trim() != '') {

        this.transactions = this.transactions.filter(item =>  item.trans_bucket.toLowerCase().indexOf(val.toLowerCase()) > -1  || 
            item.trans_type.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            item.trans_bucket_child.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            item.trans_bucket_grandChild.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            item.trans_amt.indexOf(val) > -1  || 
            item.trans_desc.toLowerCase().indexOf(val.toLowerCase()) > -1 ||
            item.trans_date.indexOf(val) > -1 );
      }

      console.log(this.transactions);
  }





  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
    this.getTransactionsApi(this.start,this.end);


    });
  }


  apiData:any;
  start:any;
  end:any;


  setApiJson(start?, end?){

    // this.storage.get('initialData').then((initialData) => {

    this.apiData = {
      "uId":this.uId,
      "start": start,
      "end": end
    };

    // });

  }


  trans:any;

  getTransactionsApi(start?, end?){

    this.setApiJson(start, end);

      let alert = this.alertCtrl.create({
        title: 'No name',
        subTitle: 'no Name',
        buttons: ['Dismiss']
      });

    console.log(this.apiData);

      this.grsp.transactions(this.apiData).then(data => {
        console.log(data);

        this.trans = data;
<<<<<<< HEAD
      this.trans = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
        // this.transactions = this.trans.transactions.split(",");
        this.transactions = JSON.parse(this.trans.transactions);

        this.loaded = 1;

        alert = this.alertCtrl.create({
          title: 'No name',
          subTitle: 'no Name'+ this.transactions[0],
          buttons: ['Dismiss']
        });
        // alert.present();

        }).catch((err) => {
          this.loaded = 1;
    
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Something broke and thats on us, Please try again',
            buttons: ['Dismiss']
          });
          alert.present();
        });
        // this.Carr = data;
        // console.log(data);

    }


    editTrans(trans){
      this.navCtrl.push('EdittransactionsPage', {
        val: 'I am View Edit-trans',
        trans: trans
      })
    }






}
