import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-menu-one',
  templateUrl: 'menu-one.html',
})
export class MenuOnePage {

  categories = [
    {
      id: 1,
      name: 'coffe',
      title: 'Coffe',
      description: 'Freshly brewed coffee',
      img: 'assets/imgs/menu/coffee.png'
    },
    {
      id: 2,
      name: 'breakfast',
      title: 'Breakfast',
      description: 'Hot & full of flavour.',
      img: 'assets/imgs/menu/breakfast.png'
    },
    {
      id: 3,
      name: 'munchies',
      title: 'Munchies',
      description: 'Perfectly baked goodies.',
      img: 'assets/imgs/menu/munchies.png'
    },
    {
      id: 4,
      name: 'sandwiches',
      title: 'Sandwiches',
      description: 'Fresh, healthy and tasty.',
      img: 'assets/imgs/menu/sandwiches.png'
    },
    {
      id: 5,
      name: 'tea',
      title: 'Tea',
      description: 'Fresh, healthy and tasty.',
      img: 'assets/imgs/menu/tea.png'
    },
    {
      id: 6,
      name: 'pancake',
      title: 'Pancake',
      description: 'Fresh, healthy and tasty.',
      img: 'assets/imgs/menu/pancake.png'
    },
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  goToSubMenu(cat) {
    /* Pass category object as a parameter */
    let param = { category: cat };
    this.navCtrl.push('SubMenuOnePage', param );
  }

}
