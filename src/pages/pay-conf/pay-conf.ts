import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, NavParams } from 'ionic-angular';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';

import { Storage } from '@ionic/storage';
import { LoadingController } from 'ionic-angular';




/**
 * Generated class for the PayConfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-pay-conf',
  templateUrl: 'pay-conf.html',
})
export class PayConfPage {

  constructor(
    public navCtrl: NavController,
    public grsp: GlobalRestServiceProvider,
    public loading: LoadingController,  
    private storage: Storage,
    private alertCtrl: AlertController,
    public navParams: NavParams) {
  }

  res:any;
  amount:any;

  ionViewDidLoad() {
    console.log('ionViewDidLoad PayConfPage');
    this.res = this.navParams.get('res');
    this.amount = this.navParams.get('amt');
    this.manageRes();

  }

  apiData:any;


  manageRes(){


       
       if (this.res=="success"){
        // this.loadWallet(this.id);
        this.storage.get('investData').then((data) => {
          console.log(data);
        
          this.apiData = data;
        
          this.buyInvestApi();
        
          console.log("done");
        
        
        });

       }
       else{
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Payment Failed, Please Confirm You were not Debited',
          buttons: ['Dismiss']
        });
        alert.present();
        // this.loader.dismiss();


        this.navCtrl.setRoot("InvestmentPage" );

       }


     
  }


  loaded:any=1;

  apiRes:any;



  buyInvestApi(){

    // this.setApiJson();
    // this.loaded = 0;
    let loader = this.loading.create({

      content: 'Processing...'

    });

    loader.present();


    console.log(this.apiData);
  console.log("done");


    this.grsp.buyInvestment(this.apiData).then(data => {

      console.log(data);

      this.apiRes = data;
<<<<<<< HEAD
      this.apiRes = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298


      if(this.apiRes.response == true){
        loader.dismiss();

        let alert = this.alertCtrl.create({
          title: 'Success',
          subTitle: 'Successfully Saved NGN'+this.amount+' of '+this.apiRes.prov+' Investment ',
          buttons: ['Ok']
        });
        alert.present();
        this.storage.set('investData', null);

        this.navCtrl.setRoot("InvestmentPage" );


      }
      else{

        let alert = this.alertCtrl.create({
          title: 'Failed',
          subTitle: 'Something went wrong, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();

      }

    }).catch((err) => {
      this.loaded = 1;
  
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Something broke and thats on us, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();
      });


      this.storage.set('investData', null);


  }



}
