import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PayConfPage } from './pay-conf';

@NgModule({
  declarations: [
    PayConfPage,
  ],
  imports: [
    IonicPageModule.forChild(PayConfPage),
  ],
})
export class PayConfPageModule {}
