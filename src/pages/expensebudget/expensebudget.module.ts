import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExpensebudgetPage } from './expensebudget';

@NgModule({
  declarations: [
    ExpensebudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(ExpensebudgetPage),
  ],
})
export class ExpensebudgetPageModule {}
