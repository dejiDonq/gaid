import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CardsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cards',
  templateUrl: 'cards.html',
})
export class CardsPage {

  categories = [
    {
      id: 1,
      name: 'Naira',
      title: 'Naira',
      description: 'Naira Cards',
      img: 'assets/imgs/menu/coffee.png',
      page: 'NairaCardPage'
    },
    {
      id: 2,
      name: 'Forex',
      title: 'Forex',
      description: 'Forex Cards',
      img: 'assets/imgs/menu/breakfast.png',
      page: 'ForexCardPage'
    },
    {
      id: 3,
      name: 'Prepaid',
      title: 'Prepaid',
      description: 'Prepaid Cards',
      img: 'assets/imgs/menu/munchies.png',
      page: 'PrepaidCardPage'
    },
    {
      id: 4,
      name: 'Credit',
      title: 'Credit',
      description: 'Credit Cards',
      img: 'assets/imgs/menu/coffee.png',
      page: 'CreditCardPage'
    },
    // {
    //   id: 2,
    //   name: 'Add Transaction(s)',
    //   title: 'Add Transaction(s)',
    //   description: 'Anything We Might Have Missed?',
    //   img: 'assets/imgs/menu/breakfast.png'
    // },
    // {
    //   id: 3,
    //   name: 'Edit Transaction(s)',
    //   title: 'Edit Transaction(s)',
    //   description: 'Because You Have Full Control',
    //   img: 'assets/imgs/menu/munchies.png'
    // },
   
  ];

  constructor(public navCtrl: NavController, 
    private storage: Storage, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.load();

    console.log('ionViewDidLoad CardsPage');
  }

  cardsAction(cat) {
    /* Pass category object as a parameter */
    // if(cat.id == 1){
      // let param = { category: 1 };
      this.navCtrl.push(cat.page );
    // }
    // else if(cat.id == 2){
    //   let param = { category: 2 };
    //   this.navCtrl.push('AddtransactionsPage', param );
    // }
    // else{ 
    //   let param = { category: 3 };
    //   this.navCtrl.push('ViewtransactionsPage', param );
    // }
  
  }


  ionViewWillEnter(){
    this.load();
  
  }


  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }





}
