import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectsbudgetPage } from './projectsbudget';

@NgModule({
  declarations: [
    ProjectsbudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectsbudgetPage),
  ],
})
export class ProjectsbudgetPageModule {}
