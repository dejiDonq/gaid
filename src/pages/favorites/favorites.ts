import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {

  items = [
    {
      id: 6,
      name: 'limeRefreshers',
      title: 'Lime Refreshers',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/limeRefreshers.png',
      category: 'coffe',
      price: '12',
      likes: '1900',
      isliked: true,
    },
    {
      id: 4,
      name: 'vanillaFrappucino',
      title: 'Vanilla Frappucino',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/vanillaFrappucino.png',
      category: 'coffe',
      price: '9.85',
      likes: '2300',
      isliked: true,
    },
    {
      id: 4,
      name: 'espresso',
      title: 'Espresso',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/espresso.png',
      category: 'coffe',
      price: '4.50',
      likes: '1800',
      isliked: true,
    },
    {
      id: 5,
      name: 'icedAmericano',
      title: 'Iced Americano',
      description: 'Locally Roasted',
      img: 'assets/imgs/submenu/icedAmericano.png',
      category: 'coffe',
      price: '10.50',
      likes: '2300',
      isliked: true,
    },
    {
      id: 3,
      name: 'kickFrappe',
      title: 'Kick Frappe',
      description: 'Coffee Kick',
      img: 'assets/imgs/submenu/kickFrappe.png',
      category: 'coffe',
      price: '12.35',
      likes: '4450',
      isliked: true,
    },
    {
      id: 2,
      name: 'caramelFrappe',
      title: 'Caramel Frappe',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/caramelFrappe.png',
      category: 'coffe',
      price: '7.85',
      likes: '3456',
      isliked: true,
    },
    {
      id: 4,
      name: 'cappuccino',
      title: 'Cappuccino',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/cappuccino.png',
      category: 'coffe',
      price: '10.65',
      likes: '2300',
      isliked: true,
    },
    {
      id: 1,
      name: 'chocoFrappe',
      title: 'Choco Frappe',
      description: 'Chocolate Whirl',
      img: 'assets/imgs/submenu/chocoFrappe.png',
      category: 'coffe',
      price: '9',
      likes: '3200',
      isliked: true,
    },
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavoritesPage');
  }

}
