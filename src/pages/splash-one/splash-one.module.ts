import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SplashOnePage } from './splash-one';

@NgModule({
  declarations: [
    SplashOnePage,
  ],
  imports: [
    IonicPageModule.forChild(SplashOnePage),
  ],
})
export class SplashOnePageModule {}
