import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-splash-one',
  templateUrl: 'splash-one.html',
})
export class SplashOnePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

}
