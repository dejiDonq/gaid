import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';


@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  setting:any = {};

  constructor(public navCtrl: NavController,
    private alertCtrl: AlertController,
    public grsp: GlobalRestServiceProvider,
    private storage: Storage,
     public navParams: NavParams) {}


     ionViewDidLoad() {

      console.log('ionViewDidLoad SettingPage');
      // this.valName = this.navParams.get('val');
      this.getUserName();
  
    }

  userName: any;
  uId: any;


  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;

      this.setting = {
        name: initialData.name.toUpperCase(),
        dob: initialData.dob,
        dow: initialData.dow,
        pen: initialData.pen,
        savInv: initialData.savInv,
        exp: initialData.exp,
        inc: initialData.inc,
        ret: initialData.ret,
        email: initialData.email,
        pword: initialData.pword,
        cpword: ""
      };

    });
  }

  apiData:any;
  apiRes:any;
  loaded:any = 1;

  setApiJson(){
    // this.storage.get('initialData').then((initialData) => {

      this.apiData = {
        "uId":this.uId,
        "name":this.setting.name,
        "dob":this.setting.dob,
        "dow":this.setting.dow,
        "pen":this.setting.pen,
        "savInv":this.setting.savInv,
        "exp":this.setting.exp,
        "inc":this.setting.inc,
        "ret":this.setting.ret,
        "email":this.setting.email,
        "pword":this.setting.pword,
        // "cpword":this.initialData.cpword,
        // date:'',
      };
      console.log(this.apiData);

  // });this.initialData.namethis.initialData.name

  }





  editUser(){

    if(this.setting.pword !== this.setting.cpword){

      let alert = this.alertCtrl.create({
        title: 'Error',
        subTitle: 'Passwords Do Not Match',
        buttons: ['Ok']
      });
        alert.present();

      return;

    }
  


   
      let alert = this.alertCtrl.create({
        title: 'Confirm Update',
        message: 'Do you Want to Make These Changes?',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          },
          {
            text: 'Yes',
            handler: () => {
              console.log('Yes clicked');

              this.setApiJson();
              this.loaded = 0;
          
              console.log(this.apiData);



              // =============================================
              this.grsp.editUser(this.apiData).then(data => {
 
                console.log(data);
          
                this.apiRes = data;
<<<<<<< HEAD
      this.apiRes = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
                this.loaded = 1;
          
                if(this.apiRes.response == true){
          
                  let alert = this.alertCtrl.create({
                    title: 'Success',
                    subTitle: 'Successfully Edited User '+this.apiRes.name,
                    buttons: ['Dismiss']
                  });
                  alert.present();


                  // alert = this.alertCtrl.create({
                  //   title: 'Info',
                  //   subTitle: 'You will be logged out now',
                  //   buttons: ['Dismiss']
                  // });
                  // alert.present();

                  // ============================
                  this.storage.set('initialData', null);

                // let page = this.pages[this.pages.length-2]

                let page =  { title: 'Sign In', component: 'SignupPage' };

                    this.navCtrl.setRoot(page.component);

                    // this.activePage = page;
                  // ============================
          
                }
                else{
          
                  let alert = this.alertCtrl.create({
                    title: 'Failed',
                    subTitle: 'Something Broke ',
                    buttons: ['Dismiss']
                  });
                  alert.present();
          
                }
          
              }).catch((err) => {
               this.loaded = 1;
           
                 let alert = this.alertCtrl.create({
                   title: 'Error',
                   subTitle: 'Something broke and thats on us, Please try again',
                   buttons: ['Dismiss']
                 });
                 alert.present();
               });
              // =============================================


            }
          }
        ]
      });
      alert.present();
    

  }

  reset(){
    this.getUserName();
  }


}
