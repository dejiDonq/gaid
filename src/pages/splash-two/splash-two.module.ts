import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SplashTwoPage } from './splash-two';

@NgModule({
  declarations: [
    SplashTwoPage,
  ],
  imports: [
    IonicPageModule.forChild(SplashTwoPage),
  ],
})
export class SplashTwoPageModule {}
