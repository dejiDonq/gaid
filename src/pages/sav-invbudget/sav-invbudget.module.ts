import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavInvbudgetPage } from './sav-invbudget';

@NgModule({
  declarations: [
    SavInvbudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(SavInvbudgetPage),
  ],
})
export class SavInvbudgetPageModule {}
