import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';


import { Storage } from '@ionic/storage';

import { Chart } from 'chart.js';

import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';


import * as moment from 'moment';


/**
 * Generated class for the SavInvbudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-sav-invbudget',
  templateUrl: 'sav-invbudget.html',
})
export class SavInvbudgetPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    private alertCtrl: AlertController,
    public grsp: GlobalRestServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SavInvbudgetPage');
    this.getUserName();

  }

  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any;
  @ViewChild('barCanvas') barCanvas;
  barChart: any;

  loaded:any = 0;


  barLabels = [];
  barLabelNums = [];
  barLabelYears = [];

  month:any = moment();
  userName:any;
  uId:any;

  apiData:any;
  inc:any;


  totalIncome:any = [];


  getUserName(){
    // this.userName = "Tayo";

    this.storage.get('initialData').then((initialData) => {
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
      this.inc = initialData.inc;
    this.barSetup();

     
    });


  }


  setApiJson(){

    // this.storage.get('initialData').then((initialData) => {

      this.apiData = {
        "uId":this.uId,
        "months": this.barLabelNums.join(),
        "years": this.barLabelYears.join(),
        // "param": this.param
      };

    // });

  }

retval:any;
barBudgetData:any = [];


  getTransSumByMonth(){
    this.loaded = 0;

    this.setApiJson();


    this.grsp.getTransSumByMonth(this.apiData).then(data => {

      console.log(data);
      this.retval = data;
<<<<<<< HEAD
      this.retval = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
      console.log(JSON.parse(this.retval.budget));

      let income = JSON.parse(this.retval.sumIncome);

      let budget = JSON.parse(this.retval.budget);

      budget.forEach((element, ind) => {

        let expPercent = element[0] ? element[0].b_savInv : 25;

        // let exp = (expPercent/100) * (element[0] ? element[0].b_income : income[ind]);
        let exp = (expPercent/100) * (element[0] ? element[0].b_income : this.inc);


      this.barBudgetData.push(exp);

        
    });

    console.log(this.barBudgetData);

this.barActualData = JSON.parse(this.retval.sumSavInv);
      this.loaded = 1;

      this.lineLabels = this.barLabels;
      this.lineBudgetData = this.barBudgetData;
      this.lineActualData = this.barActualData;
      this.bar();
      this.liNe();

    })
    .catch((err) => {
      this.loaded = 1;
      
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Something broke and thats on us, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();
      });

  }




    // =====================================
    lineLabels;
    lineBudgetData;
    lineActualData;
    liNe(){
      // if (this.toggleVarLine){
  
      this.lineChart = new Chart(this.lineCanvas.nativeElement, {
  
        type: 'line',
        data: {
            // labels: ["January", "February", "March", "April", "May", "June", "July"],
            labels: this.lineLabels,
            datasets: [
                {
                    label: "Budget(%)",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,92,192,0.4)",
                    borderColor: "rgba(75,92,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,92,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,92,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    // data: [65, 55, 90, 21, 78, 95, 20],
                    data: this.lineBudgetData,
                    spanGaps: false,
                },
                {
                    label: "Actual(%)",
                    fill: false,
                    lineTension: 0.1,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#fff",
                    pointBorderWidth: 1,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 1,
                    pointHitRadius: 10,
                    // data: [65, 59, 80, 81, 56, 55, 40],
                    data: this.lineActualData,
                    spanGaps: false,
                }
            ]
        },
        options:{
             scales: {
            xAxes: [{
                gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                }
            }],
            yAxes: [{
                gridLines: {
                    color: "rgba(0, 0, 0, 0)",
                }   
            }]
          },
        }
  
    });
    // }
    }
    // =====================================


barActualData:any;

bar(){

  this.barChart = new Chart(this.barCanvas.nativeElement, {
 
    type: 'bar',
    data: {
        // labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
        labels: this.barLabels,
        datasets: [{
            label: 'Budget(%)',
            data: this.barBudgetData,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        },
        {
          label: 'Actual(%)',
          // data: [12, 19, 13, 5, 2, 13],
          data: this.barActualData,
          backgroundColor: [
              'rgba(255, 99, 132, 0.2)',
              'rgba(5, 162, 235, 0.2)',
              'rgba(55, 206, 86, 0.2)',
              'rgba(50, 192, 192, 0.2)',
              'rgba(13, 102, 255, 0.2)',
              'rgba(250, 159, 64, 0.2)'
          ],
          borderColor: [
              'rgba(255,99,132,1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
      }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }

});

}

  barSetup(){
let i = 6;
let tday = this.month;

    while (i>0){

      // console.log(tday.format('MMMM'));

      this.barLabels.push(tday.format('MMM'));
      this.barLabelNums.push(tday.format('M'));
      this.barLabelYears.push(tday.format('Y'));


      tday = tday.subtract(1, 'month');
      i--;
      
    }

    console.log(this.barLabels);
    console.log(this.barLabelNums);
    console.log(this.barLabelYears);

    // this.barLabels = this.barLabels.reverse();

    // this.barLabels.forEach((element, ind) => {
        
    // });


    this.getTransSumByMonth();


    
  }


}
