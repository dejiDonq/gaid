import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-sub-menu-two',
  templateUrl: 'sub-menu-two.html',
})
export class SubMenuTwoPage {

  items = [
    {
      id: 1,
      name: 'chocoFrappe',
      title: 'Choco Frappe',
      description: 'Chocolate Whirl',
      img: 'assets/imgs/submenu/chocoFrappe.png',
      category: 'coffe',
      price: '9',
      likes: '3200',
      isliked: false,
    },
    {
      id: 2,
      name: 'caramelFrappe',
      title: 'Caramel Frappe',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/caramelFrappe.png',
      category: 'coffe',
      price: '7.85',
      likes: '3456',
      isliked: false,
    },
    {
      id: 3,
      name: 'kickFrappe',
      title: 'Kick Frappe',
      description: 'Coffee Kick',
      img: 'assets/imgs/submenu/kickFrappe.png',
      category: 'coffe',
      price: '12.35',
      likes: '4450',
      isliked: false,
    },
    {
      id: 4,
      name: 'cappuccino',
      title: 'Cappuccino',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/cappuccino.png',
      category: 'coffe',
      price: '10.65',
      likes: '2300',
      isliked: false,
    },
    {
      id: 5,
      name: 'icedAmericano',
      title: 'Iced Americano',
      description: 'Locally Roasted',
      img: 'assets/imgs/submenu/icedAmericano.png',
      category: 'coffe',
      price: '10.50',
      likes: '2300',
      isliked: false,
    },
    {
      id: 6,
      name: 'limeRefreshers',
      title: 'Lime Refreshers',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/limeRefreshers.png',
      category: 'coffe',
      price: '12',
      likes: '1900',
      isliked: false,
    },
    {
      id: 4,
      name: 'vanillaFrappucino',
      title: 'Vanilla Frappucino',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/vanillaFrappucino.png',
      category: 'coffe',
      price: '9.85',
      likes: '2300',
      isliked: false,
    },
    {
      id: 4,
      name: 'espresso',
      title: 'Espresso',
      description: 'Decaf Colombia',
      img: 'assets/imgs/submenu/espresso.png',
      category: 'coffe',
      price: '4.50',
      likes: '1800',
      isliked: false,
    },
  ];
  constructor(public navCtrl: NavController, public navParams: NavParams, public toastCtrl: ToastController) { }

  ionViewDidLoad() {
    let selectedCategory = this.navParams.get('category') ? this.navParams.get('category') : 'Coffee';
  }

  selectItem(item) {
    let toast = this.toastCtrl.create({
      message: 'item ' + item.title + ' Clicked',
      duration: 2500,
    });
    toast.present();
  }
}
