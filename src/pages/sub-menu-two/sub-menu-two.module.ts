import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubMenuTwoPage } from './sub-menu-two';

@NgModule({
  declarations: [
    SubMenuTwoPage,
  ],
  imports: [
    IonicPageModule.forChild(SubMenuTwoPage),
  ],
})
export class SubMenuTwoPageModule {}
