import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the FinInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fin-info',
  templateUrl: 'fin-info.html',
})
export class FinInfoPage {
  categories = [
    {
      id: 1,
      name: 'Banking',
      title: 'Banking',
      description: 'Everything You Need to Have Handy on Banking',
      img: 'assets/imgs/menu/coffee.png',
      page: 'BankingPage'
    },
    {
      id: 2,
      name: 'Cards',
      title: 'Cards',
      description: 'Get Clarity on How Your Cards are Managed',
      img: 'assets/imgs/menu/breakfast.png',
      page: 'CardsPage'
    },
    // {
    //   id: 3,
    //   name: 'Edit Transaction(s)',
    //   title: 'Edit Transaction(s)',
    //   description: 'Because You Have Full Control',
    //   img: 'assets/imgs/menu/munchies.png'
    // },
   
  ];

  constructor(public navCtrl: NavController, 
    private storage: Storage, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.load();

    console.log('ionViewDidLoad FinInfoPage');
  }


  fiAction(cat) {
    /* Pass category object as a parameter */
    // if(cat.id == 1){
      // let param = { category: 1 };
      this.navCtrl.push(cat.page );
    // }
    // else if(cat.id == 2){
    //   let param = { category: 2 };
    //   this.navCtrl.push('AddtransactionsPage', param );
    // }
    // else{ 
    //   let param = { category: 3 };
    //   this.navCtrl.push('ViewtransactionsPage', param );
    // }
  
  }


  ionViewWillEnter(){
    this.load();
  
  }


  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }





}
