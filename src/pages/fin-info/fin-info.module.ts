import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FinInfoPage } from './fin-info';

@NgModule({
  declarations: [
    FinInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(FinInfoPage),
  ],
})
export class FinInfoPageModule {}
