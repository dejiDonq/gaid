import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FixedDepositPage } from './fixed-deposit';

@NgModule({
  declarations: [
    FixedDepositPage,
  ],
  imports: [
    IonicPageModule.forChild(FixedDepositPage),
  ],
})
export class FixedDepositPageModule {}
