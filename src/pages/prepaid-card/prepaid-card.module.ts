import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrepaidCardPage } from './prepaid-card';

@NgModule({
  declarations: [
    PrepaidCardPage,
  ],
  imports: [
    IonicPageModule.forChild(PrepaidCardPage),
  ],
})
export class PrepaidCardPageModule {}
