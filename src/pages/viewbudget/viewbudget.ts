import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';



import { SignupPage } from '../signup/signup';

/**
 * Generated class for the ViewbudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-viewbudget',
  templateUrl: 'viewbudget.html',
})
export class ViewbudgetPage {

  categories = [
    {
      id: 1,
      name: 'Living Expenses',
      title: 'Living Expenses',
      description: 'Check Out What You Spent',
      img: 'assets/imgs/menu/coffee.png'
    },
    
    {
      id: 2,
      name: 'Savings and Investments',
      title: 'Savings and Investments',
      description: 'Take A Look At Your Assets',
      img: 'assets/imgs/menu/munchies.png'
    },
    {
      id: 3,
      name: 'Projects',
      title: 'Projects',
      description: 'How Close Are You To Your Goals ',
      img: 'assets/imgs/menu/munchies.png'
    },
    {
      id: 4,
      name: 'Debt Repayment',
      title: 'Debt Repayment',
      description: 'See What you Owe',
      img: 'assets/imgs/menu/breakfast.png'
    },
       
  ];

  constructor(public navCtrl: NavController,
    private storage: Storage,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.load();

    console.log('ionViewDidLoad ViewbudgetPage');
  }


  analysisAction(cat) {
    /* Pass category object as a parameter */
    if(cat.id == 1){
      let param = { category: 1 };
      this.navCtrl.push('ExpensebudgetPage', param );
    } 
    else if(cat.id == 2){ 
      let param = { category: 2 };
      this.navCtrl.push('SavInvbudgetPage', param );
    }
    else if(cat.id == 3){
      let param = { category: 3 };
      this.navCtrl.push('ProjectbudgetPage', param );
    }
    else if(cat.id == 4){ 
      let param = { category: 4 };
      this.navCtrl.push('DebtbudgetPage', param );
    }
    // else if(cat.id == 5){ 
    //   let param = { category: 5 };
    //   this.navCtrl.push('ProjectsbudgetPage', param );
    // }
  
  }

  ionViewWillEnter(){
    this.load();
  
  }


  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }

}
