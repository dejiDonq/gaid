import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewbudgetPage } from './viewbudget';

@NgModule({
  declarations: [
    ViewbudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewbudgetPage),
  ],
})
export class ViewbudgetPageModule {}
