import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';

import { Storage } from '@ionic/storage';

/**
 * Generated class for the ForeignPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-foreign',
  templateUrl: 'foreign.html',
})
export class ForeignPage {

  uId: any;

  constructor(public navCtrl: NavController, 
    private alertCtrl: AlertController,
    private storage: Storage,
    public grsp: GlobalRestServiceProvider,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForeignPage');
    this.getUserName();
  }

  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      // this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
      this.getSavingsApi();

    });
  }


  setApiJson(){

    // this.storage.get('initialData').then((initialData) => {

    this.apiData = {
      "grp":"Foreign Currency"
    };

    // });

  }

  apiData:any;
  sav:any;
  fc:any;
  loaded:any=1;



  getSavingsApi(){

    this.loaded = 0;


    this.setApiJson();

      let alert = this.alertCtrl.create({
        title: 'No name',
        subTitle: 'no Name',
        buttons: ['Dismiss']
      });

    console.log(this.apiData);

      this.grsp.banking(this.apiData).then(data => {
        console.log(data);

        this.sav = data;
<<<<<<< HEAD
      this.sav = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
        // this.transactions = this.trans.transactions.split(",");
        this.fc = JSON.parse(this.sav.banking);

        this.loaded = 1;

        // alert = this.alertCtrl.create({
        //   title: 'No name',
        //   subTitle: 'no Name'+ this.investments[0],
        //   buttons: ['Dismiss']
        // });
        // alert.present();

        }).catch((err) => {
          this.loaded = 1;
    
          let alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: 'Something broke and thats on us, Please try again',
            buttons: ['Dismiss']
          });
          alert.present();
        });
        // this.Carr = data;
        // console.log(data);

    }



}
