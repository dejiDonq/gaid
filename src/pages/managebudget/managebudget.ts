import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';


import { Storage } from '@ionic/storage';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';


/**
 * Generated class for the ManagebudgetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-managebudget',
  templateUrl: 'managebudget.html',
})
export class ManagebudgetPage {

  constructor(public navCtrl: NavController,
    private storage: Storage,
     private alertCtrl: AlertController,
     public navParams: NavParams,
     public grsp: GlobalRestServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ManagebudgetPage');
    this.getUserName();

  }


  type:any;

expenses:any= 50;
  savInv:any= 25;
  proj:any = 25;
  debt:any = 0;
  income:any = 0;
  id:any = 0;
userName:any;




apiData:any;

setApiJson(){

this.apiData = {
    "uId":this.id,
    "expenses":this.expenses,
    "savInv":this.savInv,
    "proj":this.proj,
    "debt":this.debt,
    "income":this.income
  };


}







apiRes:any;
loaded:any= 1;




// saveAddApi(){

// this.loaded = 0;

//   this.setApiJson();

//   console.log(this.apiData);

//   this.grsp.addTrans(this.apiData).then(data => {

//     console.log(data);

//     this.apiRes = data;
// this.loaded = 1;
//     if(this.apiRes.response == true){

//       let alert = this.alertCtrl.create({
//         title: 'Success',
//         subTitle: 'Successfully Saved Transaction '+this.apiRes.desc,
//         buttons: ['Ok']
//       });
//       alert.present();


//       this.type='';
//       this.desc='';
//       this.bucket='';
//       this.amt='';
//       this.date='';

//     }
//     else{

//       let alert = this.alertCtrl.create({
//         title: 'Failed',
//         subTitle: 'Something went wrong, Please try again',
//         buttons: ['Dismiss']
//       });
//       alert.present();

//     }

//   })
//   .catch((err) => {
// this.loaded = 1;

//     let alert = this.alertCtrl.create({
//       title: 'Error',
//       subTitle: 'Something broke and thats on us, Please try again',
//       buttons: ['Dismiss']
//     });
//     alert.present();
//   });


// }




  saveBudget(){
    
    if (this.sumAll() == true){
      
      console.log("save budget for the month");

this.loaded = 0;
this.setApiJson();
console.log(this.apiData);


// ============================================================
  this.grsp.addBudget(this.apiData).then(data => {

    console.log(data);

    this.apiRes = data;
<<<<<<< HEAD
    this.apiRes = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
    this.loaded = 1;
    if(this.apiRes.response == true){

      let alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: 'Successfully Saved Budget',
        buttons: ['Ok']
      });
      alert.present();

    }
    else{

      let alert = this.alertCtrl.create({
        title: 'Failed',
        subTitle: 'Something went wrong, Please try again',
        buttons: ['Dismiss']
      });
      alert.present();

    }

  })
  .catch((err) => {
  this.loaded = 1;

    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'Something broke and thats on us, Please try again',
      buttons: ['Dismiss']
    });
    alert.present();
  });
// ============================================================


    }
    else{
      let alert = this.alertCtrl.create({
        title: 'Note',
        subTitle: 'The Total of Budget Allocation Must Be Equal to 100%',
        buttons: ['Dismiss']
      });
      alert.present();
    }

  }


  sumAll(){
    
    if(this.type == 'debtor'){
      // this.debt =15;
    }
    else{
      this.debt = 0;

    }

    // console.log(this.type);
    // console.log(this.expenses);
    // console.log(this.savInv);
    // console.log(this.proj);
    // console.log(this.debt);

    if ((this.expenses + this.savInv + this.proj + this.debt) == 100){

      return true;
    }
    else{

      return false;
    }
  }



  help(){
    let alert = this.alertCtrl.create({
      title: 'Note',
      subTitle: 'The Budget Must be Set Before The Month Starts',
      buttons: ['Dismiss']
    });
    alert.present();
  }


  getUserName(){
    
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      this.userName = initialData.name.toUpperCase();
      this.id = initialData.id;

    });
  }
  


}
