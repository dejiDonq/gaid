import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ManagebudgetPage } from './managebudget';

@NgModule({
  declarations: [
    ManagebudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(ManagebudgetPage),
  ],
})
export class ManagebudgetPageModule {}
