import { Component, ViewChild  } from '@angular/core';

import { NavController, AlertController, ViewController,  Platform,  NavParams  } from 'ionic-angular';

import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';

import { LoadingController } from 'ionic-angular';

// import { InAppBrowser } from 'ionic-native';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

declare var PaystackPop;

@Component({

  selector: 'page-paystack',

  templateUrl: 'paystack.html'

})

export class PaystackPage {

  @ViewChild("email_add") email_add;

  @ViewChild("card_number") card_number;

  @ViewChild("expiryMonth") expiryMonth;

  @ViewChild("expiryYear") expiryYear;

  @ViewChild("cvc") cvc;

// customerEmail:any;

price:any;

chargeAmount:any;

emailValue: any;

cardNumberValue: any;

expiryMonthValue: any;

expiryYearValue: any;

cvcValue: any;

  constructor(public navCtrl: NavController, 
    public alertCtrl: AlertController, 
    public navParams: NavParams,  
    public grsp: GlobalRestServiceProvider,
    public loading: LoadingController,  
     public platform: Platform, 
     private iab: InAppBrowser,
     public viewCtrl: ViewController) {

  }

  apiRes:any;
  em:any;
  data:any;

ngOnInit(){

this.price= parseInt(this.navParams.get('price'));
this.em= parseInt(this.navParams.get('email'));
// this.data= parseInt(this.navParams.get('data'));

console.log(this.price);
console.log(this.em);
console.log(this.navParams.get('data'));

// this.price= 500;


this.chargeAmount = this.price*100;


// this.ChargeCard();
// let plat = this.platform.platforms();

//     alert(plat);

}


validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

  ChargeCard(){

    if(!this.validateEmail(this.emailValue)){
      alert("Please enter valid email to pay")
      return;
    }

    // alert(this.emailValue);

    // let card = this.card_number.value;

    // let month = this.expiryMonth.value;

    // let cvc = this.cvc.value;

    // let year = this.expiryYear.value;

    let amount = this.chargeAmount;

    let email = this.emailValue;
    // let email = this.em;

    // let apiData = this.data;

  // console.log(card);

  //   console.log(month);

  //     console.log(cvc);

  //       console.log(year);

          console.log(amount);

            console.log(email);

    let loader = this.loading.create({

      content: 'Processing Charge…'

    });

    loader.present();

    

        this.platform.ready().then(() => {

    //       let plat = this.platform.platforms();

    // alert(plat);


          if (this.platform.is('ios') || this.platform.is('android')) {
// alert('ios');

// =============================popup==============

var handler = PaystackPop.setup({
  // key: 'pk_test_xxxxxxxxxxxxx', // Replace with your public key
  key: this.grsp.paystack, // Replace with your public key
  email: email,
  amount: amount,

  
  ref: ''+Math.floor((Math.random() * 1000000000) + 1), // generates a pseudo-unique reference. Please replace with a reference you generated. Or remove the line entirely so our API will generate one for you
  // label: "Optional string that replaces customer email"
  onClose: function(){
    // alert('Window closed.');
    loader.dismiss();

  },
  callback: (response)=>{
    var message = 'Payment complete! Reference: ' + response.reference;
    alert(message);
    console.log(message);
    loader.dismiss();
    this.navCtrl.push("PayConfPage", {res:"success",amt:(amount/100)} );
    
  }
});
console.log(handler);
handler.openIframe();

// =============================popup==============


          }
          else if (false) {
          // else if (this.platform.is('cordova')) {

          // Now safe to use device APIs

    //       (<any>window).window.PaystackPlugin.chargeCard(

    //         (resp) =>{

    //           loader.dismiss();

    //           //this.pop.showPayMentAlert("Payment Was Successful", "We will Now Refund Your Balance");

    //           console.log('charge successful: ', resp);
    //           // console.log(apiData);

    //           alert('Payment Was Successful' );
    // // this.navCtrl.push("InvestmentPage", {res:"success"} );
    // this.navCtrl.push("PayConfPage", {res:"success",amt:(amount/100)} );


    
              

    //         },

    //         (resp) =>{

    //           loader.dismiss();

    //           console.log(resp);

    //        alert('We Encountered An Error While Charging Your Card' );
    // // this.navCtrl.push("InvestmentPage", {res:"failed"} );


    //         },

    //         {

    //           // cardNumber: card,

    //           // expiryMonth: month,

    //           // expiryYear: year,

    //           // cvc: cvc,

    //           email: email,

    //           amountInKobo: amount,

    //       })

        }else{

        }

      })

  }

whatsPaystack(){

this.platform.ready().then(() => {

          //  let browser = new InAppBrowser("https://paystack.com/what-is-paystack",'_blank');
          let browser = this.iab.create("https://paystack.com/what-is-paystack",'_blank');

         });  

}

close(){

  this.viewCtrl.dismiss();

}

}

// =======================================================

// import { Component } from '@angular/core';
// import { IonicPage, NavController, NavParams } from 'ionic-angular';

// /**
//  * Generated class for the PaystackPage page.
//  *
//  * See https://ionicframework.com/docs/components/#navigation for more info on
//  * Ionic pages and navigation.
//  */

// @IonicPage()
// @Component({
//   selector: 'page-paystack',
//   templateUrl: 'paystack.html',
// })
// export class PaystackPage {

//   constructor(public navCtrl: NavController, public navParams: NavParams) {
//   }

//   ionViewDidLoad() {
//     console.log('ionViewDidLoad PaystackPage');
//   }

// }
