import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { SignupPage } from '../signup/signup';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the BankingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-banking',
  templateUrl: 'banking.html',
})
export class BankingPage {
  categories = [
    {
      id: 1,
      name: 'Savings',
      title: 'Savings',
      description: 'Banking Savings',
      img: 'assets/imgs/menu/coffee.png',
      page: 'SavingsPage'
    },
    {
      id: 2,
      name: 'Current',
      title: 'Current',
      description: 'Banking Current',
      img: 'assets/imgs/menu/breakfast.png',
      page: 'CurrentPage'
    },
    {
      id: 3,
      name: 'Fixed Deposit',
      title: 'Fixed Deposit',
      description: 'Banking Fixed Deposit',
      img: 'assets/imgs/menu/munchies.png',
      page: 'FixedDepositPage'
    },
    {
      id: 4,
      name: 'Foreign',
      title: 'Foreign',
      description: 'Banking Foreign',
      img: 'assets/imgs/menu/coffee.png',
      page: 'ForeignPage'
    },
    // {
    //   id: 2,
    //   name: 'Add Transaction(s)',
    //   title: 'Add Transaction(s)',
    //   description: 'Anything We Might Have Missed?',
    //   img: 'assets/imgs/menu/breakfast.png'
    // },
    // {
    //   id: 3,
    //   name: 'Edit Transaction(s)',
    //   title: 'Edit Transaction(s)',
    //   description: 'Because You Have Full Control',
    //   img: 'assets/imgs/menu/munchies.png'
    // },
   
  ];

  constructor(public navCtrl: NavController, 
    private storage: Storage, 
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    this.load();

    console.log('ionViewDidLoad BankingPage');
    // alert('work here');
  }


  bankAction(cat) {
    /* Pass category object as a parameter */
    // if(cat.id == 1){
      // let param = { category: 1 };
      this.navCtrl.push(cat.page );
    // }
    // else if(cat.id == 2){
    //   let param = { category: 2 };
    //   this.navCtrl.push('AddtransactionsPage', param );
    // }
    // else{ 
    //   let param = { category: 3 };
    //   this.navCtrl.push('ViewtransactionsPage', param );
    // }
  
  }


  ionViewWillEnter(){
    this.load();
  
  }


  load(){
    // this.storage.set('initialData', '');
    this.storage.get('initialData').then((initialData) => {
      console.log(initialData);
      // console.log(initialData.id);

      

      if (initialData == null){
        this.navCtrl.setRoot(SignupPage);

      }

      else if (initialData.id == 0){

      }

    });

  }






}
