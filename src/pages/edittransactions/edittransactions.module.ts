import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EdittransactionsPage } from './edittransactions';

@NgModule({
  declarations: [
    EdittransactionsPage,
  ],
  imports: [
    IonicPageModule.forChild(EdittransactionsPage),
  ],
})
export class EdittransactionsPageModule {}
