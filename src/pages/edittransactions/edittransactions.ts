import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { GlobalRestServiceProvider } from '../../providers/global-rest-service/global-rest-service';

import { Storage } from '@ionic/storage';



/**
 * Generated class for the EdittransactionsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edittransactions',
  templateUrl: 'edittransactions.html',
})
export class EdittransactionsPage {

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    private storage: Storage,
    private alertCtrl: AlertController,
    public grsp: GlobalRestServiceProvider
  
  ) {
  }

  buckets:any;

  valName:any;
  type:any;
  desc:any;
  bucket:any;
  amt:any;
  date:any;
  userName:any;
  uId:any;
  tId:any;



  ionViewDidLoad() {
    console.log('ionViewDidLoad EdittransactionsPage');
    this.desc = this.navParams.get('trans').trans_desc;
    this.type = this.navParams.get('trans').trans_type;
    this.bucket = this.navParams.get('trans').trans_bucket;
    this.amt = this.navParams.get('trans').trans_amt;
    this.date = this.navParams.get('trans').trans_date.substring(0,10);
    this.tId = this.navParams.get('trans').trans_id;
    // this.desc = this.navParams.get('trans');
    this.getUserName();

  }


  getUserName(){
    // this.userName = "Tayo";
    this.storage.get('initialData').then((initialData) => {
      this.userName = initialData.name.toUpperCase();
      this.uId = initialData.id;
      this.buckets = this.grsp.buckets();


    });
  }


  saveEdit(){
    this.desc = this.desc ? this.desc:"";
    let alert = this.alertCtrl.create({
      title: 'Success',
      subTitle: 'Successfully Saved Transaction '+this.desc,
      buttons: ['Dismiss']
    });
    alert.present();
  }



   // ===================================================
   apiData:any;

   setApiJson(){
     this.apiData = {
       "uId":this.uId,
       "type":this.type,
       "desc":this.desc,
       "bucket":this.bucket,
       "amt":this.amt,
       "date":this.date,
       "tid":this.tId
     };
   }
 
   apiRes:any;
   loaded:any=1;
 

 
   saveEditApi(){
 
     this.setApiJson();
     this.loaded = 0;
 
 
     console.log(this.apiData);
 
     this.grsp.editTrans(this.apiData).then(data => {
 
       console.log(data);
 
       this.apiRes = data;
<<<<<<< HEAD
      this.apiRes = JSON.parse(data.data);

=======
>>>>>>> 21a1580dfad3b2d00df43151d3b964fc56bf6298
       this.loaded = 1;
 
       if(this.apiRes.response == true){
 
         let alert = this.alertCtrl.create({
           title: 'Success',
           subTitle: 'Successfully Edited Transaction '+this.apiRes.desc,
           buttons: ['Dismiss']
         });
         alert.present();
 
       }
       else{
 
         let alert = this.alertCtrl.create({
           title: 'Success',
           subTitle: 'Successfully Sav ',
           buttons: ['Dismiss']
         });
         alert.present();
 
       }
 
     }).catch((err) => {
      this.loaded = 1;
  
        let alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'Something broke and thats on us, Please try again',
          buttons: ['Dismiss']
        });
        alert.present();
      });
 
 
   }
   // ===================================================

}
