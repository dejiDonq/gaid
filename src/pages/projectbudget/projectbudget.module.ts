import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectbudgetPage } from './projectbudget';

@NgModule({
  declarations: [
    ProjectbudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(ProjectbudgetPage),
  ],
})
export class ProjectbudgetPageModule {}
