import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LiabilitiesbudgetPage } from './liabilitiesbudget';

@NgModule({
  declarations: [
    LiabilitiesbudgetPage,
  ],
  imports: [
    IonicPageModule.forChild(LiabilitiesbudgetPage),
  ],
})
export class LiabilitiesbudgetPageModule {}
