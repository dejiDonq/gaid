// import { HttpClient } from '@angular/common/http';

import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import { AlertController } from 'ionic-angular';

import { HTTP } from '@ionic-native/http';

import { Observable } from 'rxjs/Observable';

/*
  Generated class for the GlobalRestServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GlobalRestServiceProvider {

  private baseUrl = "http://gaid.io/myLeo";

  constructor(public http: Http,
    private alertCtrl: AlertController,  
    public httpPlugin: HTTP
  ) {
    console.log('Hello GlobalRestServiceProvider Provider');
  }



  showAlert(err){
    let alert = this.alertCtrl.create({
      title: 'grs alert',
      subTitle: err,
      buttons: ['Dismiss']
    });
    alert.present();
  }

  

  buckets(){
    // let buckets = ['grocery','transportation','utilities','vacation','education','entertainment','food','gifts','subscriptions','rent','Banking'];
    let buckets = ['Income','Expenses','Saving&Investments(Assets)','Liabilities','Projects (Goals)'];
    return buckets;
  }

  test(param){
    return this.httpPlugin.post(`${this.baseUrl}/test`,param,this.header)
    // return new Promise(resolve=>{
    //   this.http.post(`${this.baseUrl}/test`,param).subscribe(res=>{
    //     resolve(res.json());

    //   });
    // })
  }

  addUser(param){

    // console.log(param);
    // return new Promise(resolve=>{
    //   // this.http.post(`${this.baseUrl}/addUser`,param).subscribe(res=>resolve(res.json()));
    //   this.http.post(`${this.baseUrl}/addUser`,param).subscribe(res=>{
    //     console.log(res);
    //     resolve(res.json());

    //   });
    // })

    return this.httpPlugin.post(`${this.baseUrl}/addUser`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/addUser`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }



  editUser(param){

    return this.httpPlugin.post(`${this.baseUrl}/editUser`,param,this.header)
    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/editUser`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             // this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

   
  }

  dashboardLoad(param){
    // return new Promise(resolve=>{
    //   this.http.post(`${this.baseUrl}/dashboard`,param).subscribe(res=>{
    //     console.log(res);
    //     resolve(res.json());
    //   });
    // })

    return this.httpPlugin.post(`${this.baseUrl}/dashboard`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/dashboard`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         }); 
    // })

    
  }

   header = {
    'Content-Type': 'application/json'
};

  login(param){
    // return new Promise(resolve=>{
    //   this.http.post(`${this.baseUrl}/addTrans`,param).subscribe(res=>{
        
    //     resolve(res.json());
      
    //   });
    // })
    // this.showAlert(param.email);
    // this.showAlert(param.pword);


    // return new Promise((resolve,reject)=>{
      return this.httpPlugin.post(`${this.baseUrl}/login`,param,this.header);
      // .then(data => {
      //   // resolve(data.data);
      //   this.showAlert(data.data);

      //   console.log(data.status);
      //   console.log(data.data); // data received by server
      //   console.log(data.headers);
    
      // })
      // .catch(error => {
      //   this.showAlert(error);
      //             console.log(error);
      //             // reject(error.error);
      //   console.log(error.status);
      //   console.log(error.error); // error message as string
      //   console.log(error.headers);
    
      // });
      // .map(res=>res.json())
      
      // .subscribe(data => {resolve(data)},
      //       err => { 
      //           this.showAlert(err);
      //           console.log(err);
      //           reject(err);
      //       });



    // })


  }
 
  addBudget(param){
  
    return this.httpPlugin.post(`${this.baseUrl}/addBudget`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/addBudget`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })


  }


  addTrans(param){
    // return new Promise(resolve=>{
    //   this.http.post(`${this.baseUrl}/addTrans`,param).subscribe(res=>{
        
    //     resolve(res.json());
      
    //   });
    // })

    return this.httpPlugin.post(`${this.baseUrl}/addTrans`,param,this.header);

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/addTrans`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })


  }


  // editTrans(param){
  //   return new Promise((resolve,reject)=>{
  //     this.http.post(`${this.baseUrl}/editTrans`,param).subscribe(res=>{
  //       console.log(res);
       
  //       resolve(res.json());
  //     });
  //   })
  // }


  editTrans(param){

    return this.httpPlugin.post(`${this.baseUrl}/editTrans`,param,this.header)
    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/editTrans`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             // this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

   
  }



  buyInvestment(param){
    

    return this.httpPlugin.post(`${this.baseUrl}/buyInvestment`,param, this.header)
    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/buyInvestment`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }

  addInitialData(param){
    return  this.httpPlugin.post(`${this.baseUrl}/addInitialData`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/addInitialData`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }


  analytics(param){
   return  this.httpPlugin.post(`${this.baseUrl}/analytics`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/analytics`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })


  }


  fixedDeposits(param){
   
return this.httpPlugin.post(`${this.baseUrl}/fixedDeposits`,param,this.header)
    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/fixedDeposits`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })


  }


  // mutualFunds(param){
  //   return new Promise(resolve=>{
  //     this.http.post(`${this.baseUrl}/mutualFunds`,param).subscribe(res=>{
        
  //       resolve(res.json());
      
  //     });
  //   })
  // }


  // stocks(param){
  //   return new Promise(resolve=>{
  //     this.http.post(`${this.baseUrl}/stocks`,param).subscribe(res=>{
        
  //       resolve(res.json());
      
  //     });
  //   })
  // }

  transactions(param){
    // console.log(param);
    // return new Promise(resolve=>{
    //   this.http.post(`${this.baseUrl}/transactions`,param).subscribe(res=>{
    //     console.log(res);
    //     resolve(res.json());
    //   });
    // })
return this.httpPlugin.post(`${this.baseUrl}/transactions`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/transactions`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }
 
  getTransSumByMonth(param){
    // return new Promise(resolve=>{
    //     this.http.post(`${this.baseUrl}/transSum`,param).subscribe(res=>{
    //       console.log(res);
    //       resolve(res.json());
    //     });
    //   })
  //  ==================================================
  return this.httpPlugin.post(`${this.baseUrl}/transSum`,param,this.header)
    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/transSum`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }
 
  banking(param){
return this.httpPlugin.post(`${this.baseUrl}/banking`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/banking`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }
 
  cards(param){
return this.httpPlugin.post(`${this.baseUrl}/cards`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/cards`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }
 
  investments(param){
return this.httpPlugin.post(`${this.baseUrl}/investments`,param,this.header)
    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/investments`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }
 
  lastDate(param){
  return this.httpPlugin.post(`${this.baseUrl}/lastDate`,param,this.header)

    // return new Promise((resolve,reject)=>{
    //   this.http.post(`${this.baseUrl}/lastDate`,param).map(res=>res.json())
      
    //   .subscribe(data => {resolve(data)},
    //         err => { 
    //             this.showAlert(err);
    //             console.log(err);
    //             reject(err);
    //         });
    // })

  }

paystack = 'pk_live_a52eed168e35708420a2d0aa9ed43ac22ff64f5b';

}

